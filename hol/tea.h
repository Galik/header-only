#ifndef HEADER_ONLY_LIBRARY_CRYPTO_H
#define HEADER_ONLY_LIBRARY_CRYPTO_H

#include <algorithm>
#include <array>
#include <cstddef>
#include <iterator>
#include <numeric>
#include <vector>

#include "misc_utils.h"
#include "endian_utils.h"

namespace header_only_library {
namespace crypto {

namespace internal {
	using namespace header_only_library::misc_utils;
	using namespace header_only_library::endian_utils;

	template<typename T>
	auto* data(T& t) { return std::data(t); }

	template<typename T>
	auto* data_end(T& t) { return std::data(t) + std::size(t); }

	template<typename T>
	auto const* cdata(T& t) { return data(t); }

	template<typename T>
	auto const* cdata_end(T& t) { return data_end(t); }
}

class TinyEncryptionAlgorithm
{
	using byte = std::byte;
	using byte_rptr = byte*;
	using byte_const_rptr = byte const*;

	using char_rptr = char*;
	using char_const_rptr = char*;

	using block_type = std::array<std::uint32_t, 2>;

public:
	using byte_string = std::vector<byte>;
	using key_type = std::array<std::uint32_t, 4>;

	TinyEncryptionAlgorithm(): m_key({}) {}
	TinyEncryptionAlgorithm(key_type key): m_key(key) {}

	key_type key() const { return m_key; }
	void use_key(key_type key) { m_key = key; }

	byte_string encrypt(byte_string dec)
	{
		byte_string enc;

		if(dec.empty())
			return enc;

		auto red = internal::narrow_cast<unsigned>(8 - (dec.size() % 8));

		dec.resize(dec.size() + red);
		dec.back() = internal::narrow_cast<byte>(red);

		block_type block;

		auto const* beg_data = dec.data();
		auto const* const end_data = dec.data() + dec.size();

		while(beg_data != end_data)
		{
			beg_data = copy_bytes(beg_data, block);
			block = encrypt(block, m_key);
			enc.insert(std::end(enc), byte_const_rptr(block.data()), byte_const_rptr(block.data() + block.size()));
		}

		return enc;
	}

	byte_string decrypt(byte_const_rptr beg_data, byte_const_rptr const end_data)
	{
		byte_string dec;

		std::size_t enc_size = internal::narrow_cast<std::size_t>(std::distance(beg_data, end_data));

		if(enc_size == 0)
			return dec;

		assert(enc_size % 8 == 0);

		block_type block;

		while(beg_data != end_data)
		{
			beg_data = copy_bytes(beg_data, block);
			block = decrypt(block, m_key);
			dec.insert(std::end(dec), byte_const_rptr(block.data()), byte_const_rptr(block.data() + block.size()));
		}

		dec.resize(enc_size - internal::narrow_cast<unsigned>(*std::prev(end_data)));

		return dec;
	}

	byte_string decrypt(byte_string const& enc)
	{
		byte_string dec;

		if(enc.empty())
			return dec;

		assert(enc.size() % 8 == 0);

		block_type block;

		auto const* beg_data = enc.data();
		auto const* const end_data = enc.data() + enc.size();

		while(beg_data != end_data)
		{
			beg_data = copy_bytes(beg_data, block);
			block = decrypt(block, m_key);
			dec.insert(std::end(dec), byte_const_rptr(block.data()), byte_const_rptr(block.data() + block.size()));
		}

		dec.resize(enc.size() - internal::narrow_cast<unsigned>(dec.back()));

		return dec;
	}

private:
	static block_type encrypt(block_type b, key_type const& k)
	{
		b[0] = internal::to_network_byte_order(b[0]);
		b[1] = internal::to_network_byte_order(b[1]);

		std::uint32_t sum = 0;
		std::uint32_t delta = 0x9E3779B9;

		for(std::uint32_t i = 0; i < 32; i++)
		{
			sum += delta;
			b[0] += ((b[1] << 4) + k[0]) ^ (b[1] + sum) ^ ((b[1] >> 5) + k[1]);
			b[1] += ((b[0] << 4) + k[2]) ^ (b[0] + sum) ^ ((b[0] >> 5) + k[3]);
		}

		return b;
	}

	static block_type decrypt(block_type b, key_type const& k)
	{
		std::uint32_t sum = 0xC6EF3720;
		std::uint32_t delta = 0x9E3779B9;

		for(std::uint32_t i = 0; i < 32; i++)
		{
			b[1] -= ((b[0] << 4) + k[2]) ^ (b[0] + sum) ^ ((b[0] >> 5) + k[3]);
			b[0] -= ((b[1] << 4) + k[0]) ^ (b[1] + sum) ^ ((b[1] >> 5) + k[1]);
			sum -= delta;
		}

		b[0] = internal::from_network_byte_order(b[0]);
		b[1] = internal::from_network_byte_order(b[1]);

		return b;
	}

	static auto fill(char const* beg, char const* end, block_type& block)
	{
		char const* to = std::min(beg + 4, end);
		std::copy(beg, to, char_rptr(std::data(block)));
		std::fill_n(byte_rptr(std::data(block)), std::distance(to, beg + 4), byte(0));
		return std::distance(beg, to);
	}

	static byte_const_rptr copy_bytes(byte_const_rptr from, block_type& to)
	{
		auto from_end = from + (to.size() * 4);
		std::copy(from, from_end, byte_rptr(to.data()));
		return from_end;
	}

	static byte_rptr copy_bytes(block_type const& from, byte_rptr to)
	{
		std::copy(byte_const_rptr(from.data()), byte_const_rptr(from.data() + from.size()), to);
		return to + (from.size() * 4);
	}

	static byte_const_rptr copy_bytes(byte_const_rptr from, std::uint32_t& to)
	{
		auto from_end = from + sizeof(to);
		std::copy(from, from_end, byte_rptr(&to));
		return from_end;
	}

	static byte_rptr copy_bytes(std::uint32_t from, byte_rptr to)
	{
		std::copy(byte_const_rptr(&from), byte_const_rptr(&from) + sizeof(from), to);
		return to + sizeof(from);
	}

	key_type m_key;
};

}  // namespace crypto
}  // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_CRYPTO_H


