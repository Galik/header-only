#ifndef HEADER_ONLY_LIBRARY_SPLIT_ALGOS_H
#define HEADER_ONLY_LIBRARY_SPLIT_ALGOS_H

#include <algorithm>
#include <functional>

//#include "bug.h"
#include "misc_utils.h"
//#include "macro_exceptions.h"


namespace header_only_library {
namespace algorithm {
namespace chr {

inline char const* space(char) { return " "; }
inline wchar_t const* space(wchar_t) { return L" "; }
inline char16_t const* space(char16_t) { return u" "; }
inline char32_t const* space(char32_t) { return U" "; }

} // namespace chr

template<typename CharT>
CharT* find_null_terminator(CharT* s)
{
	while(*s)
		++s;
	return s;
}

using namespace header_only_library::misc_utils;

/**
 *
 * Split a buffer by a delimiter.
 *
 * If the buffer has 0 non-adjacent delimiters it is split 0 times producing 1 piece.
 * If the buffer has 1 non-adjacent delimiters it is split 1 times producing 2 pieces.
 * If the buffer has 2 non-adjacent delimiters it is split 2 times producing 3 pieces.
 * If the buffer has n non-adjacent delimiters it is split n times producing n + 1 pieces.
 *
 * adjacent delimiters are counted as a single delimiter
 *
 * @param beg_s
 * @param end_s
 * @param beg_d
 * @param end_d
 * @param out
 */
template<typename SearchIter, typename DelimIter, typename Inserter>
void split(SearchIter beg_s, SearchIter end_s, DelimIter beg_d, DelimIter end_d, Inserter out)
{
//	bug_fun();
	HOL_ASSERT_MSG(std::distance(beg_d, end_d), "delimiter cannot be empty");

	if(beg_s == end_s)
		return;

	auto size_d = std::distance(beg_d, end_d);

	bool splitted = false;

	for(;;)
	{
		SearchIter pos = std::search(beg_s, end_s, beg_d, end_d);

		if(pos == end_s)
			break;

		splitted = true;

		out.insert(beg_s, pos);

		beg_s = std::next(pos, size_d);
		while(std::distance(beg_s, end_s) < size_d && std::equal(beg_s, beg_s + size_d, beg_d, end_d))
			beg_s = std::next(pos, size_d);
	}

	if(splitted || beg_s != end_s)
		out.insert(beg_s, end_s);
}

/**
 *
 * Split a buffer by a delimiter.
 *
 * If the buffer has 0 delimiters it is split 0 times producing 1 piece.
 * If the buffer has 1 delimiters it is split 1 times producing 2 pieces.
 * If the buffer has 2 delimiters it is split 2 times producing 3 pieces.
 * If the buffer has n delimiters it is split n times producing n + 1 pieces.
 *
 * @param beg_s
 * @param end_s
 * @param beg_d
 * @param end_d
 * @param out
 */
template<typename SearchIter, typename DelimIter, typename Inserter>
void split_keep(SearchIter beg_s, SearchIter end_s, DelimIter beg_d, DelimIter end_d, Inserter out)
{
	HOL_ASSERT_MSG(std::distance(beg_d, end_d), "delimiter cannot be empty");

	if(beg_s == end_s)
		return;

	auto size_d = std::distance(beg_d, end_d);

	SearchIter pos = std::search(beg_s, end_s, beg_d, end_d);

	if(pos == end_s)
		return;

	for(;;)
	{
		out.insert(beg_s, pos);

		if(pos == end_s)
			break;

		beg_s = std::next(pos, size_d);

		pos = std::search(beg_s, end_s, beg_d, end_d);
	}
}

//template<typename SearchIter, typename DelimIter, typename Inserter>
//void split(SearchIter beg_s, SearchIter end_s, DelimIter beg_d, DelimIter end_d, Inserter out)
//{
//	HOL_ASSERT_MSG(std::distance(beg_d, end_d), "delimiter cannot be empty");
//
//	if(beg_s == end_s)
//		return;
//
//	auto size_d = std::distance(beg_d, end_d);
//
//	std::size_t count = 0U;
//	SearchIter pos;
//
//	while((pos = std::search(beg_s, end_s, beg_d, end_d)) != end_s)
//	{
////		if(std::distance(beg_s, pos))
//		out.insert(beg_s, pos);
//		beg_s = std::next(pos, size_d);
//		++count;
//	}
//
//	if(pos != end_s)
//		out.insert(beg_s, pos);
//	else if(!count)
//		out.insert(beg_s, end_s);
//}

template<typename SearchIter, typename DelimIter, typename Inserter>
void split_fold(SearchIter beg_s, SearchIter end_s, DelimIter beg_d, DelimIter end_d, Inserter out)
{
	if(beg_s == end_s)
		return;

	auto size_d = std::distance(beg_d, end_d);

	HOL_ASSERT_MSG(size_d, "delimiter cannot be empty");

	std::size_t count = 0U;
	SearchIter pos;

	while((pos = std::search(beg_s, end_s, beg_d, end_d)) != end_s)
	{
		if(std::distance(beg_s, pos))
			out.insert(beg_s, pos);
		beg_s = std::next(pos, size_d);
		++count;
	}

	if(std::distance(beg_s, pos))
		out.insert(beg_s, pos);
}

/**
 * Split on all delimiters between beg_d and end_d
 * @param beg_s
 * @param end_s
 * @param beg_d
 * @param end_d
 * @param out
 */
template<typename SearchIter, typename DelimsIter, typename Inserter>
void split_all(SearchIter beg_s, SearchIter end_s, DelimsIter beg_d, DelimsIter end_d, Inserter out)
{
	using value_type = typename Inserter::container_type::value_type;

	std::vector<std::pair<SearchIter, DelimsIter>> poss;
	poss.reserve(std::distance(beg_d, end_d));

	auto beg = beg_s;

	for(;;)
	{
		poss.clear();

		for(auto delim = beg_d; delim != end_d; ++delim)
		{
			auto found = std::search(beg, end_s, std::begin(*delim), std::end(*delim));

			if(found != end_s)
				poss.emplace_back(found, delim);
		}

		if(poss.empty())
			break;

		std::sort(std::begin(poss), std::end(poss));

		for(auto pos: poss)
		{
			out = value_type(beg, pos.first);
			beg = std::next(pos.first, pos.second->size());
		}
	}

	if(beg != end_s)
		out = value_type(beg, end_s);
}

template<typename Container>//, typename Contained>
class emplace_back_inserter
{
public:
	emplace_back_inserter(Container& c): c(c) {}

	template<typename Iter>

	void insert(Iter b, Iter e) { c.emplace_back(b, std::size_t(e - b)); }

private:
	Container& c;
};

template<typename Container>
auto inserter(Container& c) -> emplace_back_inserter<Container>
{
	return emplace_back_inserter<Container>(c);
}

//template<typename Container>
//class emplace_back_span_inserter
//{
//public:
//	emplace_back_span_inserter(Container& c): c(c) {}
//
//	template<typename Iter>
//	void insert(Iter b, Iter e)
//	{
//		if(b != e)
//			c.emplace_back(&*b, std::distance(b, e));
//		else
//			c.emplace_back();
//	}
//
//private:
//	Container& c;
//};
//
//template<typename Container>
//auto span_inserter(Container& c)
//{
//	return emplace_back_span_inserter<Container>(c);
//}

} // namespace algorithm
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_SPLIT_ALGOS_H
