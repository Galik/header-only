#ifndef HEADER_ONLY_LIBRARY_PRO_VECTOR_H
#define HEADER_ONLY_LIBRARY_PRO_VECTOR_H
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <vector>

//#include <hol/assertions.h>
//#include <hol/bug.h>

namespace header_only_library {
namespace containers {

/**
 * Benefits over std::vector
 *
 *     Move semantics are well defined. The moved from object
 *     is left empty after the move.
 *
 *     There is no operator[] for abuse with dodgy indexing math.
 *
 * @tparam T
 * @tparam Alloc
 */
template<typename T, typename Alloc = std::allocator<T>>
class pro_vector
: private std::vector<T, Alloc>
{
	using vector = typename std::vector<T, Alloc>;

public:
	using size_type = typename vector::size_type;

	using vector::vector;
	using vector::pop_back;
	using vector::push_back;
	using vector::emplace;
	using vector::emplace_back;
	using vector::erase;
	using vector::insert;
	using vector::clear;
	using vector::empty;
	using vector::size;
	using vector::begin;
	using vector::end;
	using vector::cbegin;
	using vector::cend;
	using vector::data;
	using vector::front;
	using vector::back;
	using vector::reserve;
	using vector::resize;
	using vector::shrink_to_fit;

	// leaves other vector in known, empty state
	pro_vector(pro_vector&& other): vector()
		{ std::swap(static_cast<vector&>(*this), static_cast<vector&>(other)); }

	// leaves other vector in known, empty state
	pro_vector& operator=(pro_vector&& other) noexcept
		{ resize(0); std::swap(static_cast<vector&>(*this), static_cast<vector&>(other)); return *this; }

//	T* data_begin() { return data(); }
//	T const* data_begin() const { return data(); }


//	T* data_end() { return data() + size(); }
//	T const* data_end() const { return data() + size(); }
};

//// additional deduction guide
//template<typename T, typename Alloc = std::allocator<T>>
//pro_vector(std::initializer_list<T>) -> pro_vector<T, Alloc>;

} // namespace containers
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_PRO_VECTOR_H

