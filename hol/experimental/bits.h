#ifndef HEADER_ONLY_LIBRARY_EXPERIMENTAL_BITS_H
#define HEADER_ONLY_LIBRARY_EXPERIMENTAL_BITS_H

#include <string>

#include "../random_numbers.h"
#include "../random_distros.h"
#include "../string_utils.h"

namespace header_only_library {
namespace bits {

// class two_dee_span requires <corelib/span.hpp>

//template<typename T>
//class two_dee_span
//{
//public:
//	using size_type = std::size_t;
//
//	two_dee_span(size_type rows, size_type cols): m_vs(rows * cols)
//	{
////		for(size_type pos = 0; pos < rows; ++pos)
////			m_sps.emplace_back(m_vs.data() + (pos * cols), cols);
//		for(size_type pos = 1; pos < rows; ++pos)
//			m_poss.emplace_back(m_vs.data() + (pos * cols));
//	}
//
//	cl::span<T> operator[](size_type row) { return m_sps(row); }
//	cl::const_span<T> operator[](size_type row) const { return m_sps(row); }
//
//	size_type rows() const { return m_poss.size() + 1; }
//	size_type cols(size_type row) const { return m_sps(row).size(); }
//
//	auto begin()	   { return std::begin(m_vs); }
//	auto end()		 { return std::end(m_vs); }
//
//	auto begin() const { return std::cbegin(m_vs); }
//	auto end()   const { return std::cend(m_vs); }
//
//private:
//
//	cl::span<T> m_sps(size_type row)
//	{
//		HOL_ASSERT(row <= m_poss.size());
//
//		T* beg = m_vs.data();
//		T* end = m_vs.data() + m_vs.size();
//
//		if(row > 0)
//			beg = m_poss[row - 1];
//
//		if(row < m_poss.size())
//			end = m_poss[row];
//
//		return cl::span<T>{beg, end};
//	}
//
//	cl::const_span<T> m_sps(size_type row) const
//	{
//		HOL_ASSERT(row <= m_poss.size());
//
//		T const* beg = m_vs.data();
//		T const* end = m_vs.data() + m_vs.size();
//
//		if(row > 0)
//			beg = m_poss[row - 1];
//
//		if(row < m_poss.size())
//			end = m_poss[row];
//
//		return cl::const_span<T>{beg, end};
//	}
//
//	std::vector<T> m_vs;
////	std::vector<cl::span<T>> m_sps;
//	std::vector<T*> m_poss;
//};
//


//std::ostream& binary_write_le(std::ostream& os, std::uint16_t u)
//{
//	unsigned char lsb = (u & 0x0F);
//	unsigned char msb = (u & 0xF0) >> 8;
//	return os.write((char*)&lsb, 1).write((char*)&msb, 1);
//}
//
//template<typename UnsignedInt,
//	typename std::enable_if<std::is_integral<UnsignedInt>::value>::type>
//std::ostream& binary_write_le(std::ostream& os, UnsignedInt u)
//{
//	unsigned char c[sizeof(u)];
//
//	for(auto b = 0U; b < sizeof(u); ++b)
//		c[b] = (u & (0x0F << (8 * b))) >> (8 * b);
//
//	return os.write((char*)c, sizeof(c));
//}
//
//template<typename Real>
//typename std::conditional
//<
//	sizeof(Real) == sizeof(std::uint32_t), std::uint32_t, std::uint64_t
//>::type to_binary(Real r)
//{
//	std::uint32_t u;
//	auto cp = reinterpret_cast<char*>(&r);
//	std::copy(cp, cp + sizeof(r), (char*)&u);
//	return u;
//}
//
//template<typename Real,
//	typename std::enable_if<std::is_floating_point<Real>::value>::type>
//std::ostream& binary_write_le(std::ostream& os, Real r)
//{
//	auto u = to_binary(r);
//	return binary_write_le(os, u);
//}

namespace heuristics {

// Running average

template<typename Numeric>
class running_average
{
public:

	void reset() { m_number = {}; m_average = {}; }

	void add(Numeric value)
	{
		auto number = m_number;
		++m_number;
		m_average = ((m_average * number) + value) / m_number;
	}

	Numeric average() const { return m_average; }

private:
	Numeric m_number = {};
	Numeric m_average = {};
};

template<typename Numeric, std::size_t N>
class running_average_small_window
{
public:
	void reset()
	{
		m_values.fill({});
		m_pos = 0;
	}

	void add(Numeric value)
	{
		m_values[m_pos] = value;
		m_pos = (m_pos + 1) % N;
	}

	Numeric average() const { return std::accumulate(std::begin(m_values), std::end(m_values), Numeric(0)) / Numeric(N); }

private:
	std::size_t m_pos = 0;
	std::array<Numeric, N> m_values = {};
};

template<typename Numeric, std::size_t N>
class running_average_large_window
{
public:
	void reset()
	{
		m_full = false;
		m_pos = 0;
		m_average = {};
	}

	void add(Numeric value)
	{
		if(!m_full)
		{
			m_values[m_pos] = value;

			auto number = m_pos;
			++m_pos;
			m_average = ((m_average * Numeric(number)) + value) / Numeric(m_pos);

			if(m_pos == N)
			{
				m_pos = 0;
				m_full = true;
			}
		}
		else
		{
			m_average = ((m_average * N) - m_values[m_pos] + value) / N;
			m_values[m_pos] = value;
			m_pos = (m_pos + 1) % N;
			if(m_pos == 0)
				m_full = true;
		}
	}

	Numeric average() const { return m_average; }

private:
	bool m_full = false;
	std::size_t m_pos = 0;
	std::array<Numeric, N> m_values;
	Numeric m_average = {};
};

using running_real_average = running_average<double>;
using running_integral_average = running_average<int>;

template<std::size_t N>
using running_real_average_small_window = running_average_small_window<double, N>;

template<std::size_t N>
using running_integral_average_small_window = running_average_small_window<int, N>;

template<std::size_t N>
using running_real_average_large_window = running_average_large_window<double, N>;

template<std::size_t N>
using running_integral_average_large_window = running_average_large_window<int, N>;

}  // namespace heuristics

namespace finance {
namespace money {

using count_value = std::ptrdiff_t;

enum class pound_value: count_value{};
enum class penny_value: count_value{};

pound_value operator+(pound_value a, pound_value b) { return pound_value(count_value(a) + count_value(b)); }
penny_value operator+(penny_value a, penny_value b) { return penny_value(count_value(a) + count_value(b)); }

penny_value operator+(pound_value a, penny_value b) { return penny_value((count_value(a) * 100) + count_value(b)); }
penny_value operator+(penny_value a, pound_value b) { return penny_value(count_value(a) + (count_value(b) * 100)); }

pound_value operator-(pound_value a, pound_value b) { return pound_value(count_value(a) - count_value(b)); }
penny_value operator-(penny_value a, penny_value b) { return penny_value(count_value(a) - count_value(b)); }

penny_value operator-(pound_value a, penny_value b) { return penny_value((count_value(a) * 100) - count_value(b)); }
penny_value operator-(penny_value a, pound_value b) { return penny_value(count_value(a) - (count_value(b) * 100)); }

penny_value operator*(penny_value a, count_value b) { return penny_value(count_value(a) * b); }
penny_value operator/(penny_value a, count_value b) { return penny_value(count_value(a) / b); }
penny_value operator%(penny_value a, count_value b) { return penny_value(count_value(a) % b); }

pound_value operator*(pound_value a, count_value b) { return pound_value(count_value(a) * b); }
pound_value operator/(pound_value a, count_value b) { return pound_value(count_value(a) / b); }
pound_value operator%(pound_value a, count_value b) { return pound_value(count_value(a) % b); }

class cost_value
{
public:
	cost_value(penny_value pence)
	: m_pounds(pound_value(pence / 100)), m_pence(pence % 100) {}

	cost_value(pound_value pounds, penny_value pence)
	: m_pounds(pounds), m_pence(pence) {}

	pound_value number_of_pounds() const { return m_pounds; }
	penny_value number_of_pence() const { return m_pence; }
	penny_value total_amount_in_pence() const { return penny_value(count_value(m_pounds) * 100) + m_pence; }

private:
	pound_value m_pounds;
	penny_value m_pence;
};

cost_value operator+(cost_value a, cost_value b)
	{ return cost_value(a.total_amount_in_pence() + b.total_amount_in_pence()); }

cost_value operator-(cost_value a, cost_value b)
	{ return cost_value(a.total_amount_in_pence() - b.total_amount_in_pence()); }

cost_value operator*(cost_value a, count_value b)
	{ return cost_value(a.total_amount_in_pence() * b); }

cost_value operator/(cost_value a, count_value b)
	{ return cost_value(a.total_amount_in_pence() / b); }

cost_value operator%(cost_value a, count_value b)
	{ return cost_value(a.total_amount_in_pence() % b); }

}  // namespace money

enum class unit{each = 1, Kg = 1};

enum class product_id: std::size_t{};

class product
{
	product_id id;
};


class cash_register
{
public:

private:
	std::vector<product> m_products;
};

}  // namespace finance

// cal requires <date>

//namespace cal {
//
//enum class latitude_type_id{};
//enum class longitude_type_id{};
//
//using latitude_t = hol::typed_number<latitude_type_id, double>;
//using longitude_t = hol::typed_number<longitude_type_id, double>;
//
//struct GPS_coordinate
//{
//	latitude_t latitude = latitude_t(0.0);
//	longitude_t longitude = longitude_t(0.0);
//
//	bool operator==(GPS_coordinate const& other) const
//		{ return latitude == other.latitude && longitude == other.longitude; }
//
//	bool operator<(GPS_coordinate const& other) const
//	{
//		if(longitude == other.longitude)
//			return latitude < other.latitude;
//		return longitude < other.longitude;
//	}
//};
//
//struct location
//{
//	std::string name;
//	GPS_coordinate position;
//
//	bool operator==(location const& other) const
//		{ return name == other.name && position == other.position;}
//
//	bool operator<(location const& other) const
//	{
//		if(position == other.position)
//			return name < other.name;
//		return position < other.position;
//	}
//};
//
//using zoned_time = date::zoned_time<std::chrono::seconds>;//decltype(date::make_zoned("", date::sys_days()));
//
//struct calendar_event
//{
//	enum class repeat_type{};
//
//	std::string what;
//	zoned_time when;
//	location where;
//
//	std::string notes;
//
//	bool operator<(calendar_event const& other) const
//	{
//		if(when.get_sys_time().time_since_epoch() == other.when.get_sys_time().time_since_epoch())
//		{
//			if(what == other.what)
//			{
//				if(where == other.where)
//					return notes < other.notes;
//				return where < other.where;
//			}
//			return what < other.what;
//		}
//		return when.get_sys_time().time_since_epoch() < other.when.get_sys_time().time_since_epoch();
//	}
//};
//
//class alarm_clock
//{
//	using mutex_type = std::shared_mutex;
//	using shared_lock = std::shared_lock<mutex_type>;
//	using unique_lock = std::unique_lock<mutex_type>;
//
//public:
//
//	template<typename TimePoint>
//	void add(std::string const& name, TimePoint when, location where = {})
////	void add(std::string const& name, zoned_time when, location where = {})
//	{
////		auto zt = date::make_zoned(date::current_zone(), when);
////		calendar_event e{name, zt, where, ""};
//		calendar_event e{name, when, where, ""};
//
//		auto w_q = m_q.open_for_writing();
//
//		w_q->insert(std::lower_bound(std::begin(*w_q), std::end(*w_q), e), e);
//	}
//
//	void remove_by_name(std::string const& name)
//	{
//		auto w_q = m_q.open_for_writing();
//
//		auto found = std::find_if(std::begin(*w_q), std::end(*w_q), [&](calendar_event const& e){
//			return e.what == name;
//		});
//
//		if(found != std::end(*w_q))
//			w_q->erase(found);
//	}
//
//	void stop() { m_done = true; m_thread.join(); }
//
//	void start()
//	{
////		using time_point = std::chrono::steady_clock::time_point;
//
//		m_thread = std::thread([this]{
//
//			auto wake_time = std::chrono::steady_clock::now();
//			auto work_time = wake_time;
//
//			while(!m_done)
//			{
//				work_time += std::chrono::minutes(1);
//
//				// wake up once a second to check exit flag: m_done
//				// once a minute, check alarms
//				while(!m_done && wake_time < work_time)
//				{
//					wake_time += std::chrono::seconds(1);
//					std::this_thread::sleep_until(wake_time);
//				}
//
//				if(m_done)
//					break;
//
//				auto w_q = m_q.open_for_writing();
//
//				auto lb = std::lower_bound(std::begin(*w_q), std::end(*w_q), wake_time, [](auto const& e, auto const& wt){
//					return e.when.get_sys_time().time_since_epoch() < wt.time_since_epoch();
//				});
//
//				for(auto e = lb; e != std::end(*w_q);)
//				{
//					std::ostringstream oss;
//					oss << e->what << '\n';
//					con_out(oss.str());
//					e = w_q->erase(e);
//				}
//			}
//		});
//	}
//
//private:
//	std::thread m_thread;
//	std::atomic_bool m_done{false};
//	hol::locked_object<std::vector<calendar_event>> m_q; // sorted by time
//};
//
//calendar_event make_event(std::string const& what, zoned_time when, location where)
//{
//	return calendar_event{what, when, where, ""};
//}
//
//}  // namespace cal

namespace ipsum_dipsum {

using CharT = char;
using String = std::string;

std::size_t normalizeX(std::size_t min, std::size_t max, double mu = 0.0, double sd = 1.0)
{
	auto size = max - min - 1;
	auto range = std::size_t(double(size) * std::abs(random_numbers::random_normal(mu, sd)));
	if(range > size)
		range = size;
//	return min + ((max - min) - range);
	return min + range;
}

std::size_t normalize(std::size_t min, std::size_t max, double mu = 0.0, double sd = 1.0)
{
//	bug_fun();
//	{
//		bug_scope("params");
//		bug_var(min);
//		bug_var(max);
//		bug_var(mu);
//		bug_var(sd);
//	}

	auto x = random_numbers::random_normal(mu, sd);
//	bug_var(x);

	auto sd_2 = sd * sd;
//	bug_var(v);

	auto const max_y = 1.0 / std::sqrt(2.0 * M_PI * sd_2);
//	bug_var(max_y);

	auto y = max_y * std::pow(M_E, -(((x - mu) * (x - mu)) / (2 * sd_2)));
//	bug_var(y);

	auto size = max - min;
//	bug_var(size);
	auto offset = size - std::size_t((y / max_y) * double(size));
//	bug_var(offset);

	return min + offset;
}

String ipsum_dipsum(std::size_t const min_words, std::size_t const max_words,
	std::size_t const min_chars, std::size_t const max_chars)
{
//	bug_fun();
//	bug_var(min_words);
//	bug_var(max_words);
//	bug_var(min_chars);
//	bug_var(max_chars);

	String s;

	static char const vowels[] = "aeiou";
	static char const consononts[] = "bcdfghjklmnpqrstvwxyz";

	bool vowel = random_numbers::random_choice(double(std::size(vowels) - 1) / double(std::size(consononts) - 1));

	auto random_char = [&](bool capitalize = false){

		auto c = vowel ? random_numbers::random_element(vowels, vowels + 5)
			: random_numbers::random_element(consononts, consononts + 21);

		assert(c >= 'a' && c <= 'z');

		vowel = !vowel;

		if(capitalize)
			c = std::toupper(c, std::locale(""));

		return CharT(c);
	};

	auto capitalize = true;
	string_utils::basic_string_separator<CharT> sep;

	auto words = normalize(min_words, max_words, 0.0, 60.0);

	while(words--)
	{
		vowel = random_numbers::random_choice(5.0 / 26.0);
		auto chars = normalize(min_chars, max_chars, 0.0, 0.001);

		if(chars--)
		{
			s = (s + sep) + random_char(capitalize);

			capitalize = false;

			while(chars--)
				s += random_char(capitalize);

		}
	}

	return s + CharT('.');
}

}  // namespace ipsum_dipsum

}  // namespace bits
}  // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_EXPERIMENTAL_BITS_H
