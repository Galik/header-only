#ifndef HEADER_ONLY_LIBRARY_NEVER_NULL_H
#define HEADER_ONLY_LIBRARY_NEVER_NULL_H
//
// Copyright (c) 2021 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <memory>

namespace header_only_library {
namespace never_null {

template<typename T>
class nn_unique_ptr;

template<typename T>
class nn_access_ptr
{
public:
	nn_access_ptr(nn_access_ptr aptr) noexcept;
	nn_access_ptr(nn_unique_ptr<T>& uptr) noexcept;

	T* operator->() noexcept;
	T const* operator->() const noexcept;

	T& operator*() noexcept;
	T const& operator*() const noexcept;

	nn_access_ptr operator=(nn_access_ptr p) noexcept = default;

private:
	T* m_ptr;
};

template<typename T>
class nn_unique_ptr
{
public:
	template<typename... Args>
	nn_unique_ptr(Args&&... args)
		: m_uptr(std::make_unique(std::forward<Args>(args)...)) {}

	nn_unique_ptr(nn_unique_ptr&&) = delete;
	nn_unique_ptr(nn_unique_ptr const&) = delete;

	nn_unique_ptr& operator=(nn_unique_ptr&&) = delete;
	nn_unique_ptr& operator=(nn_unique_ptr const&) = delete;

	T* operator->() noexcept { return m_uptr.get(); }
	T const* operator->() const noexcept { return m_uptr.get(); }

	T& operator*() noexcept { return *m_uptr; }
	T const& operator*() const noexcept { return *m_uptr; }

	nn_access_ptr<T> access() noexcept { return nn_access_ptr<T>(*this); }

private:
	std::unique_ptr<T> m_uptr;
};

template<typename T>
nn_access_ptr<T>::nn_access_ptr(nn_unique_ptr<T>& uptr) noexcept
: m_ptr(uptr.get()) {}

template<typename T>
nn_access_ptr<T>::nn_access_ptr(nn_access_ptr aptr) noexcept: m_ptr(optr.m_ptr) {}

template<typename T>
T* nn_access_ptr<T>::operator->() noexcept { return m_ptr; }

template<typename T>
T const* nn_access_ptr<T>::operator->() const noexcept { return m_ptr; }

template<typename T>
T& nn_access_ptr<T>::operator*() noexcept { return *m_ptr; }

template<typename T>
T const& nn_access_ptr<T>::operator*() const noexcept { return *m_ptr; }

} // namespace never_null
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_NEVER_NULL_H
