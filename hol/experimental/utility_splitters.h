#ifndef HEADER_ONLY_LIBRARY_UTILITY_SPLITTERS_H
#define HEADER_ONLY_LIBRARY_UTILITY_SPLITTERS_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "utility_common.h"

namespace hol {

// split

template<typename Mode>
class splitter
{
	static_assert
	(
		   std::is_same<Mode, copy>::value
#if defined(HOL_USE_STRING_VIEW)
		|| std::is_same<Mode, view>::value
#endif
#if defined(HOL_USE_GSL_SPAN)
		|| std::is_same<Mode, span>::value
#endif
		, "Mode template parameter must be copy, view or span"
	);

public:
	using mode = Mode;
	using string_type = typename mode::string_type;//std::experimental::string_view;
	using vector_type = typename mode::vector_type;

	struct config
	{
		bool fold = true;
		bool strict = false;
		std::size_t reserve = 20;
		void fold_matches() { fold = true; }
		void dont_fold_matches() { fold = false; }
		void reserve_at_least(std::size_t reserve) { this->reserve = reserve; }
	};

	void configure(config const& cfg)
	{
		this->cfg = cfg;
	}

	config& configure()
	{
		return cfg;
	}

	config configuration() const { return cfg; }

private:
//	bool fold = true;
//	bool strict = false;
//	std::size_t reserve = 20;

	config cfg;

	template<typename Iterator, typename Comparator>
	vector_type at(Iterator pos, Iterator done, Comparator cmp)
	{
		vector_type v;
		v.reserve(cfg.reserve);

		auto end = pos;

		if(cfg.fold)
		{
			while((pos = std::find_if_not(end, done, cmp)) != done)
			{
				end = std::find_if(pos, done, cmp);
				if(end > pos)
					v.emplace_back(pos, end - pos);
			}
		}
		else
		{
			while((end = std::find_if(pos, done, cmp)) != done)
			{
				if(end > pos)
					v.emplace_back(pos, end - pos);
				pos = end + 1;
			}

			if(pos != done)
				v.emplace_back(pos, end - pos);
		}

		return v;
	}

	template<typename Comparator>
	vector_type at(string_type s, Comparator cmp)
	{
		return at(s.data(), s.data() + s.size(), cmp);
	}

public:
	void fold_matches() { cfg.fold_matches(); }
	void dont_fold_matches() { cfg.dont_fold_matches(); }
	void reserve_at_least(std::size_t reserve) { cfg.reserve_at_least(reserve); }

	vector_type at_space(string_type s)
	{
		return at(s, std::ptr_fun<int, int>(std::isspace));
	}

	vector_type at_delim(string_type s, char delim)
	{
		return at(s, [delim](char c){return c == delim;});
	}

	vector_type at_delims(string_type s, string_type delims)
	{
		return at(s, [delims](char c){return delims.find(c) != string_type::npos;});
	}

	vector_type at_delim(string_type s, string_type delim)
	{
		if(delim.empty())
			return {};

		vector_type v;
		v.reserve(cfg.reserve);

		auto done = s.data() + s.size();
		auto end = s.data();
		auto pos = end;

		while((end = std::search(pos, done, delim.begin(), delim.end())) != done)
		{
			if(end > pos)
				v.emplace_back(pos, end - pos);
			pos = end + delim.size();
		}

		if(pos != done)
			if(end > pos)
				v.emplace_back(pos, end - pos);

		return v;
	}

	vector_type at_regex(string_type s, std::regex const& e)
	{
		vector_type v;
		v.reserve(cfg.reserve);

		auto pos = s.data();
		auto end = pos + s.size();
		std::cmatch m;

		while(std::regex_search(pos, end, m, e))
		{
			if(!m.empty())
				v.emplace_back(pos, m.position());
			pos = pos + m.position() + m.length();
		}

		if(end > pos)
			v.emplace_back(pos, end - pos);

		return v;
	}

	vector_type at_regex(string_type s, string const& e)
	{
		return at_regex(s, std::regex(e));
	}
};

using split_copier = splitter<copy>;

inline
split_copier& get_split_copier()
{
	thread_local static split_copier s;
	return s;
}

#if defined(HOL_USE_STRING_VIEW)
using split_viewer = splitter<view>;
inline split_viewer& get_split_viewer()
{
	thread_local static split_viewer s;
	return s;
}
#endif

#if defined(HOL_USE_GSL_SPAN)
using split_spanner = splitter<span>;
inline split_spanner& get_split_spanner()
{
	thread_local static split_spanner s;
	return s;
}
#endif

} // hol

#endif // HEADER_ONLY_LIBRARY_UTILITY_SPLITTERS_H
