#ifndef HEADER_ONLY_LIBRARY_EXPERIMENTAL_CONTAINERS_H
#define HEADER_ONLY_LIBRARY_EXPERIMENTAL_CONTAINERS_H

#include <any>
#include <cassert>
#include <fstream>
#include <string>
#include <vector>

namespace header_only_library {
namespace containers {

class varval
: public std::any
{
public:

	template<typename T>
	varval& operator=(T const& t) { return assign(t); }

	varval& assign(std::string const& s) { (*static_cast<std::any*>(this)) = s; return *this; }
	varval& assign(int		          i) { (*static_cast<std::any*>(this)) = i; return *this; }
	varval& assign(double		      f) { (*static_cast<std::any*>(this)) = f; return *this; }

};

class varmap
: public std::map<std::string, varmap>
{
public:
	varval value;

	void save(std::string const& filepath)
	{
		std::ofstream ofs(filepath);
		write(ofs, *this);
	}

private:
	static void write(std::ostream& os, varmap& vm, std::string const path = {})
	{
		if(vm.value.has_value())
		{
			if(vm.value.type() == typeid(std::string))
				os << path << ": [s] " << std::any_cast<std::string>(vm.value) << '\n';
			else if(vm.value.type() == typeid(long long))
				os << path << ": [i] " << std::any_cast<long long>(vm.value) << '\n';
			else if(vm.value.type() == typeid(long double))
				os << path << ": [f] " << std::any_cast<long double>(vm.value) << '\n';
		}

		for(auto&& e: vm)
		{
			auto new_path = path + (path.empty() ? "":".") + e.first;
			write(os, e.second, new_path);
		}
	}
};

template<typename UniquePtr>
class adopting_vector
{
public:
	using size_type = std::size_t;

	using value_type = typename UniquePtr::element_type;

	using pointer = value_type*;
	using const_pointer = value_type const*;

	using reference = value_type&;
	using const_reference = value_type const&;

	using iterator = pointer;
	using const_iterator = const_pointer;

	adopting_vector() noexcept {}

	explicit adopting_vector(UniquePtr ptr, size_type n, size_type reserve) noexcept
	: m_ptr(std::move(ptr)), m_size(n), m_reserve(reserve) {}

	explicit adopting_vector(UniquePtr ptr, size_type n) noexcept
	: adopting_vector(std::move(ptr), n, n) {}

	size_type size() const { return m_size; }
	size_type capacity() const { return m_reserve; }

	UniquePtr release() { m_size = m_reserve = 0; return std::move(m_ptr); }

	void reset(UniquePtr ptr, size_type n, size_type reserve)
	{
		m_size = n;
		m_reserve = reserve;
		std::swap(m_ptr, ptr);
	};

	void reset(UniquePtr ptr, size_type n) { reset(std::move(ptr), n, n); };

	void reset() { auto to_be_discarded = release(); };

	pointer data() { return m_ptr.get(); }
	const_pointer data() const { return m_ptr.get(); }

	pointer data_end() { return data() + size() ; }
	const_pointer data_end() const { return data() + size(); }

	iterator begin() { return data(); }
	iterator   end() { return data() + size(); }

	const_iterator begin() const { return data(); }
	const_iterator   end() const { return data() + size(); }

	const_iterator cbegin() const { return data(); }
	const_iterator   cend() const { return data() + size(); }

	reference operator[](size_type n) { return data()[n]; }
	const_reference operator[](size_type n) const { return data()[n]; }

	reference front() { return *begin(); }
	const_reference front() const { return *begin(); }

	reference back() { return *std::prev(end()); }
	const_reference back() const { return *std::prev(end()); }

	void push_back(value_type const& v)
	{
		check_push_back();
		new (data_end()) value_type(v);
		++m_size;
	}

	void push_back(value_type&& v)
	{
		check_push_back();
		new (data_end()) value_type(std::move(v));
		++m_size;
	}

	template<typename... Args>
	void emplace_back(Args&&... args)
	{
		check_push_back();
		new (data_end()) value_type(std::move(std::forward<Args>(args))...);
		++m_size;
	}

	void reserve(size_type n)
	{
		if(n == m_reserve)
			return;

		if(auto ptr = ptr(pointer(std::aligned_alloc(alignof(value_type), sizeof(value_type) * n))); !ptr)
			throw std::bad_alloc();
		else
		{
			std::uninitialized_move(cbegin(), std::min(cend(), cbegin() + n), ptr.get());
			std::for_each(std::min(cend(), cbegin() + n), cend(), [](reference r){
				r.~value_type();
			});
			std::swap(m_ptr, ptr);
		}
	}

	void resize(size_type n)
	{
		if(n == m_size)
			return;

		if(n > m_reserve)
			reserve(n);

		if(n < m_size)
		{
			std::for_each(begin() + n, begin() + size(), [](reference r){
				r.~value_type();
			});

			m_size = n;
		}
		else
		{
			// ctors
			std::for_each(begin() + size(), begin() + n, [](reference r){
				new (&r) value_type;
			});

			m_size = n;
		}
	}

private:

	void check_push_back()
	{
		constexpr size_type const default_initial_size = 32;

		if(m_size < m_reserve)
			return;

		auto new_size = m_size ? m_size + (m_size / 2) : default_initial_size;

		if(auto ptr = UniquePtr(pointer(std::aligned_alloc(alignof(value_type), sizeof(value_type) * new_size))); !ptr)
			throw std::bad_alloc();
		else
		{
			std::uninitialized_move(cbegin(), cend(), ptr.get());
			std::swap(m_ptr, ptr);
		}
	}

	UniquePtr m_ptr;
	size_type m_size = 0;
	size_type m_reserve = 0;
};

template<typename Generator>
class generating_iterator
{
public:
	using value_type = decltype(Generator()());
	using difference_type = decltype(value_type() - value_type());
	using reference = value_type&;
	using pointer = value_type*;
	using iterator_category = std::input_iterator_tag;

	generating_iterator(std::size_t n = 0): n(n) {}
	generating_iterator(std::size_t n, Generator gen): n(n), gen(gen) {}

	generating_iterator(generating_iterator const&) = default;
	generating_iterator& operator=(generating_iterator const&) = default;

	auto begin() { return generating_iterator(n, gen); }
	auto end()   { return generating_iterator(); }

	auto begin() const { return generating_iterator(n, gen); }
	auto end()   const { return generating_iterator(); }

	bool operator==(generating_iterator const& other) const { return n == other.n; }
	bool operator!=(generating_iterator const& other) const { return !(*this == other); }

	generating_iterator& operator++()    { --n; return *this; }
	generating_iterator  operator++(int) { auto copy(*this); ++(*this); return copy; }

	auto operator*()       { return gen(); }
	auto operator*() const { return gen(); }

	auto operator->()       { return this; }
	auto operator->() const { return this; }

	void swap(generating_iterator& other)
	{
		std::swap(n, other.n);
		std::swap(gen, other.gen);
	}

	friend void swap(generating_iterator<Generator>& a, generating_iterator<Generator>& b)
		{ a.swap(b); }

private:
	std::size_t n = 0;
	Generator gen;
};

template<typename Numeric>
using function_iterator = generating_iterator<std::function<Numeric()>>;

// Varisize Array

template<typename T, std::size_t N>
class varisize_array
{
public:
	varisize_array(std::size_t n = 0): n(n)
	{
		assert(n <= N);
		for(decltype(n) i = 0; i < n; ++i)
			new (&d[i]) T();
	}

	~varisize_array()
	{
		for(decltype(n) i = 0; i < n; ++i)
			d[i].~T();
	}

	varisize_array(varisize_array const& other)
	{
		for(decltype(n) i = 0; i < other.n; ++i)
			new (&d[i]) T(other.d[i]);
	}

	varisize_array(varisize_array&& other)
	{
		for(decltype(n) i = 0; i < other.n; ++i)
			new (&d[i]) T(std::move(other.d[i]));
	}

	// not exception safe
	varisize_array& operator=(varisize_array const& other)
	{
		if(&other != this)
		{
			for(decltype(n) i = 0; i < std::min(n, other.n); ++i)
				d[i] = other.d[i];

			if(n < other.n)
			{
				for(decltype(n) i = n; i < other.n; ++i)
					new (&d[i]) T(other.d[i]);
			}
			else
			{
				for(decltype(n) i = n; i < other.n; ++i)
					d[i].~T();
			}

			n = other.n;
		}

		return *this;
	}

	// not exception safe
	varisize_array& operator=(varisize_array&& other)
	{
		for(decltype(n) i = 0; i < std::min(n, other.n); ++i)
			d[i] = std::move(other.d[i]);

		if(n < other.n)
		{
			for(decltype(n) i = n; i < other.n; ++i)
				new (&d[i]) T(std::move(other.d[i]));
		}
		else
		{
			for(decltype(n) i = n; i < other.n; ++i)
				d[i].~T();
		}

		n = other.n;

		return *this;
	}

	void push_back(T const& v) { assert(n < N); new(&d[n++]) T(v); }
	void push_back(T&& v) { assert(n < N); new(&d[n++]) T(std::move(v)); }

	template<typename... Args>
	void emplace_back(Args&&... args) { assert(n < N); new(&d[n++]) T(std::forward<Args>(args)...); }

	T& operator[](std::size_t n) { assert(n < N); return d[n]; }
	T const& operator[](std::size_t n) const { assert(n < N); return d[n]; }

	T* begin() { return &d[0]; }
	T const* begin() const { return &d[0]; }
	T const* cbegin() const { return &d[0]; }

	T* end() { return &d[n]; }
	T const* end() const { return &d[n]; }
	T const* cend() const { return &d[n]; }

	auto rbegin() { return std::reverse_iterator<T*>(&d[n]); }
	auto rbegin() const { return std::reverse_iterator<T const*>(&d[n]); }
	auto crbegin() const { return std::reverse_iterator<T const*>(&d[n]); }

	auto rend() { return std::reverse_iterator<T*>(&d[0]); }
	auto rend() const { return std::reverse_iterator<T const*>(&d[0]); }
	auto crend() const { return std::reverse_iterator<T const*>(&d[0]); }

	std::size_t size() const { return n; }
	bool empty() const { return !size(); }

	T* data() { return d; }
	T const* data() const { return d; }

private:
	T d[N];
	std::size_t n;
};

template<typename T>
class linked_list
{
	struct node
	{
		T data;
		node* next = nullptr;
		node* prev = nullptr;

		node(T&& data): data(std::move(data)) {}
		node(T const& data): data(data) {}
	};

	template<typename Link>
	class basic_iterator
	{
		friend class linked_list<T>;

	public:
		basic_iterator(Link* lp): lp(lp) {}

		basic_iterator(basic_iterator const&) = default;
		basic_iterator& operator=(basic_iterator const&) = default;

		bool operator==(basic_iterator const& other) const { return lp == other.lp; }
		bool operator!=(basic_iterator const& other) const { return !(*this == other); }

		basic_iterator& operator++()    { lp = lp->next; return *this; }
		basic_iterator  operator++(int) { auto copy = lp; ++(*this); return copy; }

		basic_iterator& operator--()    { lp = lp->prev; return *this; }
		basic_iterator  operator--(int) { auto copy = lp; --(*this); return copy; }

		auto& operator*() { return lp->data; }
		auto& operator*() const { return lp->data; }

	private:
		Link* lp;
	};

public:
	using iterator = basic_iterator<node>;
	using const_iterator = basic_iterator<node const>;

	~linked_list() { delete_everything(); }

	void clear()
	{
		delete_everything();
		m_size = 0;
		m_head = m_tail = nullptr;
	}

	void push_front(T&& t)
		{ push_front(std::make_unique<node>(std::move(t)).release()); }

	void push_front(T const& t)
		{ push_front(std::make_unique<node>(t).release()); }

	template<typename... Args>
	void emplace_front(Args&&... args)
		{ push_front(std::make_unique<node>(std::forward<Args>(args)...).release()); }

	void push_back(T&& t)
		{ push_back(std::make_unique<node>(std::move(t)).release()); }

	void push_back(T const& t)
		{ push_back(std::make_unique<node>(t).release()); }

	template<typename... Args>
	void emplace_back(Args&&... args)
		{ push_back(std::make_unique<node>(std::forward<Args>(args)...).release()); }

	template<typename Iter>
	Iter erase(Iter iter) noexcept
	{
		auto n = iter.lp;
		auto prev = n->prev;
		auto next = n->next;

		delete_nodes(n, next);

		if(prev)
			prev->next = next;
		else
			m_head = next;

		if(next)
			next->prev = prev;
		else
			m_tail = prev;

		--m_size;

		return next;
	}

	void erase(const_iterator begin, const_iterator end) noexcept
		{ std::for_each(begin, end, [this](auto iter){ erase(iter); }); }

	iterator        begin()       { return iterator(m_head);        }
	iterator          end()       { return iterator(nullptr);       }

	const_iterator  begin() const { return const_iterator(m_head);  }
	const_iterator    end() const { return const_iterator(nullptr); }

	const_iterator cbegin() const { return const_iterator(m_head);  }
	const_iterator   cend() const { return const_iterator(nullptr); }

	std::size_t      size() const { return m_size; }
	bool            empty() const { return size() == 0; }

	void swap(linked_list& other) noexcept
	{
		std::swap(m_head, other.m_head);
		std::swap(m_tail, other.m_tail);
		std::swap(m_size, other.m_size);
	}

	linked_list& operator=(linked_list&& other) noexcept
		{ swap(*this, other); return *this; }

	linked_list& operator=(linked_list const& other)
	{
		if(&other != this)
		{
			clear();
			for(auto const& o: other)
				push_back(o);
		}

		return *this;
	}

	bool operator<(linked_list const& other) const
	{
		return std::lexicographical_compare(
			std::cbegin(*this), std::cend(*this),
			std::cbegin(other), std::cend(other));
	}

	bool operator==(linked_list const& other) const
	{
		return std::equal(
			std::cbegin(*this), std::cend(*this),
			std::cbegin(other), std::cend(other));
	}

	bool operator!=(linked_list const& other) const { return !(*this == other); }

//	void dump()
//	{
//		bug_fun();
//
//		bug_var(m_head);
//
//		for(auto p = m_head; p; p = p->next)
//		{
//			bug_scope("----------------");
//			bug_var(p);
//			bug_var(p->prev);
//			bug_var(p->data);
//			bug_var(p->next);
//		}
//
//		bug_var(m_tail);
//
//		for(auto p = m_tail; p; p = p->prev)
//		{
//			bug_scope("----------------");
//			bug_var(p);
//			bug_var(p->next);
//			bug_var(p->data);
//			bug_var(p->prev);
//		}
//
//		bug_var(m_head);
//	}

private:
	void push_front(node* n) noexcept
	{
		if((n->next = m_head))
			n->next->prev = n;

		m_head = n;

		if(!m_tail)
			m_tail = m_head;

		++m_size;
	}

	void push_back(node* n) noexcept
	{
		if((n->prev = m_tail))
			n->prev->next = n;

		m_tail = n;

		if(!m_head)
			m_head = m_tail;

		++m_size;
	}

	void delete_nodes(node* beg, node* end) const noexcept
	{
		while(beg != end)
			{ auto d = beg; beg = beg->next; delete d; }
	}

	void delete_everything() const noexcept
		{ delete_nodes(m_head, nullptr); }

	node* m_head = nullptr;
	node* m_tail = nullptr;
	std::size_t m_size = 0;
};

template<typename T, std::size_t N, bool Dynamic>
class basic_flexible_array
{
	template<typename TT, std::size_t NN, bool>
	struct flexible_storage {};

	template<typename TT, std::size_t NN>
	struct flexible_storage<TT, NN, false>
	{
		constexpr auto* data() noexcept { return std::launder(reinterpret_cast<TT*>(m_arr)); }
		constexpr auto const* data() const noexcept { return std::launder(reinterpret_cast<TT*>(m_arr)); }
		alignas(alignof(TT)) std::byte m_arr[sizeof(TT) * NN];
	};

	template<typename TT, std::size_t NN>
	struct flexible_storage<TT, NN, true>
	{
		~flexible_storage() { std::free(m_arr); }
		constexpr auto* data() noexcept { return m_arr; }
		constexpr auto const* data() const noexcept { return m_arr; }
		TT* m_arr = std::launder(reinterpret_cast<TT*>(std::aligned_alloc(alignof(TT), sizeof(TT) * NN)));
	};

public:
	using size_type = std::size_t;
	using value_type = T;
	using pointer = value_type*;
	using const_pointer = value_type const*;
	using reference = value_type&;
	using const_reference = value_type const&;
	using move_reference = value_type&&;

	using iterator = pointer;
	using const_iterator = const_pointer;

	basic_flexible_array() noexcept {}
	explicit basic_flexible_array(std::size_t n)
		{ std::uninitialized_fill_n(data(), n, value_type()); m_size = n; }

	~basic_flexible_array() { std::destroy_n(data(), size()); }

	constexpr auto capacity() const { return N; }

	size_type size() const { return m_size; }
	bool empty() const { return size() == 0; }

	pointer data() { return m_store.data(); }
	pointer data_end() { return data() + size(); }

	const_pointer data() const { return m_store.data(); }
	const_pointer data_end() const { return data() + size(); }

	iterator begin() { return data(); }
	iterator end() { return data_end(); }

	const_iterator begin() const { return data(); }
	const_iterator end() const { return data_end(); }

	const_iterator cbegin() const { return data(); }
	const_iterator cend() const { return data_end(); }

	void push_back(const_reference v)
	{
		assert(size() < capacity());
		std::uninitialized_fill_n(data_end(), 1, v);
		++m_size;
	}

	void push_back(move_reference v)
	{
		assert(size() < capacity());
		std::uninitialized_move_n(&v, 1, data_end());
		++m_size;
	}

	template<typename... Args>
	void emplace_back(Args&&... args)
	{
		assert(size() < capacity());
		::new (static_cast<void*>(std::addressof(*data_end()))) value_type(std::forward<Args>(args)...);
		++m_size;
	}

	void push_front(const_reference v)
	{
		assert(size() < capacity());
		std::uninitialized_move_n(data_end() - 1, 1, data_end());
		std::move_backward(data(), data_end() - 1, data_end());
		*data() = v;
		++m_size;
	}

	void push_front(move_reference v)
	{
		assert(size() < capacity());
		std::uninitialized_move_n(data_end() - 1, 1, data_end());
		std::move_backward(data(), data_end() - 1, data_end());
		*data() = std::move(v);
		++m_size;
	}

	template<typename... Args>
	void emplace_front(Args&&... args)
	{
		assert(size() < capacity());
		std::uninitialized_move_n(data_end() - 1, 1, data_end());
		std::move_backward(data(), data_end() - 1, data_end());
		std::destroy_at(data());
		::new (static_cast<void*>(std::addressof(*data()))) value_type(std::forward<Args>(args)...);
		++m_size;
	}

	void pop_back()
	{
		assert(!empty());
		--m_size;
		std::destroy_at(data_end());
	}

	void pop_front()
	{
		assert(!empty());
		std::move_backward(data() + 1, data_end(), data());
		--m_size;
		std::destroy_at(data_end());
	}

	const_iterator erase(const_iterator pos)
	{
		assert(!empty());

		auto n = size_type(std::distance(cbegin(), pos));
		auto p = data() + n;

		std::move(p + 1, data_end(), p);
		--m_size;
		std::destroy_at(data_end());

		return pos;
	}

	const_iterator erase(const_iterator begin, const_iterator end)
	{
		auto b = this->data() + std::distance(cbegin(), begin);
		auto e = this->data() + std::distance(cbegin(), end);
		auto n = size_type(std::distance(b, e));

		assert(n <= size());

		std::move(e, data_end(), b);
		std::destroy(b + n, data_end());
		m_size -= n;

		return end;
	}

	reference operator[](size_type n) { return data()[n]; }
	const_reference operator[](size_type n) const { return data()[n]; }

	size_type unused() const { return size_type(capacity() - size()); }

private:

	flexible_storage<T, N, Dynamic> m_store;
	size_type m_size = 0;
};

template<typename T, std::size_t N>
using flexible_static_array = basic_flexible_array<T, N, false>;

template<typename T, std::size_t N>
using flexible_dynamic_array = basic_flexible_array<T, N, true>;


template<typename T>
class array_of_arrays
{
public:
	array_of_arrays() {}

	template<typename Iter>
	void push_back(Iter beg, Iter end)
	{
		m_idx.push_back(m_vec.size());
		m_vec.insert(std::end(m_vec), beg, end);
	}

	template<std::size_t N>
	void push_back(const T (&a)[N])
	{
		m_idx.push_back(m_vec.size());
		m_vec.insert(std::end(m_vec), std::begin(a), std::end(a));
	}

	T* operator[](std::size_t row) { assert(row < rows()); return &m_vec[m_idx[row]]; }
	T const* operator[](std::size_t row) const { assert(row < rows()); return &m_vec[m_idx[row]]; }

	std::size_t rows() const { return m_idx.size(); }

	std::size_t cols(std::size_t row) const
	{
		assert(row <= m_idx.size());
		auto b = m_idx[row];
		auto e = row + 1 >= m_idx.size() ? m_vec.size() : m_idx[row + 1];
		return std::size_t(e - b);
	}

	std::pair<T*, T*> row(std::size_t n)
	{
		return {(*this)[n], (*this)[n] + cols(n)};
	}

	std::pair<T const*, T const*> row(std::size_t n) const
	{
		return {(*this)[n], (*this)[n] + cols(n)};
	}

private:
	std::vector<T> m_vec;
	std::vector<std::size_t> m_idx;
};

template<typename T, std::size_t N>
class local_vector
{
	using base_type = std::array<T, N>;

public:
	using size_type = typename base_type::size_type;
	using value_type = typename base_type::value_type;
	using pointer = typename base_type::pointer;
	using const_pointer = typename base_type::const_pointer;
	using reference = typename base_type::reference;
	using const_reference = typename base_type::const_reference;

	local_vector()
	{
		std::size_t n = N;
		void* r = m_raw;
		m_array = reinterpret_cast<pointer>(std::align(alignof(value_type), sizeof(value_type), r, n));
	}

	~local_vector() { while(size()) pop_back(); }

	void push_back(T&& t)
	{
		assert(m_top < N);
		new (m_array + m_top) value_type(std::move(t));
		++m_top;
	}

	void push_back(T const& t)
	{
		assert(m_top < N);
		push_back(value_type(t));
	}

	template<typename... Args>
	decltype(auto) emplace_back(Args&&... args)
	{
		assert(m_top < N);
		new (m_array + m_top) value_type(std::forward<Args>(args)...);
		++m_top;
		return back();
	}

	void pop_back()
	{
		assert(m_top > 0);
		--m_top;
		m_array[m_top].~value_type();
	}

	size_type size() const { return m_top; }

	reference back()
	{
		assert(m_top > 0);
		return m_array[m_top - 1];
	}

	const_reference back() const
	{
		assert(m_top > 0);
		return m_array[m_top - 1];
	}

private:
	std::byte m_raw[(N + 1) * sizeof(value_type)];
	pointer m_array;
	size_type m_top = 0;
};

template<typename T, std::size_t N>
using local_stack = std::stack<T, local_vector<T, N>>;

template<typename T, typename Compare = std::less<T>, typename Alloc = std::allocator<T>>
class contiguous_multiset
: private std::vector<T, Alloc>
{
	using base = std::vector<T, Alloc>;

public:
	using size_type = typename base::size_type;
	using value_type = typename base::value_type;
	using reference = typename base::reference;
	using move_reference = typename base::value_type&&;
	using const_reference = typename base::const_reference;
	using pointer = typename base::pointer;
	using const_pointer = typename base::const_pointer;
	using iterator = typename base::iterator;
	using const_iterator = typename base::const_iterator;

	contiguous_multiset() noexcept: base() {}

	template<typename Iter>
	contiguous_multiset(Iter from, Iter to) noexcept: base(from, to)
		{ internal_sort(from, to); }

	contiguous_multiset(std::initializer_list<T> il) noexcept: base()
	{
		reserve(il.size());
		for(auto&& i: il)
			insert(std::move(i));
	}

//	using base::begin;
//	using base::end;
//	using base::cbegin;
//	using base::cend;

	const_iterator  begin() const { return base::begin(); }
	const_iterator    end() const { return base::end(); }
	const_iterator cbegin() const { return base::cbegin(); }
	const_iterator   cend() const { return base::cend(); }

	const_pointer data() const { return base::data(); }
	const_pointer data_end() const { return base::data() + size(); }

	using base::size;
	using base::reserve;

	template<typename... Args>
	auto emplace(Args&&... args)
	{
		value_type t(std::forward<Args>(args)...);
		return base::insert(upper_bound(t), std::move(t));
	}

	auto insert(move_reference t, const_iterator hint)
		{ return base::insert(upper_bound(t), std::move(t), hint); }

	auto insert(move_reference t)
		{ return base::insert(upper_bound(t), std::move(t)); }

	void insert(const_reference t, const_iterator hint)
	{
		auto u = t;
		insert(std::move(u), hint);
	}

	void insert(const_reference t)
	{
		auto u = t;
		insert(std::move(u));
	}

	template<typename Iter>
	auto insert(Iter from, Iter to)
	{
		auto pos = begin();

		while(from != to)
			pos = insert(*from++, pos);

		return pos;
	}

	const_iterator lower_bound(const_reference t) const
		{ return std::lower_bound(begin(), end(), t, Compare()); }

	const_iterator upper_bound(const_reference t) const
		{ return std::upper_bound(begin(), end(), t, Compare()); }

	std::pair<const_iterator, const_iterator> equal_range(const_reference t) const
	{
		auto lb = lower_bound(t);
		return std::make_pair(lb, std::upper_bound(lb, end(), t));
	}

	const_reference operator[](size_type n) const { return (*this)[n]; }

	size_type count(const_reference t) const
	{
		auto er = equal_range(t);
		return size_type(std::distance(er.first, er.second));
	}

	bool contains(const_reference t) const { return *lower_bound(t) == t; }

private:
	void internal_sort(iterator from, iterator to) noexcept
		{ std::sort(from, to, Compare()); }
};

template<typename T>
class three_d_array
{
public:
	using size_type = std::size_t;

	using value_type = T;

	using pointer = value_type*;
	using const_pointer = value_type const*;

	using reference = value_type&;
	using const_reference = value_type const&;

	size_type slices() const
	{
		assert(m_v.size() % (m_cols * m_rows) == 0);
		return m_v.size() / (m_cols * m_rows);
	}

	size_type cols() const { return m_cols; }
	size_type rows() const { return m_rows; }

	T& operator()(size_t slice, size_t row, size_t col)
		{ return m_v[(slice * (cols() * rows())) + (row * cols()) + col]; }

	T const& operator()(size_t slice, size_t row, size_t col) const
		{ return m_v[(slice * (cols() * rows())) + (row * cols()) + col]; }

	auto begin()       { return std::begin(m_v); }
	auto begin() const { return std::begin(m_v); }
	auto cbegin() const { return std::cbegin(m_v); }

	auto end()       { return std::end(m_v); }
	auto end() const { return std::end(m_v); }
	auto cend() const { return std::cend(m_v); }

private:
	std::vector<T> m_v;
	size_type m_cols;
	size_type m_rows;
};

template<typename Container>
auto data_begin(Container& c)
	{ return std::data(c); }

template<typename Container>
auto data_end(Container& c)
	{ return std::data(c) + std::size(c); }

}  // namespace containers
}  // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_EXPERIMENTAL_CONTAINERS_H
