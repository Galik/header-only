#ifndef HEADER_ONLY_LIBRARY_EXPERIMENTAL_CONST_CORRECTED_TYPES_H
#define HEADER_ONLY_LIBRARY_EXPERIMENTAL_CONST_CORRECTED_TYPES_H

#include <array>
#include <list>
#include <set>
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <regex>
#include <filesystem>

#if __cplusplus >= 201703L
#include <cstddef>
#endif

#define cc_auto auto const
#define cc_mutable_auto auto

#define cc_auto_ref auto const&
#define cc_mutable_auto_ref auto&

#define cc_auto_ptr auto const* const
#define cc_mutable_auto_ptr auto* const
#define cc_mutable_auto_mutable_ptr auto*

#define cc_lambda_assign(type, var, code) cc_auto var = [&]{type var; code return var;}()

namespace header_only_library {
namespace const_corrected_types {

using cc_short = short const;
using cc_mutable_short = short;

using cc_ushort = unsigned short const;
using cc_mutable_ushort = unsigned short;

using cc_int = int const;
using cc_mutable_int = int;

using cc_uint = unsigned const;
using cc_mutable_uint = unsigned;

using cc_long = long const;
using cc_mutable_long = long;

using cc_ulong = unsigned long const;
using cc_mutable_ulong = unsigned long;

using cc_longlong = long long const;
using cc_mutable_longlong = long long;

using cc_ulonglong = unsigned long long const;
using cc_mutable_ulonglong = unsigned long long;

using cc_float = float const;
using cc_mutable_float = float;

using cc_double = double const;
using cc_mutable_double = double;

using cc_longdouble = long double const;
using cc_mutable_longdouble = long double;

using cc_int_8 = std::uint8_t const;
using cc_mutable_int_8 = std::uint8_t;

using cc_int_16 = std::uint16_t const;
using cc_mutable_int_16 = std::uint16_t;

using cc_int_32 = std::uint32_t const;
using cc_mutable_int_32 = std::uint32_t;

using cc_int_64 = std::uint64_t const;
using cc_mutable_int_64 = std::uint64_t;

// No point in having signed char types
using cc_char = unsigned char const;
using cc_mutable_char = unsigned char;

using cc_char_ptr = unsigned char const* const;
using cc_mutable_char_ptr = unsigned char* const;
using cc_char_mutable_ptr = unsigned char const*;
using cc_mutable_char_mutable_ptr = unsigned char*;

using cc_wchar = wchar_t const;
using cc_mutable_wchar = wchar_t;

using cc_wchar_ptr = wchar_t const* const;
using cc_mutable_wchar_ptr = wchar_t* const;
using cc_wchar_mutable_ptr = wchar_t const*;
using cc_mutable_wchar_mutable_ptr = wchar_t*;

#if __cplusplus >= 201703L
using cc_byte = std::byte const;
using cc_mutable_byte = std::byte;

using cc_byte_ptr = std::byte const* const;
using cc_mutable_byte_ptr = std::byte* const;
using cc_byte_mutable_ptr = std::byte const*;
using cc_mutable_byte_mutable_ptr = std::byte*;
#endif

// TODO: Add for c++20
//using cc_char8 = char8_t const;
//using cc_mutable_char8 = char8_t;
//
//using cc_char8_ptr = char8_t const* const;
//using cc_mutable_char8_ptr = char8_t* const;
//using cc_char8_mutable_ptr = char8_t const*;
//using cc_mutable_char8_mutable_ptr = char8_t*;

using cc_char16 = char16_t const;
using cc_mutable_char16 = char16_t;

using cc_char16_ptr = char16_t const* const;
using cc_mutable_char16_ptr = char16_t* const;
using cc_char16_mutable_ptr = char16_t const*;
using cc_mutable_char16_mutable_ptr = char16_t*;

using cc_char32 = char32_t const;
using cc_mutable_char32 = char32_t;

using cc_char32_ptr = char32_t const* const;
using cc_mutable_char32_ptr = char32_t* const;
using cc_char32_mutable_ptr = char32_t const*;
using cc_mutable_char32_mutable_ptr = char32_t*;

using cc_size_t = std::size_t const;
using cc_mutable_size_t = std::size_t;

// string

using cc_string = std::string const;
using cc_mutable_string = std::string;

// TODO: Add for c++20
//using cc_u8string = std::u8string const;
//using cc_mutable_u8string = std::u8string;

using cc_wstring = std::wstring const;
using cc_mutable_wstring = std::wstring;

using cc_u16string = std::u16string const;
using cc_mutable_u16string = std::u16string;

using cc_u32string = std::u32string const;
using cc_mutable_u32string = std::u32string;

// string_view

using cc_string_view = std::string_view const;
using cc_mutable_string_view = std::string_view;

// TODO: Add for c++20
//using cc_u8string_view = std::u8string_view const;
//using cc_mutable_u8string_view = std::u8string_view;

using cc_wstring_view = std::wstring_view const;
using cc_mutable_wstring_view = std::wstring_view;

using cc_u16string_view = std::u16string_view const;
using cc_mutable_u16string_view = std::u16string_view;

using cc_u32string_view = std::u32string_view const;
using cc_mutable_u32string_view = std::u32string_view;

// array

template<
	typename T,
	std::size_t N>
using cc_array = std::array<T, N> const;

template<
	typename T,
	std::size_t N>
using cc_mutable_array = std::array<T, N>;

// vector

template<
	typename T,
	typename Alloc = std::allocator<T>>
using cc_vector = std::vector<T, Alloc> const;

template<
	typename T,
	typename Alloc = std::allocator<T>>
using cc_mutable_vector = std::vector<T, Alloc>;

// map

template<
	typename K,
	typename V,
	typename Compare = std::less<K>,
	typename Alloc = std::allocator<std::pair<K const, V>>>
using cc_map = std::map<K, V, Compare, Alloc> const;

template<
	typename K,
	typename V,
	typename Compare = std::less<K>,
	typename Alloc = std::allocator<std::pair<K const, V>>>
using cc_mutable_map = std::map<K, V, Compare, Alloc>;

template<
	typename K,
	typename V,
	typename Compare = std::less<K>,
	typename Alloc = std::allocator<std::pair<K const, V>>>
using cc_multimap = std::multimap<K, V, Compare, Alloc> const;

template<
	typename K,
	typename V,
	typename Compare = std::less<K>,
	typename Alloc = std::allocator<std::pair<K const, V>>>
using cc_mutable_multimap = std::multimap<K, V, Compare, Alloc>;

template<typename K,
	typename V,
	typename Hash,
	typename Pred = std::less<K>,
	typename Alloc = std::allocator<std::pair<K const, V>>>
using cc_unordered_map = std::unordered_map<K, V, Hash, Pred, Alloc> const;

template<
	typename K,
	typename V,
	typename Hash,
	typename Pred = std::less<K>,
	typename Alloc = std::allocator<std::pair<K const, V>>>
using cc_mutable_unordered_map = std::unordered_map<K, V, Hash, Pred, Alloc>;

template<
	typename K,
	typename V,
	typename Hash,
	typename Pred = std::less<K>,
	typename Alloc = std::allocator<std::pair<K const, V>>>
using cc_unordered_multimap = std::unordered_multimap<K, V, Hash, Pred, Alloc> const;

template<
	typename K,
	typename V,
	typename Hash,
	typename Pred = std::less<K>,
	typename Alloc = std::allocator<std::pair<K const, V>>>
using cc_mutable_unordered_multimap = std::unordered_multimap<K, V, Hash, Pred, Alloc>;

// set

template<
	typename T,
	typename Compare = std::less<T>,
	typename Alloc = std::allocator<T>>
using cc_set = std::set<T, Compare, Alloc> const;

template<
	typename T,
	typename Compare = std::less<T>,
	typename Alloc = std::allocator<T>>
using cc_mutable_set = std::set<T, Compare, Alloc>;

template<
	typename T,
	typename Compare = std::less<T>,
	typename Alloc = std::allocator<T>>
using cc_multiset = std::multiset<T, Compare, Alloc> const;

template<
	typename T,
	typename Compare = std::less<T>,
	typename Alloc = std::allocator<T>>
using cc_mutable_multiset = std::multiset<T, Compare, Alloc>;

template<
	typename T,
	typename Hash = std::hash<T>,
	typename Pred = std::equal_to<T>,
	typename Alloc = std::allocator<T>>
using cc_unordered_set = std::unordered_set<T, Hash, Pred, Alloc> const;

template<
	typename T,
	typename Hash = std::hash<T>,
	typename Pred = std::equal_to<T>,
	typename Alloc = std::allocator<T>>
using cc_mutable_unordered_set = std::unordered_set<T, Hash, Pred, Alloc>;

template<
	typename T,
	typename Hash = std::hash<T>,
	typename Pred = std::equal_to<T>,
	typename Alloc = std::allocator<T>>
using cc_unordered_multiset = std::unordered_multiset<T, Hash, Pred, Alloc> const;

template<
	typename T,
	typename Hash = std::hash<T>,
	typename Pred = std::equal_to<T>,
	typename Alloc = std::allocator<T>>
using cc_mutable_unordered_multiset = std::unordered_multiset<T, Hash, Pred, Alloc>;

// list

template<
	typename T,
	typename Alloc = std::allocator<T>>
using cc_list = std::list<T, Alloc> const;

template<
	typename T,
	typename Alloc = std::allocator<T>>
using cc_mutable_list = std::list<T, Alloc>;

// regex

using cc_regex = std::regex const;
using cc_mutable_regex = std::regex;

using cc_wregex = std::wregex const;
using cc_mutable_wregex = std::wregex;

using cc_smatch = std::smatch const;
using cc_mutable_smatch = std::smatch;

using cc_wsmatch = std::wsmatch const;
using cc_mutable_wsmatch = std::wsmatch;

using cc_cmatch = std::cmatch const;
using cc_mutable_cmatch = std::cmatch;

using cc_wcmatch = std::wcmatch const;
using cc_mutable_wcmatch = std::wcmatch;

using cc_sregex_iterator = std::sregex_iterator const;
using cc_mutable_sregex_iterator = std::sregex_iterator;

using cc_wsregex_iterator = std::wsregex_iterator const;
using cc_mutable_wsregex_iterator = std::wsregex_iterator;

using cc_cregex_iterator = std::cregex_iterator const;
using cc_mutable_cregex_iterator = std::cregex_iterator;

using cc_wcregex_iterator = std::wcregex_iterator const;
using cc_mutable_wcregex_iterator = std::wcregex_iterator;

// filesystem

using cc_mutable_path = std::filesystem::path;
using cc_path = cc_mutable_path const;

}  // namespace const_corrected_types
}  // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_EXPERIMENTAL_CONST_CORRECTED_TYPES_H
