#ifndef HEADER_ONLY_LIBRARY_UTILITY_TRIMMERS_H
#define HEADER_ONLY_LIBRARY_UTILITY_TRIMMERS_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "utility_common.h"

namespace hol {


/**
 * Generic class for trimming characters from the left and
 * right hand sides of a string. This class works differently
 * with different Mode parameters.
 * <pre>
 * Mode = mute (mutating)
 *     Accepts a std::string& parameter and modifies the
 *     passed in std::string, returning a reference to the
 *     same string.
 *
 * Mode = copy (copying)
 *     Accepts a string_view parameter and returns a std::string
 *     that is a copy of the passed in string_view, with the
 *     configured characters removed.
 */
#if defined(HOL_USE_STRING_VIEW)
 /**
  * Mode = view (viewing)
  *     Accepts a string_view parameter and returns a string_view
  *     that is a copy of the passed in string_view, with the
  *     configured characters removed.
  */
#endif
#if defined(HOL_USE_GSL_SPAN)
 /**
  * Mode = span (spanning)
  *     Accepts a gsl::string_span parameter and returns a string_span
  *     that is a copy of the passed in string_span, with the
  *     configured characters removed.
  */
#endif
/**
 *
 * </pre>
 */
template<typename Mode>
class trimmer
{
	static_assert
	(
		   std::is_same<Mode, mute>::value
		|| std::is_same<Mode, copy>::value
#if defined(HOL_USE_STRING_VIEW)
		|| std::is_same<Mode, view>::value
#endif
#if defined(HOL_USE_GSL_SPAN)
		|| std::is_same<Mode, span>::value
#endif
		, "Mode template parameter must be mute, copy, view or span"
	);

public:
	using mode = Mode;
	using string_type = typename mode::string_type;
	using return_type = typename mode::return_type;

private:
	static constexpr const char* const ws = " \t\n\r\f\v";
	string shed_l = ws;
	string shed_r = ws;

	return_type left(string_type s, mute) const
	{
		s.erase(0, s.find_first_not_of(shed_l.c_str()));
		return s;
	}

	return_type left(string_type s, view) const
	{
		if(auto pos = s.find_first_not_of(shed_l.c_str()) + 1)
		{
			s.remove_prefix(pos - 1);
			return {s.data(), s.size()};
		}
		return {};
	}

	return_type left(string_type s, copy) const
	{
		return left(s, view());
	}

	return_type right(string_type s, mute) const
	{
		s.erase(s.find_last_not_of(shed_r.c_str()) + 1);
		return s;
	}

	return_type right(string_type s, view) const
	{
		if(auto pos = s.find_last_not_of(shed_r.c_str()) + 1)
			s.remove_suffix(s.size() - pos);
		return {s.data(), s.size()};
	}

	return_type right(string_type s, copy) const
	{
		return right(s, view());
	}

public:

	/**
	 * Configure which characters will be removed from both the
	 * left and the right of the string.
	 * @param characters The characters to be removed.
	 */
	void remove(string const& characters) { shed_l = shed_r = characters; }

	/**
	 * Configure which characters will be removed from the
	 * left the string.
	 * @param characters The characters to be removed.
	 */
	void remove_from_left(string const& characters) { shed_l = characters; }

	/**
	 * Configure which characters will be removed from the
	 * right the string.
	 * @param characters The characters to be removed.
	 */
	void remove_from_right(string const& characters) { shed_r = characters; }

	/**
	 * Remove characters from the left of the string.
	 * @param s The string to remove characters from.
	 * @return The string with trimmed characters removed.
	 */
	return_type left(string_type s) const { return left(s, Mode()); }

	/**
	 * Remove characters from the right of the string.
	 * @param s The string to remove characters from.
	 * @return The string with trimmed characters removed.
	 */
	return_type right(string_type s) const { return right(s, Mode()); }

	/**
	 * Remove characters from the left and the right of the string.
	 * @param s The string to remove characters from.
	 * @return The string with trimmed characters removed.
	 */
	return_type round(string_type s) const { return left(right(s, Mode()), Mode()); }

	return_type operator()(string_type s) const { return round(s); }
};

using trim_mutator = trimmer<mute>;
using trim_copier = trimmer<copy>;
#if defined(HOL_USE_STRING_VIEW)
using trim_viewer = trimmer<view>;
#endif
#if defined(HOL_USE_GSL_SPAN)
using trim_spanner = trimmer<span>;
#endif

inline
trim_mutator& get_trim_mutator()
{
	thread_local static trim_mutator t;
	return t;
}

inline
trim_mutator& get_trim_mutator(const char* to_trim)
{
	auto& t = get_trim_mutator();
	t.remove(to_trim);
	return t;
}

inline
trim_mutator& get_trim_mutator(const char* to_trim_left, const char* to_trim_right)
{
	auto& t = get_trim_mutator();
	t.remove_from_left(to_trim_left);
	t.remove_from_right(to_trim_right);
	return t;
}

inline
trim_copier& get_trim_copier()
{
	thread_local static trim_copier t;
	return t;
}

inline
trim_copier& get_trim_copier(const char* to_trim)
{
	auto& t = get_trim_copier();
	t.remove(to_trim);
	return t;
}

inline
trim_copier& get_trim_copier(const char* to_trim_left, const char* to_trim_right)
{
	auto& t = get_trim_copier();
	t.remove_from_left(to_trim_left);
	t.remove_from_right(to_trim_right);
	return t;
}

#if defined(HOL_USE_STRING_VIEW)
inline
trim_viewer& get_trim_viewer()
{
	thread_local static trim_viewer t;
	return t;
}

inline
trim_viewer& get_trim_viewer(const char* to_trim)
{
	auto& t = get_trim_viewer();
	t.remove(to_trim);
	return t;
}

inline
trim_viewer& get_trim_viewer(const char* to_trim_left, const char* to_trim_right)
{
	auto& t = get_trim_viewer();
	t.remove_from_left(to_trim_left);
	t.remove_from_right(to_trim_right);
	return t;
}
#endif

#if defined(HOL_USE_GSL_SPAN)
inline
trim_spanner& get_trim_spanner()
{
	thread_local static trim_spanner t;
	return t;
}

inline
trim_spanner& get_trim_spanner(const char* to_trim)
{
	auto& t = get_trim_spanner();
	t.remove(to_trim);
	return t;
}

inline
trim_spanner& get_trim_spanner(const char* to_trim_left, const char* to_trim_right)
{
	auto& t = get_trim_spanner();
	t.remove_from_left(to_trim_left);
	t.remove_from_right(to_trim_right);
	return t;
}
#endif

} // hol

#endif // HEADER_ONLY_LIBRARY_UTILITY_TRIMMERS_H
