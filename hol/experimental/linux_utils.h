#ifndef HEADER_ONLY_LIBRARY_LINUX_UTILS_H
#define HEADER_ONLY_LIBRARY_LINUX_UTILS_H

#include <memory>
#include <cassert>
#include <stdexcept>
#include <string>
#include <vector>

#include <sys/types.h>
#include <ifaddrs.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include <unistd.h>

#ifdef __GNUC__
#include <ext/stdio_filebuf.h>
#endif

#include "hol/memory_utils.h"
#include "hol/macro_exceptions.h"

namespace header_only_library {
namespace linux_utils {

struct fd_closer {void operator()(int fd) const { if(fd != -1) ::close(fd); }};
using fd_handle = memory_utils::unique_handle<int, fd_closer>;

using byte = unsigned char;

inline
std::string cwd()
{
	if(memory_utils::calloc_uptr dir{::getcwd(0, 0)})
		return dir.get();
	return {};
}

/**
 * Sets a new working directory for the current scope.
 */
struct cwd_scoper
{
	std::string old_dir;
	cwd_scoper(std::string const& new_dir = "."): old_dir(cwd()) { ::chdir(new_dir.c_str()); }
	~cwd_scoper() { chdir(old_dir.c_str()); }
};

inline
std::string unique_filename_in(std::string dir)
{
	if(dir.empty() || dir.back() != '/')
		dir += '/';

	dir += std::string("XXXXXX\0", 7);

	auto fd = fd_handle(::mkstemp(&dir[0]));
	if(fd.get() == -1)
		throw std::runtime_error("Error creating " + dir + ": " + std::strerror(errno));
//	::close(fd);

	return dir;
}

// networks

struct ifaddrs_dter{ void operator()(ifaddrs* p) const { if(p) freeifaddrs(p); }};
using  ifaddrs_uptr = std::unique_ptr<ifaddrs, ifaddrs_dter>;

struct fd_dter{ void operator()(int* fd) const { if(fd && *fd != -1) close(*fd); delete fd; }};
using  fd_uptr = std::unique_ptr<int, fd_dter>;

inline
fd_uptr make_fd_uptr(int fd)
{
	if(fd == -1)
		return {};
	return fd_uptr(new int(fd));
}

inline
ifaddrs_uptr create_ifassrs_uptr()
{
	ifaddrs* p;

	if(getifaddrs(&p))
		return nullptr;

	return ifaddrs_uptr{p};
}

using raw_mac = std::array<byte, 6>;

inline
std::map<std::string, std::string> enumerate_macs()
{
	ifaddrs_uptr ifs = create_ifassrs_uptr();

	if(!ifs)
		throw std::runtime_error(std::strerror(errno));

	std::map<std::string, std::string> m;

	for(auto p = ifs.get(); p; p = p->ifa_next)
	{
		// get MAC
		raw_mac mac = [&]
		{
			ifreq buffer{};
			fd_uptr s = make_fd_uptr(socket(PF_INET, SOCK_DGRAM, 0));

			std::strcpy(buffer.ifr_name, p->ifa_name);
			ioctl(*s, SIOCGIFHWADDR, &buffer);
			raw_mac mac;
			std::copy(buffer.ifr_hwaddr.sa_data, buffer.ifr_hwaddr.sa_data + 6, (char*)mac.data());
			return mac;
		}();

		char const* sep = "";
		std::ostringstream oss;
		oss << std::hex << std::uppercase << std::setfill('0');
		for(auto m: mac)
		{
			oss << sep << std::setw(2) << int(m);
			sep = ":";
		}

		m[p->ifa_name] = oss.str();
	}

	return m;
}

#ifdef __GNUC__

class pipe_stream
: public std::istream
, public std::ostream
{
public:
	pipe_stream(int fd0, int fd1)
	: std::istream(new __gnu_cxx::stdio_filebuf<char>(fd0, std::ios::in))
	, std::ostream(new __gnu_cxx::stdio_filebuf<char>(fd1, std::ios::out)) {}

//	~pipe_stream() { delete std::istream::rdbuf(); delete std::ostream::rdbuf(); }

	pipe_stream(pipe_stream&& pio)
	: std::istream(std::move(static_cast<std::istream&>(pio)))
	, std::ostream(std::move(static_cast<std::ostream&>(pio))) {}
};

// hopelessly broken...

//pipe_stream open_pipe_stream(std::string cmd)
//{
//	int i_pipe[2]; // parent in
//	int o_pipe[2]; // parent out
//
//	::pipe(i_pipe);
//	::pipe(o_pipe);
//
//	if(auto pid = ::fork())
//	{
//		if(pid == -1) hol_throw_errno();
//	}
//	else
//	{
//		// child
//
//		::close(i_pipe[STDIN_FILENO]);
//		::close(o_pipe[STDOUT_FILENO]);
//
////		::close(STDIN_FILENO);
//		dup2(o_pipe[STDIN_FILENO], STDIN_FILENO);
//
////		::close(STDOUT_FILENO);
//		dup2(i_pipe[STDOUT_FILENO], STDOUT_FILENO);
//
//		std::vector<char*> args;
//
//		if(auto p = std::strtok(&cmd[0], " "))
//			args.insert(std::end(args), 2, p);
//
//		while(auto p = std::strtok(nullptr, " "))
//			args.push_back(p);
//
//		args.push_back(nullptr);
//
//		::execvp(args[0], args.data() + 1);
//	}
//
//	::close(i_pipe[STDIN_FILENO]);
//	::close(o_pipe[STDOUT_FILENO]);
//
//	return pipe_stream(i_pipe[STDOUT_FILENO], o_pipe[STDIN_FILENO]);
//}

inline pipe_stream open_pipe_stream(std::string cmd)
{
	constexpr int const R = 0;
	constexpr int const W = 1;

	int parent_fds[2]; // 0 - in, 1 - out

	::pipe(parent_fds);

	if(auto pid = ::fork())
	{
		if(pid == -1) hol_throw_errno();
	}
	else
	{
		// child

		dup2(parent_fds[W], STDIN_FILENO);
		dup2(parent_fds[R], STDOUT_FILENO);

		std::vector<char*> args;

		if(auto p = std::strtok(&cmd[0], " "))
			args.insert(std::end(args), 2, p);

		while(auto p = std::strtok(nullptr, " "))
			args.push_back(p);

		args.push_back(nullptr);

		::execvp(args[0], args.data() + 1);
	}

	return pipe_stream(parent_fds[R], parent_fds[W]);
}

#endif // __GNUC__

} // namespace linux_utils
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_LINUX_UTILS_H
