#ifndef HEADER_ONLY_LIBRARY_STRING_VIEW_STREAM_H
#define HEADER_ONLY_LIBRARY_STRING_VIEW_STREAM_H

//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <istream>
#include <streambuf>
#include <experimental/string_view>

#include "../bug.h"

namespace header_only_library {
namespace string_view_utils {

template<typename CharT>
using basic_string_view = std::experimental::basic_string_view<CharT>;

using string_view = basic_string_view<char>;
using wstring_view = basic_string_view<wchar_t>;

template<typename CharT>
class basic_string_view_stream;

template<typename CharT>
class basic_string_view_buf
: public std::basic_streambuf<CharT>
{
	friend class basic_string_view_stream<CharT>;

	using char_type = CharT;
	using buf_type = std::basic_streambuf<char_type>;
	using traits_type = typename buf_type::traits_type;
	using string_view_type = basic_string_view<char_type>;
	using pos_type = typename buf_type::pos_type;
	using off_type = typename buf_type::off_type;

	using buf_type::eback;
	using buf_type::gptr;
	using buf_type::egptr;
	using buf_type::gbump;

	std::locale loc;
	string_view_type sv;

public:
	basic_string_view_buf(string_view_type sv): sv(sv)
	{
		buf_type::setg(const_cast<char_type*>(sv.data()),
			const_cast<char_type*>(sv.data()),
				const_cast<char_type*>(sv.data()) + sv.size());
	}

	string_view_type string_view() const { return sv; }

	void imbue(const std::locale& loc) override { this->loc = loc; }

	buf_type* setbuf(char_type* b, std::streamsize l) override
	{
		sv = string_view_type(b, l);
		buf_type::setg(const_cast<char_type*>(sv.data()),
			const_cast<char_type*>(sv.data()),
				const_cast<char_type*>(sv.data()) + sv.size());
		return this;
	}

	pos_type seekoff(off_type off, std::ios::seekdir dir, std::ios::openmode which) override
	{
		if(which == std::ios::in)
		{
			if(dir == std::ios::cur)
				buf_type::gbump(off);
			else if(dir == std::ios::beg)
				buf_type::setg(buf_type::eback(),
					buf_type::eback() + off,
						buf_type::egptr());
			else if(dir == std::ios::end)
				buf_type::setg(buf_type::eback(),
					buf_type::egptr() - off,
						buf_type::egptr());
		}

		return buf_type::gptr() - buf_type::eback();
	}

	pos_type seekpos(pos_type pos, std::ios::openmode which) override
	{
		return seekoff(pos, std::ios::beg, which);
	}

	std::streamsize showmanyc() override
	{
		if(buf_type::gptr() == buf_type::egptr())
			return -1;
		return buf_type::egptr() - buf_type::gptr();
	}

	std::streamsize xsgetn(char_type* s, std::streamsize n) override
	{
		char_type* e = buf_type::gptr() + n;
		if(e > buf_type::egptr())
		{
			e = buf_type::egptr();
			n = e - buf_type::gptr();
		}
		std::copy(s, s + n, buf_type::gptr());
		buf_type::gbump(n);
		return n;
	}

	string_view_type tell() const { return sv.substr(0, 0); }

	string_view_type _getline(char_type delim)
	{
		auto pos = std::find(gptr(), egptr(), delim);
		if(pos < egptr())
			++pos; // skip delim
		string_view_type line(gptr(), pos - gptr());
		gbump(egptr() - pos);
		return line;
	}
};

template<typename CharT>
basic_string_view_stream<CharT>& getline(basic_string_view_stream<CharT>& svs, string_view& sv, char delim);

template<typename CharT>
class basic_string_view_stream
: public std::basic_istream<CharT>
{
	template<typename C>
	friend
	basic_string_view_stream<C>& getline(basic_string_view_stream<C>& svs, basic_string_view<C>& sv, char delim);

	using char_type = CharT;
	using buf_type = std::basic_streambuf<char_type>;
	using traits_type = typename buf_type::traits_type;
	using string_view_type = basic_string_view<char_type>;

	basic_string_view_buf<char_type> buf;

public:

	using std::basic_istream<CharT>::get;

	basic_string_view_stream(string_view_type sv): std::basic_istream<CharT>(), buf(sv)
	{
		std::basic_istream<CharT>::rdbuf(&buf);
	}

	string_view_type string_view() const { return buf.string_view(); }

	string_view_type tell() const { return buf.tell(); }

	basic_string_view_stream& get(string_view_type& sv)
	{
		CharT c;
		if(get(c))
			sv = string_view_type(sv.data(), sv.size() + 1);
		return *this;
	}

//	template<typename C>
//	friend
//	basic_string_view_stream<C>& operator>>(basic_string_view_stream<C>& svs, basic_string_view<C>& sv)
//	{
//		auto pos = svs.tellg();
//		if(pos == svs.string_view().size())
//		{
//			svs.setstate(std::ios::eofbit);
//			return svs;
//		}
//
//		while(svs.tellg() != svs.string_view().size())
//			svs.ignore();
//
//		sv = svs.string_view().substr(pos, svs.tellg() - pos -1);
//
//		return svs;
//	}
//
//	template<typename C>
//	friend
//	basic_string_view_stream<C>& getline(basic_string_view_stream<C>& svs, basic_string_view<C>& sv, C delim = '\n')
//	{
//		sv = svs._getline(delim);
//		return svs;
//	}
//

	template<typename C>
	friend
	basic_string_view_stream<C>& operator>>(basic_string_view_stream<C>& svs, basic_string_view<C>& sv);

	template<typename C>
	friend
	basic_string_view_stream<C>& getline(basic_string_view_stream<C>& svs, basic_string_view<C>& sv, C delim);


private:
	string_view_type _getline(char_type delim) { return buf._getline(delim); }
};

using string_view_buf = basic_string_view_buf<char>;
using wstring_view_buf = basic_string_view_buf<wchar_t>;
using string_view_stream = basic_string_view_stream<char>;
using wstring_view_stream = basic_string_view_stream<wchar_t>;

template<typename C> inline
header_only_library::string_view_utils::basic_string_view_stream<C>&
	getline(header_only_library::string_view_utils::basic_string_view_stream<C>& svs,
		header_only_library::string_view_utils::basic_string_view<C>& sv, C delim = C('\n'))
{
	sv = svs._getline(delim);
	return svs;
}

} // namespace string_view_utils
} // namespace header_only_library

template<typename C> inline
header_only_library::string_view_utils::basic_string_view_stream<C>&
	operator>>(header_only_library::string_view_utils::basic_string_view_stream<C>& svs,
		header_only_library::string_view_utils::basic_string_view<C>& sv)
{
	auto pos = svs.tellg();
	if(pos == svs.string_view().size())
	{
		svs.setstate(std::ios::eofbit);
		return svs;
	}

	while(svs.tellg() != svs.string_view().size())
		svs.ignore();

	sv = svs.string_view().substr(pos, svs.tellg() - pos -1);

	return svs;
}

#endif // HEADER_ONLY_LIBRARY_STRING_VIEW_STREAM_H
