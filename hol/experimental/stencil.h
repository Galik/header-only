#ifndef HEADER_ONLY_LIBRARY_STENCILS_H
#define HEADER_ONLY_LIBRARY_STENCILS_H
/*
 *  Created on: 28 Jul 2014
 *      Author: SooKee <oasookee@gmail.com>
 */

//#include <sookee/ios.h>
//#include <sookee/types/basic.h>
//#include <sookee/types/stream.h>
//#include <sookee/types/siz_vec.h>
//#include <sookee/types/str_vec.h>
//#include <sookee/str.h>
//#include <sookee/radp.h>
//#include <sookee/bug.h>
//#include <sookee/log.h>

#include <map>
#include <string>
#include <vector>

namespace hol {

//using namespace sookee::bug;
//using namespace sookee::log;
//using namespace sookee::ios;
//using namespace sookee::types;
//using namespace sookee::utils;

using std::vector<std::string> = std::vector<std::string>;
using siz_vec = std::vector<std::size_t>;
using str_map = std::map<std::string, std::string>;

/**
 * Stencils are string templates with replaceable sections.
 *
 *
 */
class arg_stencil
{
private:
	size_t size = 0;
	std::vector<std::string> pieces;
	siz_vec params;

	std::vector<std::string> args; // prepared args

	const std::string d1 = "${";
	const std::string d2 = "}";

	std::string get_val(const str_map& m, const std::string& key) const
	{
		if(m.count(key))
			return m.at(key);
		return "";
	}

	std::string get_arg(size_t) const
	{
		return "";
	}

	std::string get_arg(size_t idx, const char* arg) const
	{
		if(!idx)
			return arg;
		throw std::runtime_error("E: idx out of range");
		return {};
	}

	std::string get_arg(size_t idx, const std::string& arg) const
	{
		if(!idx)
			return arg;
		throw std::runtime_error("E: idx out of range");
		return "";
	}

	template<typename Arg>
	std::string get_arg(size_t idx, Arg arg) const
	{
		if(!idx)
			return std::to_string(arg);
		throw std::runtime_error("E: idx out of range");
		return "";
	}

	template<typename... Args>
	std::string get_arg(size_t idx, const char* arg, Args... args) const
	{
		if(!idx)
			return arg;
		else
			return get_arg(idx - 1, args...);
	}

	template<typename... Args>
	std::string get_arg(size_t idx, const std::string& arg, Args... args) const
	{
		if(!idx)
			return arg;
		else
			return get_arg(idx - 1, args...);
	}

	template<typename Arg, typename... Args>
	std::string get_arg(size_t idx, Arg arg, Args... args) const
	{
		if(!idx)
			return std::to_string(arg);
		else
			return get_arg(idx - 1, args...);
	}

public:
	/**
	 * Create an empty stencil
	 */
	arg_stencil() {}
	arg_stencil(arg_stencil&& s): size(s.size), pieces(s.pieces), params(s.params)
	, args(s.args), d1(s.d1), d2(s.d2) {}
	arg_stencil(const arg_stencil& s): size(s.size), pieces(s.pieces), params(s.params)
	, args(s.args), d1(s.d1), d2(s.d2) {}

	/**
	 * Create a stencil whose parameter markers are delimited
	 * as beginning with @param d1 and ending with @param d2.
	 *
	 * e.g. ${n} (where n is the parameter number
	 *
	 * d1 = "${", d2 = "}"
	 *
	 * @param d1 the start delimiter of embedded parameters
	 * @param d2 the end delimiter of embedded parameters
	 */
	arg_stencil(const std::string& d1, const std::string& d2): d1(d1), d2(d2) {}

	arg_stencil(const std::string& text, const std::string& d1 = "${", const std::string& d2 = "}"): d1(d1), d2(d2) { compile(text); }

	/**
	 * Clear the stencil of all data,
	 */
	void clear()
	{
		size = 0;
		pieces.clear();
		params.clear();
		args.clear();
	}

	/**
	 * Add a constant argument for this stencil
	 *
	 * These arguments are matched first (in order they are added)
	 * before arguments supplied to the create() function.
	 *
	 * @param arg The argument to add to the list of constant args.
	 */
	void add(const std::string& arg)
	{
		args.push_back(arg);
	}

	template<typename Arg>
	void add(const Arg& arg)
	{
		add(std::to_string(arg));
	}

	void dump() const
	{
//		for(const auto& v: params)
//			con("param: " << v);
//		for(const auto& p: pieces)
//			con("piece: " << p);
//		for(const auto& a: args)
//			con("args : " << a);
	}

	/**
	 * Create a string from this template mathing first the internal
	 * args added using the add() function and then the args supplied
	 * as parameters to this function.
	 *
	 * @param args
	 * @return
	 */
	template<typename... Args>
	std::string create(Args... args) const
	{
		std::string s;
		// TODO: figure best way to guess capacity
		//s.reserve(size + params.size() * 64);
//		siz c = s.capacity();

		auto v = params.cbegin();
		auto vend = params.cend();
		auto p = pieces.cbegin();
		auto pend = pieces.cend();

		size_t arg;
		while(v != vend && p != pend)
		{
			if(p != pend)
				s.append(*p++);

			if(v != vend)
			{
				arg = (*v++) - 1;
//				bug_var(arg);
//				bug_var(this->args.size());
				if(arg < this->args.size())
					s.append(this->args[arg]);
				else
					s.append(get_arg(arg - this->args.size(), args...));
			}
		}

		while(p != pend)
			s.append(*p++);

		while(v != vend)
		{
			arg = (*v++) - 1;
			if(arg < this->args.size())
				s.append(this->args[arg]);
			else
				s.append(get_arg(arg - this->args.size(), args...));
		}

//		if(s.capacity() > c)
//			log("WARN: capacity (" << c << ") exceeded by " << (s.capacity() - c));
		return s;
	}

	/**
	 * Prepare the stencil's data.
	 * @param tmp
	 * @return
	 */
	bool compile(const std::string& tmp)
	{
		clear();

		size = tmp.size();

		size_t cur, end;
		size_t pos = cur = 0;

		while(pos != std::string::npos)
		{
			if((pos = tmp.find(d1, cur)) == std::string::npos)
			{
				pieces.emplace_back(tmp.substr(cur));
				break;
			}

			if((end = tmp.find(d2, pos)) == std::string::npos)
			{
				throw std::runtime_error("E: expected end of variable: " + std::to_string(pos));
				return false;
			}

			std::string param_spec = tmp.substr(pos + d1.size(), end - (pos  + d1.size()));
			size_t param = std::atoi(param_spec.c_str());

//			if(sookee::radp::psz(param_spec.c_str(), param) == param_spec.c_str())
//			{
//				throw std::runtime_error("E: parsing <?soo ?> parameter value: " + std::to_string(pos));
//				return false;
//			}

			pieces.emplace_back(tmp.substr(cur, pos - cur));
			params.push_back(param);
			cur = end + d2.size();
			pos = cur;
		}

		return true;
	}
};

class map_stencil
{
	// order: piece, var, piece, var, piece
	std::vector<std::string> pieces;
	std::vector<std::string> vars;

	const std::string d1 = "${";
	const std::string d2 = "}";

public:
	using dict = str_map;

	/**
	 * Create an empty stencil
	 */
	map_stencil() {}
	map_stencil(map_stencil&& s): pieces(s.pieces), vars(s.vars)
	, d1(s.d1), d2(s.d2) {}
	map_stencil(const map_stencil& s): pieces(s.pieces), vars(s.vars)
	, d1(s.d1), d2(s.d2) {}

	/**
	 * Create a stencil whose parameter markers are delimited
	 * as beginning with @param d1 and ending with @param d2.
	 *
	 * e.g. ${n} (where n is the parameter number
	 *
	 * d1 = "${", d2 = "}"
	 *
	 * @param d1 the start delimiter of embedded parameters
	 * @param d2 the end delimiter of embedded parameters
	 */
	map_stencil(const std::string& d1, const std::string& d2): d1(d1), d2(d2) {}

	map_stencil(const std::string& text, const std::string& d1 = "${", const std::string& d2 = "}")
	: d1(d1), d2(d2) { compile(text); }

	// ${var}
	void clear()
	{
		pieces.clear();
		vars.clear();
	}

	map_stencil& operator=(const std::string& text)
	{
		compile(text);
		return *this;
	}

	bool compile_file(const std::string& filename)
	{
		std::stringstream sss;
		std::ifstream ifs(filename);
		sss << ifs.rdbuf();
		return compile(sss.str());
//		return compile((sss() << sifs(filename).rdbuf()).str());
	}

	bool compile(const std::string& text)
	{
		clear();
		auto beg = text.begin();
		auto pos = beg;
		auto fin = text.end();
		auto end = pos;

		while(pos != fin)
		{
			end = std::search(pos, fin, d1.begin(), d1.end());

//			if(end != beg)
				pieces.emplace_back(pos, end);

			if(end == fin)
				break;

			pos = std::next(end, d1.size());

			if((end = std::search(pos, fin, d2.begin(), d2.end())) == fin)
			{
				throw std::runtime_error("E: expected " + d2);
				return false;
			}

			vars.emplace_back(pos, end);

			pos = std::next(end, d2.size());

//			if(pos == fin)
//				pieces.emplace_back("");
		}

		return true;
	}

//	void dump() const
//	{
//		auto p = pieces.begin();
//		auto v = vars.begin();
//
//		while(p != pieces.end())
//		{
//			con("p: " << *p++);
//			if(v != vars.end())
//				con("v: " << *v++);
//		}
//
//		while(v != vars.end())
//		{
//			con("v: " << *v++);
//			if(p != pieces.end())
//				con("p: " << *p++);
//		}
//	}

	std::string create(const dict& d) const
	{
		std::string s;
		auto p = pieces.begin();
		auto v = vars.begin();

		while(p != pieces.end())
		{
			s.append(*p++);
			if(v != vars.end())
			{
				if(!d.count(*v))
					throw std::runtime_error("E: not in dict" + *v);
				else
					s.append(d.at(*v));
				++v;
			}
		}

		while(v != vars.end())
		{
			if(!d.count(*v))
				throw std::runtime_error("E: not in dict" + *v);
			else
				s.append(d.at(*v));
			++v;
			if(p != pieces.end())
				s.append(*p++);
		}

		return s;
	}
};

} // hol

#endif // HEADER_ONLY_LIBRARY_STENCILS_H
