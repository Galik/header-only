/*
 * mail.h
 *
 *  Created on: Jan 16, 2013
 *      Author: oasookee@googlemail.com
 */

#ifndef HEADER_ONLY_LIBRARY_EMAIL_H
#define HEADER_ONLY_LIBRARY_EMAIL_H

//
// Copyright (c) 2017 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <sookee/str.h>
#include <sookee/ios.h>
#include <sookee/types.h>
#include <sookee/log.h>
#include <sookee/socketstream.h>

namespace hol {
namespace email {

using namespace sookee;
using namespace sookee::types;
using namespace sookee::utils;
using namespace sookee::ios;

class SMTP
{
	const std::string host;
	const in_port_t port;

	bool tx(std::ostream& os, const str& data = "")
	{
		return (bool)(os << data << "\r\n" << std::flush);
	}

	bool rx(std::istream& is, const str& code)
	{
		std::string reply;
		if(!sgl(is, reply))
		{
			log(strerror(errno));
			return false;
		}
		trim(reply);
		bug_var(reply);
		if(reply.find(code))
			return false;
		return true;
	}

public:
	// protocol
	std::string mailfrom;
	std::string rcptto;

	// headers
	std::string from;
	std::string to;
	std::string replyto;

	SMTP(const str& host = "localhost", in_port_t port = 25): host(host), port(port) {}

	bool sendmail(const str& subject, const str& data)
	{
//		net::socketstream ss;
		net::netstream ss;

		std::string reply;

		std::time_t t = std::time(0);

		tm* timeinfo = localtime(&t);
		char buffer[80];

		std::size_t size = strftime(buffer, 80, "%a, %d %b %Y %T %z", timeinfo);

		std::string time(buffer, size);
//		trim(time);

		ss.open(host, std::to_string(port));

		if(!rx(ss, "220"))
			return false;

//		tx(ss, "HELO sookee.dyndns.org");
		tx(ss, "HELO " + host);
		if(!rx(ss, "250"))
			return false;

		// one or more
		{
			tx(ss, "MAIL FROM: " + mailfrom);
			if(!rx(ss, "250"))
				return false;

			// one or more
			{
				tx(ss, "RCPT TO: " + rcptto);
				if(!rx(ss, "250"))
					return false;
			}

			tx(ss, "DATA");
			if(!rx(ss, "354"))
				return false;
			tx(ss, "From: " + mailfrom);
			tx(ss, "To: " + rcptto);
			tx(ss, "Date: " + time);
			tx(ss, "Subject: " + subject);
			if(!replyto.empty())
				tx(ss, "Reply-To: " + replyto);
			tx(ss);
			tx(ss, data);
			tx(ss, ".");
			if(!rx(ss, "250"))
				return false;
		}

		tx(ss, "QUIT");
		if(!rx(ss, "221"))
			return false;

		ss.close();

		return true;
	}
};

//int main()
//{
//	SMTP smtp("localhost", 25);
//
//	smtp.mailfrom = "<noreply@sookee.dyndns.org>";
//	smtp.rcptto = "<oasookee@gmail.com>";
//
//	smtp.from = "No Reply " + smtp.mailfrom;
//	smtp.to = "SooKee " + smtp.rcptto;
//	smtp.replyto = "Skivvy <oasookee@gmail.com>";
//
//	smtp.sendmail("New World Order.", "Some thing great.");
//}


} // namespace email
} // namespace hol

#endif // HEADER_ONLY_LIBRARY_EMAIL_H
