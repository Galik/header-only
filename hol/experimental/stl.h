#ifndef HEADER_ONLY_LIBRARY_STL_H
#define HEADER_ONLY_LIBRARY_STL_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <map>
#include <set>
#include <vector>
#include <algorithm>

#if __cplusplus <= 201402L
namespace std {
template<typename T, std::size_t N>
std::size_t size(T(&)[N])
{
	return N;
}

template<typename C>
std::size_t size(C const& c)
{
	return c.size();
}
} // std
#endif

namespace header_only_library { namespace stl {

template<typename Iterator>
class range
{
public:
	using iterator = Iterator;

private:
	iterator b;
	iterator e;

public:
	range(iterator b, iterator e): b(b), e(e) {}
	range(iterator b): b(b), e(iterator()) {} // sentinel

	iterator begin() const { return b; }
	iterator end() const { return e; }
};

template<typename Iterator>
range<Iterator> make_range(Iterator b, Iterator e = Iterator())
{
	return {b, e};
}

template<typename Container>
auto make_range(Container&& c)
->range<decltype(std::begin(std::forward<Container>(c)))>
{
	return {std::begin(std::forward<Container>(c)), std::end(std::forward<Container>(c))};
}

template<typename T, std::size_t N>
range<T*> make_range(T(&arr)[N])
{
	return {std::begin(arr), std::end(arr)};
}

template<typename Container>
void sort_all(Container& c)
{
	std::sort(std::begin(c), std::end(c));
}

template<typename Container, typename Comparator>
void sort_all(Container& c, Comparator cmp)
{
	std::sort(std::begin(c), std::end(c), cmp);
}

// ordered vector

template<typename Iter, typename Comp>
struct lower_bound_checker
{
	auto operator()(Iter b, Iter e, typename Iter::value_type const& v, Comp comp) const
	{
		return std::lower_bound(b, e, v, comp);
	}
};

template<typename Iter, typename Comp>
struct upper_bound_checker
{
	auto operator()(Iter b, Iter e, typename Iter::value_type const& v, Comp comp) const
	{
		return std::upper_bound(b, e, v, comp);
	}
};

template<typename T, typename Comp, bool Unique>
class ordered_vector
: private std::vector<T>
{
	using vector_type = std::vector<T>;
	using iter_type = typename vector_type::iterator;

	using bound_checker = typename std::conditional
	<
		Unique
		, lower_bound_checker<iter_type, Comp>
		, upper_bound_checker<iter_type, Comp>
	>::type;

public:
	using size_type = typename vector_type::size_type;

private:
	using std::vector<T>::insert;

	void sorted_insert(T const& t)
	{
		auto b = bound_checker()(vector_type::begin(), vector_type::end(), t, Comp());

		if(Unique && b != end() && *b == t)
			return;

		vector_type::insert(b, t);
	}

	void sanitize()
	{
		std::sort(vector_type::begin(), vector_type::end(), Comp());

		if(!Unique)
			return;

		auto cruft = std::unique(vector_type::begin(), vector_type::end());
		vector_type::erase(cruft, vector_type::end());
	}

public:

	template< class InputIt >
	ordered_vector(InputIt first, InputIt last): vector_type(first, last) { sanitize(); }
	ordered_vector(size_type count, const T& value): vector_type(Unique?1:count, value) {}
	ordered_vector(std::initializer_list<T> il): vector_type(il) { sanitize(); }

	using std::vector<T>::vector;
	using std::vector<T>::empty;
	using std::vector<T>::size;
	using std::vector<T>::max_size;
	using std::vector<T>::capacity;
	using std::vector<T>::shrink_to_fit;

	using std::vector<T>::cbegin;
	using std::vector<T>::cend;
	using std::vector<T>::crbegin;
	using std::vector<T>::crend;

	auto begin() const { return cbegin(); }
	auto end() const { return cend(); }
	auto rbegin() const { return crbegin(); }
	auto rend() const { return crend(); }

	auto& front() const { return vector_type::front(); }
	auto& back() const { return vector_type::back(); }
	auto& data() const { return vector_type::data(); }

	T const& at(size_type i) const { return vector_type::at(i); }
	T const& operator[](size_type i) const { return vector_type::operator[](i); }

	ordered_vector& operator=(ordered_vector&& v)
	{
		this->vector_type::operator=(v);
		std::sort(vector_type::begin(), vector_type::end(), Comp());
		if(Unique)
			vector_type::erase(std::unique(vector_type::begin(), vector_type::end()));
	}

	ordered_vector& operator=(ordered_vector const& v)
	{
		this->vector_type::operator=(static_cast<vector_type&>(v));
		std::sort(vector_type::begin(), vector_type::end(), Comp());
	}

	using std::vector<T>::clear;

	// FIXME: add replace(T const& t) method

	void insert(T const& t) { sorted_insert(t); }

	template<typename... Args>
	void emplace(Args&&... args)
	{
		sorted_insert(T(std::forward<Args...>(args)...));
	}

	void assign(size_type count, T const& value)
	{
		// FIXME: clear before inserting
		insert(value);

		if(!Unique)
			while(--count)
				insert(value);
	}

	template< class InputIt >
	void assign(InputIt first, InputIt last)
	{
		// FIXME: clear before inserting
		while(first != last)
			insert(*first++);
	}

	void assign(std::initializer_list<T> ilist)
	{
		assign(ilist.begin(), ilist.end());
	}

	using std::vector<T>::erase;
	using std::vector<T>::pop_back;
	using std::vector<T>::resize;

	void swap(ordered_vector& v)
	{
		vector_type::swap(static_cast<vector_type&>(v));
		sanitize();
	}
};

template<typename T, typename Comp = std::less<T>>
using sorted_vector = ordered_vector<T, Comp, false>;

template<typename T, typename Comp = std::less<T>>
using unique_sorted_vector = ordered_vector<T, Comp, true>;

// end ordered_vector


// == Unique Map ========================

template<typename K, typename V>
class unique_map
{
public:
	using key_type = K;
	using mapped_type = V;
	using value_type = std::pair<const K, const V>;

	using set_type = std::set<mapped_type>;
	using map_type = std::map<key_type, mapped_type>;
	using const_iterator = typename map_type::iterator;

private:
	std::set<V> values;
	std::map<K, V> maps;

public:
	std::pair<const_iterator, bool> insert(key_type key, value_type value)
	{
		auto found = maps.find(key);

		if(found != maps.end())
			return {found, false};

		if(values.insert(value).second)
			return maps.insert(std::make_pair(key, value));

		return {maps.end(), false};
	}

//	const mapped_type& operator[](const key_type& key) const
//	{
//		auto found = maps.find(key);
//	}

	const_iterator find(const key_type& key)
	{
		return maps.find(key);
	}

	const_iterator begin() const
	{
		return maps.begin();
	}
	const_iterator end() const
	{
		return maps.end();
	}
};

// / == Unique Map ===============

/**
 * Return a pair of iterators enclosing the first
 * group of elements that compare equal
 */
template<typename Iter>
std::pair<Iter, Iter> first_equal_group(Iter b, Iter e)
{
	auto i = b;
	while(i != e && *i == *b)
		++i;
	return {b, i};
}

/**
 * Iterate through each group of equal elements calling
 * the supplied function object with the beginning and end
 * iterators of each group's range.
 */
template<typename Iter, typename Func>
Func for_each_equal_group(Iter b, Iter e, Func func)
{
	for(auto r = first_equal_group(b, e);
		r.first != e;
		r = first_equal_group(r.first, e))
	{
		func(r.first, r.second);
		r.first = r.second;
	}

	return func;
}

// Example usage
//int main()
//{
//	std::vector<int> v {1, 1, 2, 3, 5, 5, 5, 4, 1};
//
//	if(v.empty())
//		return 0;
//
////	std::sort(v.begin(), v.end());
//
//	struct func
//	{
//		int sum = 0;
//		void operator()(std::vector<int>::iterator b, std::vector<int>::iterator e)
//		{
//			sum += *b;
//			for(auto i = b; i != e; ++i)
//				std::cout << *i << ' ';
//			std::cout << '\n';
//		}
//	};
//
//	auto f = for_each_equal_group(v.begin(), v.end(), func());
//
//	std::cout << "sum: " << f.sum << '\n';
//}


}} // hol::stl

#endif // HEADER_ONLY_LIBRARY_STL_H
