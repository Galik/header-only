#ifndef HEADER_ONLY_LIBRARY_ENV_H
#define HEADER_ONLY_LIBRARY_ENV_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <string>
#include <cstdlib>

namespace header_only_library {
namespace environment_variables {

inline std::string get(char const* var)
{
    return (var = std::getenv(var)) ? var : "";
}

namespace {

std::string const& HOME  = ::header_only_library::environment_variables::get("HOME");
std::string const& LANG  = ::header_only_library::environment_variables::get("LANG");
std::string const& MAIL  = ::header_only_library::environment_variables::get("MAIL");
std::string const& PATH  = ::header_only_library::environment_variables::get("PATH");
std::string const& PWD   = ::header_only_library::environment_variables::get("PWD");
std::string const& SHELL = ::header_only_library::environment_variables::get("SHELL");
std::string const& TERM  = ::header_only_library::environment_variables::get("TERM");
std::string const& USER  = ::header_only_library::environment_variables::get("USER");
std::string const& UID   = ::header_only_library::environment_variables::get("UID");

} // <anon>
} // namespace environment_variables
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_ENV_H
