#ifndef HEADER_ONLY_LIBRARY_IOS_H
#define HEADER_ONLY_LIBRARY_IOS_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <algorithm>
#include <type_traits>
#include <fstream>
#include <fstream>
#include <iterator>

#include "stl.h"
#include <hol/string_utils.h>

#ifndef HOL_FILE_BUFFER_SIZE
#define HOL_FILE_BUFFER_SIZE 4096
#endif

namespace header_only_library {
namespace io_utils {

using header_only_library::string_utils::trim_copy;

// Stream Iterators/Wrappers

//enum flags {none = 0, rebegin = 1, skip_blanks = 2};

namespace line_iterator {
template<typename Enum>
struct is_bitmask_enum
{
	static_assert(std::is_enum<Enum>::value, "Type must be enum");
	static const bool enable = false;
};
}

template<typename Enum>
typename std::enable_if<line_iterator::is_bitmask_enum<Enum>::enable, Enum>::type
operator|(Enum lhs, Enum rhs)
{
	static_assert(std::is_enum<Enum>::value, "Type must be enum");

	using enum_type = typename std::underlying_type<Enum>::type;

	return static_cast<Enum>
	(
		static_cast<enum_type>(lhs)
		| static_cast<enum_type>(rhs)
	);
}

template<typename Enum>
typename std::enable_if<line_iterator::is_bitmask_enum<Enum>::enable, Enum>::type
operator&(Enum lhs, Enum rhs)
{
	static_assert(std::is_enum<Enum>::value, "Type must be enum");

	using enum_type = typename std::underlying_type<Enum>::type;

	return static_cast<Enum>
	(
		static_cast<enum_type>(lhs)
		& static_cast<enum_type>(rhs)
	);
}

namespace line_iterator {
enum class flag_type: unsigned {none = 0b00, rebegin = 0b01, skip_blanks = 0b10};

template<>
struct is_bitmask_enum<flag_type> { static const bool enable = true; };
}

template<typename CharT>
class istream_line_iterator
{
public:
	using stream_type = std::basic_istream<CharT>;

private:
	stream_type* is = nullptr;
	std::string line;
	bool skip = false;

public:
	istream_line_iterator(stream_type& is, line_iterator::flag_type flags = line_iterator::flag_type::none)
	: istream_line_iterator(&is, flags) {}
	istream_line_iterator(stream_type* is = nullptr, line_iterator::flag_type flags = line_iterator::flag_type::none)
	: is(is), skip((flags & line_iterator::flag_type::skip_blanks) == line_iterator::flag_type::skip_blanks)
	{
		if(is && (line_iterator::flag_type::rebegin == (flags & line_iterator::flag_type::rebegin)))
		{
			is->clear();
			is->seekg(0);
		}
		++(*this);
	}

	istream_line_iterator& operator++()
	{
		if(is)
		{
			while(std::getline(*is, line) && skip && trim_copy(line).empty()) {}
			if(!(*is))
				is = nullptr;
		}
		return *this;
	}

	istream_line_iterator operator++(int) { istream_line_iterator i(*this); return ++i; }

	const std::string& operator*() const { return line; }

	bool operator==(const istream_line_iterator& i) const
	{
		if(is != i.is)
			return false;

		if(is && is->tellg() != i.is->tellg())
			return false;

		return false;
	}
	bool operator!=(const istream_line_iterator& i) const { return is != i.is; }
};

/**
 * A range that facilitates line by line
 * iterating of an std::istream.
 *
 * usage:
 *
 * for(auto line: line_iterable_istream(std::cin))
 *     std::cout << line << '\n';
 */
template<typename CharT>
stl::range<istream_line_iterator<CharT>>
line_iterable_istream(std::basic_istream<CharT>& is
	, line_iterator::flag_type flags = line_iterator::flag_type::none)
{
	return {istream_line_iterator<CharT>(is, flags), istream_line_iterator<CharT>()};
}

template<typename T, typename CharT>
stl::range<std::istream_iterator<T, CharT>>
value_iterable_istream(std::basic_istream<CharT>& is)
{
	return {std::istream_iterator<T, CharT>(is), std::istream_iterator<T, CharT>()};
}

/**
 * Extract text from between the next `delim_left` char and
 * the following `delim_right` char.
 * @param is The std::istream to extract the text from.
 * @param text The extracted text is placed here.
 * @param delim_l The left delimiter surrounding the text to be extracted.
 * @param delim_l The right delimiter surrounding the text to be extracted.
 * @return The supplied std::istream
 */
inline
std::istream& extract_text_between(std::istream& is, std::string& text
	, char delim_left, char delim_right)
{
	std::getline(is, text, delim_left);
	std::getline(is, text, delim_right);
	return is;
}

/**
 * Extract text surrounded the following `delim` char and the one after that.
 * @param is The std::istream to extract the text from.
 * @param text The extracted text is placed here.
 * @param delim The left and right surrounding delimiter to extract the text from.
 * @return The supplied std::istream
 */
inline
std::istream& extract_text_between(std::istream& is, std::string& text, char delim)
{
	return extract_text_between(is, text, delim, delim);
}

} // io_utils
} // hol

#endif // HEADER_ONLY_LIBRARY_IOS_H
