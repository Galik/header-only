#ifndef HEADER_ONLY_LIBRARY_LINUX_H
#define HEADER_ONLY_LIBRARY_LINUX_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <string>
#include <thread>
#include <vector>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>
#include <sys/stat.h>
#include <signal.h>
#include <wordexp.h>
#include <unistd.h>
#include <sys/types.h>

#include <ext/stdio_filebuf.h>

#include <chrono>
#include <cerrno>
#include <cstring>

#include <hol/bug.h>
#include <hol/memory_utils.h>
#include <hol/misc_utils.h>

namespace header_only_library {
namespace linux_utils {
namespace mu = misc_utils;

inline
std::string wordexp(std::string var, int flags = 0)
{
	wordexp_t p;

	auto raii = misc_utils::finally([&]{ wordfree(&p); });

	if(!wordexp(var.c_str(), &p, flags))
		if(p.we_wordc && p.we_wordv[0])
			var = p.we_wordv[0];

	return var;
}

namespace io {

struct fd_closer { void operator()(int fd) const { if(fd != -1) ::close(fd); }};

using unique_fd = header_only_library::memory_utils::unique_handle<int, fd_closer>;
using unique_socket = unique_fd;

//int open(const char *pathname, int flags);
//int open(const char *pathname, int flags, mode_t mode);
//
//int creat(const char *pathname, mode_t mode);
//
//int openat(int dirfd, const char *pathname, int flags);
//int openat(int dirfd, const char *pathname, int flags, mode_t mode);

inline
unique_fd open_unique(std::string const& filename, int flags = 0)
{
	int fd = ::open(filename.c_str(), flags);
	if(fd == -1)
		throw std::runtime_error(std::strerror(errno));
	return fd;
}

inline
unique_fd creat_unique(std::string const& filename, mode_t mode = {})
{
	int fd = ::creat(filename.c_str(), mode);
	if(fd == -1)
		throw std::runtime_error(std::strerror(errno));
	return fd;
}

inline
unique_fd openat_unique(int dirfd, std::string const& filename, int flags = 0, mode_t mode = {})
{
	int fd = ::openat(dirfd, filename.c_str(), flags, mode);
	if(fd == -1)
		throw std::runtime_error(std::strerror(errno));
	return fd;
}

} // io

// fork

namespace proc {

typedef __gnu_cxx ::stdio_filebuf<char> stdiobuf;
typedef __gnu_cxx ::stdio_filebuf<wchar_t> wstdiobuf;

template<typename Char>
class basic_stdio_stream: public std::basic_iostream<Char>
{
public:
	typedef Char char_type;
	typedef std::basic_iostream<char_type> stream_type;
	typedef __gnu_cxx ::stdio_filebuf<char_type> buf_type;

protected:
	buf_type buf;

public:
	basic_stdio_stream(int fd, const std::ios::openmode& mode)
	: stream_type(/*&buf*/), buf(fd, mode)
	{
		this->rdbuf(&buf);
	}

	void close()
	{
		::close(buf.fd()); // interrupt blocking?
	}
};

typedef basic_stdio_stream<char> stdio_stream;
typedef basic_stdio_stream<wchar_t> wstdio_stream;

typedef std::shared_ptr<stdio_stream> stdio_stream_sptr;
typedef std::unique_ptr<stdio_stream> stdio_stream_uptr;

class Fork
{
	typedef std::vector<stdio_stream_uptr> stdios_uptr_vec;
	typedef std::vector<std::array<int, 2>> pipe_vec;

	typedef stdios_uptr_vec::size_type size_type;

	enum { R = 0, W = 1, I = 0, O = 1, E = 2 };

	pid_t pid = -1;

	stdios_uptr_vec stdp;

	char** env = nullptr;

	bool log_report(const std::string& msg, bool state = false)
	{
		throw std::runtime_error(msg);
		return state;
	}

	static stdio_stream& get_null_iostream()
	{
		static stdio_stream stdionull(-1, std::ios::in | std::ios::out);
		stdionull.setstate(std::ios::failbit | std::ios::badbit);
		return stdionull;
	}

	std::string convert(const std::string& s) { return s; }

	template<typename Type>
	std::string convert(Type t)
	{
		return static_cast<std::stringstream&>(std::stringstream() << t).str();
	}

public:
	struct pipe_desc
	{
		int fd = -1;
		unsigned io = I;
		pipe_desc(unsigned io, int fd): fd(fd), io(io) {}
	};
private:

	std::vector<pipe_desc> fds;

	template<typename Arg, typename... Args>
	void set_pipes_r(Arg arg, Args... args)
	{
		fds.push_back(arg);
		set_pipes_r(args...);
	}

	void set_pipes_r()
	{
//		for(auto&& fd: fds)
//			bug('{' << fd.fd << ", " << fd.io << '}');
	}

	using clock = std::chrono::steady_clock;

	static void killer(pid_t pid, clock::duration wait)
	{
		clock::time_point die_by = clock::now() + wait;

		if(::kill(pid, SIGTERM) == -1)
			if(::kill(pid, SIGKILL) == -1)
				throw std::runtime_error("E: Unable to kill process: " + std::to_string(pid));

		struct stat s;
		std::string proc = "/proc/" + std::to_string(pid);

		while(clock::now() < die_by)
		{
			if((stat(proc.c_str() , &s) == -1) && errno == ENOENT)
				return; // killed
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}

		if(::kill(pid, SIGKILL) == -1)
			throw std::runtime_error("E: Unable to kill process: " + std::to_string(pid));

		while(clock::now() < die_by)
		{
			if((::stat(proc.c_str() , &s) == -1) && errno == ENOENT)
				return; // killed
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}
		throw std::runtime_error("E: Unable to kill process: " + std::to_string(pid));
	}

public:

	/**
	 * Currently not working for > 3 file descriptore
	 * When a 4th is opened, 0 (stdin) fails and so does
	 * the 4th (3)
	 * @param fds
	 */
	Fork(): stdp(3) {}

	Fork(Fork&& fork) { (*this) = std::move(fork); }

	Fork& operator=(Fork&& fork)
	{
		if(&fork == this)
			return *this;

		this->kill();

		for(auto&& s: fork.stdp)
			stdp.emplace_back(s.release());

		env = fork.env;
		fork.env = nullptr;

		pid = fork.pid;
		fork.pid = -1;

		return *this;
	}

	void kill(clock::duration wait = std::chrono::seconds(3))
	{
		for(auto&& s: stdp)
			s.release();
		stdp.clear();

		if(pid > 0)
			std::thread(killer, pid, wait).detach();
		pid = -1;
	}

	void setenv(char* env[]) { this->env = env; }

	explicit operator bool() const
	{
		for(auto& s: stdp)
			if(!s || !*s)
				return false;
		return pid != -1;
	}

	pid_t get_pid() const { return pid; }

	/**
	 * Get arbitrary file descriptor as stream
	 */
	stdio_stream& stdfd(int fd)
	{
		if(!(size_type(fd) < stdp.size()) || !stdp[mu::integral_cast<std::size_t>(fd)])
			return get_null_iostream();
		return *stdp[mu::integral_cast<std::size_t>(fd)];
	}

	/**
	 * Get named file descriptor 0 as stream
	 */
	stdio_stream& stdin() { return stdfd(I); }

	/**
	 * Get named file descriptor 1 as stream
	 */
	stdio_stream& stdout() { return stdfd(O); }

	/**
	 * Get named file descriptor 2 as stream
	 */
	stdio_stream& stderr() { return stdfd(E); }

	template<typename... Args>
	void set_pipes(Args... args)
	{
		fds.clear();
		set_pipes_r(args...);
	}

	/**
	 *
	 * @param dir Spaced need to be escaped "just\ like\ \ this.txt"
	 * @param prog
	 * @param args
	 * @return
	 */
	template<typename... Args>
	bool exec(std::string dir, std::string prog, Args... args)
	{
		bug_fun();
		if(true)
		{
			//bug_var(stdp.size());
			pipe_vec pipes(stdp.size());

			for(auto& a: pipes)
				if(::pipe(a.data()))
					return log_report(std::strerror(errno));

			if(env)
				environ = env;

			if((pid = fork())) // parent
			{
				if(pid < 0)
					return log_report(std::strerror(errno));

				if(!stdp.empty())
				{
					stdp[0].reset(new (std::nothrow) stdio_stream(pipes[0][W], std::ios::out));
					::close(pipes[0][R]);
				}

				for(unsigned p = 1; p < stdp.size(); ++p)
				{
					stdp[p].reset(new (std::nothrow) stdio_stream(pipes[p][R], std::ios::in));
					::close(pipes[p][W]);
				}

				for(const auto& s: stdp)
					if(!s)
						return log_report("Unable to create stdiostream object.");

				int status;
				stdin() >> status;

				return status;
			}

			// child

			for(int fd = int(pipes.size()) - 1; fd > 0; --fd)
			{
				::close(fd);
				::dup(pipes[fd][W]);
			}

			if(!pipes.empty())
			{
				::close(0);
				::dup(pipes[0][R]);
			}

			for(auto& p: pipes)
				for(auto& fd: p)
					::close(fd);
		}

		// escaped because wordexp breaks at spaces
		//replace(dir, " ", "\\ ");
		//replace(prog, " ", "\\ ");

		dir = wordexp(dir, WRDE_SHOWERR);
		prog = wordexp(prog, WRDE_SHOWERR);

		bug_var(dir);
		bug_var(prog);
//		std::ofstream("bug.log", std::ios::app) << "dir : " << dir << '\n';
//		std::ofstream("bug.log", std::ios::app) << "prog: " << prog << '\n';

		std::cout << '0' << std::flush;

		if(!dir.empty())
			if(chdir(dir.c_str()) == -1)
				return false;

//				throw std::runtime_error("E: [" + std::to_string(errno) + "] " + std::strerror(errno) + ": " + dir);

		execlp(prog.c_str(), prog.c_str(), convert(args).c_str()..., (char*)0);
//		execlp(prog.c_str(), prog.c_str(), (char*)0);
		std::cout << '1' << std::flush;

		//throw std::runtime_error("E: [" + std::to_string(errno) + "] " + std::strerror(errno));
		return false; // execl() failed
	}
};

inline
header_only_library::memory_utils::unique_PIPE_handle
open_piped_command(std::string const& cmd, char const* mode)
{
	auto p = ::popen(cmd.c_str(), mode);

	if(!p)
		throw std::runtime_error(std::strerror(errno));

	return header_only_library::memory_utils::unique_PIPE_handle{p};
}

inline
header_only_library::memory_utils::unique_PIPE_handle
open_piped_command(std::string const& cmd, std::string const& mode)
{
	return open_piped_command(cmd, mode.c_str());
}

inline
header_only_library::memory_utils::unique_PIPE_handle
open_piped_command_for_reading(std::string const& cmd)
{
	return open_piped_command(cmd, "r");
}

inline
header_only_library::memory_utils::unique_PIPE_handle
open_piped_command_for_writing(std::string const& cmd)
{
	return open_piped_command(cmd, "w");
}

inline
std::string piped_read(std::string const& cmd, int& return_code, std::size_t max_length = 0)
{
	std::string output;

	return_code = 0;

	errno = 0;
	if(auto pipe = header_only_library::linux_utils::proc::open_piped_command_for_reading(cmd))
	{
		if(errno == EINVAL)
			throw std::runtime_error("EINVAL: " + cmd);

		if(errno == ECHILD)
			throw std::runtime_error("ECHILD: " + cmd);

		char buf[512];
		while(auto len = std::fread(buf, sizeof(char), sizeof(buf), pipe.get()))
		{
			if(max_length && output.size() + len > max_length)
			{
				len = max_length - output.size();
				output.append(buf, len);
				throw std::runtime_error("maximum length exceeded: " + std::to_string(max_length));
			}
			output.append(buf, len);
		}

		if(std::ferror(pipe.get()))
			throw std::runtime_error("error reading from pipe");

		return_code = pipe.status();
	}

	return output;
}

inline
std::string piped_read(std::string const& cmd, std::size_t max_length = 0)
{
	int return_code;
	return piped_read(cmd, return_code, max_length);
}

inline
void piped_write(std::string const& cmd, std::string const& data)
{
	if(auto pipe = header_only_library::linux_utils::proc::open_piped_command_for_writing(cmd))
	{
		std::fwrite(data.data(), sizeof(char), data.size(), pipe.get());

		if(std::ferror(pipe.get()))
			throw std::runtime_error(std::strerror(errno));
	}
}

// popen2

struct popen_type
{
	enum mode {R = 1, W = 2, RW = R|W};
	pid_t pid = -1;
	int fdin = -1;
	int fdout = -1;
	popen_type(): pid(-1) {}
	operator bool() const { return pid > 0; }
};

template<typename... Args>
popen_type popen2(popen_type::mode mode, const std::string& cmd, Args&&... args)
{
	const int READ = 0;
	const int WRITE = 1;

	popen_type pt;

	int pin[2];
	int pout[2];

	if(::pipe(pin) != 0 || pipe(pout) != 0)
		throw std::runtime_error(cmd + ": " + std::strerror(errno));

	pt.pid = ::fork();

	if(pt.pid < 0)
		throw std::runtime_error(cmd + ": " + std::strerror(errno));
	else if(pt.pid == 0)
	{
		::close(pin[WRITE]);
		::dup2(pin[READ], READ);

		::close(pout[READ]);
		::dup2(pout[WRITE], WRITE);

		execlp(cmd.c_str(), cmd.c_str(), std::forward<Args>(args)..., NULL);
		std::cerr << std::strerror(errno) << '\n';
		::exit(1);
	}

	if(mode & popen_type::mode::W)
		pt.fdout = pin[WRITE];
	else
		::close(pin[WRITE]);

	if(mode & popen_type::mode::R)
		pt.fdin = pout[READ];
	else
		::close(pout[READ]);

	return pt;
}

template<typename... Args>
popen_type popen2_R(const std::string& cmd, Args&&... args)
	{ return popen2(popen_type::mode::R, cmd, std::forward<Args>(args)...); }

template<typename... Args>
popen_type popen2_W(const std::string& cmd, Args&&... args)
	{ return popen2(popen_type::mode::W, cmd, std::forward<Args>(args)...); }

template<typename... Args>
popen_type popen2_RW(const std::string& cmd, Args&&... args)
	{ return popen2(popen_type::mode::RW, cmd, std::forward<Args>(args)...); }

inline int main_test(int, const char* const* argv)
{
	if(argv[1] && !std::strcmp(argv[1], "--echo"))
	{
		std::string line;
		while(std::getline(std::cin, line))
			std::cout << "line: " << line << std::endl;
	}

	if(auto pt = popen2_RW("testcpp-debug", "--echo"))
	{
//		stdio_stream pin(pt.fdin, std::ios::in);
//		stdio_stream pout(pt.fdout, std::ios::out);

		std::string input;
		while(std::getline(std::cin, input))
		{
//			pout << input << '\n';
//
//			if(std::getline(pin, input))
//				std::cout << "got: " << input << '\n';
			input += '\n';

			ssize_t len;
			if((len = write(pt.fdout, input.data(), input.size())) == -1)
			{
				std::cout << "error 1: " << std::strerror(errno);
				return EXIT_FAILURE;
			}

//			bug_var(len);
//			bug_var(input.size());
//			syncfs(pt.fdout);
//			close(pt.fdout);

//			bug("response:");

//			int len;
			char buf[1024];
			if((len = read(pt.fdin, buf, sizeof(buf))) >= 0)
				std::cout << std::string(buf, mu::integral_cast<std::size_t>(len));
			else
			{
				std::cout << "error 2: " << std::strerror(errno);
				return EXIT_FAILURE;
			}
//			bug_var(len);
		}
//		else
//		{
//			std::cout << "error 3: " << std::strerror(errno);
//			return EXIT_FAILURE;
//		}
	}
	return EXIT_SUCCESS;
}

// / popen2


} // proc

// clocks

template<int CLOCK_TYPE>
class linux_clock
{
public:
	using rep = long;
	using duration = std::chrono::duration<rep>;
	using period = duration::period;
	using time_point = std::chrono::time_point<linux_clock<CLOCK_TYPE>>;

	static constexpr bool const is_steady = false;

	static
	time_point now() noexcept
	{
		timespec ts;
		::clock_gettime(CLOCK_TYPE, &ts);
//		return time_point(duration(rep((ts.tv_sec * 1000000) + (ts.tv_nsec / 1000))));
		return time_point(duration(rep((ts.tv_sec * 1000000000) + ts.tv_nsec)));
	}
};

using linux_realtime_clock = linux_clock<CLOCK_REALTIME>;
using linux_realtime_alarm_clock = linux_clock<CLOCK_REALTIME_ALARM>;
using linux_realtime_coarse_clock = linux_clock<CLOCK_REALTIME_COARSE>;

using linux_monotonic_clock = linux_clock<CLOCK_MONOTONIC>;
using linux_monotonic_raw_clock = linux_clock<CLOCK_MONOTONIC_RAW>;
using linux_monotonic_coarse_clock = linux_clock<CLOCK_MONOTONIC_COARSE>;

using linux_boottime_clock = linux_clock<CLOCK_BOOTTIME>;
using linux_boottime_alarm_clock = linux_clock<CLOCK_BOOTTIME_ALARM>;

using linux_thread_cputime_clock = linux_clock<CLOCK_THREAD_CPUTIME_ID>;
using linux_process_cputime_clock = linux_clock<CLOCK_PROCESS_CPUTIME_ID>;

} // linux_utils
} // header_only_library

#endif // HEADER_ONLY_LIBRARY_LINUX_H
