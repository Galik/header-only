// Copyright 2007 Edd Dawson.
// Distributed under the Boost Software License, Version 1.0.
// See accompanying file BOOST_LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt
//
// Slight modifications turning this into a header-only file by Galik <galik.bool@gmail.com>
// released under the same license.
//

#ifndef HEADER_ONLY_LIBRARY_FILE_BUFFER_H
#define HEADER_ONLY_LIBRARY_FILE_BUFFER_H

#include <streambuf>
#include <vector>
#include <cstdlib>

#include <cstdio>
#include <algorithm>
#include <cstring>

using namespace hol {

class FILE_buffer
: public std::streambuf
{
private:
	std::FILE* fptr;
	const std::size_t put_back;
	std::vector<char> buffer;

private:
	// overrides base class underflow()
	int_type underflow()
	{
		if(gptr() < egptr()) // buffer not exhausted
			return traits_type::to_int_type(*gptr());

		char* base = &buffer.front();
		char* start = base;

		if(eback() == base)
		{
			// Make arrangements for putback characters
			std::memmove(base, egptr() - put_back, put_back);
			start += put_back;
		}

		// start is now the start of the buffer, proper.
		// Read from fptr_ in to the provided buffer
		size_t n = std::fread(start, 1, buffer.size() - (start - base), fptr);
		if(n == 0)
			return traits_type::eof();

		// Set buffer pointers
		setg(base, start, start + n);

		return traits_type::to_int_type(*gptr());
	}

public:
	explicit FILE_buffer(FILE* fptr, size_t buff_sz, size_t put_back)
	: fptr(fptr)
	, put_back(std::max(put_back, size_t(1)))
	, buffer(std::max(buff_sz, this->put_back) + this->put_back)
	{
		// Set the back, current and end buffer pointers to be equal.
		// This will force an underflow() on the first read and hence
		// fill the buffer.
		char* end = &buffer.front() + buffer.size();
		setg(end, end, end);
	}

	// copy ctor and assignment not implemented;
	// copying not allowed
	FILE_buffer(const FILE_buffer &) = delete;
	FILE_buffer &operator=(const FILE_buffer &) = delete;
};

} // hol

#endif // HEADER_ONLY_LIBRARY_FILE_BUFFER_H
