#ifndef HEADER_ONLY_LIBRARY_EXPERIMENTAL_MISC_UTILS_H
#define HEADER_ONLY_LIBRARY_EXPERIMENTAL_MISC_UTILS_H

#include <algorithm>
#include <istream>
#include <memory>
#include <ostream>
#include <type_traits>
#include <vector>

#include <unistd.h>

#include "../misc_utils.h"
#include "../memory_utils.h"
#include "../random_numbers.h"

namespace header_only_library {
namespace misc_utils {

template<typename Container>
void minimize_allocation(Container& c)
{
	c = decltype(c)(const_cast<Container const&>(c));
}

template<typename Container>
void clear_to_minimum_allocation(Container& c)
{
	c = decltype(c)();
}

template<typename TypeId, typename Number>
class typed_number
{
public:
	explicit typed_number(Number n): n(n) {}
	typed_number(typed_number const& tn): n(tn.n) {}

	typed_number& operator= (typed_number const& tn) { this->n  = tn.n; return *this; }
	typed_number& operator+=(typed_number const& tn) { this->n += tn.n; return *this; }
	typed_number& operator-=(typed_number const& tn) { this->n -= tn.n; return *this; }
	typed_number& operator*=(typed_number const& tn) { this->n *= tn.n; return *this; }
	typed_number& operator/=(typed_number const& tn) { this->n /= tn.n; return *this; }

	operator Number() const { return n; }

	bool operator==(typed_number tn) const { return this->n == tn; }
	bool operator!=(typed_number tn) const { return this->n != tn; }
	bool operator<=(typed_number tn) const { return this->n <= tn; }
	bool operator>=(typed_number tn) const { return this->n >= tn; }
	bool operator< (typed_number tn) const { return this->n <  tn; }
	bool operator> (typed_number tn) const { return this->n >  tn; }

	typed_number operator+(typed_number const& tn) const { return typed_number(this->n + tn.n); }
	typed_number operator-(typed_number const& tn) const { return typed_number(this->n - tn.n); }
	typed_number operator*(typed_number const& tn) const { return typed_number(this->n * tn.n); }
	typed_number operator/(typed_number const& tn) const { return typed_number(this->n / tn.n); }

	friend std::ostream& operator<<(std::ostream& os, typed_number<TypeId, Number> n)
		{ return os << n.n; }

	friend std::istream& operator>>(std::istream& is, typed_number<TypeId, Number>& n)
		{ return is >> n.n; }

private:
	Number n;
};

template<typename Type, typename Number>
class typed_id
{
public:
	explicit typed_id(Number n): n(n) {}
	typed_id(typed_id const& id): n(id.n) {}

	typed_id& operator=(typed_id const& n) { this->n  = n.n; return *this; }

	bool operator<(typed_id id) const { return this->n < id.n; }
	bool operator>(typed_id id) const { return this->n > id.n; }

	bool operator==(typed_id id) const { return this->n == id.n; }
	bool operator!=(typed_id id) const { return this->n != id.n; }

private:
	Number n;
};

class uns_int
{
public:
	uns_int(unsigned n): n(n) {}

	uns_int& operator= (uns_int const& n) { this->n = n.n; return *this; }
	uns_int& operator+=(uns_int const& n) { this->n += n.n; return *this; }
	uns_int& operator-=(uns_int const& n) { this->n = difference(this->n, n.n); return *this; }
	uns_int& operator*=(uns_int const& n) { this->n *= n.n; return *this; }
	uns_int& operator/=(uns_int const& n) { this->n /= n.n; return *this; }

	operator unsigned() const { return n; }

	bool operator==(uns_int n) const { return this->n == n.n; }
	bool operator!=(uns_int n) const { return this->n != n.n; }
	bool operator<=(uns_int n) const { return this->n <= n.n; }
	bool operator>=(uns_int n) const { return this->n >= n.n; }
	bool operator< (uns_int n) const { return this->n <  n.n; }
	bool operator> (uns_int n) const { return this->n >  n.n; }

	uns_int operator+(uns_int const& n) const { return uns_int(this->n + n.n); }
	uns_int operator-(uns_int const& n) const { return difference(this->n, n.n); }
	uns_int operator*(uns_int const& n) const { return uns_int(this->n * n.n); }
	uns_int operator/(uns_int const& n) const { return uns_int(this->n / n.n); }

private:
	static unsigned difference(unsigned a, unsigned b)
	{
		return a < b ? (b - a):(a - b);
	}

	unsigned n;
};

template<typename TypeId, std::ptrdiff_t min, std::ptrdiff_t max>
class ranged_number
{
public:
	explicit ranged_number(std::ptrdiff_t n): n(n) {}
	ranged_number(ranged_number const& tn): n(tn.n) {}

	ranged_number& operator= (ranged_number const& tn)
	{
		this->n = tn.n;
		HOL_ASSERT(this->n >= min);
		HOL_ASSERT(this->n <= max);
		return *this;
	}
	ranged_number& operator+=(ranged_number const& tn)
	{
		this->n += tn.n;
		HOL_ASSERT(this->n >= min);
		HOL_ASSERT(this->n <= max);
		return *this;
	}
	ranged_number& operator-=(ranged_number const& tn)
	{
		this->n -= tn.n;
		HOL_ASSERT(this->n >= min);
		HOL_ASSERT(this->n <= max);
		return *this;
	}
	ranged_number& operator*=(ranged_number const& tn)
	{
		this->n *= tn.n;
		HOL_ASSERT(this->n >= min);
		HOL_ASSERT(this->n <= max);
		return *this;
	}
	ranged_number& operator/=(ranged_number const& tn)
	{
		this->n /= tn.n;
		HOL_ASSERT(this->n >= min);
		HOL_ASSERT(this->n <= max);
		return *this;
	}

	explicit operator std::ptrdiff_t() const { return n; }

	bool operator==(ranged_number tn) const { return this->n == tn; }
	bool operator!=(ranged_number tn) const { return this->n != tn; }
	bool operator<=(ranged_number tn) const { return this->n <= tn; }
	bool operator>=(ranged_number tn) const { return this->n >= tn; }
	bool operator< (ranged_number tn) const { return this->n <  tn; }
	bool operator> (ranged_number tn) const { return this->n >  tn; }

	ranged_number operator+(ranged_number const& tn) const
	{
		auto rn = ranged_number(this->n + tn.n);
		HOL_ASSERT(rn.n >= min);
		HOL_ASSERT(rn.n <= max);
		return rn;
	}
	ranged_number operator-(ranged_number const& tn) const
	{
		auto rn = ranged_number(this->n - tn.n);
		HOL_ASSERT(rn.n >= min);
		HOL_ASSERT(rn.n <= max);
		return rn;
	}
	ranged_number operator*(ranged_number const& tn) const
	{
		auto rn = ranged_number(this->n * tn.n);
		HOL_ASSERT(rn.n >= min);
		HOL_ASSERT(rn.n <= max);
		return rn;
	}
	ranged_number operator/(ranged_number const& tn) const
	{
		auto rn = ranged_number(this->n / tn.n);
		HOL_ASSERT(rn.n >= min);
		HOL_ASSERT(rn.n <= max);
		return rn;
	}

	friend std::ostream& operator<<(std::ostream& os, ranged_number<TypeId, min, max> n)
		{ return os << n.n; }

	friend std::istream& operator>>(std::istream& is, ranged_number<TypeId, min, max>& n)
		{ return is >> n.n; }

private:
	std::ptrdiff_t n;
};

template<typename Container>
struct signed_size: Container
{
	using typename Container::Container;
	std::ptrdiff_t ssize() const { return integral_cast<std::ptrdiff_t>(Container::size()); }
};

template<typename Integral>
class sign_checked
{
public:
//	template
//	<
//		typename Unknown, class = std::conditional_t
//		<
//			std::is_unsigned_v<Integral>,
//			std::enable_if_t<std::is_unsigned_v<Unknown>>,
//			std::enable_if_t<std::is_signed_v<Unknown>>
//		>
//	>

	template<typename Unknown>
	sign_checked(Unknown i): i(i)
	{
		static_assert(std::is_unsigned_v<Integral>
		|| (std::is_signed_v<Integral> && std::is_signed_v<Unknown>),
			"Sign Check Failure: signed value expected");

		static_assert(std::is_signed_v<Integral>
		|| (std::is_unsigned_v<Integral> && std::is_unsigned_v<Unknown>),
			"Sign Check Failure: signed value expected");
	}

	operator Integral() const { return i; }

private:
	Integral i;
};

template<typename Numeric>
auto random_vector(std::size_t n, Numeric min, Numeric max)
{
	std::vector<Numeric> c;
	c.reserve(n);
	std::generate_n(std::back_inserter(c), n, [&]{ return random_numbers::random_number(min, max); });
	return c;
}

} // namespace misc_utils
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_EXPERIMENTAL_MISC_UTILS_H
