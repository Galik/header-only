#ifndef HEADER_ONLY_LIBRARY_RANDOM_EXTRAS_H
#define HEADER_ONLY_LIBRARY_RANDOM_EXTRAS_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <random>
#include <limits>
#include <istream>
#include <ostream>

namespace header_only_library {
namespace random_generators {

template<typename UInteger = unsigned>
class xorshift_engine
{
    static_assert(std::is_unsigned<UInteger>::value,
		    "result_type must be an unsigned integral type");
public:
	using result_type = UInteger;

private:

	// 18446744073709551557
	static const result_type default_seed = 2147483647;

	result_type n[5] = {};
	enum {x, y, z, w, t};

public:
	static std::size_t const state_size = sizeof(n);

	explicit xorshift_engine(result_type value = default_seed) noexcept
		{ seed(value); }

	template<typename Sseq>
	explicit xorshift_engine(Sseq& seq)
		{ seed(seq); }

	void seed(result_type value = default_seed) noexcept
		{ n[x] = n[y] = n[z] = n[w] = value; }

	template<typename Sseq>
	void seed(Sseq& seq)
		{ seq.generate(std::begin(n), std::end(n)); }

	result_type operator()()
	{
		n[t] = n[x];
		n[t] ^= n[t] << 11;
		n[t] ^= n[t] >> 8;
		n[x] = n[y];
		n[y] = n[z];
		n[z] = n[w];
		n[w] ^= n[w] >> 19;
		n[w] ^= n[t];
		return n[w];
	}

	void discard(unsigned long long z) { while(z--) (*this)(); }

	static constexpr result_type min() { return std::numeric_limits<result_type>::min(); }
	static constexpr result_type max() { return std::numeric_limits<result_type>::max(); }

	template<typename UIntType>
	friend
	bool operator==(const xorshift_engine<UIntType>& lhs,
	                const xorshift_engine<UIntType>& rhs)
	{
		return lhs.n[x] == rhs.n[x]
			&& lhs.n[y] == rhs.n[y]
			&& lhs.n[z] == rhs.n[z]
			&& lhs.n[w] == rhs.n[w];
	}

	template<typename UIntType>
	friend
	bool operator!=(const xorshift_engine<UIntType>& lhs,
	               const xorshift_engine<UIntType>& rhs)
	{
		return !(lhs == rhs);
	}

	template<typename CharT, typename Traits, typename UIntType>
	friend
	std::basic_ostream<CharT, Traits>&
	operator<<(std::basic_ostream<CharT,Traits>& os,
		const xorshift_engine<UIntType>& e)
	{
		return os << e.n[x] << ' ' << e.n[y] << ' ' << e.n[z] << ' ' << e.n[w];
	}

	template<typename CharT, typename Traits, typename UIntType>
	friend
	std::basic_istream<CharT, Traits>&
	operator>>(std::basic_istream<CharT,Traits>& is,
		xorshift_engine<UIntType>& e)
	{
		return is >> e.n[x] >> e.n[y] >> e.n[z] >> e.n[w];
	}
};

using xorshift128 = xorshift_engine<std::uint32_t>;
using xorshift256 = xorshift_engine<std::uint64_t>;

// Choose between windows and POSIX random sources

#ifdef __MSVC
bool acquire_context(HCRYPTPROV* ctx)
{
	if (!CryptAcquireContext(ctx, nullptr, nullptr, PROV_RSA_FULL, 0)) {
		return CryptAcquireContext(ctx, nullptr, nullptr, PROV_RSA_FULL, CRYPT_NEWKEYSET);
	}
	return true;
}

std::size_t sysrandom(void* dst, size_t dstlen)
{
	HCRYPTPROV ctx;
	if (!acquire_context(&ctx)) {
		throw std::runtime_error("Unable to initialize Win32 crypt library.");
	}

	BYTE* buffer = reinterpret_cast<BYTE*>(dst);
	if(!CryptGenRandom(ctx, dstlen, buffer)) {
		throw std::runtime_error("Unable to generate random bytes.");
	}

	if (!CryptReleaseContext(ctx, 0)) {
		throw std::runtime_error("Unable to release Win32 crypt library.");
	}

	return dstlen;
}
#else
std::size_t sysrandom(void* buf, size_t n)
{
	return std::ifstream("/dev/urandom", std::ios_base::binary).read((char*)buf, n).gcount();
}
#endif

} // namespace random_generators
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_RANDOM_UTILS_H
