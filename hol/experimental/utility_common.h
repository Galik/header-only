#ifndef HEADER_ONLY_LIBRARY_UTILITY_COMMON_H
#define HEADER_ONLY_LIBRARY_UTILITY_COMMON_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

// TODO: remove this when condition is
// fully applied to all uses
//#define HOL_USE_STRING_VIEW

#include <regex>
#include <string>
#include <cstring>
#include <fstream>
#include <sstream>
#include <algorithm>
#if defined(HOL_USE_STRING_VIEW)
#	include <experimental/string_view>
#endif

#include "stl.h"

namespace hol {

using std::string;
using std::vector;
#if defined(HOL_USE_STRING_VIEW)
using std::experimental::string_view;
#endif

// string operator modes

struct mute
{
	using string_type = std::string&;
	using return_type = string_type;
};

struct copy
{
#if defined(HOL_USE_STRING_VIEW)
	using string_type = std::experimental::string_view;
#else
	using string_type = std::string const&;
#endif
	using return_type = std::string;
	using vector_type = std::vector<return_type>;
};

#if defined(HOL_USE_STRING_VIEW)
struct view
{
	using string_type = std::experimental::string_view;
	using return_type = string_type;
	using vector_type = std::vector<string_type>;
};
#endif

#if defined(HOL_USE_GSL_SPAN)
struct span
{
	using string_type = gsl::string_span<>;
	using return_type = string_type;
	using vector_type = std::vector<string_type>;
};
#endif

} // hol

#endif // HEADER_ONLY_LIBRARY_UTILITY_COMMON_H
