/*
 * errors.h
 *
 *  Created on: Jun 13, 2016
 *      Author: galik
 */

#ifndef HEADER_ONLY_LIBRARY_ERRORS_H_
#define HEADER_ONLY_LIBRARY_ERRORS_H_

#include <optional>

namespace hol {

struct error_type
{
	const int value;
	const char* const message;
	constexpr explicit operator bool() const { return value; }
	constexpr explicit operator int() const { return value; }
	friend std::ostream& operator<<(std::ostream& os, error_type const& s)
	{
		return os << s.message;
	}
};

struct status_type
{
	const int value;
	const char* const message;
	constexpr explicit operator bool() const { return !value; }
	constexpr explicit operator int() const { return value; }
	friend std::ostream& operator<<(std::ostream& os, status_type const& s)
	{
		return os << s.message;
	}
};

// USAGE

//	struct queue_status
//	{
//		constexpr static status_type ok = {0, "all good"};
//		constexpr static status_type empty = {1, "queue is empty"};
//		constexpr static status_type timed_out = {2, "timed out waiting for lock"};
//	};

//using error_message = std::optional<const char*>;

} // hol

//std::ostream& operator<<(std::ostream& os, hol::error_message const& s)
//{
//	return os << s.value();
//}

#endif /* HEADER_ONLY_LIBRARY_ERRORS_H_ */
