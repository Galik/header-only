#ifndef HEADER_ONLY_LIBRARY_TIME_STRINGS_H
#define HEADER_ONLY_LIBRARY_TIME_STRINGS_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <chrono>
#include <cassert>
#include <cmath>

#include <hol/time_utils.h>

namespace header_only_library {
namespace time_strings {

using header_only_library::time_utils::safe_localtime;

/**
 * Convert int to char string FAST filling buffer buf.
 * @param Size The number of characters in the buffer to fill
 * @param buf The buffer to fill, must be at least Size chars
 * @param n The number to convert from int to char string
 * @return
 */
template<int Size>
char* i_to_chars(char* buf, int n)
{
	assert(n < std::pow(10, Size));

	constexpr const int pow10[] =
	{
		0, 1, 10, 100, 1000, 10000, 100000, 1000000
		, 10000000, 100000000, 1000000000
	};

	static_assert(Size < sizeof(pow10) / sizeof(int), "too many powers of ten");

	for(int d = pow10[Size]; d; d /= 10)
	{
		*buf++ = char('0' + char(n / d));
		n -= (n / d) * d;
	}

	return buf;
}

inline
char* time_to_HH_MM_SS(char* p, std::tm const& bt)
{
	p = i_to_chars<2>(p, bt.tm_hour);
	*p++ = ':';
	p = i_to_chars<2>(p, bt.tm_min);
	*p++ = ':';
	p = i_to_chars<2>(p, bt.tm_sec);

	return p;
}

struct HH_MM_SS_mmm_config
{
	char HH_MM = ':';
	char MM_SS = ':';
	char SS_mmm = '.';
};

inline
std::string HH_MM_SS_mmm(
	std::chrono::system_clock::duration d, HH_MM_SS_mmm_config conf)
{
	using namespace std::chrono;

	auto hrs = duration_cast<hours>(d);
	auto mins = duration_cast<minutes>(d) - hrs;
	auto secs = duration_cast<seconds>(d) - hrs - mins;
	auto ms = duration_cast<milliseconds>(d) - hrs - mins - secs;

	char buf[sizeof("00:00:00.000") - 1];

	char* p = &buf[0];

	p = i_to_chars<2>(p, int(hrs.count()));
	*p++ = conf.HH_MM;
	p = i_to_chars<2>(p, int(mins.count()));
	*p++ = conf.MM_SS;
	p = i_to_chars<2>(p, int(secs.count()));
	*p++ = conf.SS_mmm;
	p = i_to_chars<3>(p, int(ms.count()));

	return {buf, sizeof(buf)};
}

inline
std::string HH_MM_SS_mmm(
	std::chrono::system_clock::time_point now, HH_MM_SS_mmm_config conf)
{
	using namespace std::chrono;

	auto ms = duration_cast<milliseconds>(now.time_since_epoch()) % 1000;

	std::tm bt = safe_localtime(system_clock::to_time_t(now));

	char buf[sizeof("00:00:00.000") - 1];

	char* p = &buf[0];

	p = i_to_chars<2>(p, bt.tm_hour);
	*p++ = conf.HH_MM;
	p = i_to_chars<2>(p, bt.tm_min);
	*p++ = conf.MM_SS;
	p = i_to_chars<2>(p, bt.tm_sec);
	*p++ = conf.SS_mmm;
	p = i_to_chars<3>(p, int(ms.count()));

	return {buf, sizeof(buf)};
}

inline
std::string HH_MM_SS_mmm(
	std::chrono::system_clock::time_point now)
{
	return HH_MM_SS_mmm(now, {':', ':', '.'});
}

inline
std::string HH_MM_SS_mmm(HH_MM_SS_mmm_config conf)
{
	return HH_MM_SS_mmm(std::chrono::system_clock::now(), conf);
}

inline
std::string HH_MM_SS_mmm()
{
	return HH_MM_SS_mmm(std::chrono::system_clock::now(), {':', ':', '.'});
}

} // time_strings
} // header_only_library

//inline
//std::string time_in_HH_MM_SS_MMM(std::chrono::system_clock::time_point now = std::chrono::system_clock::now())
//{
//	std::tm bt = hol::safe_localtime(std::chrono::system_clock::to_time_t(now));
//	auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;
//
//
//	std::array<char, 12> s;
//
//	auto p = std::begin(s);
//
//	*p++ = char('0' + ((bt.tm_hour /  10) % 10));
//	*p++ = char('0' + ((bt.tm_hour /   1) % 10));
//	*p++ = ':';
//	*p++ = char('0' + ((bt.tm_min  /  10) % 10));
//	*p++ = char('0' + ((bt.tm_min  /   1) % 10));
//	*p++ = ':';
//	*p++ = char('0' + ((bt.tm_sec  /  10) % 10));
//	*p++ = char('0' + ((bt.tm_sec  /   1) % 10));
//	*p++ = '.';
//	*p++ = char('0' + ((ms.count() / 100) % 10));
//	*p++ = char('0' + ((ms.count() /  10) % 10));
//	*p++ = char('0' + ((ms.count() /   1) % 10));
//
//	HOL_ASSERT(p == std::end(s));
//
//	return {std::begin(s), std::end(s)};
//}
//

#endif // HEADER_ONLY_LIBRARY_TIME_STRINGS_H
