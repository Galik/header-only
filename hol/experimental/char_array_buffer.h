// Copyright 2007 Edd Dawson.
// Distributed under the Boost Software License, Version 1.0.
// See accompanying file BOOST_LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt
//
// Slight modifications turning this into a header-only file by Galik <galik.bool@gmail.com>
// released under the same license.
//

// http://www.mr-edd.co.uk/blog/beginners_guide_streambuf

#ifndef HEADER_ONLY_LIBRARY_CHAR_ARRAY_BUFFER_H
#define HEADER_ONLY_LIBRARY_CHAR_ARRAY_BUFFER_H

#include <streambuf>
#include <functional>
#include <cassert>
#include <cstring>

namespace header_only_library {
namespace io {

template<typename CharT, typename Traits = std::char_traits<CharT>>
class basic_char_array_buffer
: public std::basic_streambuf<CharT, Traits>
{
public:
	using size_type = std::size_t;
	using char_type = CharT;
	using traits_type = Traits;

	using int_type = typename traits_type::int_type;

private:
	const char_type* const beg;
	const char_type* const end;
	const char_type* cur;

private:
	int_type uflow()
	{
		if(cur == end)
			return traits_type::eof();

		return *cur++;
	}

	int_type underflow()
	{
		if(cur == end)
			return traits_type::eof();

		return *cur;
	}

	int_type pbackfail(int_type ch)
	{
		if(cur == beg || (ch != traits_type::eof() && ch != int_type(cur[-1])))
			return traits_type::eof();

		return *--cur;
	}

	std::streamsize showmanyc()
	{
		assert(std::less_equal<const char_type*>()(cur, end));
		return end - cur;
	}

	static size_type internal_strlen(const char_type* s)
	{
		size_type z = 0;
		while(*s++)
			++z;
		return z;
	}

public:
	basic_char_array_buffer()
	: beg(nullptr), end(nullptr), cur(nullptr) {}

	basic_char_array_buffer(const char_type* begin, const char_type* end)
	: beg(begin), end(end), cur(begin)
	{
		assert(std::less_equal<const char_type*>()(begin, end));
	}

	explicit basic_char_array_buffer(const char_type* str, std::size_t size)
	: beg(str), end(beg + size), cur(beg)
	{
	}

	explicit basic_char_array_buffer(const char_type* str)
	: beg(str), end(beg + internal_strlen(str)), cur(beg)
	{
	}
};

using char_array_buffer = basic_char_array_buffer<char>;
using wchar_array_buffer = basic_char_array_buffer<wchar_t>;

template<typename CharT, typename Traits = std::char_traits<CharT>>
class basic_char_array_input_stream
: public std::basic_istream<CharT, Traits>
{
	using stream_type = std::basic_istream<CharT, Traits>;
	using buffer_type = basic_char_array_buffer<CharT, Traits>;

	buffer_type buf;

public:
	basic_char_array_input_stream(): stream_type(&buf), buf() {}

	template<typename Alloc = std::allocator<CharT>>
	basic_char_array_input_stream(const std::basic_string<CharT, Traits, Alloc>& s)
	: stream_type(&buf), buf(s.data(), s.size()) {}

};

using char_array_istream = basic_char_array_input_stream<char>;
using wchar_array_istream = basic_char_array_input_stream<wchar_t>;

} // io
} // header_only_library

#endif // HEADER_ONLY_LIBRARY_CHAR_ARRAY_BUFFER_H

