#ifndef HEADER_ONLY_LIBRARY_CONFIG_H
#define HEADER_ONLY_LIBRARY_CONFIG_H
/*
 *  Created on: Jul 26, 2014
 *	  Author: Galik <galik.bool@gmail.com>
 */

#include <map>
#include <set>
#include <list>
#include <deque>
#include <vector>
#include <fstream>
#include <sstream>

#include <hol/string_utils.h>
#include <hol/macro_exceptions.h>

namespace header_only_library {
namespace configuration {

using header_only_library::string_utils::trim_mute;

template <typename Container>
struct is_container : std::false_type {};

template <typename... Ts> struct is_container<std::set<Ts...> > : std::true_type {};
template <typename... Ts> struct is_container<std::list<Ts...> > : std::true_type {};
template <typename... Ts> struct is_container<std::deque<Ts...> > : std::true_type {};
//template <typename... Ts> struct is_container<std::vector<Ts...> > : std::true_type {};

template<typename T>
struct is_extractable
{
	T t;
	template<typename = decltype(std::istringstream("") >> t)>
	static std::true_type test(int);

    template<typename>
	static std::false_type test(...);

    typedef decltype(test<T>(0)) type;
};

//template <typename Type = void>
//struct is_extractable
//{
//	using type = std::false_type;
//};
//
//template <typename Type>
//struct is_extractable
//{
//	Type v;
//	decltype(std::istringstream("") >> v) extract(){}
//	using type = std::true_type;
//};

template<typename T>
class ConfigSerializer
{
public:
	virtual ~ConfigSerializer() = default;

	virtual std::ostream serialize(std::ostream& os, T const& t) = 0;
	virtual std::istream deserialize(std::istream& os, T& t) = 0;
};

class Config
{
private:
	using property_map = std::map<std::string, std::vector<std::string>>;

	property_map props;

	static property_map load(std::string const& filename)
	{
		std::ifstream ifs(filename);
		return load(ifs);
	}

	static property_map load(std::istream& is)
	{
		property_map props;

		// working variables
		std::string::size_type pos;
		std::string line, key, val;

		// read in config

		unsigned no = 0;
		while(std::getline(is, line))
		{
			++no;

			if((pos = line.find("//")) != std::string::npos)
				line.erase(pos);

			if(trim_mute(line).empty() || line[0] == '#')
				continue;

			if(!std::getline(std::getline(std::istringstream(line), key, ':') >> std::ws, val))
				hol_throw_runtime_error("E: parsing config file: ? at: " << no);

			props[key].push_back(val);
		}

		return props;
	}

public:
	Config() = default;
	Config(std::string const& filename): props(load(filename)) {}
	Config(std::istream& is): props(load(is)) {}

	void init(std::string const& filename) { props = load(filename); }

	const std::vector<std::string>& get_all(const std::string& s) const
	{
		static const std::vector<std::string> null_vec;

		const auto found = props.find(s);

		if(found == props.end())
			return null_vec;

		return found->second;
	}

	/**
	 * Get a type-converted config variable's value.
	 *
	 * @param s The config variable whose value is sought.
	 * @param dflt A default value to use if the variable was not
	 * found in the config file.
	 *
	 * @return The value that the variable s is set to in the config
	 * file else dflt if not present.
	 */
	template<typename Type>
	auto get_all(const std::string& s) const
	-> typename std::enable_if<is_container<Type>::value, Type>::type
	{
		auto v = get_all(s);
		return {std::begin(v), std::end(v)};
	}

	template<typename Type>
	auto get(const std::string& s, const Type& dflt = Type()) const
	-> typename std::enable_if<is_extractable<Type>::type::value, Type>::type
	{
		const auto found = props.find(s);

		if(found == props.end())
			return dflt;

		Type t;
		std::istringstream iss(*(found->second.begin()));
		iss >> std::boolalpha >> t;

		return t;
	}

	/**
	 * Get the native string config variable's value.
	 *
	 * @param s The config variable whose value is sought.
	 * @param dflt A default value to use if the variable was not
	 * found in the config file.
	 *
	 * @return The value that the variable s is set to in the config
	 * file else dflt if not present.
	 */
	std::string get(const std::string& s, const std::string& dflt = "") const
	{
		const auto found = props.find(s);
		return found != props.end() ? *(found->second.begin()) : dflt;
	}

	/**
	 * Check if the config file has a given variable set.
	 */
	bool has(const std::string& s) const
	{
		return props.find(s) != props.end();// && !props[s].empty();
	}

	/**
	 * Synonym for bool has(const std::string& s).
	 */
	bool have(const std::string& s) const { return has(s); }
};

} // namespace configuration
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_CONFIG_H
