#ifndef HEADER_ONLY_LIBRARY_FILESYSTEM_UTILS_H
#define HEADER_ONLY_LIBRARY_FILESYSTEM_UTILS_H

#include <set>
#include <string>
#include <filesystem>

namespace hol {

namespace fs = std::filesystem;

//==============================
// Predicates
//------------------------------

/**
 * Function object to test directory entries
 * for a (case insensitive) file extension.
 *
 * USAGE:
 *
 * 	std::vector<fs::directory_entry> entries;
 *
 *	std::copy_if(
 *		fs::directory_iterator(dir),
 *		fs::directory_iterator(),
 *		std::back_inserter(entries),
 *		with_file_extensions(".txt", ".text"));
 *
 */
struct with_file_extensions
{
public:
	template<typename... Exts>
	with_file_extensions(Exts&&... exts)
	: exts({lower(std::forward<Exts>(exts))...}) {}

	bool operator()(fs::directory_entry const& entry) const
	{
		return exts.count(lower(entry.path().extension().string()));
	}

private:
	std::string lower(std::string s) const
	{
		std::transform(s.begin(), s.end(), s.begin(),
			std::ptr_fun<int, int>(std::tolower));
		return s;
	}

	std::set<fs::path> exts;
};

} // hol

#endif // HEADER_ONLY_LIBRARY_FILESYSTEM_UTILS_H
