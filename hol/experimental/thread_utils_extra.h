#ifndef HEADER_ONLY_LIBRARY_THREAD_UTILS_EXTRAS_H
#define HEADER_ONLY_LIBRARY_THREAD_UTILS_EXTRAS_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <future>

#include "../tread_utils.h"

#undef WARN_UNUSED_RESULT
#ifdef __GNUC__
//	[[nodiscard]] // C++17
#define WARN_UNUSED_RESULT [[gnu::warn_unused_result]]
#else
#define WARN_UNUSED_RESULT
#endif

#ifdef HOL_THREAD_UTILS_DEBUG_LOGGING_ENABLED
#include <fstream>
#include <sstream>
#endif

namespace hol {
namespace thread_utils {
namespace mt {

//template<typename Object>
//class lockable_object
//: public virtual lockable_for_reading
//, public virtual lockable_for_updates
//{
//public:
//	using pointer = typename std::remove_reference<Object>::type*;
//	using reference = typename std::remove_reference<Object>::type&;
//	using const_pointer = typename std::remove_reference<Object>::type const*;
//	using const_reference = typename std::remove_reference<Object>::type const&;
//
//private:
//	reference o;
//
//public:
//	lockable_object(Object& o): o(o) {}
//	lockable_object(lockable_object&& l): lockable_base(std::move(l)), o(l.o) {}
//	lockable_object(lockable_object const&) = delete;
//
//	const_reference ro() const { return o; }
//	reference rw() { return o; }
//
//	reference operator*() { return o; }
//	pointer operator->() { return &o; }
//
//	const_reference operator*() const { return o; }
//	const_pointer operator->() const { return &o; }
//};
//
//template<typename Object>
//class locked_read_only_version_of
//{
//public:
//	using pointer = typename std::remove_reference<Object>::type*;
//	using reference = typename std::remove_reference<Object>::type&;
//	using const_pointer = typename std::remove_reference<Object>::type const*;
//	using const_reference = typename std::remove_reference<Object>::type const&;
//
//private:
//	mutable typename Object::lockable::read_only_lock lock;
//	const_reference o;
//
//	public:
//	locked_read_only_version_of(Object&& o)
//	: lock(o.lock_for_reading()), o(std::forward<Object>(o)) {}
//
////	~locked_read_only_version_of() {bug_fun();}
//
//	const_reference operator*()  const { return o; }
//	const_pointer   operator->() const { return &o; }
//
//	const_reference ro() const { return o; }
//};
//
//template<typename LockableObject>
//class locked_updatable_version_of
//{
//	typename LockableObject::lockable::updatable_lock lock;
//	LockableObject& object;
//	public:
//	locked_updatable_version_of(LockableObject& lockable)
//	: lock(lockable.lock_for_updates()), object(lockable) {}
//	LockableObject& operator*()  { return object; }
//	LockableObject* operator->() { return &object; }
//
//	LockableObject& rw() { return *object; }
//};
//
//template<typename Object>
//auto make_lockable_object_of(Object& o)
//{
//	return mt::lockable_object<Object>(o);
//}
//
//template<typename Object>
//auto make_read_only_wrapper_for_lockable(Object const& o)
//{
//	return mt::locked_read_only_version_of<decltype(o)>(o);
//}
//
//template<typename Object>
//auto make_updatable_wrapper_for_lockable(Object& o)
//{
//	return mt::locked_updatable_version_of<Object>(o);
//}

// Usage
/*

#include <hol/thread_utils.h>

using namespace hol::thread_utils;

class R
: public mt::lockable_for_reading
{
public:
	void set(int) {}
	int get() const { return 0; }
};

class W
: public mt::lockable_for_updates
{
public:
	void set(int) {}
	int get() const { return 0; }
};

int main()
{
	R r;
	W w;

	mt::locked_read_only_version_of<R> rl(r);

	rl->get();
//	rl->set(4);

	mt::locked_updatable_version_of<W> wl(w);

	wl->get();
	wl->set(5);

}


 */

//=============================================================
//== THREAD DIAGNOSTICS =======================================
//=============================================================

#ifdef HOL_THREAD_UTILS_DEBUG_LOGGING_ENABLED
#define hol_thread_utils_debug_log() \
	hol::thread_utils::mt::thread_logger::instance().update(__FILE__, __LINE__)
#define hol_thread_utils_debug_log_init(file, poll_time) \
	hol::thread_utils::mt::thread_logger::start(file, poll_time)
#define hol_thread_utils_debug_log_stop() \
	hol::thread_utils::mt::thread_logger::stop()

class thread_logger
{
	using mutex = std::shared_timed_mutex;
	using shared_lock = std::shared_lock<mutex>;
	using unique_lock = std::unique_lock<mutex>;

	struct thread_info
	{
		std::thread::id id;
		std::size_t seq;
		std::time_t when;
		char const* file;
		std::size_t line;
	};

	using thread_map = std::map<std::thread::id, thread_info>;

	mutable mutex mtx;
	thread_map m;

	static std::size_t seq()
	{
		thread_local static std::size_t n = 0;
		return n++;
	}

	std::vector<std::string> compose()
	{
		thread_map m;

		{
			auto lock = lock_for_unique_access();
			m = this->m;
		}

		std::vector<std::string> lines;

		for(auto const& p: m)
		{
			std::string seq_str = std::to_string(p.second.seq);

			if(seq_str.size() < 6)
				seq_str = std::string(6 - seq_str.size(), '0') + seq_str;

			std::ostringstream oss;
			oss << p.second.id;
			oss << ' ' << seq_str;
			oss << ' ' << stamp(p.second.when);
			oss << ' ' << p.second.file;
			oss << ':' << p.second.line;

			lines.push_back(oss.str());
		}

		return lines;
	}

	void monitor(std::string const& log_file_name,
		std::chrono::system_clock::duration poll_time)
	{
		using clock = std::chrono::system_clock;

		auto next_poll = clock::now();

		std::ofstream debug_log(log_file_name, std::ios::app);

		while(!done())
		{
			while(clock::now() < next_poll)
				std::this_thread::sleep_until(next_poll);

			auto lines = compose();

			debug_log << "poll: " << stamp(clock::to_time_t(next_poll)) << '\n';

			for(auto const& line: lines)
				debug_log << line << '\n';
			debug_log << std::flush;

			next_poll += poll_time;
		}
	}

	static std::atomic_bool& done()
	{
		static std::atomic_bool b{true};
		return b;
	}
	static std::thread& runner()
	{
		static std::thread t;
		return t;
	}

	thread_logger() {}

public:
	thread_logger(thread_logger const&) = delete;
	thread_logger& operator=(thread_logger const&) = delete;

	static thread_logger& instance()
	{
		static thread_logger logger;
		return logger;
	}

	shared_lock lock_for_shared_access() const { return shared_lock(mtx); }
	unique_lock lock_for_unique_access() const { return unique_lock(mtx); }

	void update(char const* file, std::size_t line)
	{
		thread_info info
		{
			std::this_thread::get_id(),
			seq(),
			std::time(0),
			file,
			line,
		};

		{
			auto lock = lock_for_shared_access();

			auto found = m.find(info.id);

			if(found != m.end())
			{
				found->second = info;
				return;
			}
		}

		auto lock = lock_for_unique_access();
		m[info.id] = info;
	}

	static std::string stamp(std::time_t now)
	{
		std::tm bt;
		localtime_r(&now, &bt);
		char buf[64];
		return {buf, std::strftime(buf, sizeof(buf), "%F %T", &bt)};
	}

	static void start(std::string const& log_file_name,
		std::chrono::system_clock::duration poll_time)
	{
		bug_fun();
		if(!done())
			return;

		done() = false;
		runner() = std::thread(&thread_logger::monitor,
			&thread_logger::instance(),
				log_file_name, poll_time);
	}

	static void stop()
	{
		bug_fun();
		if(done())
			return;

		done() = true;
		runner().join();
	}
};

#else // HOL_THREAD_UTILS_DEBUG_LOGGONG_ENABLED
#define hol_thread_utils_debug_log() do{}while(0)
#define hol_thread_utils_debug_log_init(file, poll_time) do{}while(0)
#define hol_thread_utils_debug_log_stop() do{}while(0)
#endif

//=============================================================

// portioning data between threads

//int main()//int, char** argv)
//{
//	try
//	{
//		unsigned number_of_threads = std::thread::hardware_concurrency();
//
//		std::vector<std::string> v{ "element1", "element2", "element3" };
//
//		const auto elements_per_thread = v.size() / (number_of_threads - 1);
//		std::vector<std::thread> threads;
//
//		for(std::size_t i = 0; i < number_of_threads; i++)
//		{
//			auto start = i * elements_per_thread;
//			auto end = start + elements_per_thread;
//
//			if(end > v.size())
//				end = v.size();
//
//			threads.emplace_back([&v](std::size_t begin, std::size_t end)
//			{
//				for(std::size_t i = begin; i < end; ++i)
//					std::cout << v[i] << '\n';
//			}, start, end);
//		}
//
//		for(auto&& thread: threads)
//			thread.join();
//
//	}
//	catch(std::exception const& e)
//	{
//		std::cerr << e.what() << '\n';
//		return EXIT_FAILURE;
//	}
//
//	return EXIT_SUCCESS;
//}

class Test
{
public:
	using mutex_type = std::shared_timed_mutex;
	using read_lock  = std::shared_lock<mutex_type>;
	using write_lock = std::unique_lock<mutex_type>;

	template<typename MemFun, typename... Args>
	auto locked(MemFun&& func, Args&&... args)
		-> decltype(std::bind(std::forward<MemFun>(func), this, std::forward<Args>(args)...)())
	{
		using ReturnType = decltype(std::bind(std::forward<MemFun>(func), this, std::forward<Args>(args)...)());

		typedef ReturnType (Test::* const_mem_fun)(Args&&...) const;

		using lock_type = typename std::conditional
		<
			std::is_same<MemFun, const_mem_fun>::value,
			read_lock,
			write_lock
		>::type;

		lock_type lock{m_mtx};
		return std::bind(std::forward<MemFun>(func), this, std::forward<Args>(args)...)();
	}

	int method_1() const { return 5; }
	void method_2(int, float) {}

//	read_lock lock_for_reading() const { return read_lock{m_mtx}; }
//	write_lock lock_for_writing() { return write_lock{m_mtx}; }

private:
	mutable mutex_type m_mtx;
};

// STL

template<typename Iterator>
void parallel_sort(Iterator begin, Iterator end)
{
	auto size = std::distance(begin, end);

	// divide the container up into 4 sections
	auto a = begin;
	auto b = a + (size / 4);
	auto c = b + (size / 4);
	auto d = c + (size / 4);
	auto e = end;

	parallel_invoke(
		[&]{ std::sort(a, b); },
		[&]{ std::sort(b, c); },
		[&]{ std::sort(c, d); },
		[&]{ std::sort(d, e); }
	);

	parallel_invoke(
		[&]{ std::inplace_merge(a, b, c); },
		[&]{ std::inplace_merge(c, d, e); }
	);

	std::inplace_merge(a, c, e);
}

template<typename Iterator>
std::vector<Iterator> split_work(Iterator begin, Iterator end, unsigned pieces)
{
	std::vector<Iterator> v;
	v.reserve(pieces + 1);

	auto f = std::distance(begin, end) / pieces;

	v.push_back(begin);
	while(--pieces)
		v.push_back(v.back() + f);
	v.push_back(end);

	return v;
}


template<typename Data>
class ThreadControlBlock
{
public:
//	ThreadControlBlock(Data d): d(d) {}

	[[nodiscard]]
	auto lock_for_updates() { return std::unique_lock<std::mutex>{mtx}; }

	bool running() const { return !done; }

	void stop()
	{
		done = true;

		for(auto& t: threads)
			t.join();

		threads.clear();
	}

	template<typename Func>
	void start_thread(Func func)
	{
		done = false;
		threads.push_back(std::thread(func, std::ref(*this)));
	}

	template<typename Pred>
	void wait(std::unique_lock<std::mutex>& lock, Pred pred)
	{
		cv.wait(lock, pred);
	}

	Data& data() { return d; }

	void notify_one() { cv.notify_one(); }
	void notify_all() { cv.notify_all(); }

private:
	Data d;

	std::mutex mtx;
	std::atomic_bool done{false};
	std::condition_variable cv;

	std::vector<std::thread> threads;
};

// correct double checked locking

//	mutex action_mutex;
//	atomic<bool> action_needed;
//
//	if (action_needed.load(memory_order_acquire))
//	{
//		lock_guard<std::mutex> lock(action_mutex);
//		if (action_needed.load(memory_order_relaxed))
//		{
//			take_action();
//			action_needed.store(false, memory_order_release);
//		}
//	}

// correctly divvy up a buffer?

//	Evaluator::evaluate(vector<inpType> input, bool parallel) {
//		if(parallel) {
//			std::thread t0(&Evaluator::evaluatePart, this, std::ref(input), 0, input.size()/2 + input.size()%2);
//			evaluatePart(input, input.size()/2 + input.size()%2 - 1, input.size()/2);
//		} else {
//			// sequential
//			evaluatePart(input, 0, input.size());
//		}
//		// some other code
//		if(parallel) t0.join();
//	}


} // mt
} // thread_utils
} // hol

#endif // HEADER_ONLY_LIBRARY_THREAD_UTILS_EXTRAS_H
