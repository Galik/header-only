#ifndef HEADER_ONLY_LIBRARY_U8STRING_H
#define HEADER_ONLY_LIBRARY_U8STRING_H

//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

// ======================================
// STATUS: experimental/unfinished
//
// Do not use
//

#include <array>
#include <string>
#include <cstring>

namespace header_only_library {
namespace unicode_utils {

using std::strncpy;
using std::strcmp;
using std::strlen;

using char16_pair = std::array<char16_t, 2>;

//inline std::vector<char> create_lookup()
//{
//	std::vector<char> lookup(256);
//	for(int c = 0b00000000; c < 0b10000000; ++c)
//		lookup[c] = 1;
//	for(int c = 0b11000000; c < 0b11100000; ++c)
//		lookup[c] = 2;
//	for(int c = 0b11100000; c < 0b11110000; ++c)
//		lookup[c] = 3;
//	for(int c = 0b11110000; c < 0b11111000; ++c)
//		lookup[c] = 4;
//	for(int c = 0b11111000; c < 0b11111100; ++c)
//		lookup[c] = 5;
//	for(int c = 0b11111100; c < 0b11111110; ++c)
//		lookup[c] = 6;
//	return lookup;
//}

//inline const std::vector<char>& get_lookup()
//{
////	static std::vector<char> lookup = create_lookup();
//	static const std::vector<char> u8char_size =
//	{
//		  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
//		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
//		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
//		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
//		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
//		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
//		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
//		, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
//		, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
//		, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
//		, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
//		, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
//		, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
//		, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
//		, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3
//		, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 0, 0
//	};
//
//	return u8char_size;
//}

// BOM EF BB BF Ob 1110 1111 01011 01011 01011 1111

class u8char
{
	friend class u8string;

public:
//	static const u8char BOM;

	enum class code_point: std::uint32_t{};

private:

	enum u8mask
	{
		  na1 = 0b10000000 // !(c & na1)
		, na2 = 0b00100000
		, na3 = 0b00010000
		, na4 = 0b00001000
		, na5 = 0b00000100
		, na6 = 0b00000010
		, no2 = 0b11000000 // (c & no2) == no2
		, no3 = 0b11100000
		, no4 = 0b11110000
		, no5 = 0b11111000
		, no6 = 0b11111100
	};

	char cp[6] = {}; // code point
//	std::uint32_t cp;

public:

	static unsigned is_continuation(char c)
	{
		return (c & 0b10000000) && !(c & 0b01000000);
	}

	static unsigned size(const unsigned char c)
	{
//		if(!(c & na1))
//			return 1;
//		if(!(c & na2) && ((c & no2) == no2))
//			return 2;
//		if(!(c & na3) && ((c & no3) == no3))
//			return 3;
//		if(!(c & na4) && ((c & no4) == no4))
//			return 4;
//		if(!(c & na5) && ((c & no5) == no5))
//			return 5;
//		if(!(c & na6) && ((c & no6) == no6))
//			return 6;
//		return 0;
		constexpr static const unsigned char u8char_size[] =
		{
			  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
			, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
			, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
			, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
			, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
			, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
			, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
			, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
			, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
			, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3
			, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 0, 0
		};
		return u8char_size[c];
	}

public:
	constexpr u8char() noexcept {}

//	u8char(char32_t c32) noexcept { ???? }
	/*explicit*/ u8char(const char* cp) noexcept { assign(cp); }
	explicit u8char(char c) noexcept { assign(c); }
	explicit constexpr u8char(char const(&cp)[6]) noexcept
	{
		this->cp[0] = cp[0];
		this->cp[1] = cp[1];
		this->cp[2] = cp[2];
		this->cp[3] = cp[3];
		this->cp[4] = cp[4];
		this->cp[5] = cp[5];
	}
	u8char(const u8char& u8c) noexcept { assign(u8c); }

	// static utils

	static unsigned size(const char* cp)
	{
		return size((unsigned char)*cp);
	}

	static char32_t char32(const char* cp)
	{
		auto sz = u8char::size(cp);

		if(sz == 1)
			return (unsigned char)*cp;

		auto c32 = char32_t((0b01111111 >> sz) & (*cp));

		for(unsigned i = 1; i < sz; ++i)
			c32 = (c32 << 6) | (cp[i] & 0b0011'1111);

		return c32;
	}

	static unsigned char16(const char* cp, char16_pair& cp16)
	{
		char32_t c32 = u8char::char32(cp);

		if(c32 < 0xD800 || (c32 > 0xDFFF && c32 < 0x10000))
		{
			cp16[0] = char16_t(c32);
			cp16[1] = 0;
			return 1;
		}

		c32 -= 0x010000;

		cp16[0] = char16_t(((0b1111'1111'1100'0000'0000 & c32) >> 10) + 0xD800);
		cp16[1] = char16_t(((0b0000'0000'0011'1111'1111 & c32) >> 00) + 0xDC00);

		return 2;
	}

	void assign(char c) noexcept { *cp = c; }
	void assign(const char* cp) noexcept { strncpy(this->cp, cp, size(cp)); }
	void assign(const u8char& u8c) noexcept { assign(u8c.data()); }

	unsigned size() const { return size(cp); }

	const char* data() const { return cp; }
	std::string string() const { return {cp, size()}; }
	char32_t char32() const { return char32(cp); }
	unsigned char16(char16_pair& cp16) const { return char16(cp, cp16); }

	bool operator==(const u8char& u8c) const { return strncmp(u8c.cp, cp, size()); }
	bool operator!=(const u8char& u8c) const { return !(*this == u8c); }

	u8char& operator=(const char* cp) noexcept { assign(cp); return *this; }
	u8char& operator=(const u8char& u8c) noexcept { assign(u8c); return *this; }
	u8char& operator=(const std::string& s) noexcept { assign(s.data()); return *this; }

	char* begin() { return cp; }
	char* end() { return cp + size(cp); }

	const char* begin() const { return cp; }
	const char* end() const { return cp + size(cp); }
};

namespace utf8 { namespace { constexpr u8char BOM = u8char({'\xEF', '\xBB', '\xBF'}); }}

class u8string
{
public:
	using container = std::string;
	using container_ref = container&;
	using container_const_ref = container const&;
	using iterator = container::iterator;
	using const_iterator = container::const_iterator;
	using size_type = container::size_type;

	constexpr static const size_type npos = container::npos;

private:
	container utf8;

	template<typename ContainerRef, typename Iterator>
	class u8char_proxy
	{
		ContainerRef c;
		Iterator i;

	public:
		u8char_proxy(ContainerRef c, Iterator i): c(c), i(i) {}
		u8char_proxy(const u8char_proxy& u8p): c(u8p.c), i(u8p.i) {}

		u8char_proxy& operator=(u8char u8c)
		{
			iterator e = i + u8char::size(*i);
			c.replace(i, e, u8c.begin(), u8c.end());
			return *this;
		}

		operator u8char() const { return u8char(&*i); }

		size_type size() const { return u8char::size(*i); }
		std::string string() const { return {&*i, &*(i + size())}; }
		char32_t char32() const { return u8char::char32(&*i); }
	};

	template<typename ContainerRef, typename Iterator>
	class UTF8StringIterator
	{
		friend class u8string;
		ContainerRef c;
		Iterator i;

		static u8char& get_u8c(const u8char& u8c)
		{
			thread_local static u8char local_u8c;
			local_u8c = u8c;
			return local_u8c;
		}

		static auto& get_u8c_proxy(ContainerRef c, Iterator i)
		{
			thread_local static u8char_proxy<ContainerRef, Iterator> local_u8c_proxy;
			local_u8c_proxy = u8char_proxy<ContainerRef, Iterator>(c, i);
			return local_u8c_proxy;
		}

		void increment()
		{
			i += u8char::size((unsigned char)*i);
		}

		void decrement()
		{
			--i;
			while(u8char::is_continuation(*i))
				--i;
		}

	public:
		UTF8StringIterator(ContainerRef c, Iterator i): c(c), i(i) {}

		bool operator==(const UTF8StringIterator& i) const { return this->i == i.i; }
		bool operator!=(const UTF8StringIterator& i) const { return !(*this == i); }

		auto operator*() { return u8char_proxy<ContainerRef, Iterator>(c, i); }
		auto* operator->() { return &get_u8c_proxy(c, i); }

		u8char operator*() const { return u8char(&*i); }
		u8char* operator->() const { return &get_u8c(&*i); }

		UTF8StringIterator& operator++() { increment(); return *this; }
		UTF8StringIterator operator++(int) { UTF8StringIterator n = i; increment(); return n; }

		UTF8StringIterator& operator--() { decrement(); return *this; }
		UTF8StringIterator operator--(int) { UTF8StringIterator n = i; decrement(); return n; }
	};

	// number of utf8 characters
	static size_type size(const char* s)
	{
		size_type n = 0;
		for(; *s; s += u8char::size(s))
			++n;
		return n;
	}

public:
	u8string() {}
	u8string(const char* s): utf8(s, s + strlen(s)) {}
	u8string(const std::string& s): utf8(s.begin(), s.end()) {}

	auto begin() { return UTF8StringIterator<container_ref, iterator>(utf8, utf8.begin()); }
	auto end() { return UTF8StringIterator<container_ref, iterator>(utf8, utf8.end()); }

	auto begin() const { return UTF8StringIterator<container_const_ref, const_iterator>(utf8, utf8.begin()); }
	auto end() const { return UTF8StringIterator<container_const_ref, const_iterator>(utf8, utf8.end()); }

	auto cbegin() const { return UTF8StringIterator<container_const_ref, const_iterator>(utf8, utf8.begin()); }
	auto cend() const { return UTF8StringIterator<container_const_ref, const_iterator>(utf8, utf8.end()); }

	auto operator[](std::size_t n)
	{
		auto i = begin();

		while(n--)
			++i;

		return u8char_proxy<container_ref, iterator>(utf8, i.i);
	}

	u8char operator[](std::size_t n) const
	{
		auto i = begin();

		while(n--)
			++i;

		return *i;
	}

	std::string string() const { return {utf8.begin(), utf8.end()}; }
	std::u16string u16string() const
	{
		std::u16string u16;

//		for(auto i = cbegin(); i != cend(); ++i)
//		{
//			auto x = *i;
//		}

//		throw std::runtime_error("Not implemented: u8string::u16string()");
		return u16;
	}
	std::u32string u32string() const
	{
		std::u32string utf32;

		for(auto&& u8c: *this)
			utf32 += u8c.char32();

		return utf32;
	}

	// number of utf8 characters
	std::size_t size() const { return utf8.empty() ? 0 : size(utf8.data()); }

	struct report
	{
		bool valid = false;
		std::size_t pos = 0; // error pos (bytes)
		std::string msg; // error message

		operator bool() const { return valid; }
	};

	report valid() const
	{
		unsigned sz;
		for(auto i = utf8.begin(); i != utf8.end(); ++i)
		{
			if(!(sz = u8char::size((unsigned char)*i)))
				return {false, std::size_t(std::distance(utf8.begin(), i)), "bad base code"};

			while(--sz && ++i != utf8.end())
				if(!u8char::is_continuation(*i))
					return {false, std::size_t(std::distance(utf8.begin(), i)), "bad continuation code"};

			if(sz)
				return {false, std::size_t(std::distance(utf8.begin(), i)), "final code point too short"};
		}
		return {true, {}, {}};
	}

	size_type find_first_not_of(const char* s, size_type pos, size_type count) const;
//	size_type find_first_not_of( const basic_string& str, size_type pos = 0 ) const;
//	size_type find_first_not_of( const CharT* s, size_type pos = 0 ) const;
//	size_type find_first_not_of( CharT ch, size_type pos = 0 ) const;

	static const char* private_find(const char* haystack_begin_cp
		, const char* haystack_end_cp, const char* needle_cp)
	{
//		return std::search(haystack_begin_cp, haystack_end_cp, needle_cp, needle_cp + u8char::size(needle_cp));
		return {};
	}
};

u8string::size_type u8string::find_first_not_of(const char* s, size_type pos, size_type count) const
{
//	for(; pos < size(); ++pos)
//		if(private_find(s, ))
//			return pos;
	return npos;
}

} // namespace unicode_utils
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_U8STRING_H
