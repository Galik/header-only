#ifndef HEADER_ONLY_LIBRARY_BASIC_UTILS_H
#define HEADER_ONLY_LIBRARY_BASIC_UTILS_H

namespace header_only_library {
namespace basic_utils {
namespace idx {

template<typename T>
constexpr typename T::size_type zero(T const&) { return 0; }

template<typename T, std::size_t N>
constexpr std::size_t zero(T(&)[N]) { return 0; }

template<typename T>
constexpr typename T::size_type size(T const& c) { return c.size(); }

template<typename T, std::size_t N>
constexpr std::size_t size(T(&a)[N]) { return sizeof(a)/sizeof(a[0]); }

} // idx

template<std::ptrdiff_t MIN, std::ptrdiff_t MAX = MIN>
class subscript
{
public:
	using index_type = std::ptrdiff_t;

	subscript() noexcept: n(0) {}
	subscript(index_type n): n(validate(n)) {}

	subscript(subscript const& idx) noexcept: n(idx.n) {}
	subscript& operator=(subscript const& idx) noexcept { n = idx.n; return *this; }

	subscript& operator+=(subscript const& idx) { n = validate(n + idx.n); return *this; }
	subscript& operator-=(subscript const& idx) { n = validate(n - idx.n); return *this; }
	subscript& operator*=(subscript const& idx) { n = validate(n * idx.n); return *this; }
	subscript& operator/=(subscript const& idx) { n = validate(n / idx.n); return *this; }

	subscript operator+(subscript const& idx) { return subscript(validate(n + idx.n)); }
	subscript operator-(subscript const& idx) { return subscript(validate(n - idx.n)); }
	subscript operator*(subscript const& idx) { return subscript(validate(n * idx.n)); }
	subscript operator/(subscript const& idx) { return subscript(validate(n / idx.n)); }

	subscript& operator=(index_type n) { this->n = validate(n); return *this; }

	subscript& operator+=(index_type n) { this->n = validate(this->n + n); return *this; }
	subscript& operator-=(index_type n) { this->n = validate(this->n - n); return *this; }
	subscript& operator*=(index_type n) { this->n = validate(this->n * n); return *this; }
	subscript& operator/=(index_type n) { this->n = validate(this->n / n); return *this; }

	subscript operator+(index_type n) { return subscript(validate(this->n + n)); }
	subscript operator-(index_type n) { return subscript(validate(this->n - n)); }
	subscript operator*(index_type n) { return subscript(validate(this->n * n)); }
	subscript operator/(index_type n) { return subscript(validate(this->n / n)); }

	subscript& operator++() { n = validate(n + 1); return *this; }
	subscript operator++(int) { subscript idx(n); ++(*this); return idx; }

	subscript& operator--() { n = validate(n - 1); return *this; }
	subscript operator--(int) { subscript idx(n); --(*this); return idx; }

	operator index_type() const { return n; }

	explicit operator bool() const { return n >= MIN && n < MAX; }

private:
	index_type validate(index_type n) const
		{ if(n < MIN || n > MAX) throw std::out_of_range(std::to_string(n)); return n; }

	std::ptrdiff_t n;
};

template<std::size_t N>
using index_type = subscript<0, N>;

//template<typename T, std::size_t N>
//class array_type
//{
//public:
//	using index_type = header_only_library::basic_utils::index_type<N>;
////	using index_type = subscript<0, N>;
//
//	T& operator[](index_type n) { return a[n]; }
//	T const& operator[](index_type n) const { return a[n]; }
//
//	index_type zero() const { return index_type(0); }
//	index_type size() const { return index_type(N); }
//
//private:
//	T a[N];
//};

template<typename Struct, typename T>
struct find_by_member_type
{
	   T Struct::* mp;
	   T v;
	   find_by_member_type(T Struct::* mp, T const& v): mp(mp), v(v) {}
	   bool operator()(Struct const& s) const { return (s.*mp) == v; }
};

template<typename Struct, typename T>
find_by_member_type<Struct, T> find_by_member(T Struct::* mp, T const& v)
{
	return find_by_member_type<Struct, T>(mp, v);
}

// USAGE:

//	class Car
//	{
//	public:
//		std::string name;
//		int age;
//		std::string manufacturer;
//	};
//
//	void print(const Car& car)
//	{
//		std::cout << "name        : " << car.name << '\n';
//		std::cout << "age         : " << car.age << '\n';
//		std::cout << "manufacturer: " << car.manufacturer << '\n';
//		std::cout << '\n';
//	}
//
//	int main()
//	{
//		std::vector<Car> cars;
//
//		cars.emplace_back();
//		cars.back().name = "Fiesta";
//		cars.back().age = 4;
//		cars.back().manufacturer = "Ford";
//
//		cars.emplace_back();
//		cars.back().name = "Capri";
//		cars.back().age = 2;
//		cars.back().manufacturer = "Ford";
//
//		cars.emplace_back();
//		cars.back().name = "Focus";
//		cars.back().age = 1;
//		cars.back().manufacturer = "Ford";
//
//		std::cout << "Input Order\n\n";
//
//		for(const auto& car: cars)
//			print(car);
//
//		std::cout << "Order by name\n\n";
//
//		std::sort(cars.begin(), cars.end(), sort_by_member(&Car::name));
//
//		for(const auto& car: cars)
//			print(car);
//
//		std::cout << "Order by age\n\n";
//
//		std::sort(cars.begin(), cars.end(), sort_by_member(&Car::age));
//
//		for(const auto& car: cars)
//			print(car);
//
//		std::cout << "Order by age [reverse]\n\n";
//
//		std::sort(cars.begin(), cars.end(), sort_by_member(&Car::age, std::greater<int>()));
//
//		for(const auto& car: cars)
//			print(car);
//
//		std::cout << "Order by age in a std::set\n\n";
//
//		std::set<Car, decltype(sort_by_member(&Car::age))> cset(sort_by_member(&Car::age));
//
//		for(const auto& car: cars)
//			cset.insert(car);
//
//		for(const auto& car: cset)
//			print(car);
//	}


/**
 * Generic comparator for class member variables
 */
template<typename CType, typename MType, typename Comp = std::less<MType>>
struct sort_by_member_type
{
	MType CType::* member;

	sort_by_member_type(MType CType::* member): member(member) {}

	bool operator()(const CType& l, const CType& r) const
	{
		return Comp()(l.*member, r.*member);
	}
};

/**
 * Convenient class member comparator creator
 * @param member Class address of member variable (&Class::member)
 * @return a suitable comparator
 */
template<typename CType, typename MType, typename Comp = std::less<MType>>
auto sort_by_member(MType CType::* member, Comp = Comp())
-> sort_by_member_type<CType, MType, Comp>
{
	return sort_by_member_type<CType, MType, Comp>(member);
}

} // basic_utils
} // header_only_library

#endif // HEADER_ONLY_LIBRARY_BASIC_UTILS_H
