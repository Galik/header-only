#ifndef HEADER_ONLY_LIBRARY_PROC_UTILS_H
#define HEADER_ONLY_LIBRARY_PROC_UTILS_H

#include <fstream>
#include <map>
#include <string>

#include "../macro_exceptions.h"
#include "../string_utils.h"

namespace header_only_library {
namespace proc_utils {
//namespace detail {
//
// /proc/path/leaf -> ""
//inline
//std::unordered_map<std::string, std::string>& leaf_cache()
//{
//	thread_local static std::unordered_map<std::string, std::string> m;
//	return m;
//}
//
//inline
//void clear_cache()
//{
//	std::unordered_map<std::string, std::string>().swap(leaf_cache());
//}
//
//inline
//std::string cache(std::string const& key)
//{
//	std::string val;
//
//	if(auto ifs = std::ifstream(key))
//	{
//		std::ostringstream oss;
//		if(oss << ifs.rdbuf());
//			val = oss.str();
//	}
//
//	if(!val.empty())
//		leaf_cache()[key] = val;
//
//	return val;
//}
//
//inline
//std::string get_cached(std::string const& key)
//{
//	return leaf_cache()[key];
//}
//
//} // namespace detail
//
//inline
//std::size_t convert_units(std::string const& units)
//{
//	if(units.empty())
//		return 1;
//	else if(units == "kB")
//		return 1024;
//	else if(units == "MB")
//		return 1024 * 1024;
//	else if(units == "GB")
//		return 1024 * 1024 * 1024;
//	return 1;
//}
//
//inline
//std::size_t meminfo(std::string const& key)
//{
////	thread_local static std::ifstream ifs("/proc/meminfo");
//
//	std::string k; // key
//	std::size_t v; // value
//	std::string u; // units
//
//	std::istringstream iss(detail::get_cached("/proc/meminfo"));
//
//	while(iss >> k >> v >> u)
//		if(key == k)
//			return v * convert_units(u);
//
//	throw std::runtime_error("/proc/meminfo: key not found: " + key);
//
//	return {};
//}
//
//inline
//std::vector<std::string> meminfo_read_syms()
//{
//	std::vector<std::string> syms;
//	std::ifstream ifs("/proc/meminfo");
//
//	std::string k; // key
//	std::size_t v; // value
//	std::string u; // units
//
//	while(ifs >> k >> v >> u)
//		syms.push_back(k);
//
//	return syms;
//}
//
//inline
//std::vector<std::string>& meminfo_syms(bool recache = false)
//{
//	thread_local static std::vector<std::string> syms = meminfo_read_syms();
//
//	if(recache)
//		syms = meminfo_read_syms();
//
//	return syms;
//}
//
//inline
//std::size_t cached_proc_meminfo(std::size_t key)
//{
//	thread_local static std::map<std::size_t, std::size_t> cache;
//
//	auto found = cache.find(key);
//
//	if(found == cache.end())
//	{
//		auto value = meminfo(proc_meminfo_syms()[key]);
//		cache.emplace(key, value);
//		return value;
//	}
//
//	return found->second;
//}

class proc_reader
{
public:

	std::string read_from_cache(std::string const& path, std::string const& key)
	{
		if(auto found_path = m_cache.find(path); found_path == std::end(m_cache))
			return read_to_cache(path, key);
		else
		{
			if(auto found_item = found_path->second.find(key); found_item == std::end(found_path->second))
				return read_to_cache(path, key);
			else
				return found_item->second;
		}
	}

	void clear() { decltype(m_cache)().swap(m_cache); }

	void clear(std::string const& path)
	{
		if(auto found = m_cache.find(path); found != std::end(m_cache))
			decltype(found->second)().swap(found->second);
	}

	void clear(std::string const& path, std::string const& key)
	{
		if(auto found_path = m_cache.find(path); found_path != std::end(m_cache))
			if(auto found_item = found_path->second.find(key); found_item == std::end(found_path->second))
				found_path->second.erase(found_item);
	}

	std::string read_live(std::string const& path, std::string const& key)
	{
		std::ifstream ifs(path);

		if(!ifs)
			hol_throw_errno_msg(path);

		for(std::string line; std::getline(ifs, line);)
		{
			if(line.find(key))
				continue;

			return string_utils::trim_copy(line.substr(key.size()));
		}

		hol_throw_runtime_error("proc_reader: no such key: " << path << ": " << key);

		return {};
	}

private:
	std::string read_to_cache(std::string const& path, std::string const& key)
	{
		auto v = read_live(path, key); // maybe exception
		return m_cache[path][key] = std::move(v); // so do this only of v obtained
	}

	std::map<std::string, std::map<std::string, std::string>> m_cache;
};

} // namespace proc_utils
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_PROC_UTILS_H
