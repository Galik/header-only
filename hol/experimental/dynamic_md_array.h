#ifndef HEADER_ONLY_LIBRARY_CONTAINERS_DYNAMIC_MD_ARRAY_H
#define HEADER_ONLY_LIBRARY_CONTAINERS_DYNAMIC_MD_ARRAY_H
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <algorithm>
#include <numeric>
#include <cassert>
#include <memory>
#include <type_traits>
#include <vector>

namespace header_only_library {
namespace containers {

template<typename T>
class dynamic_md_array
{
public:
	using size_type = std::size_t;
	using value_type = T;
	using pointer = value_type*;
	using reference = value_type&;
	using const_pointer = std::remove_cv_t<value_type> const*;
	using const_reference = std::remove_cv_t<value_type> const&;
	using iterator = pointer;
	using const_iterator = const_pointer;

	template<typename... Dims>
	dynamic_md_array(Dims... dims)
		: m_dims({size_type(dims)...}), m_data(internal_multiply(dims...)) {}

	dynamic_md_array(dynamic_md_array const& other)
		: m_dims(other.m_dims), m_data(other.m_data) {}

	dynamic_md_array(dynamic_md_array&& other)
		: m_dims(std::move(other.m_dims)), m_data(std::move(other.m_data)) {}

	dynamic_md_array& operator=(dynamic_md_array const& other)
	{
		m_dims = other.m_dims;
		m_data = other.m_data;
		return *this;
	}

	dynamic_md_array& operator=(dynamic_md_array&& other)
	{
		m_dims = std::move(other.m_dims);
		m_data = std::move(other.m_data);
	}

	constexpr size_type size() const { return total_size(); }

	constexpr size_type size(size_type dim) const
	{
		assert(dim < m_dims.size());
		return m_dims[dim];
	}

	constexpr size_type dimensions() const { return m_dims.size(); }

	template<typename Dim>
	constexpr size_type locate_offset(Dim dim) const { return dim; }

	template<typename Dim, typename... Dims>
	constexpr size_type locate_offset(Dim dim, Dims... dims) const
	{
		return  dim * total_size(m_dims.size() - sizeof...(dims)) + locate_offset(dims...);
	}

	template<typename... Dims>
	reference operator()(Dims... dims)
	{
		assert(sizeof...(dims) == m_dims.size());
		return m_data[locate_offset(dims...)];
	}

	pointer       operator[](size_type x)       { return &m_data[total_size(1) * x]; }
	const_pointer operator[](size_type x) const { return &m_data[total_size(1) * x]; }

private:

	constexpr size_type total_size(size_type dim = 0) const
	{
		assert(dim < m_dims.size());
		return std::accumulate(std::begin(m_dims) + dim, std::end(m_dims),
			size_type(1), std::multiplies<size_type>());
	}

	static constexpr size_type internal_multiply() { return 1; }

	template<typename Dim, typename... Dims>
	static constexpr size_type internal_multiply(Dim dim, Dims... dims)
	{
		return dim * internal_multiply(dims...);
	}

	std::vector<size_type> m_dims;
	std::vector<value_type> m_data;
};

// other stuff

template<typename T>
class two_dee_array
{
public:
	using size_type = std::size_t;

	two_dee_array(size_type rows, size_type cols)
		: v(rows * cols), stride(cols) {}

	T* operator[](size_type row) { return &(*this)(row, 0); }

	T const* operator[](size_type row) const { return &(*this)(row, 0); }

	T& operator()(size_type row, size_type col)
		{ return v[(row * stride) + col]; }

	T const& operator()(size_type row, size_type col) const
		{ return v[(row * stride) + col]; }

	size_type col_size() const { return stride; }
	size_type row_size() const { return v.size() / stride; }

	auto begin() { return std::begin(v); }
	auto end() { return std::end(v); }

	auto begin() const { return std::begin(v); }
	auto end() const { return std::end(v); }

	auto cbegin() const { return std::cbegin(v); }
	auto cend() const { return std::cend(v); }

private:
	std::vector<T> v;
	size_type stride;
};

template<typename T>
class three_dee_array
{
public:
	using size_type = std::size_t;

	three_dee_array(std::size_t lays, std::size_t rows, std::size_t cols)
	: m_maj_stride(rows * cols), m_min_stride(cols), m_data(lays * rows * cols) {}

	size_type size() const { return m_data.size(); }
	size_type lays() const { return size() / m_maj_stride; }
	size_type rows() const { return m_maj_stride / m_min_stride; }
	size_type cols() const { return m_min_stride; }

	size_type index(size_type lay, size_type row, size_type col) const
	{
		HOL_ASSERT(lay < lays());
		HOL_ASSERT(row < rows());
		HOL_ASSERT(col < cols());
		return (lay * m_maj_stride) + (row * m_min_stride) + col;
	}

	T* data() { return m_data.data(); }
	T const* data() const { return m_data.data(); }

	T& operator()(size_type lay, size_type row, size_type col)
		{ return m_data[index(lay, row, col)]; }

	T const& operator()(size_type lay, size_type row, size_type col) const
		{ return m_data[index(lay, row, col)]; }

	std::vector<T>& vector() { return m_data; }
	std::vector<T> const& vector() const { return m_data; }

	std::vector<T> copy_data() const { return m_data; }

	void replace_data(std::vector<T> const& v)
	{
		auto size = m_data.size();
		m_data = v;
		m_data.resize(size);
	}

	void replace_data(std::vector<T>&& v)
	{
		auto size = m_data.size();
		m_data = std::move(v);
		m_data.resize(size);
	}

	explicit operator bool() const { return !m_data.empty(); }

	std::vector<T> remove_data()
	{
		std::vector<T> r(std::move(m_data));
		m_data.clear(0);
		return r;
	}

private:
	size_type const m_maj_stride = 0;
	size_type const m_min_stride = 0;
	std::vector<T> m_data;
};

} // namespace containers
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_CONTAINERS_DYNAMIC_MD_ARRAY_H
