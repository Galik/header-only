#ifndef HEADER_ONLY_LIBRARY_EXPERIMENTAL_UNDO_H
#define HEADER_ONLY_LIBRARY_EXPERIMENTAL_UNDO_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <stack>
#include <memory>

namespace hol {
namespace raii {
namespace undo {

class undo_type
{
public:
	virtual ~undo_type() noexcept = default;
};

using undo_ptr = std::unique_ptr<undo_type>;

template <class Func>
class undo_item
: public undo_type
{
public:
    explicit undo_item(Func func) noexcept
	: func(std::move(func)), unvoke(true) {}

    undo_item(undo_item&& other) noexcept
    : func(std::move(other.func)), unvoke(other.unvoke)
		{ other.unvoke = false; }

    undo_item(const undo_item&) = delete;
    undo_item& operator=(const undo_item&) = delete;

    ~undo_item() noexcept
    	{ if(unvoke) func(); }

private:
    Func func;
    bool unvoke;
};

template <class Func>
inline undo_ptr undo_it_with(const Func& func) noexcept
{
    return std::make_unique<undo_item<Func>>(func);
}

template <class Func>
inline undo_ptr undo_it_with(Func&& func) noexcept
{
    return std::make_unique<undo_item<Func>>(std::forward<Func>(func));
}

class undo_list
{
public:
	undo_list() {}

	undo_list(undo_list const&) = delete;
	undo_list(undo_list&& u): undos(std::move(u.undos)) {}

	undo_list(undo_ptr uptr)
		{ undos.push(std::move(uptr)); }

	undo_list& operator=(undo_list const&) = delete;
	undo_list& operator=(undo_list&& u)
		{ undos.swap(u.undos); return *this; }

	void operator+=(undo_ptr uptr)
		{ undos.push(std::move(uptr)); }

	void clear()
	{
		while(!undos.empty())
		{
			undos.top().release();
			undos.pop();
		}
	}

	~undo_list()
	{
		while(!undos.empty())
			undos.pop();
	}

private:
	std::stack<undo_ptr> undos;
};

//
// EXAMPLE USE
//
//std::tuple<SSL_CTX*, SSL*> connect_with_SSL(int fd)
//{
//
//	auto ctx = gsl::not_null<SSL_CTX*>(SSL_CTX_new(SSLv23_client_method()));
//
//	undo_list undo = undo_it_with([=]{SSL_CTX_free(ctx);});
//
//	auto ssl = gsl::not_null<SSL*>(SSL_new(ctx));
//
//	undo += undo_it_with([=]{SSL_free(ssl);});
//
//	if(!SSL_set_fd(ssl, fd))
//		throw std::runtime_error(ERR_error_string(ERR_get_error(), nullptr));
//
//	if(SSL_connect(ssl) < 1)
//		throw std::runtime_error(ERR_error_string(ERR_get_error(), nullptr));
//
//	undo.clear();
//
//	return std::make_tuple(ctx, ssl);
//}

} // undo
} // raii
} // hol

#endif // HEADER_ONLY_LIBRARY_EXPERIMENTAL_UNDO_H
