#ifndef HEADER_ONLY_LIBRARY_ARGS_H
#define HEADER_ONLY_LIBRARY_ARGS_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <string>
#include <vector>
#include <sstream>

namespace header_only_library {
namespace arg_utils {

class Args
{
public:
	using arg_ptr = char const* const*;
	std::string error;

	Args(const Args& args): error(args.error), arg(args.arg) {}
private:
	arg_ptr arg;

public:
	Args(arg_ptr arg): arg(arg) {}

	operator bool() const { return *arg; }
	Args& operator++() { ++arg; return *this; }
	Args operator++(int) { Args arg(*this); ++arg; return arg; }

	// Experimental untested API (self-iterator)
	Args begin() { return *this; }
	Args end() { arg_ptr end = arg; while(*end) ++end; return Args(end); }
	Args& operator*() { return *this; }
	bool operator==(const Args& args) const { return arg == args.arg; }
	// experimental

	bool test(const std::string& opt)
	{
		return opt == *arg;
	}

	template<typename Type>
	bool get(const std::string& opt, Type& out)
	{
		if(test(opt))
		{
			if(!*(++arg))
			{
				error = "option " + opt + " requires a parameter.";
				return false;
			}
			if(!(std::istringstream(*arg) >> std::boolalpha >> out))
			{
				error = "bad parameter to option " + opt + ".";
				return false;
			}
		}
		return true;
	}

	bool test(const std::string& lopt, const std::string& sopt)
	{
		return test(lopt) || test(sopt);
	}

	template<typename Type>
	bool get(const std::string& lopt, const std::string& sopt, Type& out)
	{
		if(test(lopt))
			return get(lopt, out);
		else if(test(sopt))
			return get(sopt, out);
		return true;
	}
};

/**
 * Example usage
 */
//int main(int argc, char* argv[])
//{
//	(void) argc;
//	std::string lang = "en";
//
//	for(Args args(argv); args; ++args)
//	{
//		if(args.test("--help", "-h"))
//		{
//			usage(argv[0]);
//			return 0;
//		}
//		else if(!args.get("--lang", "-l", lang))
//		{
//			con("ERROR: " << args.error);
//			usage(argv[0]);
//			return 1;
//		}
//	}
//}

/**
 * Tool to make (and take ownership of) an argv
 * structure for passing to sub-processes etc...
 */
class ArgMaker
{

	using chr_vec = std::vector<char>;
	using chr_vec_vec = std::vector<chr_vec>;
	using arg_vec = std::vector<char*>;

	arg_vec args;
	chr_vec_vec data;

	chr_vec make_chr_vec(const std::string& s)
	{
		chr_vec cv{std::begin(s), std::end(s)};
		cv.push_back('\0');
		return cv;
	}

public:

	void clear() { args.clear(); data.clear(); }

	void add_arg(const std::string& s)
	{
		data.emplace_back(make_chr_vec(s));
	}

	char** argv()
	{
		args.clear();
		for(auto&& arg: data)
			args.push_back(arg.data());
		return args.data();
	}
};

class argiter
{
public:
	using arg_ptr = char const* const*;

	argiter(arg_ptr argv) noexcept
	: arg(argv), arg_end(argv + length(argv)) {}

	std::size_t length(arg_ptr arg)
	{
		std::size_t n = 0;
		while(*arg++)
			n++;
		return n;
	}

	arg_ptr begin() const { return arg; }
	arg_ptr end()   const { return arg_end; }

private:
	arg_ptr const arg;
	arg_ptr const arg_end;
};

// =====================================================

template<typename Var>
void set_from_arg(char const* arg, Var& v)
{
	if(!(std::istringstream(arg) >> v))
		throw std::runtime_error("cannot convert argument: " + std::string(arg));
}

template<>
void set_from_arg<std::string>(char const* arg, std::string& v)
{
	v = arg;
}

inline void set_from_argv(char const* const*) {}

template<typename Var, typename... Vars>
void set_from_argv(char const* const* argv, Var& var, Vars&... vars)
{
	if(*argv)
	{
		set_from_arg(*argv, var);
		set_from_argv(argv + 1, vars...);
	}
}

} // namespace arg_utils
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_ARGS_H
