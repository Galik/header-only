#ifndef HEADER_ONLY_LIBRARY_ANSI_TERM_CODES_H
#define HEADER_ONLY_LIBRARY_ANSI_TERM_CODES_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

//std::istringstream cols(R"(
// 0 #000000  1 #800000  2 #008000  3 #808000  4 #000080  5 #800080  6 #008080  7 #c0c0c0
// 8 #808080  9 #ff0000 10 #00ff00 11 #ffff00 12 #0000ff 13 #ff00ff 14 #00ffff 15 #ffffff
// 16 #000000  17 #00005f  18 #000087  19 #0000af  20 #0000d7  21 #0000ff
// 22 #005f00  23 #005f5f  24 #005f87  25 #005faf  26 #005fd7  27 #005fff
// 28 #008700  29 #00875f  30 #008787  31 #0087af  32 #0087d7  33 #0087ff
// 34 #00af00  35 #00af5f  36 #00af87  37 #00afaf  38 #00afd7  39 #00afff
// 40 #00d700  41 #00d75f  42 #00d787  43 #00d7af  44 #00d7d7  45 #00d7ff
// 46 #00ff00  47 #00ff5f  48 #00ff87  49 #00ffaf  50 #00ffd7  51 #00ffff
// 52 #5f0000  53 #5f005f  54 #5f0087  55 #5f00af  56 #5f00d7  57 #5f00ff
// 58 #5f5f00  59 #5f5f5f  60 #5f5f87  61 #5f5faf  62 #5f5fd7  63 #5f5fff
// 64 #5f8700  65 #5f875f  66 #5f8787  67 #5f87af  68 #5f87d7  69 #5f87ff
// 70 #5faf00  71 #5faf5f  72 #5faf87  73 #5fafaf  74 #5fafd7  75 #5fafff
// 76 #5fd700  77 #5fd75f  78 #5fd787  79 #5fd7af  80 #5fd7d7  81 #5fd7ff
// 82 #5fff00  83 #5fff5f  84 #5fff87  85 #5fffaf  86 #5fffd7  87 #5fffff
// 88 #870000  89 #87005f  90 #870087  91 #8700af  92 #8700d7  93 #8700ff
// 94 #875f00  95 #875f5f  96 #875f87  97 #875faf  98 #875fd7  99 #875fff
//100 #878700 101 #87875f 102 #878787 103 #8787af 104 #8787d7 105 #8787ff
//106 #87af00 107 #87af5f 108 #87af87 109 #87afaf 110 #87afd7 111 #87afff
//112 #87d700 113 #87d75f 114 #87d787 115 #87d7af 116 #87d7d7 117 #87d7ff
//118 #87ff00 119 #87ff5f 120 #87ff87 121 #87ffaf 122 #87ffd7 123 #87ffff
//124 #af0000 125 #af005f 126 #af0087 127 #af00af 128 #af00d7 129 #af00ff
//130 #af5f00 131 #af5f5f 132 #af5f87 133 #af5faf 134 #af5fd7 135 #af5fff
//136 #af8700 137 #af875f 138 #af8787 139 #af87af 140 #af87d7 141 #af87ff
//142 #afaf00 143 #afaf5f 144 #afaf87 145 #afafaf 146 #afafd7 147 #afafff
//148 #afd700 149 #afd75f 150 #afd787 151 #afd7af 152 #afd7d7 153 #afd7ff
//154 #afff00 155 #afff5f 156 #afff87 157 #afffaf 158 #afffd7 159 #afffff
//160 #d70000 161 #d7005f 162 #d70087 163 #d700af 164 #d700d7 165 #d700ff
//166 #d75f00 167 #d75f5f 168 #d75f87 169 #d75faf 170 #d75fd7 171 #d75fff
//172 #d78700 173 #d7875f 174 #d78787 175 #d787af 176 #d787d7 177 #d787ff
//178 #d7af00 179 #d7af5f 180 #d7af87 181 #d7afaf 182 #d7afd7 183 #d7afff
//184 #d7d700 185 #d7d75f 186 #d7d787 187 #d7d7af 188 #d7d7d7 189 #d7d7ff
//190 #d7ff00 191 #d7ff5f 192 #d7ff87 193 #d7ffaf 194 #d7ffd7 195 #d7ffff
//196 #ff0000 197 #ff005f 198 #ff0087 199 #ff00af 200 #ff00d7 201 #ff00ff
//202 #ff5f00 203 #ff5f5f 204 #ff5f87 205 #ff5faf 206 #ff5fd7 207 #ff5fff
//208 #ff8700 209 #ff875f 210 #ff8787 211 #ff87af 212 #ff87d7 213 #ff87ff
//214 #ffaf00 215 #ffaf5f 216 #ffaf87 217 #ffafaf 218 #ffafd7 219 #ffafff
//220 #ffd700 221 #ffd75f 222 #ffd787 223 #ffd7af 224 #ffd7d7 225 #ffd7ff
//226 #ffff00 227 #ffff5f 228 #ffff87 229 #ffffaf 230 #ffffd7 231 #ffffff
//232 #080808 233 #121212 234 #1c1c1c 235 #262626 236 #303030 237 #3a3a3a
//238 #444444 239 #4e4e4e 240 #585858 241 #606060 242 #666666 243 #767676
//244 #808080 245 #8a8a8a 246 #949494 247 #9e9e9e 248 #a8a8a8 249 #b2b2b2
//250 #bcbcbc 251 #c6c6c6 252 #d0d0d0 253 #dadada 254 #e4e4e4 255 #eeeeee
//)");
//

namespace header_only_library {
namespace term_utils {

const std::string ansi_color_terms[] =
{
	"xterm-256color"
};

inline
bool term_supports_color()
{
	std::string TERM = std::getenv("TERM") ? std::getenv("TERM") : "";
	auto found = std::find(std::begin(ansi_color_terms), std::end(ansi_color_terms), TERM);
	return found != std::end(ansi_color_terms);
}

namespace ansi {

const std::string ESC = "\033[";
const std::string CLS = ESC + "2J";
const std::string FGC = ESC + "3";
const std::string BGC = ESC + "4";
const std::string HOME = ESC + "1;1H";

inline std::string fgcol256(int col) { return "\033[38;5;" + std::to_string(col) + "m"; }
inline std::string bgcol256(int col) { return "\033[48;5;" + std::to_string(col) + "m"; }

enum COL
{
	black
	, red
	, green
	, yellow
	, blue
	, magenta
	, cyan
	, white
	, dflt
};

const std::vector<std::string> colors = { "0m", "1m", "2m", "3m", "4m", "5m", "6m", "7m", "8m" };

inline std::string fgcol(COL col) { return FGC + colors[col]; }
inline std::string bgcol(COL col) { return BGC + colors[col]; }
inline std::string posxy(unsigned x, unsigned y) { return ESC + std::to_string(y) + ";" + std::to_string(x) + "H"; }

const std::string norm = ESC + "0m";
const std::string col = ESC;

namespace color {
namespace FG {
	const std::string black = fgcol(COL::black);
	const std::string red = fgcol(COL::red);
	const std::string green = fgcol(COL::green);
	const std::string yellow = fgcol(COL::yellow);
	const std::string blue = fgcol(COL::blue);
	const std::string magenta = fgcol(COL::magenta);
	const std::string cyan = fgcol(COL::cyan);
	const std::string white = fgcol(COL::white);
	const std::string dflt = fgcol(COL::dflt);
} // FG
namespace BG {
	const std::string black = bgcol(COL::black);
	const std::string red = bgcol(COL::red);
	const std::string green = bgcol(COL::green);
	const std::string yellow = bgcol(COL::yellow);
	const std::string blue = bgcol(COL::blue);
	const std::string magenta = bgcol(COL::magenta);
	const std::string cyan = bgcol(COL::cyan);
	const std::string white = bgcol(COL::white);
	const std::string dflt = bgcol(COL::dflt);
} // FG
} // color

enum ANSI
{
	NORM = 0
	, BOLD_ON
	, FAINT_ON
	, ITALIC_ON
	, UNDERLINE_ON
	, BLINK_SLOW
	, BLINK_FAST
	, NEGATIVE
	, CONCEAL = 8
	, CONCEAL_ON = 8
	, STRIKE_ON // crossed out

	, FONT_0 = 10
	, FONT_1
	, FONT_2
	, FONT_3
	, FONT_4
	, FONT_5
	, FONT_6
	, FONT_7
	, FONT_8
	, FONT_9

	, X1 = 21
	, BOLD_OFF = 22
	, FAINT_OFF = 22
	, ITALIC_OFF
	, UNDERLINE_OFF
	, BLINK_OFF

	, POSITIVE = 27
	, REVEAL = 28
	, CONCEAL_OFF = 28
	, STRIKE_OFF // not crossed out

	, FG_BLACK = 30
	, FG_RED
	, FG_GREEN
	, FG_YELLOW
	, FG_BLUE
	, FG_MAGENTA
	, FG_CYAN
	, FG_WHITE

	, BG_BLACK = 40
	, BG_RED
	, BG_GREEN
	, BG_YELLOW
	, BG_BLUE
	, BG_MAGENTA
	, BG_CYAN
	, BG_WHITE
};

inline
std::string ansi_esc(std::initializer_list<int> codes)
{
	std::string esc = "\033[";

	std::string sep;
	for(int c: codes)
	{
		esc += sep + std::to_string(c);
		sep = ";";
	}

	return esc + "m";
}

inline
int code_to_int(char c)
{
	if(c >= '0' && c <= '9') return c - '0';
	if(c >= 'a' && c <= 'z') return (c - 'a') + 10;
	if(c >= 'A' && c <= 'Z') return (c - 'A') + 10;
	return 0;
}

static const std::string keys = "_*/~#";
static const int map[] =
{
	30, 31, 32, 33, 34, 35, 36, 37 // 0-7
	, 40, 41, 42, 43, 44, 45, 46, 47 // 8-F
	, 00, 00, ITALIC_ON, 00, 00, 00, 00, NEGATIVE
	, 00, POSITIVE, 00, 00, 00, 00, UNDERLINE_ON, 00
	, 00, 00, 00, 00
};

class ansi_fsm
{
private:
//	static const std::string keys;
//	static const int map[];

	bool active = true;
	bool esc, u, i, b, f;

public:

	ansi_fsm(bool active = true): active(active), esc(0), u(0), i(0), b(0), f(0) {}

	std::ostream& write(std::ostream& os, const char* buf, std::size_t len)
	{
		std::string str(buf, len);
		return write(os, str);
	}

	std::string operator()(std::string const& s)
	{
		std::ostringstream oss;
		write(oss, s);
		return oss.str();
	}

	std::ostream& write(std::ostream& os, const std::string& str)
	{
		for(char c: str)
		{
			if(c == '^')
			{
				if(esc)
					os << '^';
				esc = !esc;
			}
			else if(esc)
			{
				// 0-F
				if(keys.find(c) != std::string::npos)
				{
					os << c;
				}
				else
				{
					if(active)
						os << ansi_esc({map[code_to_int(c) % sizeof(map)]});
				}
				esc = false;
			}
			else if(c == '_')
			{
				if(active)
					os << (((u = !u)) ? ansi_esc({UNDERLINE_ON}) : ansi_esc({UNDERLINE_OFF}));
			}
			else if(c == '/')
			{
				if(active)
					os << (((i = !i)) ? ansi_esc({ITALIC_ON}) : ansi_esc({ITALIC_OFF}));
			}
			else if(c == '*')
			{
				if(active)
					os << (((b = !b)) ? ansi_esc({BOLD_ON}) : ansi_esc({BOLD_OFF}));
			}
			else if(c == '~')
			{
				if(active)
					os << (((f = !f)) ? ansi_esc({FAINT_ON}) : ansi_esc({FAINT_OFF}));
			}
			else if(c == '#')
			{
				if(active)
					os << norm;
			}
			else
			{
				os << c;
			}
		}

		return os;
	}
};

//const std::string ansi_fsm::keys = "_*/~#";
//const int ansi_fsm::map[] =
//{
//	30, 31, 32, 33, 34, 35, 36, 37 // 0-7
//	, 40, 41, 42, 43, 44, 45, 46, 47 // 8-F
//	, 00, 00, ITALIC_ON, 00, 00, 00, 00, NEGATIVE
//	, 00, POSITIVE, 00, 00, 00, 00, UNDERLINE_ON, 00
//	, 00, 00, 00, 00
//};

inline
void test()
{

//	std::string str = "0123456789ABCDefgh";
//
//	for(char c: str)
//		std::cout << c << ": " << code_to_int(c) << '\n';

	std::cout << ansi_esc({UNDERLINE_ON}) << '\n';

	ansi_fsm fsm;

	fsm.write(std::cout, "Hello ^2co^Ulor^0 and ^^ dull\n");


	fsm.write(std::cout, "Hello _^4under# ^2line# text_ and /italics/ with *bold* and ~faint~ ^^ dull\n");

//	int i;
//
//	std::cout << "Input: ";
//	while(!(std::cin >> i))
//	{
//		std::cin.clear();
//		std::cin.ignore(streamlimits::max(), '\n');
//		std::cout << "Input: ";
//	}
//
//	std::cout << "i: " << i << '\n';
}

// TERMINAL DIMENSIONS

//    winsize w;
//    ioctl(0, TIOCGWINSZ, &w);
//
//    std::cout << "lines: " << w.ws_row << '\n';
//    std::cout << "columns: " << w.ws_col << '\n';

} // ansi
} // term_utils
} // header_only_library

#endif // HEADER_ONLY_LIBRARY_ANSI_TERM_CODES_H
