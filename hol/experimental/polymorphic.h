#ifndef HEADER_ONLY_LIBRARY_POLYMORPHIC_H
#define HEADER_ONLY_LIBRARY_POLYMORPHIC_H

#include <memory>

namespace header_only_library {
namespace polymorphic {

class Polymorphic
{
public:
	virtual ~Polymorphic() = default;
};

class Cloneable
: public virtual Polymorphic
{
public:
	virtual std::unique_ptr<Cloneable> clone() const = 0;
};

template<typename V, typename U>
std::unique_ptr<V> dynamic_pointer_cast(std::unique_ptr<U> ptr)
	{ return std::unique_ptr<V>(dynamic_cast<V*>(ptr.release())); }

template<typename UniquePtr>
auto clone(UniquePtr&& ptr)
{
	using elem = typename std::remove_reference<UniquePtr>::type::element_type;
	using dter = typename std::remove_reference<UniquePtr>::type::deleter_type;
	return std::unique_ptr<elem, dter>(new elem(*std::forward<UniquePtr>(ptr)));
}

} // namespace polymorphic
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_POLYMORPHIC_H
