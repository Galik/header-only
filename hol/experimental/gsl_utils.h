#ifndef HEADER_ONLY_LIBRARY_GSL_UTILS_H
#define HEADER_ONLY_LIBRARY_GSL_UTILS_H
//
// Copyright (c) 2017 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <gsl/span>

namespace header_only_library {
namespace gsl_utils {
namespace detail {

template<typename T>
std::vector<gsl::span<T>>& span_vec() { thread_local static std::vector<gsl::span<T>> v; return v; }

template<typename T>
void span_vec_clear() { std::vector<gsl::span<T>>().swap(span_vec<T>()); }

} // namespace detail

template<typename T>
void free_memory()
{
	detail::span_vec_clear<T>();
}

template<typename T>
std::pair<gsl::span<T>, gsl::span<T>> bisect(gsl::span<T> s, T const& v)
{
	auto found = std::find(s.data(), s.data() + s.size(), v);
	return {{s.data(), found}, {found, s.data() + s.size()}};
}

template<typename T, typename Pred>
std::pair<gsl::span<T>, gsl::span<T>> bisect_if(gsl::span<T> s, Pred pred)
{
	auto found = std::find_if(s.data(), s.data() + s.size(), pred);
	return {{s.data(), found}, {found, s.data() + s.size()}};
}

template<typename T>
std::vector<gsl::span<T>> split(gsl::span<T> s, T const& v)
{
	if(s.empty())
		return {};

	detail::span_vec<T>().clear();

	auto pos = s.data();
	decltype(pos) end;

	while((end = std::find(pos, s.data() + s.size(), v)) != s.data() + s.size())
	{
		detail::span_vec<T>().emplace_back(pos, end);
		pos = end + 1;
	}

	detail::span_vec<T>().emplace_back(pos, end);

	return {detail::span_vec<T>().begin(), detail::span_vec<T>().end()};
}

template<typename T, typename Pred>
std::vector<gsl::span<T>> split_if(gsl::span<T> s, Pred pred)
{
	if(s.empty())
		return {};

	detail::span_vec<T>().clear();

	auto pos = s.data();
	decltype(pos) end;

	while((end = std::find_if(pos, s.data() + s.size(), pred)) != s.data() + s.size())
	{
		detail::span_vec<T>().emplace_back(pos, end);
		pos = end + 1;
	}

	detail::span_vec<T>().emplace_back(pos, end);

	return {detail::span_vec<T>().begin(), detail::span_vec<T>().end()};
}

} // gsl_utils
} // header_only_library

#endif // HEADER_ONLY_LIBRARY_GSL_UTILS_H
