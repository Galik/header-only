#ifndef HEADER_ONLY_LIBRARY_PROGRESS_COUNTER_H
#define HEADER_ONLY_LIBRARY_PROGRESS_COUNTER_H

#include <cstddef>
#include <chrono>

namespace header_only_library {
namespace utils {

class progress_counter
{
public:
	using clock = std::chrono::steady_clock;

	void start(std::size_t total_jobs)
	{
		m_start_time = clock::now();
		m_total_jobs = total_jobs;
		m_completed_jobs = 0;
	}

	void increment(std::size_t n = 1U)
	{
		m_completed_jobs += n;
		if(m_completed_jobs > m_total_jobs)
			m_completed_jobs = m_total_jobs;
	}

	double progress() const
	{
		return double(m_completed_jobs) / double(m_total_jobs);
	}


	double percentage_done() const
	{
		return progress() * 100.0;
	}

	double time_remaining_in_seconds() const
	{
		using namespace std::chrono;
		auto now = clock::now();
		auto time_taken = now - m_start_time;
		auto time_taken_millis = duration_cast<milliseconds>(time_taken);
		auto so_far = progress();
		auto remaining = 1.0 - so_far;
		auto remaining_time_millis = (remaining / so_far) * time_taken_millis;
		return remaining_time_millis.count() / 1000.0;
	}

private:
	clock::time_point m_start_time;
	std::size_t m_total_jobs = 0;
	std::size_t m_completed_jobs = 0;
};

} // namespace utils
} // namespace header_only_library


#endif // HEADER_ONLY_LIBRARY_PROGRESS_COUNTER_H
