#ifndef HEADER_ONLY_LIBRARY_DEBUG_UTILS_H
#define HEADER_ONLY_LIBRARY_DEBUG_UTILS_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <string>
#include <sstream>
#include <iostream>

#define HOL_COUT(m)do{std::cout<<m<<'\n';}while(0)
#define HOL_CERR(m)do{std::cerr<<m<<'\n';}while(0)
#define HOL_MT_COUT(m)do{std::ostringstream o;o<<m<<'\n';std::cout<<o.str();}while(0)
#define HOL_MT_CERR(m)do{std::ostringstream o;o<<m<<'\n';std::cerr<<o.str();}while(0)

#ifdef NDEBUG
#define HOL_BUG(m) do{}while(0)
#define HOL_BUG_FUN() do{}while(0)
#define HOL_BUG_VAR(v) do{}while(0)
#define HOL_BUG_CONTAINER(c) do{}while(0)
#else
#define HOL_BUG(m) HOL_MT_COUT(m)
namespace hol {
namespace debug_utils {
template<typename K, typename V>
std::ostream& operator<<(std::ostream& o, std::pair<K, V> const& p)
	{ return o << '{' << p.first << ", " << p.second << '}'; }
} // debug_utils
} // hol

#define HOL_BUG_FUN() \
struct hol_ ## __func__ ## _scope_bomb \
{ \
	char const* m = __func__; \
	hol_ ## __func__ ## _scope_bomb() { HOL_BUG("--> " << m); } \
	~hol_ ## __func__ ## _scope_bomb() { HOL_BUG("<-- " << m); } \
} hol_ ## __func__ ## _scope_bomb_impl
#define HOL_BUG_VAR(v) HOL_BUG(#v ": " << v)
#define HOL_BUG_CONTAINER(c) \
do{ \
	using hol::debug_utils::operator<<; \
	std::ostringstream o; \
	o << #c ": " << c.size() << '\n'; \
	int i=0; \
	for(auto&& v_c: c) \
		{o << (i<100?" ":"") << (i<10?" ":"") << i << ": " << v_c << '\n';++i;} \
	std::cout << o.str(); \
}while(0)
#endif

#endif // HEADER_ONLY_LIBRARY_DEBUG_UTILS_H
