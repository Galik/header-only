#ifndef HEADER_ONLY_LIBRARY_QUE_H_
#define HEADER_ONLY_LIBRARY_QUE_H_
/**
 *
 */

#include <queue>
#include <vector>
#include <deque>
#include <algorithm>

namespace header_only_library {
namespace containers {

class deq
{
	typedef std::size_t size_type;
	typedef std::vector<int> int_vec;
	typedef std::vector<int_vec> int_vec_vec;

	static const size_type max = 32;

	int_vec_vec f;
	int_vec_vec b;

	void inc(int_vec_vec& vv)
	{
		vv.push_back(int_vec());
	}

	void push_to(int_vec_vec& vv, int i)
	{
		if(vv.empty())
			inc(vv);

		if(vv.front().size() == 32)
			inc(vv);

		vv.front().push_back(i);
	}

public:

	void push_front(int i)
	{
		push_to(f, i);
	}

	void push_back(int i)
	{
		push_to(b, i);
	}

	int& operator[](size_type idx)
	{
		if(idx < b.size() * max)
			return b[idx / max][idx % max];

		idx -= b.size() * max;

		return f[idx / max][idx % max];
	}

	const int& operator[](size_type idx) const
	{
		return operator[](idx);
	}
};

template<typename Type, typename Container = std::vector<Type>>
class circular
{
public:
	typedef Type value_type;
	typedef Type& reference;
	typedef const Type& const_reference;
	typedef typename std::vector<Type>::size_type size_type;
	typedef Container container_type;

	static_assert(std::is_copy_constructible<Container>::value,
	                  "circular requires copying");

private:
	container_type v;
	std::size_t f;
	std::size_t b;

	void increase()
	{
		if(v.empty())
		{
			v.resize(32);
			return;
		}
		size_type top = v.size();
		v.resize(v.size() * 2);
		std::copy_n(v.begin(), b, v.begin() + top);
		b += top;
	}

public:
	circular(size_type size = 1): v(size), f(0), b(0)
	{
		if(v.empty())
			increase();
	}

	circular(Container&& ctnr): v(ctnr), f(0), b(0)
	{
		if(v.empty())
			increase();
	}

	circular(const Container& ctnr): v(ctnr), f(0), b(0)
	{
		if(v.empty())
			increase();
	}

	void clear()
	{
		f = b = 0;
	}

	reference front() { return v[f]; }
	const_reference front() const { return v[f]; }
	bool front(value_type& vt)
	{
		if(empty())
			return false;
		vt = f;
		return true;
	}

	reference back() { return v[b - 1]; }
	const_reference back() const { return v[b - 1]; }
	bool back(value_type& vt)
	{
		if(empty())
			return false;
		vt = b;
		return true;
	}

	size_type size() const
	{
		if(f < b)
			return b - f;
		if(b < f)
			return b + v.size() - f;
		return 0;
	}

	bool empty() const { return f == b; }

	template<typename... Args>
	void emplace_back(Args&&... args)
	{
		auto o = Type(std::forward<Args>(args)...);
		push_back(std::move(o));
	}

	void push_back(const Type& i)
	{
		v[b++] = i;

		if(b == v.size())
			b = 0;

		if(b == f) // full
			increase();
	}

	void pop_front()
	{
		if(empty()) // empty
			return;

		if(++f == v.size())
			f = 0;
	}
};

template<typename Type, typename Container = std::vector<Type>>
class cirque
: public std::queue<Type, circular<Type, Container>>
{
public:
	explicit cirque(Container&& ctnr = Container()): std::queue<Type, circular<Type, Container>>(circular<Type>(ctnr)) {}
	explicit cirque(const Container& ctnr): std::queue<Type, circular<Type, Container>>(circular<Type>(ctnr)) {}
	cirque(cirque&& q): std::queue<Type, circular<Type>>(q) {}
	cirque(const cirque& q): std::queue<Type, circular<Type>>(q) {}
};

} // namespace containers
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_QUE_H_
