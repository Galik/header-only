#ifndef HEADER_ONLY_LIBRARY_SOCKET_TESTS_H
#define HEADER_ONLY_LIBRARY_SOCKET_TESTS_H

#include <sys/stat.h>

namespace hol {
namespace socket_tests {
namespace net {
namespace fakeable_sockets {

using byte = unsigned char;
using stream_int = decltype(htonl(0));

namespace detail {

inline std::vector<byte>& fake_stream()
{
	thread_local static std::vector<byte> buf;
	return buf;
}

inline std::size_t& fake_gpos()
{
	thread_local static std::size_t gpos = 0;
	return gpos;
}

inline void fake_init()
{
	fake_gpos() = 0;
	std::vector<byte>().swap(fake_stream());
}

/**
 * This function will not always 'send' the whole buffer in
 * one call and will randomly block if the socket is set in
 * non-blocking mode causing an EWOULDBLOCK or a EAGAIN error.
 * @param fd
 * @param buf
 * @param size
 * @param flags
 * @return
 */
inline int fake_send(int fd, void* buf, int size, int flags)
{
	using range = std::uniform_int_distribution<int>::param_type;

	(void) fd;
	(void) flags;

	thread_local static std::mt19937 mt{std::random_device{}()};
	thread_local static std::uniform_int_distribution<int> pick;

	struct stat st;

	if((flags & MSG_DONTWAIT) && pick(mt, range{0, 100}) < 10)
		errno = pick(mt, range{0, 1}) ? EWOULDBLOCK:EAGAIN;
	else if(!fstat(fd, &st) && S_ISSOCK(st.st_mode))
		errno = ENOTSOCK;
	else
	{
		auto clear = pick(mt, range{0, size - 1});
		if(clear - (size / 2) < 0)
			size -= clear;

		std::copy((char*)buf, ((char*)buf) + size, std::back_inserter(fake_stream()));
		return size;
	}

	return -1;
}

inline int fake_recv(int fd, void* buf, int size, int flags)
{
	(void) fd;
	(void) flags;

	if(fake_gpos() >= fake_stream().size())
		return 0; // EOF

	for(int i = 0U; i < size;)
	{
		((char*)buf)[i++] = fake_stream()[fake_gpos()++];
		if(fake_gpos() >= fake_stream().size())
			return i; // EOF
	}

	return size;
}

inline int real_send(int fd, void* buf, int size, int flags)
{
	return ::send(fd, buf, size, flags);
}

inline int real_recv(int fd, void* buf, int size, int flags)
{
	return ::recv(fd, buf, size, flags);
}

} // detail

namespace {
int(*send)(int fd, void* buf, int size, int flags) = detail::fake_send;
int(*recv)(int fd, void* buf, int size, int flags) = detail::fake_recv;
} // <anon>

void fake_it()
{
	send = detail::fake_send;
	recv = detail::fake_recv;
	detail::fake_init();
}

void do_it_for_real()
{
	detail::fake_init();
	send = detail::real_send;
	recv = detail::real_recv;
}

} // fakeable_sockets
} // net
} // socket_tests
} // hol

#endif // HEADER_ONLY_LIBRARY_SOCKET_TESTS_H
