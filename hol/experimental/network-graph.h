#ifndef HEADER_ONLY_LIBRARY_NETWORK_GRAPH_H
#define HEADER_ONLY_LIBRARY_NETWORK_GRAPH_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <deque>
#include <vector>

#include <hol/basic_serialization.h>

namespace header_only_library {
namespace network {
namespace graph {

template<typename NodeInfo, typename LinkInfo>
struct node;

/**
 * \brief A link between nodes in the network
 * \tparam NodeInfo An information type for data carried by
 * each node in the network. For example the name of a town for
 * a road map or a unique-id for a neuron network.
 * \tparam LinkInfo An information type describing link
 * properties between nodes. For example distance for a road map
 * or feedback for a neural network.
 */
template<typename NodeInfo, typename LinkInfo>
struct link
{
	LinkInfo info;
	node<NodeInfo, LinkInfo>* to;

	template<typename N, typename L>
	friend std::ostream& serialize(std::ostream& s, link<N, L> const& l)
	{
		using namespace hol::basic_serialization::txt;
		serialize_ptr(s, l.to);
		serialize(s, l.info);
		return s;
	}

	template<typename N, typename L>
	friend std::istream& deserialize(std::istream& s, link<N, L>& l)
	{
		using namespace hol::basic_serialization::txt;
		deserialize_ptr(s, l.to);
		deserialize(s, l.info);
		return s;
	}
};

/**
 * \brief A node in the network
 * \tparam NodeInfo An information type for data carried by
 * each node in the network. For example the name of a town for
 * a road map or a unique-id for a neuron network.
 * \tparam LinkInfo An information type describing link
 * properties between nodes. For example distance for a road map
 * or feedback for a neural network.
 */
template<typename NodeInfo, typename LinkInfo>
struct node
{
	NodeInfo info;
	std::vector<link<NodeInfo, LinkInfo>> links;
	mutable bool done = false;

	node(){} // for hol::basic_serialization
	node(NodeInfo const& info): info(info) {}

	template<typename N, typename L>
	friend std::ostream& serialize(std::ostream& s, node<N, L> const& n)
	{
		using namespace hol::basic_serialization::txt;
		serialize(s, n.info);
		serialize(s, n.links);
		serialize(s, n.done);
		return s;
	}

	template<typename N, typename L>
	friend std::istream& deserialize(std::istream& s, node<N, L>& n)
	{
		using namespace hol::basic_serialization::txt;
		deserialize(s, n.info);
		deserialize(s, n.links);
		deserialize(s, n.done);
		return s;
	}
};

//template<typename NodeInfo, typename LinkInfo>
//using network = std::vector<node<NodeInfo, LinkInfo>>;

// TODO: Edit mode and process mode ??
// Edit mode copies nodes to a std::list for insertions/deletions
// while process mode copies nodes to a std::vector for speed.
template<typename NodeInfo, typename LinkInfo>
class network
{
public:
	using node_info = NodeInfo;
	using link_info = LinkInfo;

	using node_type = node<node_info, link_info>;
	using link_type = link<node_info, link_info>;

//	using node_vec = std::vector<node_type>;
	using node_vec = std::deque<node_type>;

	using iterator = typename node_vec::iterator;
	using const_iterator = typename node_vec::const_iterator;

	void dump(std::ostream& os)
	{
		os << "nodes:" << '\n';
		for(auto const& n: nodes)
		{
			os << "  n.info: " << n.info << '\n';
			os << "  links:" << '\n';
			for(auto const& l: n.links)
			{
				os << "    l.info: " << l.info << '\n';
				os << "      l.to: " << l.to->info << '\n';
			}
			os << "  n.done:" << n.done << '\n';
		}
	}

	void clear() { nodes.clear(); }
	void clear_flag()
	{
		for(auto& node: nodes)
			node.done = false;
	}

	template<typename... Args>
	void emplace_back(Args&&... args) { nodes.emplace_back(std::forward<Args>(args)...); }

	iterator begin() { return nodes.begin(); }
	iterator end() { return nodes.end(); }

	const_iterator begin() const { return nodes.cbegin(); }
	const_iterator end() const { return nodes.cend(); }

	template<typename N, typename L>
	friend std::ostream& serialize(std::ostream& s, network<N, L> const& n)
	{
		using namespace hol::basic_serialization::txt;
		serialize(s, n.nodes);
		return s;
	}

	template<typename N, typename L>
	friend std::istream& deserialize(std::istream& s, network<N, L>& n)
	{
		using namespace hol::basic_serialization::txt;
		deserialize(s, n.nodes);
		return s;
	}

private:
	node_vec nodes;
};

} // graph
} // network
} // header_only_library



#endif // HEADER_ONLY_LIBRARY_NETWORK_GRAPH_H
