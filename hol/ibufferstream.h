#ifndef HEADER_ONLY_LIBRARY_IBUFFERSTREAM_H
#define HEADER_ONLY_LIBRARY_IBUFFERSTREAM_H
/*
 * ibufferstream.h
 *
 *  Created on: 25 Aug 2018
 *      Author: galik
 */

#include <cstddef>
#include <istream>
#include <iterator>

#if ! (__cplusplus < 201703)
# include <cstddef>
#endif

namespace header_only_library {
namespace string_utils {

template<typename CharT, typename Traits = std::char_traits<CharT>>
class basic_char_array_buffer
: public std::basic_streambuf<CharT, Traits>
{
	static std::size_t length_of(CharT const* s)
	{
		auto p = s;
		while(*p)
			++p;
		return std::size_t(std::distance(s, p));
	}

public:
	using traits_type = Traits;
	using char_type = typename traits_type::char_type;
	using int_type = typename traits_type::int_type;

	basic_char_array_buffer(char_type const* data, unsigned int len)
	: beg(data), end(data + len), cur(data) {}

	basic_char_array_buffer(char_type const* data)
	: basic_char_array_buffer(data, length_of(data)) {}

	basic_char_array_buffer(std::basic_string<char_type, traits_type> const& s)
	: basic_char_array_buffer(s.data(), s.size()) {}

	basic_char_array_buffer()
	: beg(nullptr), end(nullptr), cur(nullptr) {}

private:
	int_type underflow() override
	{
		if(cur == end)
			return traits_type::eof();
		return traits_type::to_int_type(*cur);
	}

	int_type uflow() override
	{
		if(cur == end)
			return traits_type::eof();
		return traits_type::to_int_type(*cur++);
	}

	int_type pbackfail(int_type ch) override
	{
		if(cur == beg || (ch != traits_type::eof() && ch != cur[-1]))
			return traits_type::eof();
		return traits_type::to_int_type(*--cur);
	}

	std::streamsize showmanyc()  override { return end - cur; }

	char_type const* const beg;
	char_type const* const end;
	char_type const* cur;
};

using char_array_buffer = basic_char_array_buffer<char>;
using wchar_array_buffer = basic_char_array_buffer<wchar_t>;
using u16char_array_buffer = basic_char_array_buffer<char16_t>;
using u32char_array_buffer = basic_char_array_buffer<char32_t>;

#if ! (__cplusplus < 201703)
using byte_array_buffer = basic_char_array_buffer<std::byte>;
#endif

template<typename CharT, typename Traits = std::char_traits<CharT>>
class basic_ibufferstream
: std::basic_istream<CharT, Traits>
{
public:
	using traits_type = Traits;
	using char_type = typename traits_type::char_type;
	using int_type = typename traits_type::int_type;

	basic_ibufferstream()
	: std::basic_istream<char_type, traits_type>(&buf) {}

	basic_ibufferstream(char_type const* s)
	: std::basic_istream<char_type, traits_type>(&buf) {}

private:
	basic_char_array_buffer<CharT> buf;
};

using ibufferstream = basic_ibufferstream<char>;
using wibufferstream = basic_ibufferstream<wchar_t>;
using u16ibufferstream = basic_ibufferstream<char16_t>;
using u32ibufferstream = basic_ibufferstream<char32_t>;

#if ! (__cplusplus < 201703)
using byteibufferstream = basic_ibufferstream<std::byte>;
#endif

} // namespace string_utils
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_IBUFFERSTREAM_H
