#ifndef HEADER_ONLY_LIBRARY_STRING_UTILS_H
#define HEADER_ONLY_LIBRARY_STRING_UTILS_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

// TODO: split std::string_view and gsl::string_span parts to different header
// TODO: generalize mapped_stencil for other string types
// TODO: Rework split() SIMPLIFY

#include <algorithm>
#include <array>
#include <cerrno>
#include <codecvt>
#include <cstdlib> // std::strtol
#include <cstring>
#include <fstream>
#include <locale>
#include <numeric>
#include <regex>
#include <set>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>
#if __cplusplus >= 201703L
#include <string_view>
#include <cstddef> // std::byte
#include <optional>
#elif __cplusplus >= 201402L
#include <experimental/string_view>
#else
#endif

//#ifdef __GNUC__
//#  if __cplusplus > 201402L
//#    ifndef HOL_HAS_STD_BYTE
//#      define HOL_HAS_STD_BYTE
//#      include <cstddef>
//#    endif
//#  endif
//#endif

#include "misc_utils.h"
#include "split_algos.h"

//#ifdef HOL_USE_STRING_VIEW
//#	include <string_view>
//#endif

// TODO: just_left, just_right, just_center, just_all

namespace header_only_library {
namespace string_utils {
namespace mu = misc_utils;

#if defined(HOL_AT_LEAST_CPP17)
template<typename C, typename T = std::char_traits<C>>
using StringView = std::basic_string_view<C, T>;
#elif defined(HOL_AT_LEAST_CPP14)
template<typename C, typename T = std::char_traits<C>>
using StringView = std::experimental::basic_string_view<C, T>;
#endif

//#ifdef HOL_USE_STRING_VIEW
//using string_view = std::string_view;
//#endif

// replace_all

//inline
//std::string& replace_all(std::string& s, const std::string& from, const std::string& to)
//{
//	if(!from.empty())
//		for(size_t pos = 0; (pos = s.find(from, pos)) != std::string::npos; pos += to.size())
//			s.replace(pos, from.size(), to);
//	return s;
//}

namespace detail {

template<typename CharT>
std::basic_string<CharT>& replace_all_mute(
	std::basic_string<CharT>& s,
	std::basic_string<CharT> const& from,
	std::basic_string<CharT> const& to)
{
	if(!from.empty())
		for(std::size_t pos = 0; (pos = s.find(from, pos) + 1); pos += to.size())
			s.replace(--pos, from.size(), to);
	return s;
}

//template<typename CharT>
//std::basic_string<CharT>& lower_mute(std::basic_string<CharT>& s)
//{
//	std::transform(s.begin(), s.end(), s.begin()
//		, [&](CharT c){ return std::tolower(mu::integral_cast<int>(c)); });
//	return s;
//}
//
//template<typename CharT>
//std::basic_string<CharT>& upper_mute(std::basic_string<CharT>& s)
//{
//	std::transform(s.begin(), s.end(), s.begin()
//		, [&](CharT c){ return std::toupper(mu::integral_cast<int>(c)); });
//	return s;
//}

template<typename CharT>
std::basic_string<CharT>& lower_mute(std::basic_string<CharT>& s)
{
	std::transform(s.begin(), s.end(), s.begin()
		, [&](CharT c){ return std::tolower(c, std::locale()); });
	return s;
}

template<typename CharT>
std::basic_string<CharT>& upper_mute(std::basic_string<CharT>& s)
{
	std::transform(s.begin(), s.end(), s.begin()
		, [&](CharT c){ return std::toupper(c, std::locale()); });
	return s;
}

// trim

constexpr char const* ws(char) { return " \t\n\r\f\v\0"; }
constexpr wchar_t const* ws(wchar_t) { return L" \t\n\r\f\v\0"; }
constexpr char16_t const* ws(char16_t) { return u" \t\n\r\f\v\0"; }
constexpr char32_t const* ws(char32_t) { return U" \t\n\r\f\v\0"; }

constexpr char const* empty(char) { return ""; }
constexpr wchar_t const* empty(wchar_t) { return L""; }
constexpr char16_t const* empty(char16_t) { return u""; }
constexpr char32_t const* empty(char32_t) { return U""; }

constexpr char const* space(char) { return " "; }
constexpr wchar_t const* space(wchar_t) { return L" "; }
constexpr char16_t const* space(char16_t) { return u" "; }
constexpr char32_t const* space(char32_t) { return U" "; }

} // namespace detail


#if __cplusplus >= 201703L
//--------------------------------------------------------------
// Replace all (Output iterator version)
//
// Likely better for larger chunks of text.
//

template<typename T>
class count_output_iterator
{
public:
	count_output_iterator(std::size_t n = 0): n(n) {}

	T& operator*() { return dummy; }
	T const& operator*() const { return dummy; }

	count_output_iterator& operator++(   ) { ++n; return *this; }
	count_output_iterator  operator++(int) { count_output_iterator copy(n); ++(*this); return copy; }

	std::size_t count() const { return n; }

private:
	std::size_t n = 0;
	T dummy = {};
};

template<typename Container>
auto output_counter(Container const& c)
{
	return count_output_iterator<typename Container::value_type>();
}

template<
	typename Iter, typename Iter1, typename Searcher, typename OutIter
>
OutIter replace_all(Iter beg, Iter const end, Searcher searcher, Iter1 const to_beg, Iter1 const to_end, OutIter out)
{
	for(auto p = searcher(beg, end); p.first != p.second; p = searcher(beg, end))
	{
		while(beg != p.first)
			*out++ = *beg++;

		for(auto to = to_beg; to != to_end;)
			*out++ = *to++;

		beg = p.second;
	}

	while(beg != end)
		*out++ = *beg++;

	return out;
}

template<
	typename Iter, typename Iter1, typename Iter2, typename OutIter
>
OutIter replace_all(Iter const beg, Iter const end, Iter1 const from_beg, Iter1 const from_end,
	Iter2 const to_beg, Iter2 const to_end, OutIter out)
{
	return replace_all(beg, end, std::default_searcher(from_beg, from_end), to_beg, to_end, out);
}

#endif // __cplusplus >= 201703L

namespace old_stuff {
//--------------------------------------------------------------
// replace_all_mute
//
inline
std::string& replace_all_mute(std::string& s,
	const std::string& from, const std::string& to)
{
	return detail::replace_all_mute<char>(s, from, to);
}

inline
std::wstring& replace_all_mute(std::wstring& s,
	const std::wstring& from, const std::wstring& to)
{
	return detail::replace_all_mute<wchar_t>(s, from, to);
}

inline
std::u16string& replace_all_mute(std::u16string& s,
	const std::u16string& from, const std::u16string& to)
{
	return detail::replace_all_mute<char16_t>(s, from, to);
}

inline
std::u32string& replace_all_mute(std::u32string& s,
	const std::u32string& from, const std::u32string& to)
{
	return detail::replace_all_mute<char32_t>(s, from, to);
}

//--------------------------------------------------------------
// replace_all_copy
//

inline
std::string replace_all_copy(std::string s,
	const std::string& from, const std::string& to)
{
	return replace_all_mute(s, from, to);
}

inline
std::wstring replace_all_copy(std::wstring s,
	const std::wstring& from, const std::wstring& to)
{
	return replace_all_mute(s, from, to);
}

inline
std::u16string replace_all_copy(std::u16string s,
	const std::u16string& from, const std::u16string& to)
{
	return replace_all_mute(s, from, to);
}

inline
std::u32string replace_all_copy(std::u32string s,
	const std::u32string& from, const std::u32string& to)
{
	return replace_all_mute(s, from, to);
}
} // namespace old_stuff

/*
NEW ROUTINES TO BE ADDED AFTER TESTS
*/
//namespace new_stuff {

// mute
namespace detail {

template<
	typename CharT, typename Traits, typename Alloc,
	typename CharT1, typename Traits1, typename Alloc1,
	typename CharT2, typename Traits2, typename Alloc2
>
std::basic_string<CharT, Traits, Alloc>& replace_all_mute_f(
	std::basic_string<CharT, Traits, Alloc>& s,
	std::basic_string<CharT1, Traits1, Alloc1> const& from,
	std::basic_string<CharT2, Traits2, Alloc2> const& to)
{
	using size_type = typename std::basic_string<CharT, Traits, Alloc>::size_type;

//	if(!from.empty())
	for(size_type pos = 0; (pos = s.find(from, pos) + 1); pos += to.size())
		s.replace(--pos, from.size(), to);

	return s;
}

template<
	typename CharT, typename Traits, typename Alloc,
	typename CharT1, typename Traits1, typename Alloc1,
	typename CharT2, typename Traits2, typename Alloc2
>
std::basic_string<CharT, Traits, Alloc>& replace_all_mute_r(
	std::basic_string<CharT, Traits, Alloc>& s,
	std::basic_string<CharT1, Traits1, Alloc1> const& from,
	std::basic_string<CharT2, Traits2, Alloc2> const& to)
{
	using size_type = typename std::basic_string<CharT, Traits, Alloc>::size_type;

//    if(!from.empty())
	for(size_type pos = s.size(); (pos = s.rfind(from, pos) + 1); --pos)
		s.replace(--pos, from.size(), to);
    return s;
}

}  // namespace detail

template<
	typename CharT, typename Traits, typename Alloc,
	typename CharT1, typename Traits1, typename Alloc1,
	typename CharT2, typename Traits2, typename Alloc2
>
std::basic_string<CharT, Traits, Alloc>& replace_all_mute(
	std::basic_string<CharT, Traits, Alloc>& s,
	std::basic_string<CharT1, Traits1, Alloc1> const& from,
	std::basic_string<CharT2, Traits2, Alloc2> const& to)
{
	if(from.empty())
		return s;

	if(to.size() > from.size())
		return detail::replace_all_mute_f(s, from, to);
	else
		return detail::replace_all_mute_r(s, from, to);
}

template<
	typename CharT, typename Traits, typename Alloc,
	typename CharT1, std::size_t N1,
	typename CharT2, std::size_t N2
>
std::basic_string<CharT, Traits, Alloc>& replace_all_mute(
	std::basic_string<CharT, Traits, Alloc>& s,
	CharT1(&from)[N1], CharT2(&to)[N2])
{
	using string_1 = std::basic_string<typename std::remove_const<CharT1>::type>;
	using string_2 = std::basic_string<typename std::remove_const<CharT2>::type>;

	return replace_all_mute(s, string_1(from, N1 - (from[N1 - 1] ? 0:1)),
		string_2(to, N2 - (to[N2 - 1] ? 0:1)));
}

template<
	typename CharT, typename Traits, typename Alloc,
	typename CharT1, typename Traits1, typename Alloc1,
	typename CharT2, std::size_t N2
>
std::basic_string<CharT, Traits, Alloc>& replace_all_mute(
	std::basic_string<CharT, Traits, Alloc>& s,
	std::basic_string<CharT1, Traits1, Alloc1> const& from, CharT2(&to)[N2])
{
	using string_2 = std::basic_string<typename std::remove_const<CharT2>::type>;

	return replace_all_mute(s, from, string_2(to, (to[N2 - 1] ? 0:1)));
}

template<
	typename CharT, typename Traits, typename Alloc,
	typename CharT1, std::size_t N1,
	typename CharT2, typename Traits2, typename Alloc2
>
std::basic_string<CharT, Traits, Alloc>& replace_all_mute(
	std::basic_string<CharT, Traits, Alloc>& s,
	CharT1(&from)[N1], std::basic_string<CharT2, Traits2, Alloc2> const& to)
{
	using string_1 = std::basic_string<typename std::remove_const<CharT1>::type>;

	return replace_all_mute<CharT>(s, string_1(from, N1 - (from[N1 - 1] ? 0:1)), to);
}

// copy

template<
	typename CharT, typename Traits, typename Alloc,
	typename CharT1, typename Traits1, typename Alloc1,
	typename CharT2, typename Traits2, typename Alloc2
>
std::basic_string<CharT, Traits, Alloc> replace_all_copy(
	std::basic_string<CharT, Traits, Alloc> s,
	std::basic_string<CharT1, Traits1, Alloc1> const& from,
	std::basic_string<CharT2, Traits2, Alloc2> const& to)
{
	return replace_all_mute(s, from, to);
}

template<
	typename CharT, typename Traits, typename Alloc,
	typename CharT1, std::size_t N1,
	typename CharT2, std::size_t N2
>
std::basic_string<CharT, Traits, Alloc> replace_all_copy(
	std::basic_string<CharT, Traits, Alloc> s,
	CharT1(&from)[N1], CharT2(&to)[N2])
{
	return replace_all_mute(s, from, to);
}

template<
	typename CharT, typename Traits, typename Alloc,
	typename CharT1, typename Traits1, typename Alloc1,
	typename CharT2, std::size_t N2
>
std::basic_string<CharT, Traits, Alloc> replace_all_copy(
	std::basic_string<CharT, Traits, Alloc> s,
	std::basic_string<CharT1, Traits1, Alloc1> const& from, CharT2(&to)[N2])
{
	return replace_all_mute(s, from, to);
}

template<
	typename CharT, typename Traits, typename Alloc,
	typename CharT1, std::size_t N1,
	typename CharT2, typename Traits2, typename Alloc2
>
std::basic_string<CharT, Traits, Alloc> replace_all_copy(
	std::basic_string<CharT, Traits, Alloc> s,
	CharT1(&from)[N1], std::basic_string<CharT2, Traits2, Alloc2> const& to)
{
	return replace_all_mute(s, from, to);
}

//}  // namespace new_stuff





// -----------------------------------------------
// lower_mute
//

inline
std::string& lower_mute(std::string& s)
{
	return detail::lower_mute(s);
}

inline
std::wstring& lower_mute(std::wstring& s)
{
	return detail::lower_mute(s);
}

inline
std::u16string& lower_mute(std::u16string& s)
{
	return detail::lower_mute(s);
}

inline
std::u32string& lower_mute(std::u32string& s)
{
	return detail::lower_mute(s);
}

// -----------------------------------------------
// lower_copy
//

inline
std::string lower_copy(std::string s)
{
	return lower_mute(s);
}

inline
std::wstring lower_copy(std::wstring s)
{
	return lower_mute(s);
}

inline
std::u16string lower_copy(std::u16string s)
{
	return lower_copy(s);
}

inline
std::u32string lower_copy(std::u32string s)
{
	return lower_copy(s);
}

// -----------------------------------------------
// upper_mute
//

inline
std::string& upper_mute(std::string& s)
{
	return detail::upper_mute(s);
}

inline
std::wstring& upper_mute(std::wstring& s)
{
	return detail::upper_mute(s);
}

inline
std::u16string& upper_mute(std::u16string& s)
{
	return detail::upper_mute(s);
}

inline
std::u32string& upper_mute(std::u32string& s)
{
	return detail::upper_mute(s);
}

// -----------------------------------------------
// upper_copy
//

inline
std::string upper_copy(std::string s)
{
	return upper_mute(s);
}

inline
std::wstring upper_copy(std::wstring s)
{
	return upper_mute(s);
}

inline
std::u16string upper_copy(std::u16string s)
{
	return upper_mute(s);
}

inline
std::u32string upper_copy(std::u32string s)
{
	return upper_mute(s);
}

/**
 * Usage:
 *
 * output_separator sep;
 *
 * for(auto const& s: v)
 *     std::cout << sep << s;
 * std::cout << '\n';
 *
 */
template<typename CharT>
class basic_string_separator
{
public:
	basic_string_separator(): init(detail::empty(CharT())), s(init), next(detail::space(CharT())) {}
	basic_string_separator(CharT const* next): init(detail::empty(CharT())), s(init), next(next) {}
	basic_string_separator(CharT const* init, CharT const* next): init(init), s(init), next(next) {}
	basic_string_separator(basic_string_separator const& sep): s(sep.s), next(sep.next) {}

	template<typename CharU>
	friend std::basic_ostream<CharU>& operator<<(std::basic_ostream<CharU>& os,
		basic_string_separator<CharU>& sep)
	{
		os << sep.s;
		sep.s = sep.next;
		return os;
	}

	/**
	 * The first time this is called after construction or calling reset()
	 * this function appends the output_separator's initial state to the string,
	 * thereafter the second state is appended. By  default the initial state is
	 * empty and the second state is a single space.
	 *
	 * @param s The std::string to be appended to
	 * @param sep The output_separator object to be appended to the string.
	 * @return A new std::string with the first or second state of the
	 * output_separator appended to the input std::string
	 */
	template<typename CharU>
	friend std::basic_string<CharU> operator+(std::basic_string<CharU> const& s,
		basic_string_separator<CharU>& sep)
	{
		std::basic_string<CharU> r = s + sep.s;
		sep.s = sep.next;
		return r;
	}

	/**
	 * The first time this is called after construction or calling reset()
	 * this function prepends the output_separator's initial state to the string,
	 * thereafter the second state is prepended. By  default the initial state is
	 * empty and the second state is a single space.
	 *
	 * @param s The std::string to be prepended to
	 * @param sep The output_separator object to be prepended to the string.
	 * @return A new std::string with the first or second state of the
	 * output_separator prepended to the input std::string
	 */
	template<typename CharU>
	friend std::basic_string<CharU> operator+(basic_string_separator<CharU>& sep,
		std::basic_string<CharU> const& s)
	{
		std::basic_string<CharU> r = sep.s + s;
		sep.s = sep.next;
		return r;
	}

	/**
	 * The first time this is called after construction or calling reset()
	 * this function appends the output_separator's initial state to the string,
	 * thereafter the second state is appended. By  default the initial state is
	 * empty and the second state is a single space.
	 *
	 * @param s The std::string to be appended to
	 * @param sep The output_separator object to be appended to the string.
	 * @return A new std::string with the first or second state of the
	 * output_separator appended to the input std::string
	 */
	template<typename CharU>
	friend std::basic_string<CharU> operator+(CharU const* s,
		basic_string_separator<CharU>& sep)
	{
		std::basic_string<CharT> r{s};
		r += sep.s;
		sep.s = sep.next;
		return r;
	}

	/**
	 * The first time this is called after construction or calling reset()
	 * this function prepends the output_separator's initial state to the string,
	 * thereafter the second state is prepended. By  default the initial state is
	 * empty and the second state is a single space.
	 *
	 * @param s The std::string to be prepended to
	 * @param sep The output_separator object to be prepended to the string.
	 * @return A new std::string with the first or second state of the
	 * output_separator prepended to the input std::string
	 */
	template<typename CharU>
	friend std::basic_string<CharU> operator+(basic_string_separator<CharU>& sep,
		CharU const* s)
	{
		std::basic_string<CharU> r{sep.s};
		r += s;
		sep.s = sep.next;
		return r;
	}

	/**
	 * Reset the optput_separator to its initial state so that
	 * the next output function will receive the initial state and
	 * thereafter the second state.
	 */
	void reset() { this->s = init; }

	/**
	 * Reset the optput_separator to a new initial state `init` so that
	 * the next output function will receive the new initial state and
	 * thereafter the previously configured second state.
	 */
	void reset(CharT const* init) { this->s = this->init = init; }

	/**
	 * Reset the optput_separator to a new initial state `init` and
	 * with a new second state `ext` so that the next output function
	 * will receive the new initial state and
	 * thereafter the newly configured second state.
	 */
	void reset(CharT const* init, CharT const* next)
	{
		this->s = this->init = init;
		this->next = next;
	}

private:
	CharT const* init;
	CharT const* s;
	CharT const* next;
};

using string_separator = basic_string_separator<char>;
using wstring_separator = basic_string_separator<wchar_t>;
using u16string_separator = basic_string_separator<char16_t>;
using u32string_separator = basic_string_separator<char32_t>;

using output_separator HOL_DEPRECATED("use string_separator") = string_separator;
using woutput_separator HOL_DEPRECATED("use wstring_separator") = wstring_separator;
using u16output_separator HOL_DEPRECATED("use u16string_separator") = u16string_separator;
using u32output_separator HOL_DEPRECATED("use u32string_separator") = u32string_separator;

// trimming functions

//---------------------------------------------------------
// trim_mute
//

template<typename C, typename T, typename A>
using String = std::basic_string<C, T, A>;

/**
 * Remove leading characters from a String.
 * @param s The String to be trimmed.
 * @param ws The set of characters to remove from the beginning
 * of the String.
 * @return The same String passed in as a parameter.
 */
template<typename C, typename T, typename A>
String<C, T, A>& trim_left_mute(String<C, T, A>& s, C const* ws)
{
	s.erase(0, s.find_first_not_of(ws));
	return s;
}

template<typename C, typename T, typename A>
String<C, T, A>& trim_left_mute(String<C, T, A>& s, String<C, T, A> const& ws)
{
	return trim_left_mute(s, ws.c_str());
}

template<typename C, typename T, typename A>
String<C, T, A>& trim_left_mute(String<C, T, A>& s)
{
	return trim_left_mute(s, detail::ws(C()));
}

template<typename C, typename T, typename A, typename Func>
String<C, T, A>& trim_left_mute(String<C, T, A>& s, Func func)
{
	s.erase(std::begin(s), std::find_if_not(std::begin(s), std::end(s), func));
	return s;
}

/**
 * Remove trailing characters from a String.
 * @param s The String to be trimmed.
 * @param ws The set of characters to remove from the end
 * of the String.
 * @return The same String passed in as a parameter.
 */
template<typename C, typename T, typename A>
String<C, T, A>& trim_right_mute(String<C, T, A>& s, C const* ws)
{
	s.erase(s.find_last_not_of(ws) + 1);
	return s;
}

template<typename C, typename T, typename A>
String<C, T, A>& trim_right_mute(String<C, T, A>& s, String<C, T, A> const& ws)
{
	return trim_right_mute(s, ws.c_str());
}

template<typename C, typename T, typename A>
String<C, T, A>& trim_right_mute(String<C, T, A>& s)
{
	return trim_right_mute(s, detail::ws(C()));
}

template<typename C, typename T, typename A, typename Func>
String<C, T, A>& trim_right_mute(String<C, T, A>& s, Func func)
{
	s.erase(std::find_if_not(s.rbegin(), s.rend(), func).base(), std::end(s));
	return s;
}

/**
 * Remove surrounding characters from a String.
 * @param s The String to be trimmed.
 * @param ws The set of characters to remove from each end
 * of the String.
 * @return The same String passed in as a parameter.
 */
template<typename C, typename T, typename A>
String<C, T, A>& trim_mute(String<C, T, A>& s, C const* ws)
{
	return trim_left_mute(trim_right_mute(s, ws), ws);
}

template<typename C, typename T, typename A>
String<C, T, A>& trim_mute(String<C, T, A>& s, String<C, T, A> const& ws)
{
	return trim_mute(s, ws.c_str());
}

template<typename C, typename T, typename A>
String<C, T, A>& trim_mute(String<C, T, A>& s)
{
	return trim_mute(s, detail::ws(C()));
}

template<typename C, typename T, typename A, typename Func>
String<C, T, A>& trim_mute(String<C, T, A>& s, Func func)
{
	return trim_left_mute(trim_right_mute(s, func), func);
}

//---------------------------------------------------------
// trim_copy
//

/**
 * Obtain a copy of a string with leading characters removed.
 * @param s The std::string to be trimmed.
 * @param t The set of characters to remove from the beginning
 * of the String.
 * @return A copy of the string passed in as a parameter.
 */
template<typename C, typename T, typename A>
String<C, T, A> trim_left_copy(String<C, T, A> s, C const* ws)
{
	return trim_left_mute(s, ws);
}

template<typename C, typename T, typename A>
String<C, T, A> trim_left_copy(String<C, T, A> s, String<C, T, A> const& ws)
{
	return trim_left_mute(s, ws);
}

template<typename C, typename T, typename A>
String<C, T, A> trim_left_copy(String<C, T, A> s)
{
	return trim_left_mute(s);
}

template<typename C, typename T, typename A, typename Func>
String<C, T, A> trim_left_copy(String<C, T, A> s, Func func)
{
	return trim_left_mute(s, func);
}

/**
 * Obtain a copy of a string with trailing characters removed.
 * @param s The std::string to be trimmed.
 * @param t The set of characters to remove from the end
 * of the String.
 * @return A copy of the string passed in as a parameter.
 */
template<typename C, typename T, typename A>
String<C, T, A> trim_right_copy(String<C, T, A> s, C const* ws)
{
	return trim_right_mute(s, ws);
}

template<typename C, typename T, typename A>
String<C, T, A> trim_right_copy(String<C, T, A> s, String<C, T, A> const& ws)
{
	return trim_right_mute(s, ws);
}

template<typename C, typename T, typename A>
String<C, T, A> trim_right_copy(String<C, T, A> s)
{
	return trim_right_mute(s);
}

template<typename C, typename T, typename A, typename Func>
String<C, T, A> trim_right_copy(String<C, T, A> s, Func func)
{
	return trim_right_mute(s, func);
}

/**
 * Obtain a copy of a String with surrounding characters removed.
 * @param s The String to be trimmed.
 * @param t The set of characters to remove from each end
 * of the String.
 * @return A copy of the String passed in as a parameter.
 */
template<typename C, typename T, typename A>
String<C, T, A> trim_copy(String<C, T, A> s, C const* ws)
{
	return trim_mute(s, ws);
}

template<typename C, typename T, typename A>
String<C, T, A> trim_copy(String<C, T, A> s, String<C, T, A> const& ws)
{
	return trim_mute(s, ws);
}

template<typename C, typename T, typename A>
String<C, T, A> trim_copy(String<C, T, A> s)
{
	return trim_mute(s);
}

template<typename C, typename T, typename A, typename Func>
String<C, T, A> trim_copy(String<C, T, A> s, Func func)
{
	return trim_mute(s, func);
}

// const char* versions
// TODO: specify ws? seek a more holistic solution?

template<typename C>
String<C, std::char_traits<C>, std::allocator<C>> trim_left_copy(C const* s, C const* ws = C(' '))
{
	return trim_left_copy(String<C, std::char_traits<C>, std::allocator<C>>(s, ws));
}

template<typename C>
String<C, std::char_traits<C>, std::allocator<C>> trim_right_copy(C const* s, C const* ws = C(' '))
{
	return trim_right_copy(String<C, std::char_traits<C>, std::allocator<C>>(s, ws));
}

template<typename C>
String<C, std::char_traits<C>, std::allocator<C>> trim_copy(C const* s, C const* ws = C(' '))
{
	return trim_copy(String<C, std::char_traits<C>, std::allocator<C>>(s, ws));
}

//
//template<typename C, std::size_t N>
//String<C, std::char_traits<C>, std::allocator<C>> trim_left_copy(C(&s)[N])
//{
//	return trim_left_copy(String<C, std::char_traits<C>, std::allocator<C>>(s, N));
//}
//
//template<typename C, std::size_t N>
//String<C, std::char_traits<C>, std::allocator<C>> trim_right_copy(C(&s)[N])
//{
//	return trim_right_copy(String<C, std::char_traits<C>, std::allocator<C>>(s, N));
//}
//
//template<typename C, std::size_t N>
//String<C, std::char_traits<C>, std::allocator<C>> trim_copy(C(&s)[N])
//{
//	return trim_copy(String<C, std::char_traits<C>, std::allocator<C>>(s, N));
//}

// std::string_view versions
#if __cplusplus >= 201703L
//---------------------------------------------------------
// trim_mute
//

/**
 * Obtain a copy of a StringView with leading characters removed.
 * @param s The StringView to be trimmed.
 * @param t The set of characters to remove from the beginning
 * of the StringView.
 * @return A trimmed copy of the StringView passed in as a parameter.
 */
template<typename C, typename T>
StringView<C, T> trim_left(StringView<C, T> s, C const* ws)
{
	return s.substr(std::min(s.find_first_not_of(ws), std::size(s)));
}

template<typename C, typename T>
StringView<C, T> trim_left(StringView<C, T> s, StringView<C, T> const& ws)
{
//	return s.substr(0, s.find_first_not_of(ws));
	return s.substr(std::min(s.find_first_not_of(ws), std::size(s)));
}

template<typename C, typename T>
StringView<C, T> trim_left(StringView<C, T> s)
{
	return trim_left_mute(s, detail::ws(C()));
}

template<typename C, typename T, typename Func>
StringView<C, T> trim_left(StringView<C, T> s, Func func)
{
	return s.substr(std::distance(std::begin(s), std::find_if_not(std::begin(s), std::end(s), func)));
}

/**
 * Obtain a copy of a string with trailing characters removed.
 * @param s The std::string to be trimmed.
 * @param t The set of characters to remove from the end
 * of the String.
 * @return A copy of the string passed in as a parameter.
 */
template<typename C, typename T>
StringView<C, T> trim_right(StringView<C, T> s, C const* ws)
{
	return s.substr(0, s.find_last_not_of(ws) + 1);
}

template<typename C, typename T>
StringView<C, T> trim_right(StringView<C, T> s, StringView<C, T> const& ws)
{
	return s.substr(0, s.find_last_not_of(ws) + 1);
}

template<typename C, typename T>
StringView<C, T> trim_right(StringView<C, T> s)
{
	return trim_right_mute(s, detail::ws(C()));
}

template<typename C, typename T, typename Func>
StringView<C, T> trim_right(StringView<C, T> s, Func func)
{
	return s.substr(0, std::distance(std::begin(s), std::find_if_not(std::rbegin(s), std::rend(s), func).base()));
}

/**
 * Obtain a copy of a String with surrounding characters removed.
 * @param s The String to be trimmed.
 * @param t The set of characters to remove from each end
 * of the String.
 * @return A copy of the String passed in as a parameter.
 */
template<typename C, typename T>
StringView<C, T> trim(StringView<C, T> s, C const* ws)
{
	return trim_left(trim_right(s, ws), ws);
}

template<typename C, typename T>
StringView<C, T> trim(StringView<C, T> s, StringView<C, T> const& ws)
{
	return trim_left(trim_right(s, ws), ws);
}

template<typename C, typename T>
StringView<C, T> trim(StringView<C, T> s)
{
	return trim_left(trim_right(s));
}

template<typename C, typename T, typename Func>
StringView<C, T> trim(StringView<C, T> s, Func func)
{
	return trim_left(trim_right(s, func), func);
}

#endif

//---------------------------------------------------------
// trim_keep
//

/**
 * Remove (and keep) leading characters from a String.
 * @param s The String to be trimmed.
 * @param ws The set of characters to remove from the beginning
 * of the String.
 * @return The String of characters that were removed.
 */
template<typename C, typename T, typename A>
String<C, T, A> trim_left_keep(String<C, T, A>& s, C const* ws)
{
	typename String<C, T, A>::size_type pos;
	String<C, T, A> keep = s.substr(0, (pos = s.find_first_not_of(ws)));
	s.erase(0, pos);
	return keep;
}

template<typename C, typename T, typename A>
String<C, T, A> trim_left_keep(String<C, T, A>& s, String<C, T, A> const& ws)
{
	return trim_left_keep(s, ws.c_str());
}

template<typename C, typename T, typename A>
String<C, T, A> trim_left_keep(String<C, T, A>& s)
{
	return trim_left_keep(s, detail::ws(C()));
}

/**
 * Remove (and keep) trailing characters from a String.
 * @param s The String to be trimmed.
 * @param ws The set of characters to remove from the end
 * of the String.
 * @return The String of characters that were removed.
 */
template<typename C, typename T, typename A>
String<C, T, A> trim_right_keep(String<C, T, A>& s, C const* ws)
{
	typename String<C, T, A>::size_type pos;
	String<C, T, A> keep = s.substr((pos = s.find_last_not_of(ws) + 1));
	s.erase(pos);
	return keep;
}

template<typename C, typename T, typename A>
String<C, T, A> trim_right_keep(String<C, T, A>& s, String<C, T, A> const& ws)
{
	return trim_right_keep(s, ws.c_str());
}

template<typename C, typename T, typename A>
String<C, T, A> trim_right_keep(String<C, T, A>& s)
{
	return trim_right_keep(s, detail::ws(C()));
}

/**
 * Remove (and keep) leading and trailing characters from a String.
 * @param s The String to be trimmed.
 * @param ws The set of characters to remove from each end
 * of the String.
 * @return A structure of the form `struct { String left; String right; };`
 * containing each String of characters that was removed.
 */

namespace detail {
template<typename C, typename T, typename A>
struct rv
{
	String<C, T, A> left;
	String<C, T, A> right;
};
} // namespace detail

template<typename C, typename T, typename A>
auto trim_keep(String<C, T, A>& s, C const* ws) -> detail::rv<C, T, A>
{
	return detail::rv<C, T, A>{trim_left_keep(s, ws), trim_right_keep(s, ws)};
}

template<typename C, typename T, typename A>
auto trim_keep(String<C, T, A>& s, String<C, T, A> const& ws) -> detail::rv<C, T, A>
{
	return trim_keep(s, ws.c_str());
}

template<typename C, typename T, typename A>
auto trim_keep(String<C, T, A>& s) -> detail::rv<C, T, A>
{
	return trim_keep(s, detail::ws(C()));
}

// SPLIT ======================================================================================

namespace generic {
template<typename StringT, typename CharT>
std::vector<StringT> split_keep(CharT const* s1, CharT const* s2, CharT const* t1, CharT const* t2)
{
	std::vector<StringT> v;
	algorithm::split_keep(s1, s2, t1, t2, algorithm::inserter(v));
	return v;
}

template<typename StringT, typename CharT>
std::vector<StringT> split(CharT const* s1, CharT const* s2, CharT const* t1, CharT const* t2)
{
	std::vector<StringT> v;
	algorithm::split(s1, s2, t1, t2, algorithm::inserter(v));
	return v;
}
} // namespace generic

template<typename CharT, typename Traits, typename Alloc, typename Traits2, typename Alloc2>
auto split_keep(std::basic_string<CharT, Traits, Alloc> const& s, std::basic_string<CharT, Traits2, Alloc2> const& t)
-> std::vector<std::basic_string<CharT, Traits, Alloc>>
{
	return generic::split_keep<std::basic_string<CharT, Traits, Alloc>>(s.data(), s.data() + s.size(), t.data(), t.data() + t.size());
}

template<typename CharT,
	typename Traits,
	typename Alloc>
auto split_keep(std::basic_string<CharT, Traits, Alloc> const& s)
-> std::vector<std::basic_string<CharT, Traits, Alloc>>
{
	auto t = algorithm::chr::space(CharT());
	return generic::split_keep<std::basic_string<CharT, Traits, Alloc>>(s.data(), s.data() + s.size(), t, algorithm::find_null_terminator(t));
}

template<typename CharT, std::size_t N1, std::size_t N2>
auto split_keep(CharT(&s)[N1], CharT(&t)[N2])
-> std::vector<std::basic_string<typename std::remove_const<CharT>::type>>
{
	HOL_ASSERT_MSG(*(s + N1 - 1) == CharT('\0'), "First parameter is not a null terminates string.");
	HOL_ASSERT_MSG(*(t + N2 - 1) == CharT('\0'), "Second parameter is not a null terminates string.");

	using String = std::basic_string<typename std::remove_const<CharT>::type>;
	return generic::split_keep<String>(s, s + N1 - 1, t, t + N2 -1);
}

template<typename CharT, std::size_t N>
auto split_keep(CharT(&s)[N])
-> std::vector<std::basic_string<typename std::remove_const<CharT>::type>>
{
	HOL_ASSERT_MSG(*(s + N - 1) == CharT('\0'), "Not a null terminated string.");

	auto t = algorithm::chr::space(CharT());
	using String = std::basic_string<typename std::remove_const<CharT>::type>;
	return generic::split_keep<String>(s, s + N - 1, t, t + algorithm::find_null_terminator(t));
}

template<typename CharT,
	typename Traits,
	typename Alloc,
	typename CharT2,
	std::size_t N>
auto split_keep(std::basic_string<CharT, Traits, Alloc> const& s, CharT2(&t)[N])
-> std::vector<std::basic_string<CharT, Traits, Alloc>>
{
	HOL_ASSERT_MSG(*(t + N - 1) == CharT('\0'), "Not a null terminated string.");
	return generic::split_keep<std::basic_string<CharT, Traits, Alloc>>(s.data(), s.data() + s.size(), t, t + N - 1);
}

template<typename CharT,
	std::size_t N,
	typename CharT2,
	typename Traits,
	typename Alloc>
auto split_keep(CharT(&s)[N], std::basic_string<CharT2, Traits, Alloc> const& t)
-> std::vector<std::basic_string<typename std::remove_const<CharT>::type>>
{
	HOL_ASSERT_MSG(*(s + N - 1) == CharT('\0'), "Not a null terminated string.");
	using String = std::basic_string<typename std::remove_const<CharT>::type>;
	return generic::split_keep<String>(s, s + N - 1, t.data(), t.data() + t.size());
}

// Fold versions

template<typename CharT, typename Traits, typename Alloc, typename Traits2, typename Alloc2>
auto split(std::basic_string<CharT, Traits, Alloc> const& s, std::basic_string<CharT, Traits2, Alloc2> const& t)
-> std::vector<std::basic_string<CharT, Traits, Alloc>>
{
	return generic::split<std::basic_string<CharT, Traits, Alloc>>(s.data(), s.data() + s.size(), t.data(), t.data() + t.size());
}

template<typename CharT,
	typename Traits,
	typename Alloc>
auto split(std::basic_string<CharT, Traits, Alloc> const& s)
-> std::vector<std::basic_string<CharT, Traits, Alloc>>
{
	auto t = algorithm::chr::space(CharT());
	return generic::split<std::basic_string<CharT, Traits, Alloc>>(s.data(), s.data() + s.size(), t, algorithm::find_null_terminator(t));
}

template<typename CharT, std::size_t N1, std::size_t N2>
auto split(CharT(&s)[N1], CharT(&t)[N2])
-> std::vector<std::basic_string<typename std::remove_const<CharT>::type>>
{
	HOL_ASSERT_MSG(*(s + N1 - 1) == CharT('\0'), "First parameter is not a null terminates string.");
	HOL_ASSERT_MSG(*(t + N2 - 1) == CharT('\0'), "Second parameter is not a null terminates string.");

	using String = std::basic_string<typename std::remove_const<CharT>::type>;
	return generic::split<String>(s, s + N1 - 1, t, t + N2 - 1);
}

template<typename CharT, std::size_t N>
auto split(CharT(&s)[N])
-> std::vector<std::basic_string<typename std::remove_const<CharT>::type>>
{
	HOL_ASSERT_MSG(*(s + N - 1) == CharT('\0'), "Not a null terminated string.");

	auto t = algorithm::chr::space(CharT());
	using String = std::basic_string<typename std::remove_const<CharT>::type>;
	return generic::split<String>(s, s + N - 1, t, t + algorithm::find_null_terminator(t));
}

template<typename CharT,
	typename Traits,
	typename Alloc,
	typename CharT2,
	std::size_t N>
auto split(std::basic_string<CharT, Traits, Alloc> const& s, CharT2(&t)[N])
-> std::vector<std::basic_string<CharT, Traits, Alloc>>
{
	HOL_ASSERT_MSG(*(t + N - 1) == CharT('\0'), "Not a null terminated string.");
	return generic::split<std::basic_string<CharT, Traits, Alloc>>(s.data(), s.data() + s.size(), t, t + N - 1);
}

// NEW
template<typename CharT,
	typename Traits,
	typename Alloc>
auto split(std::basic_string<CharT, Traits, Alloc> const& s, CharT t)
-> std::vector<std::basic_string<CharT, Traits, Alloc>>
{
	return generic::split<std::basic_string<CharT, Traits, Alloc>>(s.data(), s.data() + s.size(), &t, &t + 1);
}

template<typename CharT,
	std::size_t N,
	typename CharT2,
	typename Traits,
	typename Alloc>
auto split(CharT(&s)[N], std::basic_string<CharT2, Traits, Alloc> const& t)
-> std::vector<std::basic_string<typename std::remove_const<CharT>::type>>
{
	HOL_ASSERT_MSG(*(s + N - 1) == CharT('\0'), "Not a null terminated string.");
	using String = std::basic_string<typename std::remove_const<CharT>::type>;
	return generic::split<String>(s, s + N - 1, t.data(), t.data() + t.size());
}

// string_view
#if __cplusplus >= 201402L
template<typename CharT, typename Traits, typename Traits2>
auto split_keep(StringView<CharT, Traits> s, StringView<CharT, Traits2> t)
{
	return generic::split_keep<StringView<CharT, Traits>>(s.data(), s.data() + s.size(), t.data(), t.data() + t.size());
}

template<typename CharT, typename Traits>
auto split_keep(StringView<CharT, Traits> s)
{
	auto t = algorithm::chr::space(CharT());
	return generic::split_keep<StringView<CharT, Traits>>(s.data(), s.data() + s.size(), t, algorithm::find_null_terminator(t));
}

template<typename CharT,
	typename Traits,
	typename CharT2,
	std::size_t N>
auto split_keep(StringView<CharT, Traits> s, CharT2(&t)[N])
{
	HOL_ASSERT_MSG(*(t + N - 1) == CharT('\0'), "Not a null terminated string.");
	return generic::split_keep<StringView<CharT, Traits>>(s.data(), s.data() + s.size(), t, t + N - 1);
}

// Fold versions

template<typename CharT, typename Traits, typename Traits2>
auto split(StringView<CharT, Traits> s, StringView<CharT, Traits2> t)
{
	return generic::split<StringView<CharT, Traits>>(s.data(), s.data() + s.size(), t.data(), t.data() + t.size());
}

template<typename CharT,
	typename Traits>
auto split(StringView<CharT, Traits> s)
{
	auto t = algorithm::chr::space(CharT());
	return generic::split<StringView<CharT, Traits>>(s.data(), s.data() + s.size(), t, algorithm::find_null_terminator(t));
}

template<typename CharT,
	typename Traits,
	typename Alloc,
	typename CharT2,
	std::size_t N>
auto split(StringView<CharT, Traits> s, CharT2(&t)[N])
{
	HOL_ASSERT_MSG(*(t + N - 1) == CharT('\0'), "Not a null terminated string.");
	return generic::split<StringView<CharT, Traits>>(s.data(), s.data() + s.size(), t, t + N - 1);
}

#endif // __cplusplus >= 201402L

// JOIN ===========================================================================================

template<typename Iter,
	typename C, typename T = std::char_traits<C>, typename A = std::allocator<C>>
String<C, T, A> join(Iter begin, Iter end, String<C, T, A> const& delim)
{
//	auto dist = std::distance(begin, end);
	auto dist = std::size_t(std::distance(begin, end));

	if(!dist)
		return {};

	auto size = ((dist - 1) * delim.size());

	size += std::accumulate(begin, end, std::size_t(0),
		[](std::size_t n, decltype(*begin) const& s){ return n + s.size(); });

	String<C, T, A> s;
	s.reserve(size);

	if(begin != end)
		s = *begin;

	for(++begin; begin != end; ++begin)
		s.append(delim).append(*begin);

	return s;
}

template<typename Iter,
	typename C, typename T = std::char_traits<C>, typename A = std::allocator<C>>
String<C, T, A> join(Iter begin, Iter end, C const* delim)
{
	return join(begin, end, String<C, T, A>(delim));
}

template<typename Iter,
	typename C, typename T = std::char_traits<C>, typename A = std::allocator<C>>
String<C, T, A> join(Iter begin, Iter end)
{
	return join(begin, end, String<C, T, A>(detail::empty(C())));
}

//

template<template<class> class Container,
	typename C, typename T = std::char_traits<C>, typename A = std::allocator<C>>
String<C, T, A> join(Container<String<C, T, A>> const& c, String<C, T, A> const& delim)
{
	return join(std::begin(c), std::end(c), delim);
}

template<template<class> class Container,
	typename C, typename T = std::char_traits<C>, typename A = std::allocator<C>>
String<C, T, A> join(Container<String<C, T, A>> const& c, C const* delim)
{
	return join(c, String<C, T, A>(delim));
}

template<template<class> class Container,
	typename C, typename T = std::char_traits<C>, typename A = std::allocator<C>>
String<C, T, A> join(Container<String<C, T, A>> const& c)
{
	return join(c, String<C, T, A>(detail::empty(C())));
}

// --------------------------------------------------------------------

template<typename Container, class = typename std::enable_if<std::is_convertible<typename Container::pointer,
decltype(std::declval<Container>().data())>::value>::type>
void read_file_into(std::istream& is, Container& c)
{
	if(!is)
		throw std::runtime_error(std::strerror(errno));

	for(char ch; is.get(ch);)
		c.push_back(ch);
}

template<typename Container, class = typename std::enable_if<std::is_convertible<typename Container::pointer,
decltype(std::declval<Container>().data())>::value>::type>
void load_file_into(std::string const& filepath, Container& c)
{
	std::ifstream ifs(filepath, std::ios::binary|std::ios::ate);

	if(!ifs)
		throw std::runtime_error(filepath + ": " + std::strerror(errno));

	auto end = ifs.tellg();
	ifs.seekg(0, std::ios::beg);

	c.resize(std::size_t(end - ifs.tellg()));

	if(!ifs.read((char*)&c[0], mu::integral_cast<std::streamsize>(c.size())))
		throw std::runtime_error(filepath + ": " + std::strerror(errno));
}

template<typename Container, class = typename std::enable_if<std::is_convertible<typename Container::pointer,
decltype(std::declval<Container>().data())>::value>::type>
void load_file_into(std::string const& filepath, Container& c, bool& err)
{
	err = false;

	std::ifstream ifs(filepath, std::ios::binary|std::ios::ate);

	if(!ifs) { err = true; return; }

	auto end = ifs.tellg();
	ifs.seekg(0, std::ios::beg);

	c.resize(std::size_t(end - ifs.tellg()));

	if(!ifs.read((char*)&c[0], mu::integral_cast<std::streamsize>(c.size())))
		err = true;
}


//template<typename Container, class = typename std::enable_if<std::is_convertible<typename Container::pointer,
//decltype(std::declval<Container>().data())>::value>::type>
//void load_file_into(std::string const& filepath, Container& c)
//{
//	std::vector<typename Container::value_type> v;
//	load_file_into(filepath, v);
//	c.assign(std::begin(v), std::end(v));
//}

template<typename Container>
Container load_file_as_or_throw(std::string const& filepath)
{
	Container c;
	load_file_into(filepath, c);
	return c;
}

//HOL_DEPRECATED("Use load_file_as_or_throw()")

template<typename Container>
HOL_DEPRECATED("Use load_file_as_or_throw()")
inline
Container load_file_as(std::string const& filepath)
	{ return load_file_as_or_throw<Container>(filepath); }

inline
std::string load_file_or_throw(std::string const& filepath)
	{ return load_file_as_or_throw<std::string>(filepath); }

HOL_DEPRECATED("Use load_file_or_throw()")
inline
std::string load_file(std::string const& filepath)
	{ return load_file_or_throw(filepath); }

#if __cplusplus >= 201703L

template<typename Container>
std::optional<Container> load_optional_file_as(std::string const& filepath)
{
	Container c;
	bool err;
	load_file_into(filepath, c, err);
	if(err)
		return {};
	return c;
}

inline
auto load_optional_file(std::string const& filepath)
	{ return load_optional_file_as<std::string>(filepath); }

inline
std::vector<std::byte> load_file_as_bytes_or_throw(std::string const& filepath)
	{ return load_file_as_or_throw<std::vector<std::byte>>(filepath); }

HOL_DEPRECATED("load_file_as_bytes_or_throw()")
inline
std::vector<std::byte> load_file_as_bytes(std::string const& filepath)
	{ return load_file_as_bytes_or_throw(filepath); }

inline
auto load_optional_file_as_bytes(std::string const& filepath)
	{ return load_optional_file_as<std::vector<std::byte>>(filepath); }

#endif // __cplusplus >= 201703L

/**
 * Self-erasing buffer
 */
template<std::size_t N, typename CharT = char>
class erasing_buffer
{
public:
	erasing_buffer() noexcept: buff({}) {}
	~erasing_buffer() { fill(data()); }

	CharT volatile* data() { return buff.data(); }
	CharT volatile const* data() const { return buff.data(); }

	std::size_t size() const { return buff.size(); }

	typename std::array<CharT volatile, N>::iterator begin() { return std::begin(buff); }
	typename std::array<CharT volatile, N>::const_iterator begin() const { return std::begin(buff); }

	typename std::array<CharT volatile, N>::iterator end() { return std::end(buff); }
	typename std::array<CharT volatile, N>::const_iterator end() const { return std::end(buff); }

private:
	static void opaque_fill(CharT volatile* data)
		{ std::fill(data, data + N, CharT(0)); }

	// call through function pointer can't be optimized away
	// because it can't be inlined
	void (*fill)(CharT volatile*) = &opaque_fill;
	std::array<CharT volatile, N> buff;
};

// buffer-safe user input with std::strings
//inline
//std::istream& getline(std::istream& is, std::string& s, std::streamsize num, char delim = '\n')
//{
//	std::vector<char> buf(num + 1);
//	if(is.getline(buf.data(), buf.size(), delim))
//		s.assign(buf.data(), is.gcount() - 1);
//	return is;
//}

template<typename CharT,
	typename Traits = std::char_traits<CharT>>
inline
std::basic_istream<CharT, Traits>& getline(std::basic_istream<CharT, Traits>& is,
	std::basic_string<CharT, Traits>& s, std::streamsize num, CharT delim = CharT('\n'))
{
	std::vector<CharT> buf(num + 1);
	if(is.getline(buf.data(), buf.size(), delim))
		s.assign(buf.data(), is.gcount() - 1);
	return is;
}

// General purpose (fast?) stencil.
template<typename StringType>
class basic_stencil
{
	using string_type = StringType;
	using char_type  = typename StringType::value_type; //char;
	using iter = typename string_type::const_iterator;

	struct break_type
	{
		unsigned idx;
		iter first; //beg;
		iter second; //end;

		break_type(unsigned idx): idx(idx) {}
		break_type(unsigned idx, iter beg, iter end): idx(idx), first(beg), second(end) {}

		bool operator==(break_type const& other) const { return first == other.first && second == other.second; }

		bool operator<(break_type const& other)
		{
			if(idx == other.idx)
			{
				if(first == other.first)
					return second < other.second;

				return first < other.first;
			}

			return idx < other.idx;
		}
	};

public:
	basic_stencil(string_type s): m_s(std::move(s)) {}

	basic_stencil(basic_stencil&&) = default;
	basic_stencil(basic_stencil const&) = default;

	basic_stencil& operator=(basic_stencil&&) = default;
	basic_stencil& operator=(basic_stencil const&) = default;

	void compile(std::initializer_list<StringType> list)
		{ compile(std::begin(list), std::end(list)); }

	template<typename StringIter>
	void compile(StringIter beg, StringIter end)
	{
		auto len = std::distance(std::begin(*beg), std::end(*beg));

		for(unsigned n = 0; beg != end; ++beg, ++n)
		{
			len = std::distance(std::begin(*beg), std::end(*beg));

			auto beg_s = m_s.cbegin();

			for(;;)
			{
				auto found = std::search(beg_s, m_s.cend(), beg->cbegin(), beg->cend());

				if(found == m_s.cend())
					break;

				m_breaks.emplace_back(n, found, found + len);

				beg_s = found + len;
			}
		}
	}

	template<typename OutIter>
	void print(std::initializer_list<StringType> list, OutIter out)
		{ print(std::begin(list), std::end(list), out); }

	template<typename StringIter, typename OutIter>
	void print(StringIter beg, StringIter end, OutIter out)
	{
		if(m_breaks.empty())
		{
			std::copy(std::begin(m_s), std::end(m_s), out);
			return;
		}

		auto idx = m_breaks.front().idx;

		auto p = m_s.cbegin();

		for(auto const& b: m_breaks)
		{
			if(idx != b.idx)
			{
				++beg;
				idx = b.idx;
			}

			if(beg == end)
				break;

			std::copy(p, b.first, out);
			std::copy(beg->cbegin(), beg.cend(), out);

			p = b.second;
		}

		std::copy(p, m_s.cend(), out);
	}

private:
	string_type m_s;
	std::vector<break_type> m_breaks;
};

using stencil = basic_stencil<std::string>;
using wstencil = basic_stencil<std::wstring>;
using u16stencil = basic_stencil<std::u16string>;
using u32stencil = basic_stencil<std::u32string>;

#if __cplusplus >= 201402L
using stencil_view = basic_stencil<StringView<char>>;
using wstencil_view = basic_stencil<StringView<wchar_t>>;
using u16stencil_view = basic_stencil<StringView<char16_t>>;
using u32stencil_view = basic_stencil<StringView<char32_t>>;
#endif

/**
 * Usage:
 *
  	mapped_stencil ms;

	mapped_stencil::dict d =
	{
		{"${a}", "A"},
		{"${b}", "Beeeef"},
		{"${c}", "C"},
	};

	std::string text = "${c}some text to ${a} test mapped_stencil ${b}";

	ms.compile(text, std::set<std::string>{"${a}", "${b}", "${c}"});

	ms.create(d, std::cout);
	std::cout << '\n';

	ms.preallocate(d);
	auto s = ms.create(d);
 *
 */
class mapped_stencil
{
	struct place
	{
		using set = std::set<place>;
		using vec = std::vector<place>;

		char const* pos;
		std::size_t len;

		place(char const* pos, std::size_t len): pos(pos), len(len) {}

		bool operator<(place const& other) const { return pos < other.pos; }
	};

	// order: piece, var, piece, var, piece
	place::vec vars;
	std::string text;
	std::ptrdiff_t diff = 0;

public:
	using dict = std::unordered_map<std::string, std::string>;

	/**
	 * Create an empty stencil
	 */
	mapped_stencil() {}
	mapped_stencil(mapped_stencil&& s): vars(std::move(s.vars)), text(std::move(s.text)) {}
	mapped_stencil(const mapped_stencil& s): vars(s.vars), text(s.text) {}

	mapped_stencil& operator=(mapped_stencil&& s) { vars = std::move(s.vars); text = std::move(s.text); return *this; }
	mapped_stencil& operator=(mapped_stencil const& s) { vars = s.vars; text = s.text; return *this; }

	void clear() { vars.clear(); text.clear(); diff = 0; }

	template<typename Container>
	void compile(std::string const& text, Container const& vars)
	{
		clear();

		this->text = text;

		for(auto&& v: vars)
			for(auto pos = 0ULL; (pos = text.find(v, pos)) != std::string::npos; pos += v.size())
				this->vars.emplace_back(this->text.data() + pos, v.size());

		std::sort(std::begin(this->vars), std::end(this->vars));

		diff = text.size();
	}

	void create(dict const& d, std::ostream& os) const
	{
		auto pos = text.data();

		for(auto const& v: vars)
		{
			if(v.pos > pos)
				os.write(pos, v.pos - pos);

			auto found = d.find({v.pos, v.len});

			if(found != d.end())
				os.write(found->second.data(), mu::integral_cast<std::streamsize>(found->second.size()));

			pos = v.pos + v.len;
		}

		if(pos < text.data() + text.size())
			os.write(pos, mu::integral_cast<std::streamsize>(text.size()));
	}

	/**
	 * Call preallocate() before calling `std::string create(dict const& d)`
	 * in order to set the internal string allocation size to exactly match
	 * the created string (rather than using an estimation).
	 *
	 * This function only needs to be called once every time the dictionary `dict`
	 * changes.
	 *
	 * @param d The `dict` that will be used when calling the `create()` function.
	 */
	void preallocate(dict const& d)
	{
		for(auto const& v: vars)
		{
			auto found = d.find({v.pos, v.len});
			if(found == d.end())
				continue;

			diff -= mu::integral_cast<decltype(diff)>(found->first.size());
			diff += mu::integral_cast<decltype(diff)>(found->second.size());
		}
	}

	/**
	 * Call preallocate first to obtain exact string allocation.
	 * Call preallocate only needed when `dict`changes.
	 * @param d
	 * @return
	 */
	std::string create(dict const& d) const
	{
		if(vars.empty())
			return text;

		std::string out;
		out.reserve(text.size() + mu::integral_cast<std::size_t>(diff));

		auto pos = text.data();

		for(auto const& v: vars)
		{
			if(v.pos > pos)
				out.append(pos, v.pos);

			auto found = d.find({v.pos, v.len});

			if(found != d.end())
				out.append(found->second);

			pos = v.pos + v.len;
		}

		if(pos < text.data() + text.size())
			out.append(pos, text.size());

		return out;
	}
};

// General text processing

namespace detail {

inline
std::size_t strlen(char const* s) { return std::strlen(s); }

template<typename CharT>
std::size_t strlen(CharT const* s)
{
	auto p = s;
	while(*p) ++p;
	return std::size_t(p - s);
}

template<typename CharT>
std::size_t size(CharT const* s) { return strlen(s); }

template<typename C, typename T, typename A>
std::size_t size(std::basic_string<C, T, A> const& s) { return s.size(); }

} // namespace detail

// Delimited Text

template<typename C, typename T, typename A, typename Delim1, typename Delim2>
typename std::basic_string<C, T, A>::size_type extract_delimited_text(
	std::basic_string<C, T, A> const& s,
	Delim1 const& d1,
	Delim2 const& d2,
	std::basic_string<C, T, A>& out,
	std::size_t pos = 0)
{
	out.clear();

	if(auto beg = s.find(d1, pos) + 1)
	{
		beg += detail::size(d1) - 1;
		if(auto end = s.find(d2, beg) + 1)
		{
			--end;
			out = s.substr(beg, end - beg);
			return end + detail::size(d2);
		}
	}
	return std::basic_string<C, T, A>::npos;
}

template<typename C, typename T, typename A, typename Delim1, typename Delim2>
std::basic_string<C, T, A> extract_delimited_text(
	C const* s,
	Delim1 const& d1,
	Delim2 const& d2,
	std::size_t pos = 0)
{
	std::basic_string<C, T, A> out;
	pos = extract_delimited_text(std::basic_string<C, T, A>(s), d1, d2, out, pos);
	return out;
}

template<typename C, typename T = std::char_traits<C>, typename A = std::allocator<C>,
	typename Integer>
std::basic_string<C, T, A> pad(Integer i, std::size_t n, C padding = C('0'))
{
	std::basic_ostringstream<C, T, A> oss;
	oss << i;

	auto s = oss.str();

	if(s.empty())
		return std::basic_string<C, T, A>(n - s.size(), padding);

	if(s.size() < n)
	{
		if(s[0] == C('-'))
			return C('-') + std::basic_string<C, T, A>(n - s.size(), padding) + s.substr(1);

		return std::basic_string<C, T, A>(n - s.size(), padding) + s;
	}
	return s;
}

template<typename Integer>
std::string spad(Integer i, std::size_t n, char padding = '0') { return pad<char>(i, n, padding); }

template<typename Integer>
std::wstring wpad(Integer i, std::size_t n, wchar_t padding = wchar_t('0')) { return pad<wchar_t>(i, n, padding); }

template<typename Integer>
std::u16string u16pad(Integer i, std::size_t n, char16_t padding = char16_t('0')) { return pad<char16_t>(i, n, padding); }

template<typename Integer>
std::u32string u32pad(Integer i, std::size_t n, char32_t padding = char32_t('0')) { return pad<char32_t>(i, n, padding); }

// String padding

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc>& pad_left_mute(std::basic_string<CharT, Traits, Alloc>& s,
	std::size_t w, CharT filler = CharT(' '))
{
	if(s.size() < w)
		s.insert(std::end(s), w - s.size(), filler);
	return s;
}

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc> pad_left_copy(std::basic_string<CharT, Traits, Alloc> s,
	std::size_t w, CharT filler = CharT(' '))
{
	return pad_left_mute(s, w, filler);
}

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc>& pad_right_mute(std::basic_string<CharT, Traits, Alloc>& s,
	std::size_t w, CharT filler = CharT(' '))
{
	if(s.size() < w)
		s.insert(std::begin(s), w - s.size(), filler);
	return s;
}

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc> pad_right_copy(std::basic_string<CharT, Traits, Alloc> s,
	std::size_t w, CharT filler = CharT(' '))
{
	return pad_right_mute(s, w, filler);
}

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc>& pad_center_mute(std::basic_string<CharT, Traits, Alloc>& s,
	std::size_t w, CharT filler = CharT(' '))
{
	if(s.size() < w)
	{
		auto lw = (w - s.size()) / 2;
		auto rw = (w - s.size()) - lw;
		s.insert(std::begin(s), lw, filler);
		s.insert(std::end(s), rw, filler);
	}

	return s;
}

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc> pad_center_copy(std::basic_string<CharT, Traits, Alloc> s,
	std::size_t w, CharT filler = CharT(' '))
{
	return pad_center_mute(s, w, filler);
}

//namespace utf8 {
//
//inline std::string from_ws(std::wstring const& s)
//{
//	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> cnv;
//	std::string utf8 = cnv.to_bytes(s);
//	if(cnv.converted() < s.size())
//		throw std::runtime_error("incomplete conversion");
//	return utf8;
//}
//
//inline std::wstring to_ws(std::string const& utf8)
//{
//	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> cnv;
//	std::wstring s = cnv.from_bytes(utf8);
//	if(cnv.converted() < utf8.size())
//		throw std::runtime_error("incomplete conversion");
//	return s;
//}
//
//} // namespace utf8


// NEW TRIM FUNCTIONS 2019-01-14
#if __cplusplus >= 201703L

namespace trial {
namespace detail {

template<typename CharT, typename Traits>
bool contains(std::basic_string_view<CharT, Traits> s, CharT c)
	{ return std::find(std::begin(s), std::end(s), c) != std::end(s); }

template<typename CharT>
constexpr CharT const* ws();// { return " \n\t"; }

template<>
constexpr char const* ws<char>() { return " \n\t"; }

template<>
constexpr wchar_t const* ws<wchar_t>() { return L" \n\t"; }

template<>
constexpr char16_t const* ws<char16_t>() { return u" \n\t"; }

template<>
constexpr char32_t const* ws<char32_t>() { return U" \n\t"; }

namespace {
constexpr std::byte const internal_byte_buf_ws[] = {std::byte(' '), std::byte('\n'), std::byte('\t'), std::byte('\0')};
} // namespace

template<>
constexpr std::byte const* ws<std::byte>() { return internal_byte_buf_ws; }

}  // namespace detail

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc>& trim_from_left(
	std::basic_string<CharT, Traits, Alloc>& s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
{
	s.erase(std::begin(s), std::find_if(std::begin(s), std::end(s), [t](CharT c){
		return not detail::contains(t, c);
	}));

	return s;
}

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc>& trim_from_right(
	std::basic_string<CharT, Traits, Alloc>& s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
{
	s.erase(std::find_if(std::rbegin(s), std::rend(s), [t](CharT c){
		return not detail::contains(t, c);
	}).base(), std::end(s));

	return s;
}

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc>& trim(
	std::basic_string<CharT, Traits, Alloc>& s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
		{ return trim_from_left(trim_from_right(s, t), t); }

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc> left_trimmed_copy_of(
	std::basic_string<CharT, Traits, Alloc> s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
		{ return trim_from_left(s, t); }

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc> right_trimmed_copy_of(
	std::basic_string<CharT, Traits, Alloc> s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
		{ return trim_from_right(s, t); }

template<typename CharT, typename Traits, typename Alloc>
std::basic_string<CharT, Traits, Alloc> trimmed_copy_of(
	std::basic_string<CharT, Traits, Alloc> s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
		{ return trim(s, t); }

template<typename CharT, typename Traits = std::char_traits<CharT>, typename Alloc = std::allocator<CharT>>
std::basic_string<CharT, Traits, Alloc> left_trimmed_copy_of(
	CharT const* s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
		{ std::basic_string<CharT, Traits, Alloc> r(s); return trim_from_left(r, t); }

template<typename CharT, typename Traits = std::char_traits<CharT>, typename Alloc = std::allocator<CharT>>
std::basic_string<CharT, Traits, Alloc> right_trimmed_copy_of(
	CharT const* s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
		{ std::basic_string<CharT, Traits, Alloc> r(s); return trim_from_right(r, t); }

template<typename CharT, typename Traits = std::char_traits<CharT>, typename Alloc = std::allocator<CharT>>
std::basic_string<CharT, Traits, Alloc> trimmed_copy_of(
	CharT const* s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
		{ std::basic_string<CharT, Traits, Alloc> r(s); return trim(r, t); }

// std::string_view

template<typename CharT, typename Traits>
std::basic_string_view<CharT, Traits> trim_from_left(
	std::basic_string_view<CharT, Traits> s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
{
	s.remove_prefix(std::size_t(std::distance(std::begin(s), std::find_if(std::begin(s), std::end(s), [t](CharT c){
		return not detail::contains(t, c);
	}))));

	return s;
}

template<typename CharT, typename Traits>
std::basic_string_view<CharT, Traits> trim_from_right(
	std::basic_string_view<CharT, Traits> s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
{
	s.remove_suffix(std::size_t(std::distance(std::find_if(std::rbegin(s), std::rend(s), [t](CharT c){
		return not detail::contains(t, c);
	}).base(), std::end(s))));

	return s;
}

template<typename CharT, typename Traits>
std::basic_string_view<CharT, Traits> trim(
	std::basic_string_view<CharT, Traits> s,
	std::basic_string_view<CharT, Traits> t = detail::ws<CharT>())
		{ return trim_from_left(trim_from_right(s, t), t); }

}  // namespace trial

#endif // __cplusplus >= 201703L

} // string_utils
} // header_only_library

#endif // HEADER_ONLY_LIBRARY_STRING_UTILS_H
