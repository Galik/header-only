#ifndef HEADER_ONLY_LIBRARY_BYTE_BUFFER_H
#define HEADER_ONLY_LIBRARY_BYTE_BUFFER_H
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <cerrno>
#include <cstddef>
#include <cstring>
#include <fstream>
#include <memory>
#include <vector>

#include "misc_utils.h"

#if __cplusplus < 201703
#error "This library requires C++17 or later."
#endif

namespace header_only_library {
namespace containers {
namespace mu = misc_utils;

class byte_buffer
{
public:
	using size_type = std::size_t;

	using value_type = std::byte;
	using pointer = value_type*;
	using const_pointer = value_type const*;
	using reference = value_type&;
	using const_reference = value_type const&;
	using iterator = pointer;
	using const_iterator = const_pointer;

	explicit byte_buffer() noexcept {}

	explicit byte_buffer(size_type n)
	: m_size(n), m_bytes(std::make_unique<std::byte[]>(n)) {}

	explicit byte_buffer(const_pointer beg, const_pointer end)
	: m_size(distance(beg, end)), m_bytes(std::make_unique<std::byte[]>(m_size))
		{ std::copy(beg, end, m_bytes.get()); }

	explicit byte_buffer(const_pointer pos, size_type n)
	: byte_buffer(pos, pos + n) {}

	byte_buffer(byte_buffer&&) = default;
	byte_buffer(byte_buffer const&) = default;

	byte_buffer& operator=(byte_buffer&&) = default;
	byte_buffer& operator=(byte_buffer const&) = default;

	size_type size() const { return m_size; }
	bool empty() const { return m_size == 0; }

	reference       operator[](size_type n)       { return m_bytes[n]; }
	const_reference operator[](size_type n) const { return m_bytes[n]; }

	pointer       data()       { return m_bytes.get(); }
	const_pointer data() const { return m_bytes.get(); }

	pointer       data_end()       { return data() + size(); }
	const_pointer data_end() const { return data() + size(); }

	iterator begin() { return data(); }
	iterator   end() { return data() + size(); }

	const_iterator begin() const { return data(); }
	const_iterator   end() const { return data() + size(); }

	const_iterator cbegin() const { return data(); }
	const_iterator   cend() const { return data() + size(); }

	void resize(size_type n)
	{
		auto bytes = std::make_unique<value_type[]>(n);
		std::copy(begin(), begin() + std::min(n, size()), bytes.get());
		std::swap(m_bytes, bytes);
	}

	byte_buffer copy() const
		{ return byte_buffer(cbegin(), cend()); }

	byte_buffer copy(size_type pos) const
		{ return byte_buffer(cbegin() + pos, cend()); }

	byte_buffer copy(size_type pos, size_type n) const
		{ return byte_buffer(cbegin() + pos, std::min(n, size() - pos)); }

	std::unique_ptr<std::byte[]> release() { m_size = 0; return std::move(m_bytes); };

	explicit operator bool() const { return m_bytes && m_size; }

private:
	static
	size_type distance(const_pointer beg, const_pointer end)
		{ return mu::integral_cast<size_type>(std::distance(beg, end)); }

	std::size_t m_size = 0;
	std::unique_ptr<value_type[]> m_bytes;
};



inline
byte_buffer load_file(char const* filepath)
{
    std::ifstream ifs(filepath, std::ios::binary|std::ios::ate);

    if(!ifs)
        throw std::runtime_error(std::string(filepath) + ": " + std::strerror(errno));

    auto end = ifs.tellg();
    ifs.seekg(0, std::ios::beg);

    auto size = mu::integral_cast<std::size_t>(end - ifs.tellg());

    if(size == 0)
        return byte_buffer();

    byte_buffer file_data(size);

    if(!ifs.read((char*)file_data.data(), std::streamsize(file_data.size())))
    	throw std::runtime_error(std::string(filepath) + ": " + std::strerror(errno));

    return file_data;
}

inline
byte_buffer load_file(std::string const& filepath)
{
	return load_file(filepath.c_str());
}

//
//inline
//byte_buffer load_file(std::string const& filepath)
//{
//    std::ifstream ifs(filepath, std::ios::binary|std::ios::ate);
//
//    if(!ifs)
//        throw std::runtime_error(filepath + ": " + std::strerror(errno));
//
//    auto end = ifs.tellg();
//    ifs.seekg(0, std::ios::beg);
//
//    auto size = mu::integral_cast<std::size_t>(end - ifs.tellg());
//
//    if(size == 0)
//        return byte_buffer();
//
//    byte_buffer file_data(size);
//
//    if(!ifs.read((char*)file_data.data(), std::streamsize(file_data.size())))
//    	throw std::runtime_error(filepath + ": " + std::strerror(errno));
//
//    return file_data;
//}

} // namespace containers
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_BYTE_BUFFER_H
