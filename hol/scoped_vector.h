#ifndef HEADER_ONLY_LIBRARY_SCOPED_VECTOR_H
#define HEADER_ONLY_LIBRARY_SCOPED_VECTOR_H
/*
 * scoped_vector.h
 *
 *  Created on: 22 May 2019
 *      Author: galik
 */

#include <algorithm>
#include <array>
#include <cassert>
#include <memory>

namespace header_only_library {
namespace containers {

template<typename T, std::size_t MaxSize>
class scoped_vector
{
public:
	using value_type = T;
	using pointer = value_type*;
	using const_pointer = value_type const*;
	using reference = value_type&;
	using const_reference = value_type const&;
	using copy_reference = const_reference;
	using move_reference = value_type&&;

	using iterator = pointer;
	using const_iterator = const_pointer;
	using reverse_iterator = std::reverse_iterator<iterator>;
	using const_reverse_iterator = std::reverse_iterator<const_iterator>;

	using size_type = std::size_t;
	using ssize_type = std::ptrdiff_t;

	scoped_vector() {}

	explicit scoped_vector(size_type n)
	{
		resize(n);
	}

	scoped_vector(scoped_vector&&) = delete;
	scoped_vector(scoped_vector const&) = delete;

	scoped_vector& operator=(scoped_vector&&) = delete;
	scoped_vector& operator=(scoped_vector const& other)
	{
		assign(std::begin(other), std::end(other));
		return *this;
	}

	~scoped_vector() { delete_all(); }

	size_type capacity() const { return max_size(); }

	size_type size() const { return size_type(m_end - m_beg); }
	ssize_type ssize() const { return ssize_type(size()); }

	size_type max_size() const { return MaxSize; }

	bool empty() const { return size() == 0; }


	pointer data() { return m_beg; }
	const_pointer data() const { return m_beg; }

	pointer data_end() { return m_end; }
	const_pointer data_end() const { return m_end; }


	iterator                begin()       { return m_beg; }
	iterator                  end()       { return m_end; }

	const_iterator          begin() const { return m_beg; }
	const_iterator            end() const { return m_end; }

	const_iterator         cbegin() const { return const_cast<T const*>(m_beg); }
	const_iterator           cend() const { return const_cast<T const*>(m_end); }

	reverse_iterator       rbegin()       { return reverse_iterator(begin()); }
	reverse_iterator         rend()       { return reverse_iterator(end()); }

	const_reverse_iterator rbegin() const { return const_reverse_iterator(begin()); }
	const_reverse_iterator   rend() const { return const_reverse_iterator(end()); }


	reference       operator[](size_type n)       { assert(n < size()); return m_beg[n]; }
	const_reference operator[](size_type n) const { assert(n < size()); return m_beg[n]; }

	reference       at(size_type n)       { if(n < size()) throw std::out_of_range(); return m_beg[n]; }
	const_reference at(size_type n) const { if(n < size()) throw std::out_of_range(); return m_beg[n]; }


	reference       front()       { return *m_beg; }
	const_reference front() const { return *m_beg; }

	reference       back()       { return *std::prev(m_end); }
	const_reference back() const { return *std::prev(m_end); }

	void clear() noexcept { delete_all(); }

	void assign(size_type n, T const& v)
	{
		auto new_end = m_beg + n;

		assert(new_end <= capacity());

		auto end = std::clamp(new_end, m_beg, m_end);

		std::for_each(m_beg, end, [&](reference vv){
			vv = v;
		});

		std::for_each(end, new_end, [&](reference vv){
			new (&vv) value_type(v);
		});

		m_end = new_end;
	}

	template<typename InputIt>
	void assign(InputIt first, InputIt last)
	{
		auto new_end = m_beg + std::distance(first, last);

		assert(new_end <= capacity());

		auto end = std::clamp(new_end, m_beg, m_end);

		std::for_each(m_beg, end, [&](reference vv){
			vv = *first++;
		});

		std::for_each(end, new_end, [&](reference vv){
			new (&vv) value_type(*first++);
		});

		m_end = new_end;
	}

	void assign(std::initializer_list<T> ilist)
	{
		assign(std::begin(ilist), std::end(ilist));
	}

	void push_back(move_reference v)
	{
		assert(size() < capacity());
		new (m_end) T(std::move(v));
		++m_end;
	}

	void push_back(copy_reference v)
	{
		assert(size() < capacity());
		auto copy = v;
		push_back(std::move(copy));
	}

	template<typename... Args>
	void emplace_back(Args&&... args)
	{
		assert(size() < capacity());
		new (m_end) T(std::forward<Args>(args)...);
		++m_end;
	}

	iterator insert(const_iterator pos, move_reference v)
	{
		assert(size() < capacity());
		auto new_beg = m_end;
		push_back(std::move(v));
		std::rotate(m_beg + std::distance(const_iterator(m_beg), pos), new_beg, m_end);
		return iterator(pos);
	}

	iterator insert(const_iterator pos, copy_reference v)
	{
		assert(size() < capacity());
		auto copy = v;
		return insert(pos, std::move(copy));
	}

	iterator insert(const_iterator pos, size_type n, copy_reference v)
	{
		assert(size() + n < capacity());
		auto new_beg = m_end;
		while(n--)
			push_back(v);
		std::rotate(m_beg + std::distance(const_iterator(m_beg), pos), new_beg, m_end);
		return iterator(pos);
	}

	// TODO: make a std::move friendly version?
	template<typename InputIt>
	iterator insert(const_iterator pos, InputIt first, InputIt last)
	{
		assert(size() + size_type(std::distance(first, last)) < capacity());
		auto new_beg = m_end;
		while(first != last)
			push_back(*first++);
		std::rotate(m_beg + std::distance(const_iterator(m_beg), pos), new_beg, m_end);
		return iterator(pos);
	}

	// TODO: make this std::move friendly?
	iterator insert(const_iterator pos, std::initializer_list<T> ilist)
	{
		return insert(pos, std::begin(ilist), std::end(ilist));
	}

	template<typename... Args>
	iterator emplace(const_iterator pos, Args&&... args)
	{
		assert(size() < capacity());
		auto new_beg = m_end;
		emplace_back(std::forward<Args>(args)...);
		std::rotate(m_beg + std::distance(const_iterator(m_beg), pos), new_beg, m_end);
		return iterator(pos);
	}

	iterator erase(const_iterator pos)
	{
		assert(pos != m_end);
		return erase(pos, std::next(pos));
	}

	iterator erase(const_iterator first, const_iterator last)
	{
		auto beg = m_beg + std::distance(const_iterator(m_beg), first);
		auto end = m_beg + std::distance(const_iterator(m_beg), last);

		assert(std::distance(m_beg, beg) <= ssize());
		assert(std::distance(m_beg, end) <= ssize());
		assert(std::distance(m_beg, beg) <= std::distance(m_beg, end));

		auto new_end = std::move(end, m_end, beg);
		std::for_each(end, m_end, [](T& v){ v.~T(); });
		m_end = new_end;

		return end;
	}

	void pop_back()
	{
		assert(m_beg != m_end);
		(--m_end)->~T();
	}

	void resize(size_type n)
	{
		auto new_end = m_beg + n;

		if(new_end < m_end)
			std::for_each(new_end, m_end, [](T& v){ v.~T(); });
		else
			std::for_each(m_end, new_end, [](T& v){ new (&v) value_type(); });

		m_end = new_end;
	}

	void resize(size_type n, value_type const& v);


private:

	void delete_all()  noexcept
	{
		std::for_each(m_beg, m_end, [](T& v){ v.~T(); });
		m_end = m_beg;
	}

	std::array<std::byte, (MaxSize + 1) * sizeof(T)> m_arr;
	pointer const m_beg = [this]{
		size_type space = m_arr.size();
		auto data = reinterpret_cast<void*>(m_arr.data());
		auto m = std::align(alignof(T), sizeof(T), data, space);
		return reinterpret_cast<pointer>(m);
	}();
	pointer m_end = m_beg;
};

template<typename T, std::size_t MaxSize, typename V>
void erase(scoped_vector<T, MaxSize>& c, V const& v)
	{ c.erase(std::remove(std::begin(c), std::end(c), v), std::end(c)); }

template<typename T, std::size_t MaxSize, typename Pred>
void erase_if(scoped_vector<T, MaxSize>& c, Pred pred)
	{ c.erase(std::remove_if(std::begin(c), std::end(c), pred), std::end(c)); }

}  // namespace containers
}  // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_SCOPED_VECTOR_H
