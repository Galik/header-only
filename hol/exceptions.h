#ifndef HEADER_ONLY_LIBRARY_EXCEPTIONS_H
#define HEADER_ONLY_LIBRARY_EXCEPTIONS_H
//
// Copyright (c) 2019 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <cerrno>
#include <sstream>
#include <stdexcept>
#include <cstring>

#include <memory> // hol::backtrace()

#ifdef NDEBUG
#   define hol_throw_exception(e, m) do{std::ostringstream o;o<<m;throw e(o.str());}while(0)
#else
#   define hol_throw_exception(e, m) do{ \
	std::ostringstream o; \
	o << __FILE__ << ":" << __LINE__ << ":error: " << m; \
	throw e(o.str());}while(0)
#endif // NDEBUG

#define hol_throw_runtime_error(m) hol_throw_exception(std::runtime_error, m)
#define hol_throw_errno() hol_throw_runtime_error(std::strerror(errno))
#define hol_throw_errno_msg(m) hol_throw_runtime_error(m << ": " << std::strerror(errno))

#ifdef __linux__
#include <string>
#include <vector>
#ifndef NDEBUG
#include <cxxabi.h>
#include <csignal>
#include <ucontext.h>
#include <execinfo.h>

namespace header_only_library {
namespace exceptions {

class traceable_runtime_error
: public std::runtime_error
{
	friend std::vector<std::string> backtrace(traceable_runtime_error const& e);

public:
	traceable_runtime_error(): std::runtime_error(std::string()) {}
	traceable_runtime_error(std::string const& msg): std::runtime_error(msg)
		{ m_trace_size = std::size_t(::backtrace(m_trace, sizeof(m_trace) / sizeof(m_trace[0]))); }

private:
	void* m_trace[32];
	std::size_t m_trace_size = sizeof(m_trace) / sizeof(m_trace[0]);
};

inline
std::vector<std::string> backtrace(traceable_runtime_error const& e)
{
	std::vector<std::string> bt;

	struct messages_dter{void operator()(void*vp)const{std::free(vp);}};
	using messages_table_ptr = std::unique_ptr<char*[], messages_dter>;

	struct name_dter{void operator()(void*vp)const{std::free(vp);}};
	using name_ptr = std::unique_ptr<char, name_dter>;

	messages_table_ptr messages(backtrace_symbols(e.m_trace, int(e.m_trace_size)));

	for(std::size_t i = 2; i < e.m_trace_size - 2; ++i)
	{
		if(auto pos = std::strchr(messages[i], '('))
		{
			if(auto end = std::strchr(++pos, '+'))
			{
				std::string mangled(pos, end);
				int status;
				if(auto name = name_ptr(abi::__cxa_demangle(mangled.c_str(), 0, 0, &status)))
					mangled = name.get();

				std::string pre(messages[i], pos);
				std::string post(end);
				bt.push_back(pre + mangled + post);
				continue;
			}
		}

		bt.push_back(messages[i]);
	}

//	std::free(messages);

	return bt;
}

} // namespace exceptions
} // namespace header_only_library

#else // NDEBUG

namespace header_only_library {
namespace exceptions {

class traceable_runtime_error
: public std::runtime_error
{
	friend std::vector<std::string> backtrace(traceable_runtime_error const& e);

public:
	traceable_runtime_error(): std::runtime_error(std::string()) {}
	traceable_runtime_error(std::string const& msg): std::runtime_error(msg) {}

private:
};

} // namespace exceptions
} // namespace header_only_library

#endif // NDEBUG
// # define hol_throw_tracable_error(m) hol_throw_exception(header_only_library::exceptions::tracable_runtime_error, m)
#endif // __linux__

#endif // HEADER_ONLY_LIBRARY_EXCEPTIONS_H
