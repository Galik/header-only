#ifndef HEADER_ONLY_LIBRARY_ENDIAN_UTILS_H
#define HEADER_ONLY_LIBRARY_ENDIAN_UTILS_H
//
// Copyright (c) 2017 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <algorithm>
#include <cinttypes>

#if __cplusplus >= 201703
#include <cstddef>
#endif

#if __cplusplus > 201703L
#include <bit> // std::endian
#endif

#ifndef IF_CONSTEXPR
#ifdef __cpp_if_constexpr
#define IF_CONSTEXPR(cond) if constexpr(cond)
#else
#define IF_CONSTEXPR(cond) if(cond)
#endif // __cpp_if_constexpr
#endif // IF_CONSTEXPR

namespace header_only_library {
namespace endian_utils {

#if __cplusplus > 201703L
using std::endian;
#else
enum class endian
{
#ifdef _WIN32
    little = 0,
    big    = 1,
    native = little
#else
	little  = __ORDER_LITTLE_ENDIAN__,
	big     = __ORDER_BIG_ENDIAN__,
	native  = __BYTE_ORDER__,
#endif
};
#endif

constexpr bool is_little_endian()
{
#if __cplusplus > 201703L
	return std::endian::native == std::endian::little;
#else
	return endian::native == endian::little;
#endif
}

constexpr bool is_big_endian()
{
	return !is_little_endian();
}

constexpr endian endianness()
{
	return is_little_endian() ? endian::little : endian::big;
}

template<typename Numeric>
Numeric swap_endianness(Numeric n)
{
	std::reverse((char*)&n, ((char*)&n) + sizeof(n));
	return n;
}

template<typename Numeric>
constexpr Numeric from_little_endian(Numeric n)
{
	IF_CONSTEXPR(is_little_endian())
		return n;
	return swap_endianness(n);
}

template<typename Numeric>
constexpr Numeric from_big_endian(Numeric n)
{
	IF_CONSTEXPR(is_big_endian())
		return n;
	return swap_endianness(n);
}

template<typename Numeric>
constexpr Numeric from_network_byte_order(Numeric n)
	{ return from_big_endian(n); }

template<typename Numeric>
constexpr Numeric to_little_endian(Numeric n)
{
	IF_CONSTEXPR(is_little_endian())
		return n;
	return swap_endianness(n);
}

template<typename Numeric>
constexpr Numeric to_big_endian(Numeric n)
{
	IF_CONSTEXPR(is_big_endian())
		return n;
	return swap_endianness(n);
}

template<typename Numeric>
constexpr Numeric to_network_byte_order(Numeric n)
	{ return to_big_endian(n); }

//template<typename Numeric>
//endian discover_endianness(Numeric const& field, Numeric&& expected_value)
//{
//	if(from_little_endian(field) == expected_value)
//		return endian::little;
//	else if(from_big_endian(field))
//		return endian::big;
//	return endian::unknown;
//}

template<typename Numeric>
constexpr Numeric from_endian(endian e, Numeric n)
{
	if(e == endian::little)
		return from_little_endian(n);
	else if(e == endian::big)
		return from_big_endian(n);
	return n;
}

// I'll just put these here for now (they may move)

template<typename Char, typename T>
Char* encode_big_endian_order(T& t, Char* data)
{
	using const_char_type_ptr = typename std::remove_const<Char>::type const*;

	static_assert(std::is_standard_layout<T>::value && std::is_trivial<T>::value, "First parameter must be a POD.");

	static_assert(std::is_same<Char*, char*>::value
		|| std::is_same<Char*, unsigned char*>::value
#if __cplusplus >= 201703
		|| std::is_same<Char*, std::byte*>::value
		, "Second parameter must be a char*, unsigned char* or std::byte*");
#else
		, "Second parameter must be a char* or an unsigned char*");
#endif

//	using char_type = typename std::remove_const<typename std::remove_pointer<CharPtr>::type>::type;

	IF_CONSTEXPR(is_big_endian())
		std::copy(reinterpret_cast<const_char_type_ptr>(&t), reinterpret_cast<const_char_type_ptr>(&t) + sizeof(T), data);
	else
		std::reverse_copy(reinterpret_cast<const_char_type_ptr>(&t), reinterpret_cast<const_char_type_ptr>(&t) + sizeof(T), data);

	return data + sizeof(T);
}

template<typename Char, typename T>
Char* decode_big_endian_order(Char* data, T& t)
{
	using char_type_ptr = typename std::remove_const<Char>::type*;

	static_assert(!std::is_const<T>::value ,"Second parameter may not be const");

	static_assert(std::is_same<char_type_ptr, char*>::value
		|| std::is_same<char_type_ptr, unsigned char*>::value
#if __cplusplus >= 201703
		|| std::is_same<char_type_ptr, std::byte*>::value
		, "First parameter must be a char const*, unsigned char const* or std::byte const*");
#else
		, "First parameter must be a char const* or an unsigned char const*");
#endif

	static_assert(std::is_standard_layout<T>::value && std::is_trivial<T>::value, "Second parameter must be a POD.");

	IF_CONSTEXPR(is_big_endian())
		std::copy(data, data + sizeof(T), reinterpret_cast<char_type_ptr>(&t));
	else
		std::reverse_copy(data, data + sizeof(T), reinterpret_cast<char_type_ptr>(&t));

	return data + sizeof(T);
}

template<typename Char, typename T>
Char* encode_network_byte_order(T const& t, Char* data)
	{ return encode_big_endian_order(t, data); }

template<typename Char, typename T>
Char* decode_network_byte_order(Char* data, T& t)
	{ return decode_big_endian_order(data, t); }

template<typename Char, typename T>
Char* encode_little_endian_order(T const& t, Char* data)
{
	using const_char_type_ptr = typename std::remove_const<Char>::type const*;

	static_assert(std::is_standard_layout<T>::value && std::is_trivial<T>::value, "First parameter must be a POD.");

	static_assert(std::is_same<Char*, char*>::value
		|| std::is_same<Char*, unsigned char*>::value
#if __cplusplus >= 201703
		|| std::is_same<Char*, std::byte*>::value
		, "Second parameter must be a char const*, unsigned char const* or std::byte const*");
#else
		, "Second parameter must be a char const* or an unsigned char const*");
#endif

//	using char_type = typename std::remove_const<typename std::remove_pointer<CharPtr>::type>::type;

	IF_CONSTEXPR(is_little_endian())
		std::copy(reinterpret_cast<const_char_type_ptr>(&t), reinterpret_cast<const_char_type_ptr>(&t) + sizeof(T), data);
	else
		std::reverse_copy(reinterpret_cast<const_char_type_ptr>(&t), reinterpret_cast<const_char_type_ptr>(&t) + sizeof(T), data);

	return data + sizeof(T);
}

template<typename Char, typename T>
Char* decode_little_endian_order(Char* data, T& t)
{
	using char_type_ptr = typename std::remove_const<Char>::type*;

	static_assert(!std::is_const<T>::value ,"Second parameter may not be const");

	static_assert(std::is_same<char_type_ptr, char*>::value
		|| std::is_same<char_type_ptr, unsigned char*>::value
#if __cplusplus >= 201703
		|| std::is_same<char_type_ptr, std::byte*>::value
		, "First parameter must be a char const*, unsigned char const* or std::byte const*");
#else
		, "First parameter must be a char const* or an unsigned char const*");
#endif

	static_assert(std::is_standard_layout<T>::value && std::is_trivial<T>::value,
		"Second parameter must be a POD.");

	IF_CONSTEXPR(is_little_endian())
		std::copy(data, data + sizeof(T), reinterpret_cast<char_type_ptr>(&t));
	else
		std::reverse_copy(data, data + sizeof(T), reinterpret_cast<char_type_ptr>(&t));

	return data + sizeof(T);
}

template<typename Char, typename T>
Char* encode_endian_order(endian e, T& t, Char* data)
{
	if(e == endian::big)
		return encode_big_endian_order(t, data);
	return encode_little_endian_order(t, data);
}

template<typename Char, typename T>
Char* decode_endian_order(endian e, Char* data, T& t)
{
	if(e == endian::big)
		return decode_big_endian_order(t, data);
	return decode_little_endian_order(t, data);
}

// ============= EXPERIMENTAL ==================

#if __cplusplus >= 201703

// Strings

template<typename CharT, typename Traits, typename Alloc>
std::byte* encode_string_network_byte_order(std::basic_string<CharT, Traits, Alloc> const& s,
	std::byte* pos)
{
	std::size_t n = s.size();
	pos = encode_network_byte_order(n, pos);

	for(auto c: s)
		pos = encode_network_byte_order(c, pos);

	return pos;
}

template<typename CharT, typename Traits, typename Alloc>
std::byte const* decode_string_network_byte_order(std::byte const* pos,
	std::basic_string<CharT, Traits, Alloc>& s)
{
	std::size_t n;
	pos = decode_network_byte_order(pos, n);

	s.clear();

	typename std::basic_string<CharT, Traits, Alloc>::value_type c;
	while(n--)
	{
		pos = decode_network_byte_order(pos, c);
		s += c;
	}

	return pos;
}

template<typename Container>
struct pod_encoder
{
	using value_type = typename Container::value_type;
	std::byte* operator()(value_type const& v, std::byte* buf) const
		{ return encode_network_byte_order(v, buf); }
};

template<typename Container>
struct pod_decoder
{
	using value_type = typename Container::value_type;
	std::byte const* operator()(std::byte const* buf, value_type& v) const
		{ return decode_network_byte_order(buf, v); }
};

template<typename Container>
using value_type_t = typename Container::value_type;

// Iterators

template<typename Iter>
using deref_type_t = std::remove_reference_t<decltype(*Iter())>;

template
<
	typename Iter, typename Encoder = std::conditional_t
	<
		std::is_invocable_r_v
		<
			std::byte*, deref_type_t<Iter>, deref_type_t<Iter> const&, std::byte*
		>,
		deref_type_t<Iter>,
		pod_encoder<std::array<deref_type_t<Iter>, 0>>
	>
>
//template<typename Iter, typename Encoder = deref_type_t<Iter>>
std::byte* encode_network_byte_order(Iter begin, Iter end,
	std::byte* pos, Encoder encode = {})
{
	static_assert(
		std::is_invocable<Encoder,
			deref_type_t<Iter> const&, std::byte*>::value,
				"bad encoder, must be like: std::byte* encode(value_type const&, std::byte*)");

	for(; begin != end; ++begin)
		pos = encode(*begin, pos);

	return pos;
}

template
<
	typename Iter, typename Decoder = std::conditional_t
	<
		std::is_invocable_r_v
		<
			std::byte const*, deref_type_t<Iter>, std::byte const*, deref_type_t<Iter>&
		>,
		deref_type_t<Iter>,
		pod_decoder<std::array<deref_type_t<Iter>, 0>>
	>
>
std::byte const* decode_network_byte_order(std::byte const* pos,
	Iter begin, Iter end, Decoder decode = {})
{
	static_assert(
		std::is_invocable<Decoder,
			std::byte const*, deref_type_t<Iter>&>::value,
				"bad decoder, must be like: std::byte const* decode(std::byte const*, value_type&)");

	for(; begin != end; ++begin)
		pos = decode(pos, *begin);

	return pos;
}

// Containers

template
<
	typename Container, typename Encoder = std::conditional_t
	<
		std::is_invocable_r_v
		<
			std::byte*, value_type_t<Container>, value_type_t<Container> const&, std::byte*
		>,
		value_type_t<Container>,
		pod_encoder<Container>
	>
>
std::byte* encode_container_network_byte_order(Container const& c,
	std::byte* pos, Encoder encode = {})
{
	static_assert(
		std::is_invocable_r_v<std::byte*, Encoder,
		value_type_t<Container> const&, std::byte*>,
				"bad encoder, must be like: std::byte* encode(value_type const&, std::byte*)");

	std::size_t n = c.size();
	pos = encode_network_byte_order(n, pos);
	pos = encode_network_byte_order(std::begin(c), std::end(c), pos, encode);

	return pos;
}

template
<
	typename Container, typename Decoder = std::conditional_t
	<
		std::is_invocable_r_v
		<
			std::byte const*, value_type_t<Container>, std::byte const*, value_type_t<Container>&
		>,
		value_type_t<Container>,
		pod_encoder<Container>
	>
>
std::byte const* decode_container_network_byte_order(std::byte const* pos,
	Container& c, Decoder decode = {})
{
	static_assert(
		std::is_invocable_r_v<std::byte const*, Decoder,
			std::byte const*, value_type_t<Container>&>,
				"bad decoder, must be like: std::byte const* decode(std::byte const*, value_type&)");

	std::size_t n;
	pos = decode_network_byte_order(pos, n);
	c.resize(n);
	pos = decode_network_byte_order(pos, std::begin(c), std::end(c), decode);

	return pos;
}

#endif // __cplusplus >= 201703

} // namespace endian_utils
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_ENDIAN_UTILS_H
