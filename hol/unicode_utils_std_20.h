#ifndef HEADER_ONLY_LIBRARY_UNICODE_UTILS_STD_20_H
#define HEADER_ONLY_LIBRARY_UNICODE_UTILS_STD_20_H
//
// Copyright (c) 2021 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//


#include "unicode_utils.h"

namespace header_only_library {
namespace unicode_utils {
namespace mu = misc_utils;

//#define char8_t char8_t

inline
std::u32string utf8_to_utf32(std::u8string const& utf8)
{
	using codecvt_32_8_type = std::codecvt<char32_t, char8_t, std::mbstate_t>;

	struct codecvt2
	: public codecvt_32_8_type
		{ codecvt2(std::size_t refs = 0): codecvt_32_8_type(refs) {} };

	std::u32string utf32(utf8.size(), char32_t('\0'));

	std::mbstate_t state{};

	char8_t const* from_next;
	char32_t* to_next;

	auto r = codecvt2().in(state,
		std::data(utf8),
		std::data(utf8) + std::size(utf8),
		from_next,
		&utf32[0],
		(&utf32[0]) + utf32.size(),
		to_next);

	(void)r;

	utf32.resize(std::size_t(std::distance(std::data(utf32), to_next)));

	return utf32;
}

/**
 *
 * @param state To be passed in each batch
 * @param from_beg Beginning of input batch
 * @param from_end End of input batch (updated to reflect actual use)
 * @param to_beg Beginning of output batch
 * @param to_end End of output batch (updated to reflect actual use)
 */
inline
void utf32_to_utf8(std::mbstate_t& state,
	char32_t const* from_beg,
	char32_t const*& from_end,
	char8_t* to_beg,
	char8_t*& to_end)
{
	using codecvt_32_8_type = std::codecvt<char32_t, char8_t, std::mbstate_t>;

	struct codecvt2
	: public codecvt_32_8_type
	{
		codecvt2(std::size_t refs = 0): codecvt_32_8_type(refs) {}
	};

	char32_t const* from_end_w;
	char8_t* to_end_w;

	auto r = codecvt2().out(state,
		from_beg,
		from_end,
		from_end_w,
		to_beg,
		to_end,
		to_end_w);

	(void)r;

	from_end = from_end_w;
	to_end = to_end_w;
}

inline
std::u8string utf32_to_utf8(std::mbstate_t& state, std::u32string const& utf32)
{
	using codecvt_32_8_type = std::codecvt<char32_t, char8_t, std::mbstate_t>;

	struct codecvt2
	: public codecvt_32_8_type
		{ codecvt2(std::size_t refs = 0): codecvt_32_8_type(refs) {} };

	std::u8string utf8(utf32.size() * 4, char8_t('\0'));

	const char32_t* from_next;
	char8_t* to_next;

	auto r = codecvt2().out(state,
		std::data(utf32),
		std::data(utf32) + std::size(utf32),
		from_next,
		&utf8[0],
		(&utf8[0]) + std::size(utf8),
		to_next);

	(void)r;

	utf8.resize(std::size_t(std::distance(std::data(utf8), to_next)));

	return utf8;
}

//inline
//std::u8string utf32_to_utf8(std::u32string const& utf32)
//{
//	std::mbstate_t state;
//	return utf32_to_utf8(state, utf32);
//}

inline
bool unicode_convert(std::u8string const& s,   std::u32string& u) { try{u = utf8_to_utf32(s); }catch(...){return false;}return true; }

//inline
//bool unicode_convert(std::u32string const& s,   std::u8string& u) { try{u = utf32_to_utf8(s); }catch(...){return false;}return true; }

// std::codecvt_byname<wchar_t, char, std::mbstate_t>

inline
std::wstring u8_to_ws(std::u8string const& u8)
{
	bug_fun();

	using codecvt_w_8_type = std::codecvt_byname<wchar_t, char, std::mbstate_t>;

	struct codecvt2
	: public codecvt_w_8_type
		{ codecvt2(std::size_t refs = 0): codecvt_w_8_type("", refs) {} };

	std::wstring ws(u8.size(), wchar_t('\0'));

	std::mbstate_t state{};

	char const* from_next;
	wchar_t* to_next;

	auto r = codecvt2().in(state,
		(char const*)std::data(u8),
		(char const*)std::data(u8) + std::size(u8),
		from_next,
		&ws[0],
		(&ws[0]) + ws.size(),
		to_next);

	(void)r;

	ws.resize(std::size_t(std::distance(std::data(ws), to_next)));

	return ws;
}

inline
std::u8string ws_to_u8(std::wstring const& ws)
{
	using codecvt_w_8_type = std::codecvt_byname<wchar_t, char, std::mbstate_t>;

	struct codecvt2
	: public codecvt_w_8_type
		{ codecvt2(std::size_t refs = 0): codecvt_w_8_type("", refs) {} };

	std::u8string u8(ws.size() * sizeof(std::wstring::value_type), char8_t('\0'));

	std::mbstate_t state{};

	const wchar_t* from_next;
	char* to_next;

	auto r = codecvt2().out(state,
		std::data(ws),
		std::data(ws) + std::size(ws),
		from_next,
		(char*)&u8[0],
		(char*)(&u8[0]) + std::size(u8),
		to_next);

	(void)r;

	u8.resize(std::size_t(std::distance((char*)std::data(u8), to_next)));

	return u8;
}

} // unicode_utils
} // header_only_library

#endif // HEADER_ONLY_LIBRARY_UNICODE_UTILS_STD_20_H
