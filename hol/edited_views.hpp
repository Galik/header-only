#ifndef HEADER_ONLY_LIBRARY_EDITED_VIEWS_HPP
#define HEADER_ONLY_LIBRARY_EDITED_VIEWS_HPP

#include <deque>
#include <variant>

#include "assertions.h"
#include "macro_exceptions.h"
//#include "string_utils.h"

namespace header_only_library {
namespace edited_views {

//template<typename CharT>
//struct is_std_char
//{
//	/*! true or false */
//	constexpr static bool value =
//	   std::is_same<CharT, char>::value
//	|| std::is_same<CharT, char const>::value
//	|| std::is_same<CharT, wchar_t>::value
//	|| std::is_same<CharT, wchar_t const>::value
//	|| std::is_same<CharT, char16_t>::value
//	|| std::is_same<CharT, char16_t const>::value
//	|| std::is_same<CharT, char32_t>::value
//	|| std::is_same<CharT, char32_t const>::value;
//};

template<typename T>
class edited_view_iterator;

template<typename T>
class edited_view
{
public:
	using value_type = T;
	using const_value_type = value_type const;
	using pointer = value_type*;
	using const_pointer = value_type const*;
	using reference = value_type&;
	using const_reference = value_type const&;

	using const_iterator = edited_view_iterator<const_value_type>;

	edited_view(edited_view const* data = nullptr): m_view(data) {}

	virtual std::size_t size() const = 0;
	virtual const_reference operator[](std::size_t n) const = 0;

	edited_view const* view() const { return m_view; }

private:
	edited_view const* m_view;
};

template<typename T>
class edited_view_iterator
{
public:
	using value_type = typename std::iterator_traits<T*>::value_type;
	using difference_type = typename std::iterator_traits<T*>::difference_type;
	using pointer = typename std::iterator_traits<T*>::pointer;
	using reference = typename std::iterator_traits<T*>::reference;

	edited_view_iterator(edited_view<T> const* view, std::size_t pos): m_view(view), m_pos(pos)
	{
		HOL_ASSERT(view);
		HOL_ASSERT(pos <= view->size());
	}

	T& operator*()
	{
		HOL_ASSERT(m_pos < m_view->size());
		return (*m_view)[m_pos];
	}

	T const& operator*() const
	{
		HOL_ASSERT(m_pos < m_view->size());
		return (*m_view)[m_pos];
	}

	T* operator->()
	{
		HOL_ASSERT(m_pos < m_view->size());
		return &((*m_view)[m_pos]);
	}

	T const* operator->() const
	{
		HOL_ASSERT(m_pos < m_view->size());
		return &((*m_view)[m_pos]);
	}

	edited_view_iterator& operator++()
	{
		HOL_ASSERT(m_pos < m_view->size());
		++m_pos;
		return *this;
	}

	edited_view_iterator operator++(int)
	{
		HOL_ASSERT(m_pos < m_view->size());
		auto copy = *this;
		++(*this);
		return copy;
	}

	edited_view_iterator& operator--()
	{
		HOL_ASSERT(m_pos);
		HOL_ASSERT(m_pos < m_view->size());
		return --m_pos;
		return *this;
	}

	edited_view_iterator operator--(int)
	{
		HOL_ASSERT(m_pos);
		HOL_ASSERT(m_pos < m_view->size());
		auto copy = *this;
		--(*this);
		return copy;
	}

	edited_view_iterator& operator+=(difference_type n)
	{
		HOL_ASSERT(m_pos + n <= m_view->size());
		m_pos += n;
		return *this;
	}

	edited_view_iterator operator+(difference_type n) const
	{
		HOL_ASSERT(m_pos + n <= m_view->size());
		auto copy = *this;
		copy += n;
		return copy;
	}

	edited_view_iterator& operator-=(difference_type n)
	{
		HOL_ASSERT(m_pos);
		HOL_ASSERT(m_pos + n <= m_view->size());
		m_pos -= n;
		return *this;
	}

	edited_view_iterator operator-(difference_type n) const
	{
		HOL_ASSERT(m_pos);
		HOL_ASSERT(m_pos + n <= m_view->size());
		auto copy = *this;
		copy -= n;
		return copy;
	}

	bool operator==(edited_view_iterator const& other) const { return m_pos == other.m_pos; }
	bool operator!=(edited_view_iterator const& other) const { return m_pos != other.m_pos; }

	bool operator<(edited_view_iterator const& other) const { return m_pos < other.m_pos; }
	bool operator>(edited_view_iterator const& other) const { return m_pos > other.m_pos; }

	bool operator<=(edited_view_iterator const& other) const { return m_pos <= other.m_pos; }
	bool operator>=(edited_view_iterator const& other) const { return m_pos >= other.m_pos; }

	difference_type operator-(edited_view_iterator const& other) const { return other.m_pos - m_pos; }

private:
	edited_view<T> const* m_view = nullptr;
	std::size_t m_pos = 0;
};

template<typename T>
class edited_view_data
: public edited_view<T>
{
public:
	using typename edited_view<T>::value_type;
	using typename edited_view<T>::const_value_type;
	using typename edited_view<T>::pointer;
	using typename edited_view<T>::const_pointer;
	using typename edited_view<T>::reference;
	using typename edited_view<T>::const_reference;

	edited_view_data(const_pointer beg, const_pointer end)
	: edited_view<T>(), m_beg(beg), m_end(end) { HOL_ASSERT(beg <= end); }

	std::size_t size() const override
		{ return std::size_t(m_end - m_beg); }

	const_reference operator[](std::size_t n) const override
		{ HOL_ASSERT(n < size()); return *(m_beg + n); }

private:
	const_pointer m_beg;
	const_pointer m_end;
};

template<typename T>
class edited_view_cpy
: public edited_view<T>
{
protected:
	using edited_view<T>::view;

public:
	using typename edited_view<T>::value_type;
	using typename edited_view<T>::const_value_type;
	using typename edited_view<T>::pointer;
	using typename edited_view<T>::const_pointer;
	using typename edited_view<T>::reference;
	using typename edited_view<T>::const_reference;

	edited_view_cpy(edited_view<T> const* view, std::size_t from, std::size_t to)
	: edited_view<T>(view), m_from(from), m_to(to) {}

	std::size_t size() const override
		{ return view()->size(); }

	const_reference operator[](std::size_t n) const override
		{ HOL_ASSERT(n < size()); return (*view())[n]; }

	auto copied_from() const { return m_from; }

	const_reference copied(std::size_t n) const {  HOL_ASSERT(n < copied_size()); return (*view())[m_from + n]; }
	std::size_t copied_size() const { return std::size_t(m_to - m_from); }

private:
	std::size_t m_from;
	std::size_t m_to;
};

template<typename T>
class edited_view_cut
: public edited_view_cpy<T>
{
protected:
	using edited_view<T>::view;

public:
	using typename edited_view<T>::value_type;
	using typename edited_view<T>::const_value_type;
	using typename edited_view<T>::pointer;
	using typename edited_view<T>::const_pointer;
	using typename edited_view<T>::reference;
	using typename edited_view<T>::const_reference;

	edited_view_cut(edited_view<T> const* view, std::size_t from, std::size_t to)
	: edited_view_cpy<T>(view, from, to)
	{
		HOL_ASSERT(from <= to);
		HOL_ASSERT(from < view->size());
		HOL_ASSERT(to < view->size());
	}

	std::size_t size() const override
		{ return view()->size() - this->copied_size(); }

	T const& operator[](std::size_t n) const override
	{
		HOL_ASSERT(n < size());
		if(n < this->copied_from())
			return (*view())[n];
		return (*view())[n + this->copied_size()];
	}
};

template<typename T>
class edited_view_paste
: public edited_view<T>
{
	using edited_view<T>::view;

public:
	using typename edited_view<T>::value_type;
	using typename edited_view<T>::const_value_type;
	using typename edited_view<T>::pointer;
	using typename edited_view<T>::const_pointer;
	using typename edited_view<T>::reference;
	using typename edited_view<T>::const_reference;

	edited_view_paste(edited_view<T> const* view, edited_view_cpy<T> const* cpy, std::size_t at)
	: edited_view<T>(view), m_cpy(cpy), at(at) { HOL_ASSERT(at < view->size()); }

	std::size_t size() const override
		{ return view()->size() + m_cpy->copied_size(); }

	T const& operator[](std::size_t n) const override
	{
		 HOL_ASSERT(n < size());

		 if(n < at)
			return (*view())[n];

		if(n < at + m_cpy->copied_size())
			return m_cpy->copied(n - at);

		return (*view())[n - m_cpy->copied_size()];
	}

private:
	edited_view_cpy<T> const* m_cpy;
	std::size_t at;
};

template<typename T>
class edited_view_insert
: public edited_view<T>
{
	using edited_view<T>::view;

public:
	using typename edited_view<T>::value_type;
	using typename edited_view<T>::const_value_type;
	using typename edited_view<T>::pointer;
	using typename edited_view<T>::const_pointer;
	using typename edited_view<T>::reference;
	using typename edited_view<T>::const_reference;

	edited_view_insert(edited_view<T> const* view, const_pointer beg, const_pointer end, std::size_t at)
	: edited_view<T>(view), m_beg(beg), m_end(end), m_at(at)
	{
		HOL_ASSERT(beg <= end);
		HOL_ASSERT(at < view->size());
	}

	std::size_t size() const override
		{ return view()->size() + inserted_size(); }

	T const& operator[](std::size_t n) const override
	{
		HOL_ASSERT(n < size());

		if(n < m_at)
			return (*view())[n];

		if(n < m_at + inserted_size())
			return m_beg[n - m_at];

		return (*view())[n - inserted_size()];
	}

private:
	std::size_t inserted_size() const { return std::size_t(std::distance(m_beg, m_end)); }

	const_pointer m_beg;
	const_pointer m_end;
	std::size_t m_at;
};

//template<typename T>
//void dump(edited_view<T>& vv)
//{
//	string_utils::output_separator sep;
//	for(auto i = 0ULL; i < vv.size(); ++i)
//	{
//		std::cout << sep << vv[i];
//	}
//
//	std::cout << '\n';
//}

template<typename T>
class view_editor
{
	using edited_view_type = std::variant
	<
		edited_view_cpy<T>,
		edited_view_cut<T>,
		edited_view_data<T>,
		edited_view_insert<T>,
		edited_view_paste<T>
	>;

public:
	using value_type = T;
	using non_const_value_type = typename std::remove_const<value_type>::type;
	using const_value_type = non_const_value_type const;
	using iterator = edited_view_iterator<value_type>;
	using const_iterator = edited_view_iterator<const_value_type>;

	using copy_type = edited_view_cpy<T> const*;
	using cut_type = edited_view_cut<T> const*;

	view_editor(T const* beg, T const* end)
	: m_edits(1, edited_view_data<T>(beg, end))
	, m_top(&std::get<edited_view_data<T>>(m_edits.back())) {}

	template<typename TT>
	void realize(std::size_t from, std::size_t to, TT out) const
	{
		HOL_ASSERT(from <= to);

		while(from != to)
			*out++ = (*m_top)[from++];
	}

	template<typename TT>
	void realize(TT out) const
		{ realize(0, size(), out); }

	std::size_t size() const { return m_top->size(); }

	value_type& operator[](std::size_t n) { return (*m_top)[n]; }
	const_value_type& operator[](std::size_t n) const { return (*m_top)[n]; }

	iterator begin() { return iterator(m_top, 0); }
	iterator end() { return iterator(m_top, m_top->size()); }

	const_iterator begin() const { return const_iterator(m_top, 0); }
	const_iterator end() const { return const_iterator(m_top, m_top->size()); }

	const_iterator cbegin() const { return const_iterator(m_top, 0); }
	const_iterator cend() const { return const_iterator(m_top, m_top->size()); }

	edited_view_cpy<T> const* copy(std::size_t from, std::size_t to)
	{
		m_edits.push_back(edited_view_cpy<T>(m_top, from, to));
		m_top = &std::get<edited_view_cpy<T>>(m_edits.back());
		return &std::get<edited_view_cpy<T>>(m_edits.back());
	}

	edited_view_cut<T> const* cut(std::size_t from, std::size_t to)
	{
		m_edits.push_back(edited_view_cut<T>(m_top, from, to));
		m_top = &std::get<edited_view_cut<T>>(m_edits.back());
		return &std::get<edited_view_cut<T>>(m_edits.back());
	}

	edited_view_paste<T> const* paste(edited_view_cpy<T> const* cpy, std::size_t at)
	{
		m_edits.push_back(edited_view_paste<T>(m_top, cpy, at));
		m_top = &std::get<edited_view_paste<T>>(m_edits.back());
		return &std::get<edited_view_paste<T>>(m_edits.back());
	}

	edited_view_insert<T> const* insert(T const* beg, T const* end, std::size_t at)
	{
		m_edits.push_back(edited_view_insert<T>(m_top, beg, end, at));
		m_top = &std::get<edited_view_insert<T>>(m_edits.back());
		return &std::get<edited_view_insert<T>>(m_edits.back());
	}

	void reset(T const* beg, T const* end)
	{
		clear();
		m_edits.back() = edited_view_data<T>(beg, end);
	}

	void clear()
	{
		if(m_edits.size() > 1)
			m_edits.erase(std::next(std::begin(m_edits)), std::end(m_edits));
		m_edits.shrink_to_fit();

		HOL_ASSERT(m_edits.size() == 1);
		m_top = &std::get<edited_view_data<T>>(m_edits.back());
	}

	void undo()
	{
		if(m_edits.size() < 2)
			hol_throw_runtime_error("nothing left to undo");

		m_edits.pop_back();
		m_top = std::visit([](edited_view<T> const& v){ return &v; }, m_edits.back());
	}

private:
	std::deque<edited_view_type> m_edits;
	edited_view<T> const* m_top;
};

}  // namespace edited_views
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_EDITED_VIEWS_HPP
