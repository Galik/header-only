#ifndef HEADER_ONLY_LIBRARY_MATH_H
#define HEADER_ONLY_LIBRARY_MATH_H
/*
 * math.h
 *
 *  Created on: Oct 21, 2018
 *      Author: galik
 */

#include <cmath>

namespace header_only_library {
namespace math {
namespace constants {
namespace low {

auto constexpr const e          = 2.7182818284590452354;
auto constexpr const log2e      = 1.4426950408889634074;
auto constexpr const log10e     = 0.43429448190325182765;
auto constexpr const ln2        = 0.69314718055994530942;
auto constexpr const ln10       = 2.30258509299404568402;
auto constexpr const pi         = 3.14159265358979323846;
auto constexpr const pi_2       = 1.57079632679489661923;
auto constexpr const pi_4       = 0.78539816339744830962;
auto constexpr const r_1_pi     = 0.31830988618379067154;
auto constexpr const r_2_pi     = 0.63661977236758134308;
auto constexpr const r_2_sqrtpi = 1.12837916709551257390;
auto constexpr const sqrt2      = 1.41421356237309504880;
auto constexpr const sqrt1_2    = 0.70710678118654752440;

//auto constexpr const c  = 299792458; // m/s


} // namespace low

namespace high {

auto constexpr const e          = 2.718281828459045235360287471352662498L;
auto constexpr const log2e      = 1.442695040888963407359924681001892137L;
auto constexpr const log10e     = 0.434294481903251827651128918916605082L;
auto constexpr const ln2        = 0.693147180559945309417232121458176568L;
auto constexpr const ln10       = 2.302585092994045684017991454684364208L;
auto constexpr const pi         = 3.141592653589793238462643383279502884L;
auto constexpr const pi_2       = 1.570796326794896619231321691639751442L;
auto constexpr const pi_4       = 0.785398163397448309615660845819875721L;
auto constexpr const r_1_pi     = 0.318309886183790671537767526745028724L;
auto constexpr const r_2_pi     = 0.636619772367581343075535053490057448L;
auto constexpr const r_2_sqrtpi = 1.128379167095512573896158903121545172L;
auto constexpr const sqrt2      = 1.414213562373095048801688724209698079L;
auto constexpr const sqrt1_2    = 0.707106781186547524400844362104849039L;

}  // namespace high
} // namespace constants
} // namespace math
} // namespace header_only_library




#endif // HEADER_ONLY_LIBRARY_MATH_H
