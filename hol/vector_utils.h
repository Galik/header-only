#ifndef HEADER_ONLY_LIBRARY_VECTOR_UTILS_H
#define HEADER_ONLY_LIBRARY_VECTOR_UTILS_H
/*
 * ibufferstream.h
 *
 *  Created on: 25 Aug 2018
 *      Author: galik
 */

#include <algorithm>
#include <vector>

namespace header_only_library {
namespace vector_utils {

template<typename T, typename Alloc>
auto insert_sorted(std::vector<T, Alloc>& v, T&& t)
	{ return v.insert(std::upper_bound(std::begin(v), std::end(v), t), std::forward<T>(t)); }

template<typename T, typename Alloc>
auto insert_sorted(std::vector<T, Alloc>& v, T const& t)
	{ auto copy = t; return insert_sorted(v, std::move(copy)); }

template<typename T, typename Alloc>
auto remove_sorted(std::vector<T, Alloc>& v, T const& t)
	-> std::pair<typename std::vector<T, Alloc>::iterator, bool>
{
	auto er = std::equal_range(std::begin(v), std::end(v), t);
	return {v.erase(er.first, er.second), er.first != er.second};
}

template<typename T, typename Alloc>
auto insert_unique(std::vector<T, Alloc>& v, T&& t)
	-> std::pair<typename std::vector<T, Alloc>::iterator, bool>
{
	auto lb = std::lower_bound(std::begin(v), std::end(v), t);

	if(lb != std::end(v) && *lb == t)
		return {lb, false};

	return {v.insert(lb, std::forward<T>(t)), true};
}

template<typename T, typename Alloc>
auto insert_unique(std::vector<T, Alloc>& v, T const& t)
	-> std::pair<typename std::vector<T, Alloc>::iterator, bool>
		{ auto copy = t; return insert_unique(v, std::move(copy)); }

template<typename Vector, typename... Args>
auto emplace_unique(Vector& v, Args&&... args)
	-> std::pair<typename Vector::iterator, bool>
		{ return insert_unique(v, typename Vector::value_type(std::forward<Args>(args)...)); }

template<typename T, typename Alloc>
auto remove_unique(std::vector<T, Alloc>& v, T const& t)
	-> std::pair<typename std::vector<T, Alloc>::iterator, bool>
{
	auto lb = std::lower_bound(std::begin(v), std::end(v), t);

	if(lb != std::end(v) && *lb == t)
		return {v.erase(lb), true};

	return {std::end(v), false};
}

template<typename T, typename Alloc, typename Pred>
void erase_if(std::vector<T, Alloc>& v, Pred pred)
{
	v.erase(std::remove_if(std::begin(v), std::end(v), pred), std::end(v));
}

template<typename Iter, typename Pred,
	typename Alloc = std::allocator<typename std::iterator_traits<Iter>::value_type>>
void erase_if(std::vector<typename std::iterator_traits<Iter>::value_type, Alloc>& v,
	Iter begin, Iter end, Pred pred)
{
	v.erase(std::remove_if(begin, end, pred), end);
}

} // namespace vector_utils
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_VECTOR_UTILS_H
