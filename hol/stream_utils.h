#ifndef HEADER_ONLY_LIBRARY_STREAM_UTILS_H
#define HEADER_ONLY_LIBRARY_STREAM_UTILS_H
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <algorithm>
#include <string>

#include "misc_utils.h"

namespace header_only_library {
namespace stream_utils {
namespace mu = misc_utils;

// FIXME: creating a buffer every single line read is slow. Pass buffer in?
//template<typename CharT, typename Traits, typename Alloc> inline
//std::basic_istream<CharT, Traits>& getline_n(std::basic_istream<CharT, Traits>& is,
//	std::basic_string<CharT, Traits, Alloc>& line, std::size_t n, CharT delim = CharT('\n'))
//{
//	std::basic_string<CharT, Traits, Alloc> buf(n, CharT('\0'));
//	is.getline(&buf[0], mu::narrow_cast<std::streamsize>(n), delim);
//	line.assign(&buf[0], mu::narrow_cast<typename std::basic_string<CharT, Traits, Alloc>::size_type>(is.gcount() - !is.eof()));
//
//	return is;
//}

/**
 * Safe version of std::getline(). This version takes a maximum length
 * parameter to fend off possible DOS attacks from super long line
 * length data. If the maximum length in exceeded the stream is
 * set to `fail`.
 *
 * NOTE: This function does NOT discard the line terminator character
 * as this may be needed by the caller to determine precisely what was
 * originally in the file.
 *
 * @param is The input stream to read from.
 * @param line The line string to read into
 * @param n The maximum number of characters to read.
 * @param delim The character to read up to.
 * @return The passed in stream.
 */
template<typename CharT, typename Traits, typename Alloc> inline
std::basic_istream<CharT, Traits>& getline_n(std::basic_istream<CharT, Traits>& is,
	std::basic_string<CharT, Traits, Alloc>& line,
	std::size_t n, CharT const delim = CharT('\n'))
{
	line.clear();
	while(n-- && is && is.peek() != delim)
		line += CharT(is.get());

	if(is && is.peek() == delim)
		line += CharT(is.get());

	if(!n)
		is.setstate(std::ios::failbit);

	return is;
}

} // stream_utils
} // hol

#endif // HEADER_ONLY_LIBRARY_STREAM_UTILS_H
