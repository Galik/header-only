#ifndef HEADER_ONLY_LIBRARY_TIME_UTILE_H
#define HEADER_ONLY_LIBRARY_TIME_UTILE_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <ctime>
#include <mutex>

namespace header_only_library {
namespace time_utils {
namespace detail {

inline std::mutex& ctime_mutex()
{
	static std::mutex mtx;
	return mtx;
}

}  // namespace detail

inline
std::time_t time()
	{ return std::time(nullptr); }

/**
 * Thread-safe cross platform localtime function
 * @param timer
 * @return
 */
inline
std::tm localtime(std::time_t timer = time())
{
	std::tm bt {};
#if defined(__unix__)
	localtime_r(&timer, &bt);
#elif defined(_MSC_VER)
	localtime_s(&bt, &timer);
#else
	std::lock_guard<std::mutex> lock(detail::ctime_mutex());
	bt = *std::localtime(&timer);
#endif
	return bt;
}

/**
 * Thread-safe cross platform gmtime function
 * @param timer
 * @return
 */
inline
std::tm gmtime(std::time_t timer = time())
{
#if defined(__unix__)
	std::tm bt {};
	return *gmtime_r(&timer, &bt);
#elif defined(_MSC_VER)
	std::tm bt {};
	gmtime_s(&bt, &timer);
	return bt;
#else
	std::lock_guard<std::mutex> lock(detail::ctime_mutex());
	return *std::gmtime(&timer);
#endif
}

inline
std::time_t mktime(std::tm& bt)
	{ return std::mktime(&bt); }

inline
std::time_t mktime(std::tm const& bt)
	{ auto lbt = bt; return std::mktime(&lbt); }

/**
 * Thread-safe cross platform asctime function
 * @param timer
 * @return
 */
inline
std::string asctime(std::tm const& tmb)
{
	std::lock_guard<std::mutex> lock(detail::ctime_mutex());
	return std::asctime(&tmb);
}

/**
 * Thread-safe cross platform ctime function
 * @param timer
 * @return
 */
std::string ctime(std::time_t timer = std::time(0))
{
#if defined(__unix__)
	char buf[26];
	ctime_r(&timer, buf);
	return {buf};
#else
	std::lock_guard<std::mutex> lock(detail::ctime_mutex());
	return ctime(&timer);
#endif
}

[[deprecated("use time()")]] inline
std::time_t safe_time() { return time(); }
[[deprecated("use localtime()")]] inline
std::tm safe_localtime(std::time_t timer = time()) { return localtime(timer); }
[[deprecated("use gmtime()")]] inline
std::tm safe_gmtime(std::time_t timer = time()) { return gmtime(timer); }
[[deprecated("use ctime()")]] inline
std::time_t safe_mktime(std::tm& tmb) { return mktime(tmb); }
[[deprecated("use mktime()")]] inline
std::time_t safe_mktime(std::tm const& tmb) { return mktime(tmb); }
[[deprecated("use asctime()")]] inline
std::string safe_asctime(std::tm const& tmb) { return asctime(tmb); }
[[deprecated("use ctime()")]] inline
std::string safe_ctime(std::time_t timer = time()) { return ctime(timer); }

// default = "YYYY-MM-DD HH:MM:SS"
inline
std::string time_stamp_at(std::time_t when, char const* fmt = "%F %T")
{
	char buf[64];
	auto bt = localtime(when);
	return {buf, std::strftime(buf, sizeof(buf), fmt, &bt)};
}

inline
std::string time_stamp_at(std::tm const& bt, char const* fmt = "%F %T")
	{ return time_stamp_at(mktime(bt), fmt); }

inline
std::string time_stamp_at(std::time_t when, std::string const& fmt)
	{ return time_stamp_at(when, fmt.c_str()); }

inline
std::string time_stamp_now(char const* fmt = "%F %T")
	{ return time_stamp_at(time(), fmt); }

inline
std::string time_stamp_now(std::string const& fmt)
	{ return time_stamp_now(fmt.c_str()); }

} // time_utils
} // header_only_library

#endif // HEADER_ONLY_LIBRARY_TIME_UTILE_H
