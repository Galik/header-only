#ifndef HEADER_ONLY_LIBRARY_OPENSSL_UTILS_H
#define HEADER_ONLY_LIBRARY_OPENSSL_UTILS_H
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <string>
#include <vector>

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/pem.h>

namespace header_only_library {
namespace openssl_utils {

using char_ptr = char*;
using char_cptr = char const*;

using uchar_ptr = unsigned char*;
using uchar_cptr = unsigned char const*;

using byte = std::byte;
using byte_ptr = byte*;
using byte_cptr = byte const*;

using byte_buff = std::vector<byte>;
using byte_buff_ref = byte_buff&;
using byte_buff_cref = byte_buff const&;

struct openssl_deleter
{
	void operator()(EVP_CIPHER_CTX* p) const { if(p) EVP_CIPHER_CTX_free(p); }
};

using EVP_CIPHER_CTX_ptr = std::unique_ptr<EVP_CIPHER_CTX, openssl_deleter>;

inline auto make_EVP_CIPHER_CTX() { return EVP_CIPHER_CTX_ptr(EVP_CIPHER_CTX_new()); }

class evp_paired_key
{
public:
	~evp_paired_key() { decrement(); }

	evp_paired_key(): m_ptr(EVP_PKEY_new())
	{
		// generate a new one
	}

//	evp_private_key(std::string const& filename)
//	{
//		privkey = EVP_PKEY_new();
//
//		std::ostringstream oss;
//		oss << std::ifstream(filename, std::ios::binary).rdbuf();
//		fp = fopen("test-key.pem", "r");
//
//		PEM_read_bio_PrivateKey(bp, x, cb, u)read_PrivateKey(fp, &privkey, NULL, NULL);
//
//		fclose (fp);
//
//		rsakey = EVP_PKEY_get1_RSA(privkey);
//
//		if(RSA_check_key (rsakey))
//		{
//			printf("RSA key is valid.\n");
//		}
//		else
//		{
//			printf("Error validating RSA key.\n");
//		}
//
//	}

	evp_paired_key(evp_paired_key const& other): m_ptr(other.m_ptr)
		{ increment(); }

	evp_paired_key& operator=(evp_paired_key const& other)
	{
		decrement();
		m_ptr = other.m_ptr;
		increment();
		return *this;
	}

	EVP_PKEY* get() { return m_ptr; }
	EVP_PKEY const* get() const { return m_ptr; }

private:
	void decrement() { if(m_ptr) EVP_PKEY_free(m_ptr); }
	void increment() { if(m_ptr) EVP_PKEY_up_ref(m_ptr); }

	EVP_PKEY* m_ptr = nullptr;
};

//The EVP_PKEY_new() function allocates an empty EVP_PKEY structure which is used by OpenSSL to store private keys. The reference count is set to 1.
//
//EVP_PKEY_up_ref() increments the reference count of key.
//
//EVP_PKEY_free() decrements the reference count of key and, if the reference count is zero, frees it up. If key is NULL, nothing is done.




// EVP Asymmetric Encryption and Decryption of an Envelope
//
// Encryption and decryption with asymmetric keys is computationally expensive.
// Typically then messages are not encrypted directly with such keys but are
// instead encrypted using a symmetric "session" key. This key is itself then
// encrypted using the public key. In OpenSSL this combination is referred to
// as an envelope. It is also possible to encrypt the session key with multiple
// public keys. This way the message can be sent to a number of different
// recipients (one for each public key used). The session key is the same for
// each recipient.
//

class EVPAsymetricEnvelope
{
	inline std::string error_message() const
	{
		char buf[1024];
		ERR_error_string_n(ERR_get_error(), buf, sizeof(buf));
		return {buf};
	}

	void handle_errors() const { throw std::runtime_error(error_message()); }

public:
	// Sealing an Envelope
	// An envelope is sealed using the EVP_Seal* set of functions, and an operation consists of the
	//     following steps:
	//
	// Initialise the context.
	// Initialise the seal operation, providing the symmetric cipher that will be used,
	//     along with the set of public keys to encrypt the session key with.
	// Provide the message to be encrypted.
	// Complete the encryption operation.

//	int envelope_seal(EVP_PKEY** pub_key, unsigned char* plaintext,
//		int plaintext_len, unsigned char** encrypted_key, int* encrypted_key_len,
//		unsigned char* iv, unsigned char* ciphertext)
	int envelope_seal(std::vector<evp_paired_key> pub_keys, unsigned char* plaintext,
		int plaintext_len, unsigned char** encrypted_key, int* encrypted_key_len,
		unsigned char* iv, unsigned char* ciphertext)
	{
//		EVP_CIPHER_CTX* ctx;

		int ciphertext_len;

		int len;

		auto ctx = make_EVP_CIPHER_CTX();

		if(!ctx)
			throw std::runtime_error(error_message());

		/* Initialise the envelope seal operation. This operation generates
		 * a key for the provided cipher, and then encrypts that key a number
		 * of times (one for each public key provided in the pub_key array). In
		 * this example the array size is just one. This operation also
		 * generates an IV and places it in iv. */
		auto pubs = std::vector<EVP_PKEY*>();
		for(auto&& pk: pub_keys)
			pubs.push_back(pk.get());
		if(EVP_SealInit(ctx.get(), EVP_aes_256_cbc(), encrypted_key,
				encrypted_key_len, iv, pubs.data(), 1) != hol::narrow_cast<int>(pub_keys.size()))
			handle_errors();

		/* Provide the message to be encrypted, and obtain the encrypted output.
		 * EVP_SealUpdate can be called multiple times if necessary
		 */
		if(1 != EVP_SealUpdate(ctx.get(), ciphertext, &len, plaintext, plaintext_len))
			handle_errors();
		ciphertext_len = len;

		/* Finalise the encryption. Further ciphertext bytes may be written at
		 * this stage.
		 */
		if(1 != EVP_SealFinal(ctx.get(), ciphertext + len, &len))
			handle_errors();
		ciphertext_len += len;

		return ciphertext_len;
	}

	// Opening and Envelope
	// An envelope is opened using the EVP_Open* set of functions in the following steps:
	//
	// Initialise the context.
	// Initialise the open operation, providing the symmetric cipher that has been used,
	//     along with the private key to decrypt the session key with.
	// Provide the message to be decrypted and decrypt using the session key.
	// Complete the decryption operation.
	//

	int envelope_open(EVP_PKEY* priv_key, unsigned char* ciphertext,
		int ciphertext_len, unsigned char* encrypted_key, int encrypted_key_len,
		unsigned char* iv, unsigned char* plaintext)
	{
		EVP_CIPHER_CTX* ctx;

		int len;

		int plaintext_len;

		/* Create and initialise the context */
		if(!(ctx = EVP_CIPHER_CTX_new()))
			handle_errors();

		/* Initialise the decryption operation. The asymmetric private key is
		 * provided and priv_key, whilst the encrypted session key is held in
		 * encrypted_key */
		if(1
			!= EVP_OpenInit(ctx, EVP_aes_256_cbc(), encrypted_key,
				encrypted_key_len, iv, priv_key))
			handle_errors();

		/* Provide the message to be decrypted, and obtain the plaintext output.
		 * EVP_OpenUpdate can be called multiple times if necessary
		 */
		if(1 != EVP_OpenUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
			handle_errors();
		plaintext_len = len;

		/* Finalise the decryption. Further plaintext bytes may be written at
		 * this stage.
		 */
		if(1 != EVP_OpenFinal(ctx, plaintext + len, &len))
			handle_errors();
		plaintext_len += len;

		/* Clean up */
		EVP_CIPHER_CTX_free(ctx);

		return plaintext_len;
	}
};

// EVP Symmetric Encryption and Decryption
//
//

class EVPSymetricEncryption
{
	struct cypher_info
	{
		char const* name = nullptr;
		EVP_CIPHER const* type = nullptr;
		std::size_t const iv_size = 0;
		std::size_t const key_size = 0;
	};

	static auto const& cyphers()
	{
		static std::array<cypher_info, 1> const array =
		{
			{"aes_256_cbc", EVP_aes_256_cbc(), 256, 256}
		};

		return array;
	}

public:
	static auto number_of_ciphers() { return cyphers().size(); }

	static auto list_ciphers()
	{
		std::vector<std::string> list;

		for(auto const& c: cyphers())
			list.push_back(c.name);

		return list;
	}

	static cypher_info const* cypher(char const* name)
	{
		auto found = std::find_if(std::begin(cyphers()), std::end(cyphers()),
		[name](auto const& c){
			return !std::strcmp(name, c.name);
		});

		if(found == std::end(cyphers()))
			return nullptr;

		return found;
	}

private:

	inline std::string error_message() const
	{
		char buf[1024];
		ERR_error_string_n(ERR_get_error(), buf, sizeof(buf));
		return {buf};
	}

	void handle_errors() const { throw std::runtime_error(error_message()); }

public:
	// Nice API
//	byte_buff encrypt(byte_buff_cref text, byte_buff_cref key, byte_buff_cref iv,
//		cypher_info const* cipher)
//	{
//		auto block_size = std::size_t(EVP_CIPHER_block_size(cipher->type));
//		byte_buff out(std::size(text) + block_size - 1);
//		auto n = encrypt(std::data(text), std::size(text),
//			std::data(key), std::data(iv), cipher, out.data());
//		out.resize(std::size_t(n));
//		return out;
//	}

	// String API
	template<typename C, typename T, typename A>
	byte_buff encrypt(std::basic_string<C, T, A> const& text,
		byte_buff_cref key, byte_buff_cref iv, cypher_info const* cipher)
	{
		auto block_size = std::size_t(EVP_CIPHER_block_size(cipher->type));
		byte_buff out(std::size(text) + block_size);
		auto n = encrypt(byte_cptr(std::data(text)), std::size(text),
			std::data(key), std::data(iv), cipher->type, out.data());
		out.resize(std::size_t(n));
		return out;
	}

	// String API
	template<typename C, typename T = std::char_traits<C>, typename A = std::allocator<C>>
	std::basic_string<C, T, A> decrypt(byte_buff enc,
		byte_buff_cref key, byte_buff_cref iv, cypher_info const* cipher)
	{
		auto block_size = std::size_t(EVP_CIPHER_block_size(cipher->type));
		std::basic_string<C, T, A> out(std::size(enc) + block_size, C('\0'));
		auto n = decrypt(std::data(enc), std::size(enc),
			std::data(key), std::data(iv), cipher->type, byte_ptr(&out[0]));
		out.resize(std::size_t(n));
		return out;
	}

	// This will take as parameters the plaintext, the length of the plaintext,
	// the key to be used, and the IV. We'll also take in a buffer to put the
	// ciphertext in (which we assume to be long enough), and will return the
	// length of the ciphertext that we have written.
	//
	// Encrypting consists of the following stages:
	//
	// Setting up a context.
	// Initialising the encryption operation.
	// Providing plaintext bytes to be encrypted.
	// Finalising the encryption operation.
	//
	// During initialisation we will provide an EVP_CIPHER object. In this case we
	// are using EVP_aes_256_cbc(), which uses the AES algorithm with a 256-bit key
	// in CBC mode.
	//
	// NOTE: " the amount of data written may be anything from zero bytes to (inl + cipher_block_size - 1)"
	int encrypt(byte_cptr plaintext, std::size_t plaintext_len, byte_cptr key,
		byte_cptr iv, EVP_CIPHER const* cypher, byte_ptr ciphertext)
	{
		auto ctx = make_EVP_CIPHER_CTX();

		if(!ctx)
			handle_errors();

		/* Create and initialise the context */

		/* Initialise the encryption operation. IMPORTANT - ensure you use a key
		 * and IV size appropriate for your cipher
		 * In this example we are using 256 bit AES (i.e. a 256 bit key). The
		 * IV size for *most* modes is the same as the block size. For AES this
		 * is 128 bits */
		if(EVP_EncryptInit_ex(ctx.get(), cypher, NULL, uchar_cptr(key), uchar_cptr(iv)) != 1)
			handle_errors();

		int len;

		/* Provide the message to be encrypted, and obtain the encrypted output.
		 * EVP_EncryptUpdate can be called multiple times if necessary
		 */
		if(EVP_EncryptUpdate(ctx.get(), uchar_ptr(ciphertext), &len, uchar_cptr(plaintext),
			hol::narrow_cast<int>(plaintext_len)) != 1)
				handle_errors();

		int ciphertext_len = len;

		/* Finalise the encryption. Further ciphertext bytes may be written at
		 * this stage.
		 */
		if(EVP_EncryptFinal_ex(ctx.get(), uchar_ptr(ciphertext) + len, &len) != 1)
			handle_errors();

		ciphertext_len += len;

		return ciphertext_len;
	}

	// Decrypting consists of the following stages:
	//
	// Setting up a context.
	// Initialising the decryption operation.
	// Providing ciphertext bytes to be decrypted.
	// Finalising the decryption operation.
	//
	// Again through the parameters we will receive the ciphertext to be decrypted,
	// the length of the ciphertext, the key and the IV. We'll also receive a buffer
	// to place the decrypted text into, and return the length of the plaintext we
	// have found.
	//
	// Note that we have passed the length of the ciphertext. This is required as you
	// cannot use functions such as "strlen" on this data - its binary! Similarly, even
	// though in this example our plaintext really is ASCII text, OpenSSL does not know
	// that. In spite of the name plaintext could be binary data, and therefore no NULL
	// terminator will be put on the end (unless you encrypt the NULL as well of course).

	int decrypt(byte_cptr ciphertext, std::size_t ciphertext_len,
		byte_cptr key, byte_cptr iv, EVP_CIPHER const* cypher, byte_ptr plaintext)
	{
		auto ctx = make_EVP_CIPHER_CTX();

		if(!ctx)
			handle_errors();

		/* Initialise the decryption operation. IMPORTANT - ensure you use a key
		 * and IV size appropriate for your cipher
		 * In this example we are using 256 bit AES (i.e. a 256 bit key). The
		 * IV size for *most* modes is the same as the block size. For AES this
		 * is 128 bits */
		if(EVP_DecryptInit_ex(ctx.get(), cypher, NULL,
			uchar_cptr(key), uchar_cptr(iv)) != 1)
				handle_errors();

		int len;

		/* Provide the message to be decrypted, and obtain the plaintext output.
		 * EVP_DecryptUpdate can be called multiple times if necessary
		 */
		if(EVP_DecryptUpdate(ctx.get(), uchar_ptr(plaintext), &len,
			uchar_cptr(ciphertext), int(ciphertext_len)) != 1)
				handle_errors();

		int plaintext_len = len;

		/* Finalise the decryption. Further plaintext bytes may be written at
		 * this stage.
		 */
		if(EVP_DecryptFinal_ex(ctx.get(), uchar_ptr(plaintext) + len, &len) != 1)
			handle_errors();

		plaintext_len += len;

		return plaintext_len;
	}

	static int bio_stream_cb(void const* data, std::size_t len, void* u)
	{
		std::ostream& os = *reinterpret_cast<std::ostream*>(u);
		if(os.write(data, len))
			return int(len);
		return 0;
	}

	static std::ostream& bio_dump(std::ostream& os, byte_cptr data, std::size_t len)
	{
		BIO_dump_cb(&bio_stream_cb, reinterpret_cast<void*>(&os), char_cptr(data), int(len));
		return os;
	}

	int test()
	{
	  /* Set up the key and iv. Do I need to say to not hard code these in a
	   * real application? :-)
	   */

	  /* A 256 bit key */
	  auto key = (std::byte const*)"01234567890123456789012345678901";

	  /* A 128 bit IV */
	  auto iv = (std::byte const*)"0123456789012345";

	  /* Message to be encrypted */
	  auto plaintext = (std::byte const*)"The quick brown fox jumps over the lazy dog";

	  /* Buffer for ciphertext. Ensure the buffer is long enough for the
	   * ciphertext which may be longer than the plaintext, dependant on the
	   * algorithm and mode
	   */
	  std::byte ciphertext[128];

	  /* Buffer for the decrypted text */
	  std::byte decryptedtext[128];

	  int decryptedtext_len, ciphertext_len;

	  /* Encrypt the plaintext */
	  ciphertext_len = encrypt(plaintext, strlen((char const*)plaintext), key, iv,
		  EVP_aes_256_cbc(), ciphertext);

	  /* Do something useful with the ciphertext here */
	  std::cout << "Ciphertext is: " << '\n';
//	  BIO_dump_fp(stdout, (const char *)ciphertext, ciphertext_len);
	  BIO_dump_cb(&bio_stream_cb, reinterpret_cast<void*>(&std::cout), char_cptr(ciphertext), ciphertext_len);

	  /* Decrypt the ciphertext */
	  decryptedtext_len = decrypt(ciphertext, std::size_t(ciphertext_len), key, iv,
		  EVP_aes_256_cbc(), decryptedtext);

	  /* Add a NULL terminator. We are expecting printable text */
	  decryptedtext[decryptedtext_len] = std::byte('\0');

	  /* Show the decrypted text */
	  printf("Decrypted text is:\n");
	  printf("%s\n", (char const*)decryptedtext);

	  return 0;
	}

	static byte_buff random_bytes(std::size_t n)
	{
		byte_buff b(n);
		std::generate(std::begin(b), std::end(b), []{ return byte(hol::random_number<short>()); });
		return b;
	}
};

} // namespace openssl_utils
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_OPENSSL_UTILS_H
