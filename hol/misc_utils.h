#ifndef HEADER_ONLY_LIBRARY_MISC_UTILS_H
#define HEADER_ONLY_LIBRARY_MISC_UTILS_H
//
// Copyright (c) 2017 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <cassert>
#include <iostream>
#include <limits>
#include <memory>
#include <type_traits>

//#include "bug.h"
#include "assertions.h"

// VERSIONS

#if __cplusplus >= 199711L
#  define HOL_AT_LEAST_CPP98
#  define HOL_AT_LEAST_CPP03
#endif

#if __cplusplus >= 201103L
#  define HOL_AT_LEAST_CPP11
#endif

#if __cplusplus >= 201402L
#  define HOL_AT_LEAST_CPP14
#endif

#if __cplusplus >= 201703L
#  define HOL_AT_LEAST_CPP17
#endif

#ifdef __GNUG__
//
// Branch Prediction Hints
//
// Usage:
//
// if(HOL_LIKELY(my_condition(i)))
// {
//     // most visited code here
// }
//
#define HOL_LIKELY(x)    __builtin_expect(!!(x), 1)
#define HOL_UNLIKELY(x)  __builtin_expect(!!(x), 0)
#else
#define HOL_LIKELY(x)    (x)
#define HOL_UNLIKELY(x)  (x)
#endif

// Attributes

#ifndef __has_cpp_attribute
#define __has_cpp_attribute(name) 0
#endif

#ifndef HOL_NODISCARD
#if __has_cpp_attribute(nodiscard)
#define HOL_NODISCARD [[nodiscard]]
#else
#define HOL_NODISCARD
#endif
#endif // HOL_NODISCARD

#ifndef HOL_NORETURN
#if __has_cpp_attribute(noreturn)
#define HOL_NORETURN [[noreturn]]
#else
#define HOL_NORETURN
#endif
#endif // HOL_NORETURN

#ifndef HOL_DEPRECATED
#if __has_cpp_attribute(deprecated)
#define HOL_DEPRECATED(reason) [[deprecated(reason)]]
#else
#define HOL_DEPRECATED(reason)
#endif
#endif // HOL_DEPRECATED

#ifndef HOL_FALLTHROUGH
#if __has_cpp_attribute(fallthrough)
#define HOL_FALLTHROUGH [[fallthrough]]
#else
#define HOL_FALLTHROUGH
#endif
#endif // HOL_FALLTHROUGH

#ifndef HOL_MAYBE_UNUSED
#if __has_cpp_attribute(maybe_unused)
#define HOL_MAYBE_UNUSED [[maybe_unused]]
#else
#define HOL_MAYBE_UNUSED
#endif
#endif // HOL_MAYBE_UNUSED

// casting

namespace header_only_library {
namespace misc_utils {

template <class To, class From>
inline To integral_cast(From from) HOL_ASSERT_NOEXCEPT
{
	static_assert(std::is_integral<To>::value||std::is_enum<To>::value, "Casting to type must be integral.");
	static_assert(std::is_integral<From>::value||std::is_enum<From>::value, "Casting from type must be integral.");

	To to = static_cast<To>(from);
	HOL_ASSERT_MSG(static_cast<From>(to) == from, "narrowing changed the value from: " << std::size_t(from) << " to: " << std::size_t(to));
	HOL_ASSERT_MSG((std::is_signed<To>::value == std::is_signed<From>::value) || (to < To{}) == (from < From{}),
		"narrowing changed the sign of the value from: " << std::size_t(from) << " to: " << std::size_t(to));
	return to;
}

// TODO: Fix this version 2021-06-30
//
//template <class To, class From>
//inline To integral_cast(From from) HOL_ASSERT_NOEXCEPT
//{
//	static_assert(std::is_integral<To>::value||std::is_enum<To>::value, "Casting to type must be integral.");
//	static_assert(std::is_integral<From>::value||std::is_enum<From>::value, "Casting from type must be integral.");
//
//	To to = static_cast<To>(from);
//	HOL_ASSERT_MSG(static_cast<From>(to) == from, "narrowing changed the value from: " << std::size_t(from) << " to: " << std::size_t(to));
//	HOL_ASSERT_MSG(std::is_same_signedness<To, From>::value || ((to < To{}) == (from < From{})),
//		"narrowing changed the sign of the value from: " << (from<0?"-ve":"+ve") << " --> " << (from<0?"+ve":"-ve"));
//	return to;
//}

/**
 * Cast from float type to integral type checking size compatibility
 * when NDEBUG is undfined.
 * @tparam I Integral type.
 * @tparam F Floating type.
 * @param f Floating value to cast.
 * @return
 */
template <class I, class F>
inline I floating_cast(F f) HOL_ASSERT_NOEXCEPT
{
	static_assert(std::is_floating_point<F>::value,
		"Can only be used to cast from floating types.");

	static_assert(std::is_integral<I>::value,
		"Can only be used to cast to integral types.");

	HOL_ASSERT_MSG(F(std::numeric_limits<I>::max()) >= f && F(std::numeric_limits<I>::lowest() <= f),
		"target type is not large enough to contain the source value: " << f);

	return static_cast<I>(f);
}

template<typename Iter1, typename Iter2>
std::size_t positive_distance(Iter1 a, Iter2 b)
{
	HOL_ASSERT(a < b);
	return std::size_t(b - a);
}

template<typename Iter1, typename Iter2>
std::size_t absolute_distance(Iter1 a, Iter2 b)
{
	if(a < b)
		return positive_distance(a, b);
	return positive_distance(b, a);
}

//namespace detail {
//
//template <class T>
//struct remove_enum
//{
//	using type = typename std::conditional
//	<
//		std::is_enum<T>::value,
//		typename std::underlying_type<T>::type,
//		T
//	>::type;
//};
//
//}  // namespace detail

//template <class T, class U>
//inline T narrow_cast(U u) noexcept
//{
////	using type = typename detail::remove_enum<U>::type;
////	HOL_ASSERT_MSG(std::numeric_limits<T>::max() >= type(u) && std::numeric_limits<T>::lowest() <= type(u),
//	HOL_ASSERT_MSG(std::numeric_limits<T>::max() >= u && std::numeric_limits<T>::lowest() <= u,
//	"target type is not large enough to contain the source value: " << u);
//
//	return static_cast<T>(u);
//}

// C++14 version, option 2
//
#ifdef HOL_AT_LEAST_CPP17
auto inline as_signed   = [](auto x){ return std::make_signed_t  <decltype(x)>(x); };
auto inline as_unsigned = [](auto x){ return std::make_unsigned_t<decltype(x)>(x); };
#endif

template<typename Func>
class HOL_NODISCARD scoped_action
{
public:
	explicit scoped_action(Func&& func): func(std::forward<Func>(func)), active(true) {}
	~scoped_action() { if(active) func(); }

	scoped_action(scoped_action const&) = delete;
	scoped_action& operator=(scoped_action const&) = delete;

	scoped_action(scoped_action&& other)
		: func(std::move(other.func)), active(std::move(other.active))
			{ other.active = false; }

	scoped_action& operator=(scoped_action&& other)
		{ func = std::move(other.func); other.active = false; return *this; }

private:
	Func func;
	bool active = true;
};

template<typename Func>
scoped_action<Func> finally(Func&& func)
{
	return scoped_action<Func>(std::forward<Func>(func));
}

template<typename Container>
typename Container::value_type& at(Container&& c, std::size_t n)
{
#ifndef NDEBUG
	return std::forward<Container>(c).at(n);
#else
	return (*std::forward<Container>(c))[n];
#endif
}

template<typename Ptr>
class not_null
{
public:
	explicit not_null(Ptr ptr): ptr(ptr) { HOL_ASSERT(ptr != nullptr); }

	not_null(not_null const& other): ptr(other.ptr) { HOL_ASSERT(ptr != nullptr); }
//	not_null(not_null&& other): ptr(std::move(other.ptr)) { HOL_ASSERT(ptr != nullptr); }

    constexpr operator Ptr() const { return get(); }
    constexpr Ptr operator->() const { return get(); }
    constexpr decltype(*Ptr()) operator*() const { return *get(); }

private:
//	Ptr get()       { HOL_ASSERT(ptr != nullptr); return ptr; }
	Ptr get() const { HOL_ASSERT(ptr != nullptr); return ptr; }

	Ptr ptr;
};

template<typename Ptr>
auto get_if_not_null(std::unique_ptr<decltype(*Ptr())> const& p)
-> decltype(*Ptr())
{
	return not_null<Ptr>(p.get());
}

//template<typename Ptr>
//auto get_if_not_null(std::shared_ptr<decltype(*Ptr())> const& p)
//-> decltype(*Ptr())
//{
//	return not_null<Ptr>(p.get());
//}

template<typename Swapable>
void reduce_memory(Swapable& s)
{
	static_assert(std::is_copy_constructible<Swapable>::value,
		"Parameter must be copy constructable.");

	static_assert(std::is_void<decltype(Swapable().swap(s))>::value,
		"Parameter must be swapable.");

	Swapable(s).swap(s);
}

template<typename Swapable>
void clear_and_reduce_memory(Swapable& s)
{
	static_assert(std::is_default_constructible<Swapable>::value,
		"Parameter must be default constructable.");

	static_assert(std::is_void<decltype(Swapable().swap(s))>::value,
		"Parameter must be swapable.");

	Swapable().swap(s);
}

//template<typename T>
//void swap(T&& t1, T&& t2) { std::swap(std::forward<T>(t1), std::forward<T>(t2)); }
//
//template<typename T>
//void move(T& t1, T&& t2) { std::swap(t1, std::forward<T>(t2)); std::forward<T>(t2) = {}; }
//
//template<typename T>
//void move(T& t1, T& t2) { std::swap(t1, t2); t2 = {}; }

template<typename Swapable> HOL_NODISCARD
Swapable move_and_clear(Swapable& s)
	{ Swapable t{}; std::swap(s, t); return std::move(t); }

#if defined(__GNUC__)

template<typename Integer>
[[nodiscard]]
constexpr bool overflowing_add(Integer a, Integer b) noexcept
	{ return __builtin_add_overflow_p(a, b, a + b); }

template<typename Integer>
[[nodiscard]]
constexpr bool overflowing_sub(Integer a, Integer b) noexcept
	{ return __builtin_sub_overflow_p(a, b, a - b); }

template<typename Integer>
[[nodiscard]]
constexpr bool overflowing_mul(Integer a, Integer b) noexcept
	{ return __builtin_mul_overflow_p(a, b, a * b); }

template<typename Integer>
[[nodiscard]]
constexpr auto asserted_add(Integer a, Integer b) HOL_ASSERT_NOEXCEPT
-> decltype(a + b)
	{ HOL_ASSERT_MSG(!overflowing_add(a, b), a << " + " << b); return a + b; }

template<typename Integer>
[[nodiscard]]
constexpr auto asserted_sub(Integer a, Integer b) HOL_ASSERT_NOEXCEPT
-> decltype(a - b)
	{ HOL_ASSERT_MSG(!overflowing_sub(a, b), a << " - " << b); return a - b; }

template<typename Integer>
[[nodiscard]]
constexpr auto asserted_mul(Integer a, Integer b) HOL_ASSERT_NOEXCEPT
-> decltype(a * b)
	{ HOL_ASSERT_MSG(!overflowing_mul(a, b), a << " * " << b); return a * b; }

template<typename Integer>
[[nodiscard]]
constexpr auto add_or_throw(Integer a, Integer b)
-> decltype(a + b)
{
	if(overflowing_add(a, b))
		throw std::logic_error("overflow");
	return a + b;
}

template<typename Integer>
[[nodiscard]]
constexpr auto sub_or_throw(Integer a, Integer b)
-> decltype(a - b)
{
	if(overflowing_sub(a, b))
		throw std::logic_error("overflow");
	return a - b;
}

template<typename Integer>
[[nodiscard]]
constexpr auto mul_or_throw(Integer a, Integer b)
-> decltype(a * b)
{
	if(overflowing_mul(a, b))
		throw std::logic_error("overflow");
	return a * b;
}

#endif // __GNUC__

// == Sort by member variables =============

// USAGE:

//	class Car
//	{
//	public:
//		std::string name;
//		int age;
//		std::string manufacturer;
//	};
//
//	void print(const Car& car)
//	{
//		std::cout << "name        : " << car.name << '\n';
//		std::cout << "age         : " << car.age << '\n';
//		std::cout << "manufacturer: " << car.manufacturer << '\n';
//		std::cout << '\n';
//	}
//
//	int main()
//	{
//		std::vector<Car> cars;
//
//		cars.emplace_back();
//		cars.back().name = "Fiesta";
//		cars.back().age = 4;
//		cars.back().manufacturer = "Ford";
//
//		cars.emplace_back();
//		cars.back().name = "Capri";
//		cars.back().age = 2;
//		cars.back().manufacturer = "Ford";
//
//		cars.emplace_back();
//		cars.back().name = "Focus";
//		cars.back().age = 1;
//		cars.back().manufacturer = "Ford";
//
//		std::cout << "Input Order\n\n";
//
//		for(const auto& car: cars)
//			print(car);
//
//		std::cout << "Order by name\n\n";
//
//		std::sort(cars.begin(), cars.end(), make_mem_cmp(&Car::name));
//
//		for(const auto& car: cars)
//			print(car);
//
//		std::cout << "Order by age\n\n";
//
//		std::sort(cars.begin(), cars.end(), make_mem_cmp(&Car::age));
//
//		for(const auto& car: cars)
//			print(car);
//
//		std::cout << "Order by age [reverse]\n\n";
//
//		std::sort(cars.begin(), cars.end(), make_mem_cmp(&Car::age, std::greater<int>()));
//
//		for(const auto& car: cars)
//			print(car);
//
//		std::cout << "Order by age in a std::set\n\n";
//
//		std::set<Car, decltype(make_mem_cmp(&Car::age))> cset(make_mem_cmp(&Car::age));
//
//		for(const auto& car: cars)
//			cset.insert(car);
//
//		for(const auto& car: cset)
//			print(car);
//	}


/**
 * Generic comparator for class member variables
 */
template<typename CType, typename MType, typename Comp = std::less<MType>>
struct mem_cmp
{
	MType CType::* member;

	mem_cmp(MType CType::* member): member(member) {}

	bool operator()(const CType& l, const CType& r)
	{
		return Comp()(l.*member, r.*member);
	}
};

/**
 * Convenient class member comparator creator
 * @param member Class address of member variable (&Class::member)
 * @return a suitable comparator
 */
template<typename CType, typename MType, typename Comp = std::less<MType>>
auto make_mem_cmp(MType CType::* member, Comp = Comp())
-> mem_cmp<CType, MType, Comp>
{
	return mem_cmp<CType, MType, Comp>(member);
}

// == end of member sort ================

template<typename Container> inline
auto clear_move(Container& c)
{
	struct HOL_NODISCARD raii_clearer
	{
		raii_clearer(Container& c): c(c) {}
		~raii_clearer() { c.clear(); }
		operator Container&&() { return std::move(c); }
		Container& c;
	};

	return raii_clearer(c);
}

template<typename Container>
auto* data_begin(Container&& c)
	{ return std::forward<Container>(c).data(); }

template<typename Container>
auto* data_end(Container&& c)
	{ return std::forward<Container>(c).data() + std::forward<Container>(c).size(); }

template<typename Container>
auto const* data_cbegin(Container&& c)
	{ return std::forward<Container>(c).data(); }

template<typename Container>
auto const* data_cend(Container&& c)
	{ return std::forward<Container>(c).data() + std::forward<Container>(c).size(); }

} // namespace misc_utils
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_MISC_UTILS_H
