#ifndef HEADER_ONLY_LIBRARY_UNICODE_UTILS_H
#define HEADER_ONLY_LIBRARY_UNICODE_UTILS_H
//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//


// TODO: Look at this: http://bjoern.hoehrmann.de/utf-8/decoder/dfa/

#include <cassert>
#include <codecvt>
#include <locale>
#include <regex>
#include <stdexcept>
#include <string>

// std::byte
#if __cplusplus >= 201703L
#  include <cstddef>
#endif

//#include "bug.h"
//#include "macro_exceptions.h"
#include "misc_utils.h"

namespace header_only_library {
namespace unicode_utils {
namespace mu = misc_utils;
namespace detail {

using cvt_utf8_utf8 = std::wstring_convert<std::codecvt<char, char, std::mbstate_t>, char>;
using cvt_ws_utf8 = std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>;
using cvt_ucs2_utf8 = std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t>;
using cvt_ucs4_utf8 = std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>;
using cvt_utf16_utf8 = std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t>;
using cvt_utf32_utf8 = std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>;
using cvt_utf32_utf16 = std::wstring_convert<std::codecvt_utf16<char32_t>, char32_t>; // ??

template<typename StringType>
struct default_converter_for
{
	using type = typename std::conditional<
		std::is_same<StringType, std::wstring>::value, cvt_ws_utf8,
		typename std::conditional<
			std::is_same<StringType, std::u16string>::value, cvt_utf16_utf8,
			typename std::conditional<
				std::is_same<StringType, std::u32string>::value, cvt_utf32_utf8,
				cvt_utf8_utf8 // TODO: broken!
			>::type
		>::type
	>::type;
};

template<typename CodeCvt, typename FromString, typename ToString = std::string>
ToString convert_to(FromString const& s)
{
	CodeCvt cnv;
	ToString to = cnv.to_bytes(s);
	if(cnv.converted() < s.size())
		throw std::runtime_error("incomplete conversion");
	return to;
}

template<typename CodeCvt, typename ToString, typename FromString = std::string>
ToString convert_from(FromString const& s)
{
	CodeCvt cnv;
	ToString to = cnv.from_bytes(s);
	if(cnv.converted() < s.size())
		throw std::runtime_error("incomplete conversion");
	return to;
}

} //namespace detail

// UTF-32/UTF-16/UCS-4/UCS-2 to UTF-8/UTF-16

inline
std::string ucs2_to_utf8(std::u16string const& ucs2)
{
	return detail::convert_to<detail::cvt_ucs2_utf8, std::u16string>(ucs2);
}

inline
std::string utf16_to_utf8(std::u16string const& utf16)
{
	return detail::convert_to<detail::cvt_utf16_utf8, std::u16string>(utf16);
}

inline
std::string ucs4_to_utf8(std::u32string const& ucs4)
{
	return detail::convert_to<detail::cvt_ucs4_utf8, std::u32string>(ucs4);
}

inline
std::string utf32_to_utf8(std::u32string const& utf32)
{
	return detail::convert_to<detail::cvt_utf32_utf8, std::u32string>(utf32);
}

inline
std::string ws_to_utf8(std::wstring const& s)
{
	return detail::convert_to<detail::cvt_ws_utf8, std::wstring>(s);
}

inline
std::string utf_to_utf8(std::u16string const& s) { return utf16_to_utf8(s); }

inline
std::string utf_to_utf8(std::u32string const& s) { return utf32_to_utf8(s); }

inline
std::string utf_to_utf8(std::wstring const& s) { return ws_to_utf8(s); }

template<typename StringType>
StringType utf8_to_utf(std::string const& utf8)
{
	return detail::convert_from<typename detail::default_converter_for<StringType>::type, StringType>(utf8);
}

//inline
//std::u16string utf32_to_utf16(std::u32string const& s)
//{
//	return detail::convert_to<detail::cvt_utf32_utf16, std::u32string, std::u16string>(s);
//}

// UTF-8/UTF-16 to UTF-32/UTF-16/UCS-4/UCS-2

inline
std::u16string utf8_to_ucs2(std::string const& utf8)
{
	return detail::convert_from<detail::cvt_ucs2_utf8, std::u16string>(utf8);
}

inline
std::u16string utf8_to_utf16(std::string const& utf8)
{
	return detail::convert_from<detail::cvt_utf16_utf8, std::u16string>(utf8);
}

inline
std::u32string utf8_to_ucs4(std::string const& utf8)
{
	return detail::convert_from<detail::cvt_ucs4_utf8, std::u32string>(utf8);
}

inline
std::u32string utf8_to_utf32(std::string const& utf8)
{
	return detail::convert_from<detail::cvt_utf32_utf8, std::u32string>(utf8);
}

inline
std::wstring utf8_to_ws(std::string const& utf8)
{
	return detail::convert_from<detail::cvt_ws_utf8, std::wstring>(utf8);
}

//inline
//std::u32string utf16_to_utf32(std::u16string const& utf16)
//{
//	return detail::convert_from<detail::cvt_utf32_utf16, std::u32string, std::u16string>(utf16);
//}

//===================================================================//
// locale sensitive conversion between multibyte and wide characters //
//===================================================================//

inline
std::wstring mb_to_ws(std::mbstate_t& ps, char const* mp)
{
	std::wstring ws;
	char const* src = mp;

	std::size_t len = 1 + std::mbsrtowcs(0, &src, 3, &ps);

	ws.resize(len);
	src = mp;

	std::mbsrtowcs(&ws[0], &src, ws.size(), &ps);

	if(src)
		throw std::runtime_error("invalid multibyte character after: '"
			+ std::string(mp, src) + "'");

	ws.pop_back();

	return ws;
}

inline
std::wstring mb_to_ws(char const* mp)
{
	std::mbstate_t ps{};
	return mb_to_ws(ps, mp);
}

inline
std::wstring mb_to_ws(std::mbstate_t& ps, std::string const& mb)
{
	return mb_to_ws(ps, mb.data());
}

inline
std::wstring mb_to_ws(std::string const& mb)
{
	std::mbstate_t ps{};
	return mb_to_ws(ps, mb);
}

inline
std::string ws_to_mb(std::mbstate_t& ps, wchar_t const* wp)
{
	std::string mb;
	wchar_t const* src = wp;

	std::size_t len = 1 + std::wcsrtombs(0, &src, 0, &ps);

	mb.resize(len);
	src = wp;

	std::wcsrtombs(&mb[0], &src, mb.size(), &ps);

	if(src)
		throw std::runtime_error("invalid wide character");

	mb.pop_back();

	return mb;
}

inline
std::string ws_to_mb(wchar_t const* wp)
{
	std::mbstate_t ps{};
	return ws_to_mb(ps, wp);
}

inline
std::string ws_to_mb(std::mbstate_t& ps, std::wstring const& ws)
{
	return ws_to_mb(ps, ws.data());
}

inline
std::string ws_to_mb(std::wstring const& ws)
{
	std::mbstate_t ps{};
	return ws_to_mb(ps, ws);
}

inline
std::string wc_to_mb(wchar_t wc)
{
	wchar_t tmp[2]{wc, L'\0'};
	return ws_to_mb(tmp);
}

// Codepoint conversions

inline
std::string codepoint_to_utf8(char32_t cp)
{
    char utf8[4];
    char* end_of_utf8;

	char32_t const* from = &cp;

	std::mbstate_t mbs;
	std::codecvt_utf8<char32_t> ccv;

	if(ccv.out(mbs, from, from + 1, from, utf8, utf8 + 4, end_of_utf8))
		throw std::runtime_error("bad conversion");

	return {utf8, end_of_utf8};
}

// UTF-16 may contain either one or two char16_t characters so
// we return a string to potentially contain both.
//
inline
std::u16string codepoint_to_utf16(char32_t cp)
{
	// convert UTF-32 (standard unicode codepoint) to UTF-8 intermediate value
    char utf8[4];
    char* end_of_utf8;

    {
        char32_t const* from = &cp;

        std::mbstate_t mbs;
	    std::codecvt_utf8<char32_t> ccv;

		if(ccv.out(mbs, from, from + 1, from, utf8, utf8 + 4, end_of_utf8))
			throw std::runtime_error("bad conversion");
    }

    // Now convert the UTF-8 intermediate value to UTF-16

    char16_t utf16[2];
    char16_t* end_of_utf16;

    {
        char const* from = nullptr;

		std::mbstate_t mbs;
	    std::codecvt_utf8_utf16<char16_t> ccv;

		if(ccv.in(mbs, utf8, end_of_utf8, from, utf16, utf16 + 2, end_of_utf16))
			throw std::runtime_error("bad conversion");
    }

    return {utf16, end_of_utf16};
}

using utf_regex_traits = std::regex_traits<char32_t>;

/**
 * This string class uses `UTF-32` internally but accepts and
 * outputs `UTF-8`.
 */
class utf_string
: private std::u32string
{
	using base = std::u32string;

public:
	using value_type = base::value_type;
	using size_type = base::size_type;
	using pointer = base::pointer;
	using const_pointer = base::const_pointer;
	using reference = base::reference;
	using const_reference = base::const_reference;

	utf_string() {}
	utf_string(char const* utf8): std::u32string(utf8_to_utf32(std::string(utf8))) {}
	utf_string(std::string const& utf8): std::u32string(utf8_to_utf32(utf8)) {}
	utf_string(std::u32string const& utf32): std::u32string(utf32) {}

	utf_string& operator=(std::string const& utf8)
		{ *this = utf8_to_utf32(utf8); return *this; }

	utf_string& operator=(char const* utf8)
		{ return *this = std::string(utf8); }

	size_type size() const { return base::size(); }
	std::ptrdiff_t ssize() const { return mu::integral_cast<std::ptrdiff_t>(base::size()); }

	reference operator[](size_type n)
	{
		HOL_ASSERT(n < size());
		return base::operator[](n);
	}

	const_reference operator[](size_type n) const
	{
		HOL_ASSERT(n < size());
		return base::operator[](n);
	}

#if defined(HOL_AT_LEAST_CPP17)

	std::byte* bytes() { return reinterpret_cast<std::byte*>(data()); }
	std::byte const* bytes() const { return reinterpret_cast<std::byte const*>(data()); }
	size_type size_in_bytes() const { return size() * sizeof(base::value_type); }

#endif

	friend utf_string operator+(utf_string const& s1, char32_t c)
		{ return utf_string(static_cast<base>(s1) + c); }

	friend utf_string operator+(utf_string const& s1, utf_string const& s2)
		{ return utf_string(static_cast<base>(s1) + static_cast<base>(s2)); }

	friend utf_string operator+(utf_string const& utf32, char const* utf8)
		{ return utf32 + utf_string(utf8); }

	friend utf_string operator+(char const* utf8, utf_string const& utf32)
		{ return utf_string(utf8) + utf32; }

	friend utf_string operator+(utf_string const& utf32, std::string const& utf8)
		{ return utf32 + utf_string(utf8); }

	friend utf_string operator+(std::string const& utf8, utf_string const& utf32)
		{ return utf_string(utf8) + utf32; }


	friend bool operator==(utf_string const& s1, utf_string const& s2)
		{ return static_cast<base>(s1) == static_cast<base>(s2); }

	friend bool operator==(utf_string const& utf32, char const* utf8)
		{ return utf32 == utf_string(utf8); }

	friend bool operator==(char const* utf8, utf_string const& utf32)
		{ return utf_string(utf8) == utf32; }

	friend bool operator==(utf_string const& utf32, std::string const& utf8)
		{ return utf32 == utf_string(utf8); }

	friend bool operator==(std::string const& utf8, utf_string const& utf32)
		{ return utf_string(utf8) == utf32; }


	friend bool operator!=(utf_string const& s1, utf_string const& s2)
		{ return static_cast<base>(s1) != static_cast<base>(s2); }

	friend bool operator!=(utf_string const& utf32, char const* utf8)
		{ return utf32 != utf_string(utf8); }

	friend bool operator!=(char const* utf8, utf_string const& utf32)
		{ return utf_string(utf8) != utf32; }

	friend bool operator!=(utf_string const& utf32, std::string const& utf8)
		{ return utf32 != utf_string(utf8); }

	friend bool operator!=(std::string const& utf8, utf_string const& utf32)
		{ return utf_string(utf8) != utf32; }


	friend bool operator<=(utf_string const& s1, utf_string const& s2)
		{ return static_cast<base>(s1) <= static_cast<base>(s2); }

	friend bool operator<=(utf_string const& utf32, char const* utf8)
		{ return utf32 <= utf_string(utf8); }

	friend bool operator<=(char const* utf8, utf_string const& utf32)
		{ return utf_string(utf8) <= utf32; }

	friend bool operator<=(utf_string const& utf32, std::string const& utf8)
		{ return utf32 <= utf_string(utf8); }

	friend bool operator<=(std::string const& utf8, utf_string const& utf32)
		{ return utf_string(utf8) <= utf32; }


	friend bool operator>=(utf_string const& s1, utf_string const& s2)
		{ return static_cast<base>(s1) >= static_cast<base>(s2); }

	friend bool operator>=(utf_string const& utf32, char const* utf8)
		{ return utf32 >= utf_string(utf8); }

	friend bool operator>=(char const* utf8, utf_string const& utf32)
		{ return utf_string(utf8) >= utf32; }

	friend bool operator>=(utf_string const& utf32, std::string const& utf8)
		{ return utf32 >= utf_string(utf8); }

	friend bool operator>=(std::string const& utf8, utf_string const& utf32)
		{ return utf_string(utf8) >= utf32; }


	friend bool operator<(utf_string const& s1, utf_string const& s2)
		{ return static_cast<base>(s1) < static_cast<base>(s2); }

	friend bool operator<(utf_string const& utf32, char const* utf8)
		{ return utf32 < utf_string(utf8); }

	friend bool operator<(char const* utf8, utf_string const& utf32)
		{ return utf_string(utf8) < utf32; }

	friend bool operator<(utf_string const& utf32, std::string const& utf8)
		{ return utf32 < utf_string(utf8); }

	friend bool operator<(std::string const& utf8, utf_string const& utf32)
		{ return utf_string(utf8) < utf32; }


	friend bool operator>(utf_string const& s1, utf_string const& s2)
		{ return static_cast<base>(s1) > static_cast<base>(s2); }

	friend bool operator>(utf_string const& utf32, char const* utf8)
		{ return utf32 > utf_string(utf8); }

	friend bool operator>(char const* utf8, utf_string const& utf32)
		{ return utf_string(utf8) > utf32; }

	friend bool operator>(utf_string const& utf32, std::string const& utf8)
		{ return utf32 > utf_string(utf8); }

	friend bool operator>(std::string const& utf8, utf_string const& utf32)
		{ return utf_string(utf8) > utf32; }

	std::string string() const { return utf32_to_utf8(*this); }

	operator std::string() const { return string(); }

	using base::clear;

	void release() { utf_string().swap(*this); }
	void shrink() { utf_string(*this).swap(*this); }
	void shrink_to_fit() { base::shrink_to_fit(); }

	auto begin() { return base::begin(); }
	auto begin() const { return base::begin(); }

	auto end() { return base::end(); }
	auto end() const { return base::end(); }

	auto cbegin() const { return base::cbegin(); }
	auto cend() const { return base::cend(); }

	auto rbegin() { return base::rbegin(); }
	auto rbegin() const { return base::rbegin(); }

	auto rend() { return base::rend(); }
	auto rend() const { return base::rend(); }

	auto crbegin() const { return base::crbegin(); }
	auto crend() const { return base::crend(); }

	friend
	std::ostream& operator<<(std::ostream& os, utf_string const& s)
		{ return os << s.string(); }

	friend
	std::istream& operator>>(std::istream& is, utf_string& s)
	{
		std::string utf8;
		if(is >> utf8)
			s = utf8;
		return is;
	}

	// TODO: Look for optimization opportunities
	// here (many of these are super inefficient.
	//
	// For example a finder that converts one UTF-8 char
	// at a time as and when necessary rather than converting
	// the whole string up front (see search_utf8_in_utf32).
	//
	// Also utf8_utf32(char const*, std::size_t) primitives.
	//

	using base::find;

	size_type find(std::string const& s, size_type pos = 0) const
		{ return base::find(utf8_to_utf32(s), pos); }

	size_type find(char const* s, size_type pos, size_type count) const
		{ return base::find(utf8_to_utf32(s).c_str(), pos, count); }

	size_type find(char const* s, size_type pos = 0) const
		{ return base::find(utf8_to_utf32(s).c_str(), pos); }

	size_type find(char c, size_type pos = 0) const
		{ return base::find(char32_t(c), pos); }

#if defined(HOL_AT_LEAST_CPP17)
	size_type find(std::string_view s, size_type pos = 0) const
		{ return base::find(utf8_to_utf32(std::string(s)), pos); }
#endif

	using base::find_first_not_of;
	using base::find_first_of;
	using base::find_last_not_of;


	utf_string substr(std::size_t pos, std::size_t n = npos) const
		{ return base::substr(pos, n); }

	using base::c_str;
	using base::data;
	using base::empty;

	constexpr static std::size_t const npos = std::size_t(-1);

private:
//	char32_t* search_utf8_in_utf32(char32_t* beg_32, char32_t* end_32,
//		char const* beg_8, char const* end_8)
//	{
//		auto cmp_32_ptr = std::make_unique<char32_t>(std::distance(beg_8, end_8));
//		auto cmp_32_pos = cmp_32_ptr.get();
//
//		std::mbstate_t state;
//		std::codecvt_utf8<char32_t> cvt;
//
//		while(beg_32 != end_32)
//		{
//			if(beg_8 != end_8) // do we have the full search string?
//				auto res = cvt.in(state, beg_8, end_8, beg_8, cmp_32_pos, cmp_32_pos + 1, cmp_32_pos);
//
//			if(res == std::codecvt::error)
//				throw std::runtime_error("bad encoding");
//
//			HOL_ASSERT(ret != std::codecvt::noconv);
//			HOL_ASSERT(ret != std::codecvt::partial);
//
//			// search with what we have
//
//			auto found = std::search(beg_32, end_32, cmp_32_ptr.get(), cmp_32_pos);
//
//			if(found > end_32 - std::distance(cmp_32_ptr.get(), cmp_32_pos))
//				return end_32; // not in string
//
//			// convert & match one at a time until success or failure
//			beg_32 = found + std::distance(cmp_32_ptr.get(), cmp_32_pos);
//
//			while(true) // do we have the full search string?
//			{
//				auto tst = cmp_32_pos;
//
//				auto res = cvt.in(state, beg_8, end_8, beg_8, cmp_32_pos, cmp_32_pos + 1, cmp_32_pos);
//
//				if(res == std::codecvt::error)
//					throw std::runtime_error("bad encoding");
//
//				HOL_ASSERT(ret != std::codecvt::noconv);
//				HOL_ASSERT(ret != std::codecvt::partial);
//
//				if(*tst != *beg_32)
//					break;
//			}
//
//		return 0;
//	}
};

enum class encoding_type{unknown, utf8, utf16le, utf16be, utf32le, utf32be};

#if __cplusplus >= 201703L
	using byte = std::byte;
#else
	using byte = unsigned char;
#endif

inline
encoding_type get_encoding_from_BOM(byte* buf, std::size_t n)
{
	// utf8	- EF BB BF
	// utf16le - FF FE
	// utf16be - FE FF
	// utf32le - FF FE 00 00
	// utf32be - 00 00 FE FF

	if(n > 1) // UTF-16
	{
		if(buf[0] == byte(0xFF) && buf[1] == byte(0xFE))
			return encoding_type::utf16le;
		else if(buf[0] == byte(0xFE) && buf[1] == byte(0xFF))
			return encoding_type::utf16be;
	}

	if(n > 2) // UTF-8
	{
		if(buf[0] == byte(0xEF) && buf[1] == byte(0xBB) && buf[2] == byte(0xBF))
			return encoding_type::utf8;
	}

	if(n == 4) // UTF-32
	{
		if(buf[0] == byte(0xFF) && buf[1] == byte(0xFE) && buf[2] == byte(0x00) && buf[3] == byte(0x00))
			return encoding_type::utf32le;
		else if(buf[0] == byte(0x00) && buf[1] == byte(0x00) && buf[2] == byte(0xFE) && buf[3] == byte(0xFF))
			return encoding_type::utf32be;
	}

	return encoding_type::unknown;
}

#if __cplusplus > 201703L

//#define char8_t char8_t
//
//std::u32string utf8_to_utf32(std::u8string const& utf8)
//{
//	using codecvt_32_8_type = std::codecvt<char32_t, char8_t, std::mbstate_t>;
//
//	struct codecvt2
//	: public codecvt_32_8_type
//	{
//		codecvt2(std::size_t refs = 0): codecvt_32_8_type(refs) {}
//	};
//
//	std::u32string utf32(utf8.size(), char32_t('\0'));
//
////	codecvt2 f;
//
//
//	std::mbstate_t state{};
//
//	char8_t const* from_next;
//	char32_t* to_next;
//
//	auto r = codecvt2().in(state,
//		std::data(utf8),
//		std::data(utf8) + std::size(utf8),
//		from_next,
//		&utf32[0],
//		(&utf32[0]) + utf32.size(),
//		to_next);
//
//	(void)r;
//
//	utf32.resize(std::size_t(std::distance(std::data(utf32), to_next)));
//
//	return utf32;
//}
//
///**
// *
// * @param state To be passed in each batch
// * @param from_beg Beginning of input batch
// * @param from_end End of input batch (updated to reflect actual use)
// * @param to_beg Beginning of output batch
// * @param to_end End of output batch (updated to reflect actual use)
// */
//void utf32_to_utf8(std::mbstate_t& state,
//	char32_t const* from_beg,
//	char32_t const*& from_end,
//	char8_t* to_beg,
//	char8_t*& to_end)
//{
//	using codecvt_32_8_type = std::codecvt<char32_t, char8_t, std::mbstate_t>;
//
//	struct codecvt2
//	: public codecvt_32_8_type
//	{
//		codecvt2(std::size_t refs = 0): codecvt_32_8_type(refs) {}
//	};
//
//	char32_t const* from_end_w;
//	char8_t* to_end_w;
//
//	auto r = codecvt2().out(state,
//		from_beg,
//		from_end,
//		from_end_w,
//		to_beg,
//		to_end,
//		to_end_w);
//
//	(void)r;
//
//	from_end = from_end_w;
//	to_end = to_end_w;
//}
//
//std::u8string utf32_to_utf8(std::mbstate_t& state, std::u32string const& utf32)
//{
//	using codecvt_32_8_type = std::codecvt<char32_t, char8_t, std::mbstate_t>;
//
//	struct codecvt2
//	: public codecvt_32_8_type
//	{
//		codecvt2(std::size_t refs = 0): codecvt_32_8_type(refs) {}
//	};
//
//	std::u8string utf8(utf32.size() * 4, char8_t('\0'));
//
////	std::mbstate_t state{};
//
//	const char32_t* from_next;
//	char8_t* to_next;
//
//	auto r = codecvt2().out(state,
//		std::data(utf32),
//		std::data(utf32) + std::size(utf32),
//		from_next,
//		&utf8[0],
//		(&utf8[0]) + std::size(utf8),
//		to_next);
//
//	(void)r;
//
//	utf8.resize(std::size_t(std::distance(std::data(utf8), to_next)));
//
//	return utf8;
//}
//
//std::u8string utf32_to_utf8(std::u32string const& utf32)
//{
//	std::mbstate_t state;
//	return utf32_to_utf8(state, utf32);
//}
//

#endif


inline
bool unicode_convert(std::string const& s, std::string& s2)   { s2 = s; return true; }

inline
bool unicode_convert(std::string const& s,   std::wstring& u) { try{u = utf8_to_ws(s);   }catch(...){return false;}return true; }
inline
bool unicode_convert(std::string const& s, std::u16string& u) { try{u = utf8_to_utf16(s);}catch(...){return false;}return true; }
inline
bool unicode_convert(std::string const& s, std::u32string& u) { try{u = utf8_to_utf32(s);}catch(...){return false;}return true; }

inline
bool unicode_convert(  std::wstring const& u, std::string& s) { try{s = ws_to_utf8(u);   }catch(...){return false;}return true; }
inline
bool unicode_convert(std::u16string const& u, std::string& s) { try{s = utf16_to_utf8(u);}catch(...){return false;}return true; }
inline
bool unicode_convert(std::u32string const& u, std::string& s) { try{s = utf32_to_utf8(u);}catch(...){return false;}return true; }

} // unicode_utils
} // header_only_library

#endif // HEADER_ONLY_LIBRARY_UNICODE_UTILS_H
