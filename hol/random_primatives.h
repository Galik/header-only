#ifndef HEADER_ONLY_LIBRARY_RANDOM_PRIMATIVES_H
#define HEADER_ONLY_LIBRARY_RANDOM_PRIMATIVES_H
//
// Copyright (c) 2017 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <algorithm>
#include <array>
#include <cassert>
#include <chrono>
#include <limits>
#include <random>
#include <thread>

#include "assertions.h"
//#include "misc_utils.h"

#ifndef HOL_AT_LEAST_CPP17
#  if __cplusplus >= 201703L
#    define HOL_AT_LEAST_CPP17
#  endif
#endif // HOL_AT_LEAST_CPP17

namespace header_only_library {

/*! \namespace random_numbers
    \brief Random number functions, making modern C++ random number generation simple.
*/
namespace random_numbers {
namespace detail {

/*! \struct std_int_type_test
	\brief SFNAE test for integer types
 */
template<typename Integer>
struct std_int_type_test
{
	/*! true or false */
	constexpr static bool value =
	std::is_same<Integer, short>::value
	|| std::is_same<Integer, int>::value
	|| std::is_same<Integer, long>::value
	|| std::is_same<Integer, long long>::value
	|| std::is_same<Integer, unsigned short>::value
	|| std::is_same<Integer, unsigned int>::value
	|| std::is_same<Integer, unsigned long>::value
	|| std::is_same<Integer, unsigned long long>::value;
};

/*! \struct std_real_type_test
	\brief SFNAE test for float types
 */
template<typename Real>
struct std_real_type_test
{
	/*! true or false */
	constexpr static bool value =
	std::is_same<Real, float>::value
	|| std::is_same<Real, double>::value
	|| std::is_same<Real, long double>::value;
};

template<typename PRNG, typename Number>
Number random_number(PRNG& prng, Number from, Number to)
{
	static_assert(detail::std_int_type_test<Number>::value
		|| detail::std_real_type_test<Number>::value,
		   "Parameters must be integer or floating point numbers");

	using Distribution = typename std::conditional
	<
		std::is_integral<Number>::value,
		std::uniform_int_distribution<Number>,
		std::uniform_real_distribution<Number>
	>::type;

	thread_local static Distribution dist;

	return dist(prng, typename Distribution::param_type{from, to});
}

template<typename PRNG, typename Number>
Number random_number(PRNG& prng)
{
	static_assert(std_int_type_test<Number>::value||std_real_type_test<Number>::value,
		"Parameters must be integer or floating point numbers");

	using Distribution = typename std::conditional
	<
		std::is_integral<Number>::value,
		std::uniform_int_distribution<Number>,
		std::uniform_real_distribution<Number>
	>::type;

	thread_local static Distribution dist;

	return dist(prng);
}

template<typename PRNG, typename Distribution, typename... Args>
typename Distribution::result_type
randomly_distributed_number(PRNG& prng, Args&&... args)
{
	using param_type = typename Distribution::param_type;

	thread_local static Distribution dist;
	return dist(prng, param_type(std::forward<Args>(args)...));
}

} // detail

template<typename PRNG> inline
void random_reseed(PRNG& prng, std::seed_seq& seeds)
	{ prng.seed(seeds); }

template<typename PRNG, typename Number>
void random_reseed(PRNG& prng, Number number)
	{ prng.seed(number); }

/**
 * Randomly return true or false.
 *
 * @param p The probability that the function will return `true`.
 *
 * @return true or false
 */
template<typename Real = double>
bool random_choice(typename PRNG, Real p = 0.5)
{
	HOL_ASSERT(p >= 0.0);
	HOL_ASSERT(p <= 1.0);
	return detail::randomly_distributed_number<std::bernoulli_distribution>(prng, p);
}

/**
 * Return an iterator to a randomly selected container element
 * from the `begin` iterator up to, and including the `end` iterator.
 *
 * @param begin The first iterator in the range to be considered.
 * @param end The last iterator in the range.
 *
 * @return An iterator to a pseudo randomly selected element
 * from the supplied range (or the `end` iterator).
 */
template<typename PRNG, typename Iter>
Iter random_iterator(PRNG& prng, Iter begin, Iter end)
{
	return std::next(begin,
		random_number(prng, std::distance(begin, end)));
}

/**
 * Return a randomly selected iterator to container.
 *
 * @param c The container to select an iterator from.
 *
 * @return A randomly selected iterator, including std::end(c).
 */
template<typename PRNG, typename Container>
auto random_iterator(PRNG& prng, Container& c) -> decltype(std::begin(c))
{
	assert(!c.empty());
	return random_iterator(prng, std::begin(c), std::end(c));
}

/**
 * Return a randomly selected iterator to an array.
 *
 * @param array The array to select an element from.
 *
 * @return A randomly selected iterator, including std::end(array).
 */
template<typename PRNG, typename T, std::size_t N>
auto random_iterator(PRNG& prng, T(&array)[N]) -> decltype(std::begin(array))
{
	assert(N > 0);
	return std::next(std::begin(array), random_number(prng, N));
}

/**
 * Return a randomly selected element from a container. If the
 * container is empty the behavior is undefined.
 *
 * @param c The container to select an element from.
 *
 * @return A reference to a pseudo randomly selected element from the supplied container.
 */
template<typename PRNG, typename Container>
auto random_element(PRNG& prng, Container&& c) -> decltype(*std::begin(c))
{
	assert(!c.empty());
	return *random_iterator(prng, std::begin(std::forward<Container>(c)),
		std::prev(std::end(std::forward<Container>(c))));
}

/**
 * Return a randomly selected element from a range of iterators. If the
 * distance between the iterators is zero,  the behavior is undefined.
 *
 * @param begin The first iterator in the range to be considered.
 * @param end The last iterator in the range which will not be included.
 *
 * @return A reference to a pseudo randomly selected element from the supplied container.
 */
template<typename PRNG, typename Iter>
auto random_element(PRNG& prng, Iter begin, Iter end) -> decltype(*begin)
{
	assert(std::distance(begin, end) > 0);
	return *random_iterator(prng, begin, std::prev(end));
}

template<typename PRNG, typename T, std::size_t N>
auto random_element(PRNG& prng, T(&array)[N]) -> T&
{
	return array[random_number(prng, N - 1)];
}

template<typename PRNG, typename Iter>
void random_shuffle(PRNG& prng, Iter begin, Iter end)
{
	std::shuffle(begin, end, prng);
}

template<typename PRNG, typename Container>
void random_shuffle(PRNG& prng, Container& c)
{
	std::shuffle(std::begin(c), std::end(c), prng);
}

template<typename PRNG, typename T, std::size_t N>
void random_shuffle(PRNG, T(&array)[N])
{
	std::shuffle(std::begin(array), std::end(array), prng);
}

template<typename PRNG, typename Duration1, typename Duration2>
void random_sleep_for(PRNG& prng, Duration1 min, Duration2 max)
{
	using common_duration_type = typename std::common_type<Duration1, Duration2>::type;
	auto duration = common_duration_type(random_number(prng, min.count(), max.count()));
	std::this_thread::sleep_for(duration);
}

#ifdef HOL_AT_LEAST_CPP17

template<typename PRNG, typename PIter, typename SIter>
auto random_sample(PRNG& prng, PIter begin, PIter end, SIter out, std::size_t n)
	{ std::sample(begin, end, out, n, prng); }

template<typename PRNG, typename Container>
auto random_sample(PRNG& prng, Container&& c, std::size_t n)
{
	if(n >= c.size())
		return c;

	typename std::remove_reference<Container>::type s;
	s.reserve(n);
	random_sample(prng, std::begin(std::forward<Container>(c)), std::end(std::forward<Container>(c)),
		std::inserter(s, std::end(s)), n);
	return s;
}

#endif // C++17

} // namespace random_numbers
} // namespace header_only_library

#endif // HEADER_ONLY_LIBRARY_RANDOM_PRIMATIVES_H

