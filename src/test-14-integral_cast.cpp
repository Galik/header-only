//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "hol/misc_utils.h"

namespace hol {
	using namespace header_only_library::misc_utils;
}

TEST_CASE("Integral Cast", "[integral_cast]")
{
	// int16_t limits
	std::int32_t i16_max = std::numeric_limits<std::int16_t>::max();
	std::int32_t i16_min = std::numeric_limits<std::int16_t>::lowest();
	std::int32_t i16_nil = 0;

	std::int32_t u16_max = std::numeric_limits<std::uint16_t>::max();
	std::int32_t u16_min = std::numeric_limits<std::uint16_t>::lowest();
	std::int32_t u16_nil = 0;

	std::int32_t i16_pos = +1;
	std::int32_t i16_neg = -1;

	SECTION("")
	{
		REQUIRE_NOTHROW(hol::integral_cast<std::int16_t>(i16_max));
		REQUIRE_NOTHROW(hol::integral_cast<std::int16_t>(i16_min));
		REQUIRE_NOTHROW(hol::integral_cast<std::int16_t>(i16_nil));
		REQUIRE_THROWS (hol::integral_cast<std::int16_t>(u16_max));
		REQUIRE_NOTHROW(hol::integral_cast<std::int16_t>(u16_min));
		REQUIRE_NOTHROW(hol::integral_cast<std::int16_t>(u16_nil));

		REQUIRE_NOTHROW(hol::integral_cast<std::uint16_t>(i16_max));
		REQUIRE_THROWS (hol::integral_cast<std::uint16_t>(i16_min));
		REQUIRE_NOTHROW(hol::integral_cast<std::uint16_t>(i16_nil));
		REQUIRE_NOTHROW(hol::integral_cast<std::uint16_t>(u16_max));
		REQUIRE_NOTHROW(hol::integral_cast<std::uint16_t>(u16_min));
		REQUIRE_NOTHROW(hol::integral_cast<std::uint16_t>(u16_nil));

		REQUIRE_NOTHROW(hol::integral_cast<std::uint16_t>(i16_pos));
		REQUIRE_THROWS (hol::integral_cast<std::uint16_t>(i16_neg));
	}
}
