//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

//#include <algorithm>
#include <string>
#include <vector>

#include "hol/bug.h"
#include "hol/random_numbers.h"

namespace hol{
	using namespace header_only_library::random_numbers;
}

TEST_CASE("types", "[]")
{
	using random_generator_type = std::remove_reference_t<decltype(hol::random_generator())>;

	SECTION("short")
	{
		random_generator_type mt{1};
		std::uniform_int_distribution<short> pick;
		hol::random_reseed(1);
		for(auto n = 0; n < 100; ++n)
			REQUIRE(hol::random_number<short>() == pick(mt));
	}

	SECTION("int")
	{
		random_generator_type mt{1};
		std::uniform_int_distribution<int> pick;
		hol::random_reseed(1);
		for(auto n = 0; n < 100; ++n)
			REQUIRE(hol::random_number<int>() == pick(mt));
	}

	SECTION("long")
	{
		random_generator_type mt{1};
		std::uniform_int_distribution<long> pick;
		hol::random_reseed(1);
		for(auto n = 0; n < 100; ++n)
			REQUIRE(hol::random_number<long>() == pick(mt));
	}

	SECTION("long long")
	{
		random_generator_type mt{1};
		std::uniform_int_distribution<long long> pick;
		hol::random_reseed(1);
		for(auto n = 0; n < 100; ++n)
			REQUIRE(hol::random_number<long long>() == pick(mt));
	}

	SECTION("unsigned short")
	{
		random_generator_type mt{1};
		std::uniform_int_distribution<unsigned short> pick;
		hol::random_reseed(1);
		for(auto n = 0; n < 100; ++n)
			REQUIRE(hol::random_number<unsigned short>() == pick(mt));
	}

	SECTION("unsigned int")
	{
		random_generator_type mt{1};
		std::uniform_int_distribution<unsigned int> pick;
		hol::random_reseed(1);
		for(auto n = 0; n < 100; ++n)
			REQUIRE(hol::random_number<unsigned int>() == pick(mt));
	}

	SECTION("unsigned long")
	{
		random_generator_type mt{1};
		std::uniform_int_distribution<unsigned long> pick;
		hol::random_reseed(1);
		for(auto n = 0; n < 100; ++n)
			REQUIRE(hol::random_number<unsigned long>() == pick(mt));
	}

	SECTION("unsigned long long")
	{
		std::mt19937 mt{1};
		std::uniform_int_distribution<unsigned long long> pick;
		hol::random_reseed(1);
		for(auto n = 0; n < 100; ++n)
			REQUIRE(hol::random_number<unsigned long long>() == pick(mt));
	}
}

//TEST_CASE("functions")
//{
//	SECTION("random_choice")
//	{
//		REQUIRE_NOTHROW(hol::random_choice(0.0));
//		REQUIRE_NOTHROW(hol::random_choice(1.0));
//
//		REQUIRE_THROWS(hol::random_choice(-0.1));
//		REQUIRE_THROWS(hol::random_choice(1.1));
//	}
//}
