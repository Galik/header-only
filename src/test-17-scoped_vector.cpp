//
// Copyright (c) 2019 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <algorithm>
#include <sstream>
#include <string>

#include "hol/scoped_vector.h"

namespace hol{
	using namespace header_only_library::containers;
}

TEST_CASE("Containers", "[scoped_vector]")
{
	SECTION("default constructed")
	{
		hol::scoped_vector<int, 3> sv;

		REQUIRE(sv.empty());
		REQUIRE(sv.size() == 0);
		REQUIRE(std::begin(sv) == std::end(sv));
		REQUIRE(std::cbegin(sv) == std::cend(sv));
		REQUIRE(std::data(sv) + std::size(sv) == sv.data_end());
	}

	SECTION("size_type n")
	{
		hol::scoped_vector<int, 3> sv(3);

		REQUIRE_FALSE(sv.empty());
		REQUIRE(sv.size() == 3);
		REQUIRE(sv[0] == 0);
		REQUIRE(sv[1] == 0);
		REQUIRE(sv[2] == 0);
		REQUIRE(std::all_of(std::begin(sv), std::end(sv), [](int i){
			return i == 0;
		}));

	}
}
