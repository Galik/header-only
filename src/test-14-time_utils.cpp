//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <ctime>
#include <string>

#include "hol/time_utils.h"

namespace hol{
	using namespace header_only_library::time_utils;
}

bool operator==(std::tm const& a, std::tm const& b)
{
	return a.tm_gmtoff == b.tm_gmtoff
	&& a.tm_hour == b.tm_hour
	&& a.tm_isdst == b.tm_isdst
	&& a.tm_mday == b.tm_mday
	&& a.tm_min == b.tm_min
	&& a.tm_mon == b.tm_mon
	&& a.tm_sec == b.tm_sec
	&& a.tm_wday == b.tm_wday
	&& a.tm_yday == b.tm_yday
	&& a.tm_year == b.tm_year
	&& !std::strcmp(a.tm_zone, b.tm_zone);
}

TEST_CASE("Time Utils", "[stamp]")
{
	auto timer = hol::time();
	auto tmb = *std::localtime(&timer);

	SECTION("standardish functions")
	{
		REQUIRE(hol::gmtime(timer) == *std::gmtime(&timer));
		REQUIRE(hol::localtime(timer) == *std::localtime(&timer));
		REQUIRE(hol::mktime(tmb) == std::mktime(&tmb));
		REQUIRE(hol::asctime(tmb) == std::asctime(&tmb));
		REQUIRE(hol::ctime(timer) == std::ctime(&timer));
	}

	SECTION("stamp functions")
	{
	}
}
