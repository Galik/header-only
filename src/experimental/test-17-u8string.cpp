//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <algorithm>
#include <string>
#include <vector>

//#include "test.h"
#include "hol/bug.h"
#include "hol/unicode_utils.h"
#include "hol/experimental/u8string.h"

namespace hol{
	using namespace header_only_library::unicode_utils;
}

using namespace std::literals;

TEST_CASE("u8char", "[]")
{
	SECTION("")
	{
		hol::u8char u8c = u8"貓";

		REQUIRE(u8c.size() == 3);
		REQUIRE(u8c.char32() == U'貓');

		hol::char16_pair c16p;
		auto size = u8c.char16(c16p);
		REQUIRE(size == 1);
		REQUIRE(c16p[0] == u'貓');

		u8c = u8"在";
		REQUIRE(u8c.size() == 3);
		REQUIRE(u8c.char32() == U'在');
	}
}

TEST_CASE("u8string", "[]")
{
	SECTION("")
	{
		hol::u8string u8s = u8"貓坐在墊子上。";

		REQUIRE(u8s.size() == 7);
//		REQUIRE(u8s.u16string() == u"貓坐在墊子上。");
		REQUIRE(u8s.u32string() == U"貓坐在墊子上。");
	}
}

