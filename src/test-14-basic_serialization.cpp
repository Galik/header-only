//
// Copyright (c) 2019 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <hol/basic_serialization.h>

TEST_CASE("Text Serialization")
{
	namespace bs = header_only_library::basic_serialization::txt;

	SECTION("fundamental types")
	{
		int const i = -1;
		long const l = 2;
		float const f = -3.f;
		double const d = 4.0;
		long double const ld = 5.0L;

		std::stringstream ss;

		bs::serialize(ss, i);
		bs::serialize(ss, l);
		bs::serialize(ss, f);
		bs::serialize(ss, d);
		bs::serialize(ss, ld);

		REQUIRE(ss.str() == "\n-1\n2\n-3\n4\n5");

		int out_i = 0;
		long out_l = 0;
		float out_f = 0.f;
		double out_d = 0.0;
		long double out_ld = 0L;

		bs::deserialize(ss, out_i);
		bs::deserialize(ss, out_l);
		bs::deserialize(ss, out_f);
		bs::deserialize(ss, out_d);
		bs::deserialize(ss, out_ld);

		REQUIRE(out_i == i);
		REQUIRE(out_l == l);
		REQUIRE(out_f == f);
		REQUIRE(out_d == d);
		REQUIRE(out_ld == ld);
	}

	SECTION("std::string")
	{
		std::string const cs = "abc d efg";

		std::stringstream ss;

		bs::serialize(ss, cs);

		REQUIRE(ss.str() == "\n9\nabc d efg");

		std::string ms;

		bs::deserialize(ss, ms);

		REQUIRE(ms == cs);
	}
}
