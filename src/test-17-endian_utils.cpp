//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <algorithm>

#include "hol/endian_utils.h"

namespace hol{
	using namespace header_only_library::endian_utils;
}

TEST_CASE("Endian Utils", "[endian]")
{
	SECTION("queries")
	{
		REQUIRE(hol::is_little_endian() != hol::is_big_endian());

		auto is_lil = hol::endianness() == hol::endian::little;
		auto is_big = hol::endianness() == hol::endian::big;

		REQUIRE(is_lil != is_big);
		REQUIRE(is_lil + hol::is_big_endian() == 1);
		REQUIRE(is_big + hol::is_little_endian() == 1);
	}

	SECTION("modifiers")
	{
		std::uint32_t n = 0x01020304;
		REQUIRE(hol::swap_endianness(n) == 0x04030201);

		std::byte d[] = {std::byte(0x01), std::byte(0x02), std::byte(0x03), std::byte(0x04)};
		std::copy(std::begin(d), std::end(d), (std::byte*)&n);

		if(hol::is_little_endian())
			REQUIRE(hol::from_little_endian(n) == 0x04030201);

		if(hol::is_big_endian())
			REQUIRE(hol::from_little_endian(n) == 0x01020304);

		REQUIRE(hol::from_little_endian(n) == hol::from_endian(hol::endian::little, n));
		REQUIRE(hol::from_big_endian(n) == hol::from_endian(hol::endian::big, n));
	}

	SECTION("parameters")
	{
		int i{};
		int const ci{};

		char a[sizeof(i)]{};
		char const ca[sizeof(i)]{};

		char* p{};
		char const* cp{};

		p = hol::encode_big_endian_order(i, a);
		cp = hol::encode_big_endian_order(i, a);

//		p = hol::encode_big_endian_order(i, ca);
//		cp = hol::encode_big_endian_order(i, ca);

		p = hol::encode_little_endian_order(i, a);
//		cp = hol::encode_little_endian_order(i, ca);

		p = hol::encode_network_byte_order(i, a);
//		cp = hol::encode_network_byte_order(i, cp);

		p = hol::decode_big_endian_order(a, i);
		cp = hol::decode_big_endian_order(ca, i);

		p = hol::decode_little_endian_order(a, i);
		cp = hol::decode_little_endian_order(ca, i);

		p = hol::decode_network_byte_order(a, i);
		cp = hol::decode_network_byte_order(ca, i);


		p = hol::encode_big_endian_order(ci, a);
		cp = hol::encode_big_endian_order(ci, a);

//		p = hol::encode_big_endian_order(ci, ca);
//		cp = hol::encode_big_endian_order(ci, ca);

		p = hol::encode_little_endian_order(ci, a);
//		cp = hol::encode_little_endian_order(ci, ca);

		p = hol::encode_network_byte_order(ci, a);
//		cp = hol::encode_network_byte_order(ci, ca);

//		p = hol::decode_big_endian_order(a, ci);
//		cp = hol::decode_big_endian_order(ca, ci);

//		p = hol::decode_little_endian_order(a, ci);
//		cp = hol::decode_little_endian_order(ca, ci);

//		p = hol::decode_network_byte_order(a, ci);
//		cp = hol::decode_network_byte_order(ca, ci);
	}

	SECTION("encoding")
	{
		std::uint32_t u = 0x01020304;
		char buf[sizeof(u)];
		auto pos = hol::encode_big_endian_order(u, buf);

		REQUIRE(pos == buf + sizeof(u));
	}
}
