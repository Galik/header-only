//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <string>
#include <vector>
#include <hol/string_utils.h>

#if __cplusplus >= 201703L
#include <string_view>
using string_view = std::string_view;
#else
#include <experimental/string_view>
using string_view = std::experimental::string_view;
#endif

namespace hol {
	using namespace header_only_library::string_utils;
}

using namespace std::literals;

using str_vec = std::vector<std::string>;
using svw_vec = std::vector<string_view>;

TEST_CASE("strings be splitted", "string_utils")
{
	SECTION("Definitive Split Algorithm [assertions]")
	{
		char const t1[] = {' '};
		char const t2[] = {' ', '\0'};

		REQUIRE_THROWS(hol::split_keep("", ""));
		REQUIRE_THROWS(hol::split_keep("", ""s));
		REQUIRE_THROWS(hol::split_keep(""s, ""));
		REQUIRE_THROWS(hol::split_keep(""s, ""s));
		REQUIRE_THROWS(hol::split_keep("", t1));
		REQUIRE_THROWS(hol::split_keep(string_view(""), string_view("")));

		REQUIRE_NOTHROW(hol::split_keep("", " "));
		REQUIRE_NOTHROW(hol::split_keep("", " "s));
		REQUIRE_NOTHROW(hol::split_keep(""s, " "));
		REQUIRE_NOTHROW(hol::split_keep(""s, " "s));
		REQUIRE_NOTHROW(hol::split_keep("", t2));
		REQUIRE_NOTHROW(hol::split_keep(string_view(""), string_view(" ")));
	}

	// TODO: This test is wrong
	// hol::split should retain empty matches but treat adjacent delimiters
	//     as a single delimiter
	// hol::split_keep should retain empty matches but and treat adjacent delimiters
	//     individually

	SECTION("Definitive Split Algorithm")
	{
		REQUIRE(hol::split("", " ")      == str_vec({}));
		REQUIRE(hol::split("x", " ")     == str_vec({"x"}));
		REQUIRE(hol::split(" x", " ")    == str_vec({"x"}));
		REQUIRE(hol::split("x ", " ")    == str_vec({"x"}));
		REQUIRE(hol::split(" x ", " ")   == str_vec({"x"}));
		REQUIRE(hol::split("x  ", " ")   == str_vec({"x"}));
		REQUIRE(hol::split("  x", " ")   == str_vec({"x"}));
		REQUIRE(hol::split("  x  ", " ") == str_vec({"x"}));
		REQUIRE(hol::split("a b", " ")   == str_vec({"a", "b"}));
		REQUIRE(hol::split(" a b", " ")  == str_vec({"a", "b"}));
		REQUIRE(hol::split("a b ", " ")  == str_vec({"a", "b"}));
		REQUIRE(hol::split("  a b", " ") == str_vec({"a", "b"}));
		REQUIRE(hol::split("a b  ", " ") == str_vec({"a", "b"}));
		REQUIRE(hol::split("a  b", " ")  == str_vec({"a", "b"}));
	}

	SECTION("Definitive Split Algorithm (keeping empty matches)")
	{
		REQUIRE(hol::split_keep("", " ")      == str_vec({}));
		REQUIRE(hol::split_keep("x", " ")     == str_vec({}));
		REQUIRE(hol::split_keep(" x", " ")    == str_vec({"", "x"}));
		REQUIRE(hol::split_keep("x ", " ")    == str_vec({"x", ""}));
		REQUIRE(hol::split_keep(" x ", " ")   == str_vec({"", "x", ""}));
		REQUIRE(hol::split_keep("x  ", " ")   == str_vec({"x", "", ""}));
		REQUIRE(hol::split_keep("  x", " ")   == str_vec({"", "", "x"}));
		REQUIRE(hol::split_keep("  x  ", " ") == str_vec({"", "", "x", "", ""}));
		REQUIRE(hol::split_keep("a b", " ")   == str_vec({"a", "b"}));
		REQUIRE(hol::split_keep(" a b", " ")  == str_vec({"", "a", "b"}));
		REQUIRE(hol::split_keep("a b ", " ")  == str_vec({"a", "b", ""}));
		REQUIRE(hol::split_keep("  a b", " ") == str_vec({"", "", "a", "b"}));
		REQUIRE(hol::split_keep("a b  ", " ") == str_vec({"a", "b", "", ""}));
		REQUIRE(hol::split_keep("a  b", " ")  == str_vec({"a", "", "b"}));
	}

	SECTION("Definitive Split Algorithm (for string_view)")
	{
		REQUIRE(hol::split(string_view(""),      string_view(" ")) == svw_vec({}));
		REQUIRE(hol::split(string_view("x"),     string_view(" ")) == svw_vec({"x"}));
		REQUIRE(hol::split(string_view(" x"),    string_view(" ")) == svw_vec({"x"}));
		REQUIRE(hol::split(string_view("x "),    string_view(" ")) == svw_vec({"x"}));
		REQUIRE(hol::split(string_view(" x "),   string_view(" ")) == svw_vec({"x"}));
		REQUIRE(hol::split(string_view("x  "),   string_view(" ")) == svw_vec({"x"}));
		REQUIRE(hol::split(string_view("  x"),   string_view(" ")) == svw_vec({"x"}));
		REQUIRE(hol::split(string_view("  x  "), string_view(" ")) == svw_vec({"x"}));
		REQUIRE(hol::split(string_view("a b"),   string_view(" ")) == svw_vec({"a", "b"}));
		REQUIRE(hol::split(string_view(" a b"),  string_view(" ")) == svw_vec({"a", "b"}));
		REQUIRE(hol::split(string_view("a b "),  string_view(" ")) == svw_vec({"a", "b"}));
		REQUIRE(hol::split(string_view("  a b"), string_view(" ")) == svw_vec({"a", "b"}));
		REQUIRE(hol::split(string_view("a b  "), string_view(" ")) == svw_vec({"a", "b"}));
		REQUIRE(hol::split(string_view("a  b"),  string_view(" ")) == svw_vec({"a", "b"}));
	}

	SECTION("Definitive Split Algorithm (keeping empty matches) (for string_view)")
	{
		REQUIRE(hol::split_keep(string_view(""),      string_view(" ")) == svw_vec({}));
		REQUIRE(hol::split_keep(string_view("x"),     string_view(" ")) == svw_vec({}));
		REQUIRE(hol::split_keep(string_view(" x"),    string_view(" ")) == svw_vec({"", "x"}));
		REQUIRE(hol::split_keep(string_view("x "),    string_view(" ")) == svw_vec({"x", ""}));
		REQUIRE(hol::split_keep(string_view(" x "),   string_view(" ")) == svw_vec({"", "x", ""}));
		REQUIRE(hol::split_keep(string_view("x  "),   string_view(" ")) == svw_vec({"x", "", ""}));
		REQUIRE(hol::split_keep(string_view("  x"),   string_view(" ")) == svw_vec({"", "", "x"}));
		REQUIRE(hol::split_keep(string_view("  x  "), string_view(" ")) == svw_vec({"", "", "x", "", ""}));
		REQUIRE(hol::split_keep(string_view("a b"),   string_view(" ")) == svw_vec({"a", "b"}));
		REQUIRE(hol::split_keep(string_view(" a b"),  string_view(" ")) == svw_vec({"", "a", "b"}));
		REQUIRE(hol::split_keep(string_view("a b "),  string_view(" ")) == svw_vec({"a", "b", ""}));
		REQUIRE(hol::split_keep(string_view("  a b"), string_view(" ")) == svw_vec({"", "", "a", "b"}));
		REQUIRE(hol::split_keep(string_view("a b  "), string_view(" ")) == svw_vec({"a", "b", "", ""}));
		REQUIRE(hol::split_keep(string_view("a  b"),  string_view(" ")) == svw_vec({"a", "", "b"}));
	}
}
