//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <algorithm>
#include <string>
#include <vector>

//#include "test.h"
//#include "hol/bug.h"
#include "hol/string_utils.h"

namespace hol{
	using namespace header_only_library::string_utils;
}

using namespace std::literals;

TEST_CASE("Trimming Utils (string_view)", "[trimming]")
{
	SECTION("trim_left")
	{
		{
			auto const t = ""sv;
			auto const s = ""sv;
			auto const x = ""sv;
			auto const o = s;
			auto const r = hol::trim_left(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = ""sv;
			auto const s = "{}"sv;
			auto const x = "{}"sv;
			auto const o = s;
			auto const r = hol::trim_left(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{}"sv;
			auto const s = ""sv;
			auto const x = ""sv;
			auto const o = s;
			auto const r = hol::trim_left(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{"sv;
			auto const s = "{{{}}}"sv;
			auto const x = "}}}"sv;
			auto const o = s;
			auto const r = hol::trim_left(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{"sv;
			auto const s = "{ {} }"sv;
			auto const x = " {} }"sv;
			auto const o = s;
			auto const r = hol::trim_left(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{"sv;
			auto const s = " {{}} "sv;
			auto const x = " {{}} "sv;
			auto const o = s;
			auto const r = hol::trim_left(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		// functors

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto const s = ""sv;
			auto const x = ""sv;
			auto const o = s;
			auto const r = hol::trim_left(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto const s = "abc"sv;
			auto const x = "abc"sv;
			auto const o = s;
			auto const r = hol::trim_left(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto const s = ".;abc;."sv;
			auto const x = "abc;."sv;
			auto const o = s;
			auto const r = hol::trim_left(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}
	}

	SECTION("trim_right")
	{
		{
			auto const t = ""sv;
			auto const s = ""sv;
			auto const x = ""sv;
			auto const o = s;
			auto const r = hol::trim_right(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = ""sv;
			auto const s = "{}"sv;
			auto const x = "{}"sv;
			auto const o = s;
			auto const r = hol::trim_right(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{}"sv;
			auto const s = ""sv;
			auto const x = ""sv;
			auto const o = s;
			auto const r = hol::trim_right(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "}"sv;
			auto const s = "{{{}}}"sv;
			auto const x = "{{{"sv;
			auto const o = s;
			auto const r = hol::trim_right(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "}"sv;
			auto const s = "{ {} }"sv;
			auto const x = "{ {} "sv;
			auto const o = s;
			auto const r = hol::trim_right(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "}"sv;
			auto const s = " {{}} "sv;
			auto const x = " {{}} "sv;
			auto const o = s;
			auto const r = hol::trim_right(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		// functors

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto const s = ""sv;
			auto const x = ""sv;
			auto const o = s;
			auto const r = hol::trim_right(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto const s = "abc"sv;
			auto const x = "abc"sv;
			auto const o = s;
			auto const r = hol::trim_right(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto const s = ".;abc;."sv;
			auto const x = ".;abc"sv;
			auto const o = s;
			auto const r = hol::trim_right(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}
	}

	SECTION("trim")
	{
		{
			auto const t = ""sv;
			auto const s = ""sv;
			auto const x = ""sv;
			auto const o = s;
			auto const r = hol::trim(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = ""sv;
			auto const s = "{}"sv;
			auto const x = "{}"sv;
			auto const o = s;
			auto const r = hol::trim(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{}"sv;
			auto const s = ""sv;
			auto const x = ""sv;
			auto const o = s;
			auto const r = hol::trim(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		// functors

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto const s = ""sv;
			auto const x = ""sv;
			auto const o = s;
			auto const r = hol::trim(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto const s = "abc"sv;
			auto const x = "abc"sv;
			auto const o = s;
			auto const r = hol::trim(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto const s = ".;abc;."sv;
			auto const x = "abc"sv;
			auto const o = s;
			auto const r = hol::trim(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}
	}
}

TEST_CASE("replace_all functions", "[replace]")
{
	SECTION("standard")
	{
		{
			std::string const s = "";
			std::string const f = "";
			std::string const t = "";
			std::string const x = "";
			std::string o;

			hol::replace_all(std::begin(s), std::end(s),
				std::begin(f), std::end(f),
				std::begin(t), std::end(t),
				std::back_inserter(o));

			REQUIRE(o == x);
		}

		{
			std::string const s = "a";
			std::string const f = "a";
			std::string const t = "";
			std::string const x = "";
			std::string o;

			hol::replace_all(std::begin(s), std::end(s),
				std::begin(f), std::end(f),
				std::begin(t), std::end(t),
				std::back_inserter(o));

			REQUIRE(o == x);
		}

		{
			std::string const s = "a";
			std::string const f = "a";
			std::string const t = "b";
			std::string const x = "b";
			std::string o;

			hol::replace_all(std::begin(s), std::end(s),
				std::begin(f), std::end(f),
				std::begin(t), std::end(t),
				std::back_inserter(o));

			REQUIRE(o == x);
		}

		{
			std::string const s = "a b a";
			std::string const f = "a";
			std::string const t = "b";
			std::string const x = "b b b";
			std::string o;

			hol::replace_all(std::begin(s), std::end(s),
				std::begin(f), std::end(f),
				std::begin(t), std::end(t),
				std::back_inserter(o));

			REQUIRE(o == x);
		}

		{
			std::string const s = "a b a";
			std::string const f = "a b";
			std::string const t = "c";
			std::string const x = "c a";
			std::string o;

			hol::replace_all(std::begin(s), std::end(s),
				std::begin(f), std::end(f),
				std::begin(t), std::end(t),
				std::back_inserter(o));

			REQUIRE(o == x);
		}
	}

	SECTION("searcher")
	{
		{
			std::string const s = "";
			std::string const f = "";
			std::string const t = "";
			std::string const x = "";
			std::string o;

			hol::replace_all(std::begin(s), std::end(s),
				std::boyer_moore_searcher(std::begin(f), std::end(f)),
				std::begin(t), std::end(t),
				std::back_inserter(o));

			REQUIRE(o == x);
		}

		{
			std::string const s = "a";
			std::string const f = "a";
			std::string const t = "";
			std::string const x = "";
			std::string o;

			hol::replace_all(std::begin(s), std::end(s),
				std::boyer_moore_searcher(std::begin(f), std::end(f)),
				std::begin(t), std::end(t),
				std::back_inserter(o));

			REQUIRE(o == x);
		}

		{
			std::string const s = "a";
			std::string const f = "a";
			std::string const t = "b";
			std::string const x = "b";
			std::string o;

			hol::replace_all(std::begin(s), std::end(s),
				std::boyer_moore_searcher(std::begin(f), std::end(f)),
				std::begin(t), std::end(t),
				std::back_inserter(o));

			REQUIRE(o == x);
		}

		{
			std::string const s = "a b a";
			std::string const f = "a";
			std::string const t = "b";
			std::string const x = "b b b";
			std::string o;

			hol::replace_all(std::begin(s), std::end(s),
				std::boyer_moore_searcher(std::begin(f), std::end(f)),
				std::begin(t), std::end(t),
				std::back_inserter(o));

			REQUIRE(o == x);
		}

		{
			std::string const s = "a b a";
			std::string const f = "a b";
			std::string const t = "c";
			std::string const x = "c a";
			std::string o;

			hol::replace_all(std::begin(s), std::end(s),
				std::boyer_moore_searcher(std::begin(f), std::end(f)),
				std::begin(t), std::end(t),
				std::back_inserter(o));

			REQUIRE(o == x);
		}
	}
}
