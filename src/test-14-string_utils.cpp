//
// Copyright (c) 2016 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <algorithm>
#include <string>
#include <vector>

//#include "test.h"
#include "hol/bug.h"
#include "hol/random_numbers.h"
#include "hol/string_utils.h"
#include "hol/unicode_utils.h"

namespace hol{
	using namespace header_only_library::random_numbers;
	using namespace header_only_library::string_utils;
	using namespace header_only_library::unicode_utils;
}

using namespace std::literals;

namespace tools {

template<typename T, std::size_t N>
constexpr std::size_t size(T(&)[N]) { return N; }

}  // namespace tools

template<typename String>
String random_unique_string(std::size_t n)
{
	std::string s;
	while(s.size() < n)
	{
		s += char(hol::random_number(int('A'), int('Z')));
		std::sort(std::begin(s), std::end(s));
		s.erase(std::unique(std::begin(s), std::end(s)), std::end(s));
	}

	std::shuffle(std::begin(s), std::end(s), hol::random_generator());

	String u;
	hol::unicode_convert(s, u);

	return u;
}

TEST_CASE("Trimming Utils (mute)", "[trimming]")
{
	SECTION("trim_left_mute")
	{
		{
			auto const t = ""s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const& r = hol::trim_left_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = ""s;
			auto s = "{}"s;
			auto const x = "{}"s;
			auto const o = s;
			auto const& r = hol::trim_left_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = "{}"s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const& r = hol::trim_left_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = "{"s;
			auto s = "{{{}}}"s;
			auto const x = "}}}"s;
			auto const o = s;
			auto const& r = hol::trim_left_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s != o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = "{"s;
			auto s = "{ {} }"s;
			auto const x = " {} }"s;
			auto const o = s;
			auto const& r = hol::trim_left_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s != o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = "{"s;
			auto s = " {{}} "s;
			auto const x = " {{}} "s;
			auto const o = s;
			auto const& r = hol::trim_left_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		// functors

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const& r = hol::trim_left_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = "abc"s;
			auto const x = "abc"s;
			auto const o = s;
			auto const& r = hol::trim_left_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ".;abc;."s;
			auto const x = "abc;."s;
			auto const o = s;
			auto const& r = hol::trim_left_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s != o);
			REQUIRE(&s == &r);
		}
	}

	SECTION("trim_right_mute")
	{
		{
			auto const t = ""s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const& r = hol::trim_right_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = ""s;
			auto s = "{}"s;
			auto const x = "{}"s;
			auto const o = s;
			auto const& r = hol::trim_right_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = "{}"s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const& r = hol::trim_right_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = "}"s;
			auto s = "{{{}}}"s;
			auto const x = "{{{"s;
			auto const o = s;
			auto const& r = hol::trim_right_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s != o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = "}"s;
			auto s = "{ {} }"s;
			auto const x = "{ {} "s;
			auto const o = s;
			auto const& r = hol::trim_right_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s != o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = "}"s;
			auto s = " {{}} "s;
			auto const x = " {{}} "s;
			auto const o = s;
			auto const& r = hol::trim_right_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		// functors

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const& r = hol::trim_right_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = "abc"s;
			auto const x = "abc"s;
			auto const o = s;
			auto const& r = hol::trim_right_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ".;abc;."s;
			auto const x = ".;abc"s;
			auto const o = s;
			auto const& r = hol::trim_right_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s != o);
			REQUIRE(&s == &r);
		}
	}

	SECTION("trim_mute")
	{
		{
			auto const t = ""s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const& r = hol::trim_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = ""s;
			auto s = "{}"s;
			auto const x = "{}"s;
			auto const o = s;
			auto const& r = hol::trim_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = "{}"s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const& r = hol::trim_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		// functors

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const& r = hol::trim_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = "abc"s;
			auto const x = "abc"s;
			auto const o = s;
			auto const& r = hol::trim_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
			REQUIRE(&s == &r);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ".;abc;."s;
			auto const x = "abc"s;
			auto const o = s;
			auto const& r = hol::trim_mute(s, t);
			REQUIRE(r == x);
			REQUIRE(s != o);
			REQUIRE(&s == &r);
		}
	}
}

TEST_CASE("Trimming Utils (copy)", "[trimming]")
{
	SECTION("trim_left_copy")
	{
		{
			auto const t = ""s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const r = hol::trim_left_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = ""s;
			auto s = "{}"s;
			auto const x = "{}"s;
			auto const o = s;
			auto const r = hol::trim_left_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{}"s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const r = hol::trim_left_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{"s;
			auto s = "{{{}}}"s;
			auto const x = "}}}"s;
			auto const o = s;
			auto const r = hol::trim_left_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{"s;
			auto s = "{ {} }"s;
			auto const x = " {} }"s;
			auto const o = s;
			auto const r = hol::trim_left_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{"s;
			auto s = " {{}} "s;
			auto const x = " {{}} "s;
			auto const o = s;
			auto const r = hol::trim_left_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		// functors

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const r = hol::trim_left_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = "abc"s;
			auto const x = "abc"s;
			auto const o = s;
			auto const r = hol::trim_left_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ".;abc;."s;
			auto const x = "abc;."s;
			auto const o = s;
			auto const r = hol::trim_left_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}
	}

	SECTION("trim_right_copy")
	{
		{
			auto const t = ""s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const r = hol::trim_right_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = ""s;
			auto s = "{}"s;
			auto const x = "{}"s;
			auto const o = s;
			auto const r = hol::trim_right_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{}"s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const r = hol::trim_right_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "}"s;
			auto s = "{{{}}}"s;
			auto const x = "{{{"s;
			auto const o = s;
			auto const r = hol::trim_right_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "}"s;
			auto s = "{ {} }"s;
			auto const x = "{ {} "s;
			auto const o = s;
			auto const r = hol::trim_right_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "}"s;
			auto s = " {{}} "s;
			auto const x = " {{}} "s;
			auto const o = s;
			auto const r = hol::trim_right_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		// functors

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const r = hol::trim_right_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = "abc"s;
			auto const x = "abc"s;
			auto const o = s;
			auto const r = hol::trim_right_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ".;abc;."s;
			auto const x = ".;abc"s;
			auto const o = s;
			auto const r = hol::trim_right_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}
	}

	SECTION("trim_copy")
	{
		{
			auto const t = ""s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const r = hol::trim_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = ""s;
			auto s = "{}"s;
			auto const x = "{}"s;
			auto const o = s;
			auto const r = hol::trim_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = "{}"s;
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const r = hol::trim_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		// functors

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ""s;
			auto const x = ""s;
			auto const o = s;
			auto const r = hol::trim_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = "abc"s;
			auto const x = "abc"s;
			auto const o = s;
			auto const r = hol::trim_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}

		{
			auto const t = [](char c){ return std::ispunct(c); };
			auto s = ".;abc;."s;
			auto const x = "abc"s;
			auto const o = s;
			auto const r = hol::trim_copy(s, t);
			REQUIRE(r == x);
			REQUIRE(s == o);
		}
	}
}

//// TODO: std::string_view versions
//
//// std::string_view versions
//#if __cplusplus >= 201703L
//	// string_view tests go here
//TEST_CASE("Trimming Utils (string_view)", "[trimming]")
//{
//	SECTION("trim_left")
//	{
//		{
//			auto const t = ""sv;
//			auto s = ""sv;
//			auto const x = ""sv;
//			auto const o = s;
//			auto const r = hol::trim_left(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = ""sv;
//			auto s = "{}"sv;
//			auto const x = "{}"sv;
//			auto const o = s;
//			auto const r = hol::trim_left(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = "{}"sv;
//			auto s = ""sv;
//			auto const x = ""sv;
//			auto const o = s;
//			auto const r = hol::trim_left(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = "{"sv;
//			auto s = "{{{}}}"sv;
//			auto const x = "}}}"sv;
//			auto const o = s;
//			auto const r = hol::trim_left(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = "{"sv;
//			auto s = "{ {} }"sv;
//			auto const x = " {} }"sv;
//			auto const o = s;
//			auto const r = hol::trim_left(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = "{"sv;
//			auto s = " {{}} "sv;
//			auto const x = " {{}} "sv;
//			auto const o = s;
//			auto const r = hol::trim_left(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		// functors
//
//		{
//			auto const t = [](char c){ return std::ispunct(c); };
//			auto s = ""sv;
//			auto const x = ""sv;
//			auto const o = s;
//			auto const r = hol::trim_left(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = [](char c){ return std::ispunct(c); };
//			auto s = "abc"sv;
//			auto const x = "abc"sv;
//			auto const o = s;
//			auto const r = hol::trim_left(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = [](char c){ return std::ispunct(c); };
//			auto s = ".;abc;."sv;
//			auto const x = "abc;."sv;
//			auto const o = s;
//			auto const r = hol::trim_left(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//	}
//
//	SECTION("trim_right_copy")
//	{
//		{
//			auto const t = ""sv;
//			auto s = ""sv;
//			auto const x = ""sv;
//			auto const o = s;
//			auto const r = hol::trim_right(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = ""sv;
//			auto s = "{}"sv;
//			auto const x = "{}"sv;
//			auto const o = s;
//			auto const r = hol::trim_right(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = "{}"sv;
//			auto s = ""sv;
//			auto const x = ""sv;
//			auto const o = s;
//			auto const r = hol::trim_right(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = "}"sv;
//			auto s = "{{{}}}"sv;
//			auto const x = "{{{"sv;
//			auto const o = s;
//			auto const r = hol::trim_right(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = "}"sv;
//			auto s = "{ {} }"sv;
//			auto const x = "{ {} "sv;
//			auto const o = s;
//			auto const r = hol::trim_right(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = "}"sv;
//			auto s = " {{}} "sv;
//			auto const x = " {{}} "sv;
//			auto const o = s;
//			auto const r = hol::trim_right(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		// functors
//
//		{
//			auto const t = [](char c){ return std::ispunct(c); };
//			auto s = ""sv;
//			auto const x = ""sv;
//			auto const o = s;
//			auto const r = hol::trim_right(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = [](char c){ return std::ispunct(c); };
//			auto s = "abc"sv;
//			auto const x = "abc"sv;
//			auto const o = s;
//			auto const r = hol::trim_right(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = [](char c){ return std::ispunct(c); };
//			auto s = ".;abc;."sv;
//			auto const x = ".;abc"sv;
//			auto const o = s;
//			auto const r = hol::trim_right(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//	}
//
//	SECTION("trim_copy")
//	{
//		{
//			auto const t = ""sv;
//			auto s = ""sv;
//			auto const x = ""sv;
//			auto const o = s;
//			auto const r = hol::trim(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = ""sv;
//			auto s = "{}"sv;
//			auto const x = "{}"sv;
//			auto const o = s;
//			auto const r = hol::trim(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = "{}"sv;
//			auto s = ""sv;
//			auto const x = ""sv;
//			auto const o = s;
//			auto const r = hol::trim(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		// functors
//
//		{
//			auto const t = [](char c){ return std::ispunct(c); };
//			auto s = ""sv;
//			auto const x = ""sv;
//			auto const o = s;
//			auto const r = hol::trim(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = [](char c){ return std::ispunct(c); };
//			auto s = "abc"sv;
//			auto const x = "abc"sv;
//			auto const o = s;
//			auto const r = hol::trim(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//
//		{
//			auto const t = [](char c){ return std::ispunct(c); };
//			auto s = ".;abc;."sv;
//			auto const x = "abc"sv;
//			auto const o = s;
//			auto const r = hol::trim(s, t);
//			REQUIRE(r == x);
//			REQUIRE(s == o);
//		}
//	}
//}
//
//#endif // __cplusplus >= 201703L

TEST_CASE("Output Utils", "[string_separator]")
{
	std::ostringstream oss;

	SECTION("default constructed")
	{
		hol::string_separator sep;
		oss.str("");

		for(int i = 0; i < 3; ++i)
			oss << sep << i;

		REQUIRE(oss.str() == "0 1 2");
	}

	SECTION("constructor single param")
	{
		hol::string_separator sep{", "};
		oss.str("");

		for(int i = 0; i < 3; ++i)
			oss << sep << i;

		REQUIRE(oss.str() == "0, 1, 2");
	}

//	SECTION("constructor two param")
//	{
//		hol::string_separator sep{"[", ", "};
//		oss.str("");
//
//		for(int i = 0; i < 3; ++i)
//			oss << sep << i;
//
//		sep.reset("] [", ", ");
//
//		for(int i = 0; i < 3; ++i)
//			oss << sep << i;
//
//		bug_var(oss.str());
//
//		REQUIRE(oss.str() == "[0, 1, 2");
//	}
}

TEST_CASE("Splitting Utils", "[split ref types]")
{
	SECTION("split")
	{
		{
			std::string s("");
			std::string t("||");
			std::vector<std::string> const x = {};

			auto const r = hol::split(s, t);

			REQUIRE(r == x);
		}

		{
			std::string s("||");
			std::string t("||");
			std::vector<std::string> const x = {"", ""};

			auto const r = hol::split(s, t);

			REQUIRE(r == x);
		}

		{
			std::string s("some||text||to||split");
			std::string t("||");
			std::vector<std::string> const x = {"some", "text", "to", "split"};

			auto const r = hol::split(s, t);

			REQUIRE(r == x);
		}

		{
			std::string s("||text");
			std::string t("||");
			std::vector<std::string> const x = {"", "text"};

			auto const r = hol::split(s, t);

			REQUIRE(r == x);
		}

		{
			std::string s("text||");
			std::string t("||");
			std::vector<std::string> const x = {"text", ""};

			auto const r = hol::split(s, t);

			REQUIRE(r == x);
		}

		{
			std::string s("||||");
			std::string t("||");
			std::vector<std::string> const x = {"", "", ""};

			auto const r = hol::split(s, t);

			REQUIRE(r == x);
		}
	}
}

TEST_CASE("Replacing in strings", "[string.replace]")
{
	SECTION("replace_all_mute")
	{
		{
			std::string       s = "";
			std::string const f = "";
			std::string const t = "";
			std::string const x = "";

			hol::replace_all_mute(s, f, t);

			REQUIRE(s == x);
		}

		{
			std::string       s = "a";
			std::string const f = "a";
			std::string const t = "";
			std::string const x = "";

			hol::replace_all_mute(s, f, t);

			REQUIRE(s == x);
		}

		{
			std::string       s = "a";
			std::string const f = "a";
			std::string const t = "b";
			std::string const x = "b";

			hol::replace_all_mute(s, f, t);

			REQUIRE(s == x);
		}

		{
			std::string       s = "a b a";
			std::string const f = "a";
			std::string const t = "b";
			std::string const x = "b b b";

			hol::replace_all_mute(s, f, t);

			REQUIRE(s == x);
		}

		{
			std::string       s = "a b a";
			std::string const f = "a b";
			std::string const t = "c";
			std::string const x = "c a";

			hol::replace_all_mute(s, f, t);

			REQUIRE(s == x);
		}
	}

	SECTION("replace_all_copy (with strings)")
	{
		{
			std::string const s = "";
			std::string const f = "";
			std::string const t = "";
			std::string const x = "";

			std::string o = hol::replace_all_copy(s, f, t);

			REQUIRE(o == x);
		}

		{
			std::string const s = "a";
			std::string const f = "a";
			std::string const t = "";
			std::string const x = "";

			std::string o = hol::replace_all_copy(s, f, t);

			REQUIRE(o == x);
		}

		{
			std::string const s = "a";
			std::string const f = "a";
			std::string const t = "b";
			std::string const x = "b";

			std::string o = hol::replace_all_copy(s, f, t);

			REQUIRE(o == x);
		}

		{
			std::string const s = "a b a";
			std::string const f = "a";
			std::string const t = "b";
			std::string const x = "b b b";

			std::string o = hol::replace_all_copy(s, f, t);

			REQUIRE(o == x);
		}

		{
			std::string const s = "a b a";
			std::string const f = "a b";
			std::string const t = "c";
			std::string const x = "c a";

			std::string o = hol::replace_all_copy(s, f, t);

			REQUIRE(o == x);
		}
	}

	SECTION("replace_all_copy (with string literals)")
	{
		{
			std::string const s = "";
			std::string const x = "";

			std::string o = hol::replace_all_copy(s, "", "");

			REQUIRE(o == x);
		}

		{
			std::string const s = "a";
			std::string const x = "";

			std::string o = hol::replace_all_copy(s, "a", "");

			REQUIRE(o == x);
		}

		{
			std::string const s = "a";
			std::string const x = "b";

			std::string o = hol::replace_all_copy(s, "a", "b");

			REQUIRE(o == x);
		}

		{
			std::string const s = "a b a";
			std::string const x = "b b b";

			std::string o = hol::replace_all_copy(s, "a", "b");

			REQUIRE(o == x);
		}

		{
			std::string const s = "a b a";
			std::string const x = "c a";

			std::string o = hol::replace_all_copy(s, "a b", "c");

			REQUIRE(o == x);
		}
	}

	SECTION("replace_all_copy (odds and ends)")
	{
		{
			std::wstring const s = L"fat";
			std::wstring const x = L"cat";

			std::wstring o = hol::replace_all_copy(s, L"f", L"c");

			REQUIRE(o == x);
		}

		{
			std::string const s = "fat";
			std::string const x = "cat";
			char f[] = {'f', 0};
			char t[] = {'c', 0};

			std::string o = hol::replace_all_copy(s, f, t);

			REQUIRE(o == x);
		}

		{
			std::string const s = "fat";
			std::string const x = "cat";
			char f[] = {'f'};
			char t[] = {'c'};

			std::string o = hol::replace_all_copy(s, f, t);

			REQUIRE(o == x);
		}

		{
			std::string const s = "fat";
			std::string const x = "cat";
			std::string const t = "c";

			std::string o = hol::replace_all_copy(s, "f", t);

			REQUIRE(o == x);
		}

		{
			std::string const s = "fat";
			std::string const x = "cat";
			std::string const f = "f";

			std::string o = hol::replace_all_copy(s, f, "c");

			REQUIRE(o == x);
		}

		{
			std::string const s = "fat";
			std::string const x = "cat";

			std::string o = hol::replace_all_copy(s, "f", "c");

			REQUIRE(o == x);
		}
	}
}

