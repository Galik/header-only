//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <algorithm>
#include <sstream>
#include <string>

#include "hol/byte_buffer.h"

namespace hol{
	using namespace header_only_library::containers;
}

TEST_CASE("Containers", "[byte_buffer]")
{
	SECTION("default constructed")
	{
		hol::byte_buffer bb;

		REQUIRE(bb.empty());
		REQUIRE(bb.size() == 0);
		REQUIRE(std::begin(bb) == std::end(bb));
		REQUIRE(std::cbegin(bb) == std::cend(bb));
		REQUIRE(std::data(bb) + std::size(bb) == bb.data_end());
	}

	SECTION("")
	{
		hol::byte_buffer bb(3);

		REQUIRE_FALSE(bb.empty());
		REQUIRE(bb.size() == 3);

		std::istringstream iss("abc");
		iss.read((char*)bb.data(), bb.size());

		std::string s((char*)std::data(bb), std::size(bb));
		REQUIRE(s == "abc");
	}
}
